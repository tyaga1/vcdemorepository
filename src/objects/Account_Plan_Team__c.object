<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object is used to store team members against a particular plan
S-210814</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account_Plan__c</fullName>
        <description>Story # S-210541- Used for Account Plan visibility</description>
        <externalId>false</externalId>
        <label>Account Plan</label>
        <referenceTo>Account_Plan__c</referenceTo>
        <relationshipLabel>Account Plan Teams</relationshipLabel>
        <relationshipName>Account_Plan_Teams</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Account_Team_Role__c</fullName>
        <description>See case #01848189 - Account Planning Enhancements.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Account Team Role</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <description>Story # S-210541- Used for Account Plan visibility</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(User__r.Business_Unit__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_Team_Member_Job_Title__c</fullName>
        <description>See case #01848189 - Account Planning Enhancements.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>External Team Member Job Title</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_Team_Member_Name__c</fullName>
        <description>See case #01848189 - Account Planning Enhancements.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>External Team Member Name</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job_Title__c</fullName>
        <description>Story # S-210541- Used for Account Plan visibility</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>User__r.Title</formula>
        <label>Job Title</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210541- Used for Account Plan visibility</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Account_Plan_Teams</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Account Plan Team</label>
    <nameField>
        <displayFormat>APTM-{0000}</displayFormat>
        <label>Account Plan Team Member</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Plan Teams</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Account_Plan_Team</fullName>
        <availability>online</availability>
        <description>Similar to standard button, except that it will redirect to the Account Plan record after clicking &quot;Save&quot;</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Account Plan Team</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$ObjectType.Account_Plan_Team__c}/e?{!$Setup.Custom_Fields_Ids__c.Account_Plan_Team_Account_Plan__c}={!Account_Plan__c.Name}&amp;{!$Setup.Custom_Fields_Ids__c.Account_Plan_Team_Account_Plan__c}_lkid={!Account_Plan__c.Id}&amp;retURL={!Account_Plan__c.Id}&amp;saveURL={!Account_Plan__c.Id}</url>
    </webLinks>
</CustomObject>
