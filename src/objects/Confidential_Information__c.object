<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to account</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Attachment_Count__c</fullName>
        <externalId>false</externalId>
        <label>Attachment Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <length>100</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contract_Created_By_Id__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Contract__r.CreatedBy.User_ID_18_characters__c</formula>
        <label>Contract Created By Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract_Document__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Flag to denote if the document added to this Confidential Information container is a Contract</description>
        <externalId>false</externalId>
        <label>Contract Document</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Contract_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Contract__r.Contract_Name__c</formula>
        <label>Contract Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract_Requestor__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Contract Requestor</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract_Type__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Text(Contract__r.Contract_Type__c )</formula>
        <label>Contract Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contract</label>
        <referenceTo>Contract__c</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Created_via_DumpTruck__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Created via DumpTruck</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Document_Type__c</fullName>
        <externalId>false</externalId>
        <label>Document Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>BIS Community Document</fullName>
                    <default>false</default>
                    <label>BIS Community Document</label>
                </value>
                <value>
                    <fullName>CIS Community Document</fullName>
                    <default>false</default>
                    <label>CIS Community Document</label>
                </value>
                <value>
                    <fullName>Strat Clients Document</fullName>
                    <default>false</default>
                    <label>Strat Clients Document</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>E_Sign_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>This field allows you to specify the default primary recipient for any new Adobe Agreements.  Note that this field does represent or change any Recipients to pre-existing Agreements, regardless of their status.</inlineHelpText>
        <label>E-Sign Contact</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.Account__c</valueField>
            </filterItems>
            <infoMessage>Contacts Limited to Account Listed</infoMessage>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Membership__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Membership</label>
        <referenceTo>Membership__c</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Informations</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Primary_Product_Org__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Text(Contract__r.Primary_Product_Org__c)</formula>
        <label>Primary Product Org</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Confidential Information</relationshipLabel>
        <relationshipName>Confidential_Information</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Seal_OCR_File_Exists__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Seal OCR File Exists</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Synch_Account_Team_Members__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Synch Account Team Members</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Synch_Opportunity_Team_Members__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Synch Opportunity Team Members</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Confidential Information</label>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>Contract__c</columns>
        <columns>Contract_Name__c</columns>
        <columns>Account__c</columns>
        <columns>Membership__c</columns>
        <columns>Opportunity__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Check</fullName>
        <columns>NAME</columns>
        <columns>Contract__c</columns>
        <columns>Contract_Name__c</columns>
        <columns>Membership__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Synch_Account_Team_Members__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Confidential_Information__c.Confidential_Information_Contract_Header</value>
        </filters>
        <filters>
            <field>Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA CS CIS</value>
        </filters>
        <filters>
            <field>CREATED_DATE</field>
            <operation>lessOrEqual</operation>
            <value>9/12/2015 12:00 AM</value>
        </filters>
        <label>Check</label>
        <language>en_US</language>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <nameField>
        <label>Confidential Information Name</label>
        <trackFeedHistory>true</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Confidential Information</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypes>
        <fullName>Community_Document</fullName>
        <active>true</active>
        <description>Used to store Community related documents</description>
        <label>Community Document</label>
        <picklistValues>
            <picklist>Document_Type__c</picklist>
            <values>
                <fullName>BIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strat Clients Document</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Confidential_Information_Contract_Header</fullName>
        <active>true</active>
        <description>Used for Confidential Information related to Contract Headers</description>
        <label>Confidential Information - Contract Header</label>
        <picklistValues>
            <picklist>Document_Type__c</picklist>
            <values>
                <fullName>BIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strat Clients Document</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Confidential_Information_Opportunity</fullName>
        <active>true</active>
        <description>Used for Confidential Information related to Opportunities</description>
        <label>Confidential Information - Opportunity</label>
        <picklistValues>
            <picklist>Document_Type__c</picklist>
            <values>
                <fullName>BIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strat Clients Document</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Confidential_Information_Project</fullName>
        <active>true</active>
        <description>Used for Confidential Information related to Projects</description>
        <label>Confidential Information - Project</label>
        <picklistValues>
            <picklist>Document_Type__c</picklist>
            <values>
                <fullName>BIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CIS Community Document</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strat Clients Document</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LAST_UPDATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <sharingReasons>
        <fullName>Account_Team__c</fullName>
        <label>Account Team</label>
    </sharingReasons>
    <sharingReasons>
        <fullName>Opportunity_Team__c</fullName>
        <label>Opportunity Team</label>
    </sharingReasons>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Confidential_Information_Contract</fullName>
        <availability>online</availability>
        <description>Contract&apos;s Confidential Information related List</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Confidential Information</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Confidential_Information__c}/e?Name={!Contract__c.Name}&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Contract__c}={!Contract__c.Name}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Contract__c}_lkid={!Contract__c.Id}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}={!Contract__c.Account__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}_lkid={!Contract__c.AccountId__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Membership__c}={!Contract__c.Membership__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Membership__c}_lkid={!Contract__c.MembershipId__c}
&amp;Owner={!Contract__c.OwnerId}
&amp;{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Synch_ATM__c}=1
&amp;RecordType={!$Setup.Record_Type_Ids__c.Confidential_Information_Contract_Head__c}
&amp;retURL={!Contract__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Confidential_Information_Membership</fullName>
        <availability>online</availability>
        <description>Membership&apos;s Confidential Information related List</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Confidential Information</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Confidential_Information__c}/e?Name={!Membership__c.Name}&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Membership__c}={!Membership__c.Name}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Membership__c}_lkid={!Membership__c.Id}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Opportunity__c}={!Membership__c.Opportunity__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Opportunity__c}={!Membership__c.OpportunityId__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}={!Membership__c.Account__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}_lkid={!Membership__c.AccountId__c}
&amp;Owner={!Membership__c.OwnerId}&amp;{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Synch_ATM__c}=1&amp;retURL={!Membership__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Confidential_Information_Opportunity</fullName>
        <availability>online</availability>
        <description>Custom New button to replace Standard new button on Opportunity&apos;s related list.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Confidential Information</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Confidential_Information__c}/e
?Name={!Opportunity.Name}&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Opportunity__c}={!Opportunity.Name}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Opportunity__c}_lkid={!Opportunity.Id}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}_lkid={!Opportunity.AccountId}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}={!Opportunity.Account}
&amp;{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Synch_OTM__c}=1
&amp;RecordType={!$Setup.Record_Type_Ids__c.Confidential_Information_Opportunity__c}
&amp;retURL={!Opportunity.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Confidential_Information_Project</fullName>
        <availability>online</availability>
        <description>Custom New button to replace Standard new button on Project&apos;s related list.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Confidential Information</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Confidential_Information__c}/e
?CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Project__c}={!Project__c.Name}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Project__c}_lkid={!Project__c.Id}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}={!Project__c.Account__c}
&amp;CF{!$Setup.Custom_Fields_Ids__c.Confidential_Information_Account__c}_lkid={!Project__c.AccountId__c}
&amp;RecordType={!$Setup.Record_Type_Ids__c.Confidential_Information_Project__c}
&amp;retURL={!Project__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Send_Email</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Send Email</masterLabel>
        <openType>newWindow</openType>
        <page>ResellerComm_ConfidentialInfoEmail</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
    <webLinks>
        <fullName>Send_for_Signature</fullName>
        <availability>online</availability>
        <description>Creates an Adobe Sign Agreement Record with default parameters</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Send for Signature</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/echosign_dev1__AgreementTemplateProcess?masterId={!Confidential_Information__c.Id}&amp;templateId=a2vi000000086Ah</url>
    </webLinks>
    <webLinks>
        <fullName>Send_for_Signature_project</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Send for Signature (Project)</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/echosign_dev1__AgreementTemplateProcess?masterId={!Confidential_Information__c.Id}&amp;templateId=a2vi00000008GWA</url>
    </webLinks>
</CustomObject>
