<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>APXTConga4__About_Conga_Composer</defaultLandingTab>
    <description>Conga Composer makes it easy for salesforce.com customers to create sophisticated documents, presentations and reports using data from any standard or custom object.</description>
    <formFactors>Large</formFactors>
    <label>Conga Composer</label>
    <tab>APXTConga4__About_Conga_Composer</tab>
    <tab>APXTConga4__Conga_Setup</tab>
    <tab>APXTConga4__Conga_Template__c</tab>
    <tab>APXTConga4__Conga_Email_Template__c</tab>
    <tab>APXTConga4__Conga_Merge_Query__c</tab>
    <tab>APXTConga4__CongaMerge</tab>
    <tab>standard-report</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
