<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Financial Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Multi-Sector</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa North Region</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa SME Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa South Region</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Automotive,UK&amp;I GTM Banking,UK&amp;I GTM CG,UK&amp;I GTM FS&amp;DCAs,UK&amp;I GTM ID&amp;F,UK&amp;I GTM Indirect,UK&amp;I GTM Insurance,UK&amp;I GTM Nationals,UK&amp;I GTM Telcos &amp; Utilities</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Automotive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Banking</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM CG</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM FS&amp;DCAs</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM ID&amp;F</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Indirect</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Insurance</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Nationals</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Telcos &amp; Utilities</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS AdTruth,UK&amp;I MS Cross-Channel,UK&amp;I MS Data Quality,UK&amp;I MS Targeting</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS AdTruth</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Cross-Channel</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Data Quality</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Targeting</values>
        </dashboardFilterOptions>
        <name>Business Unit</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>This dashboard allows the user to look at elements of the regional review scorecard.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Number_of_open_opportunities__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of open opportunities</metricLabel>
            <report>Regional_review_scorecard/of_overdue_opps_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sum of TCV</metricLabel>
            <report>Regional_review_scorecard/Pipeline_BU</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>% of overdue opportunities</metricLabel>
            <report>Regional_review_scorecard/of_overdue_opps_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>This component shows the current pipeline including the number of open opportunities, the value (TCV) of these open opportunities and what % of the opportunities and TCV is overdue.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>% of overdue TCV</metricLabel>
            <report>Regional_review_scorecard/of_overdue_opps_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Win rate</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Win rate %</metricLabel>
            <report>Regional_review_scorecard/Win_rate_BU_Summary</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Loss rate %</metricLabel>
            <report>Regional_review_scorecard/Win_rate_BU_Summary</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the win, loss and no decision rate for opportunities that have closed this fiscal year.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>No decision rate %</metricLabel>
            <report>Regional_review_scorecard/Win_rate_BU_Summary</report>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>AGE</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Cycle duration</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average life cycle</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Days_in_Stage_2__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average days in identify stage</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Days_in_Stage_3__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average days in qualify stage</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average days in propose stage</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Days_in_Stage_5__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average days in commit stage</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Days_in_Stage_6__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>This component shows the current sales cycle duration including the breakdown of days spent in each sales stage.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average days in contract stage</metricLabel>
            <report>Regional_review_scorecard/Cycle_duration_Snapshot_Comparison</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the number of open opportunities currently owned by inactive users.</footer>
            <header>Opportunities owned by inactive users</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opportunities</metricLabel>
            <report>Regional_review_scorecard/JWGJL_RRT_Opps_owned_by_inactive_users</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>This component shows the number of open opportunities currently owned by data (typically migration related) users.</footer>
            <header>Opportunities owned by data users</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opportuniities</metricLabel>
            <report>Regional_review_scorecard/Opps_owned_by_Data_user</report>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Opportunities with misaligned revenue schedules</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opportunities</metricLabel>
            <report>Regional_review_scorecard/Opps_with_misaligned_rev_schedules</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sum of TCV</metricLabel>
            <report>Regional_review_scorecard/Opps_with_misaligned_rev_schedules</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>REVENUE_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the number of open opportunities, value (TCV) and scheduled revenue of opportunities which have misaligned scheduled revenue.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sum of revenue schedules</metricLabel>
            <report>Regional_review_scorecard/Opps_with_misaligned_rev_schedules</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>This component shows the number of open opportunities which have a value (TCV) less than or equal to $100.</footer>
            <header>Low TCV opps</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opportunities</metricLabel>
            <report>Regional_review_scorecard/JWGJL_RRT_Low_TCV_opps_USD_100</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Opportunity_DQ_Score__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current average opportunity dq score for open opportunities due to close this fiscal year.</footer>
            <header>Opportunity DQ score</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average DQ score</metricLabel>
            <report>Regional_review_scorecard/JWGJL_RRT_Average_Opp_DQ_score_BU</report>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Regional scorecard review dashboard - snapshot</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
