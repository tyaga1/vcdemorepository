<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Financial Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Multi-Sector</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa North Region</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa SME Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LATAM Serasa South Region</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Automotive,UK&amp;I GTM Banking,UK&amp;I GTM CG,UK&amp;I GTM FS&amp;DCAs,UK&amp;I GTM ID&amp;F,UK&amp;I GTM Indirect,UK&amp;I GTM Insurance,UK&amp;I GTM Nationals,UK&amp;I GTM Telcos &amp; Utilities</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Automotive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Banking</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM CG</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM FS&amp;DCAs</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM ID&amp;F</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Indirect</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Insurance</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Nationals</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I GTM Telcos &amp; Utilities</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS AdTruth,UK&amp;I MS Cross-Channel,UK&amp;I MS Data Quality,UK&amp;I MS Targeting</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS AdTruth</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Cross-Channel</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Data Quality</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I MS Targeting</values>
        </dashboardFilterOptions>
        <name>Business Unit</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>This dashboard allows the user to look at elements of the regional review scorecard.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win rate %, loss rate % and no decision rate % in the current and last 4 FQs.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Win rate % by FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>Regional_review_scorecard/Win_rate_FQ_Matrix</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closing in current + last 4 FQs.</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_3__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_5__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_6__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average stage duration (in days) for won opportunities. Only opportunities created since the NA CSDA migration Go Live data have been included in the report.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Average stage duration for won opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Regional_review_scorecard/Cycle_duration_Trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opportunities created since 12 May 2015</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>TAMOUNT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the trended pipeline, measured in TCV, and split by sales stage for the current and last 12 months.</footer>
            <groupingColumn>INTERVAL_DATE</groupingColumn>
            <groupingColumn>TSTAGE</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>Regional_review_scorecard/Pipeline_BU_Trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>TCV by sales stage</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of opportunities with a low TCV (under US $100).</footer>
            <header>Low TCV opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Regional_review_scorecard/Low_TCV_opps_USD_100_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title># of opportunities with TCV &gt; US $100</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Business_Unit__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average cycle duration (in days) for won opportunities. Only opportunities created since the NA CSDA migration Go Live data have been included in the report.</footer>
            <header>Average cycle duration for won opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Regional_review_scorecard/Cycle_duration_Trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opportunities created since 12 May 2015</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Regional scorecard review dashboard - Trend</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
