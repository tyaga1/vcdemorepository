<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <name>Opportunity Owner&apos;s Region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credited</values>
        </dashboardFilterOptions>
        <name>Opportunity Type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>PowerCurve Connectivity &amp; Enrichment </values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>PowerCurve Customer Management,PowerCurve Customer Management Retail Banking, PowerCurve Customer Management Service (CMS) </values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>PowerCurve OnDemand </values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Powercurve Originations,Powercurve Originations Pre-Configured for Consumer Lending</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Powercurve Strategy Management</values>
        </dashboardFilterOptions>
        <name>Product Name</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>PowerCurve win rate benchmark</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>Product2.Global_Business_Line__c</groupingColumn>
            <header>Win Rates - PowerCurve</header>
            <legendPosition>Bottom</legendPosition>
            <report>Global_DA_reports/Win_Rates_PowerCurve_DB</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>PC Win Rate by FQ (based on Opp count)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows Win Rate by quarter</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>BucketField_46715909</groupingColumn>
            <header>Win Rates - PowerCurve</header>
            <legendPosition>Bottom</legendPosition>
            <report>Global_DA_reports/Win_Rates_PowerCurve</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Win Rate by Quarter (based on Opp count)</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>PowerCurve Win Rate Benchmark</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>Product2.Global_Business_Line__c</groupingColumn>
            <header>Win Rates - PowerCurve</header>
            <legendPosition>Bottom</legendPosition>
            <report>Global_DA_reports/Win_Rates_PowerCurve_DB</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>PC Win Rate by FQ (based on Sales Price)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>BucketField_46715909</groupingColumn>
            <header>Win Rates - PowerCurve</header>
            <legendPosition>Bottom</legendPosition>
            <report>Global_DA_reports/Win_Rates_PowerCurve</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Win Rate by FQ (based on Sales Price)</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>User.Sales_Team__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Top 10 Wins</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Global_DA_reports/PowerCurve_Top_Won_Opps_DB</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opportunities closing this FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>PRODUCT_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Account.Reporting_Name__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_Count__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Top 10 Accounts</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Global_DA_reports/PowerCurve_Top_Won_Accounts_DB</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opportunities closing the FY</title>
        </components>
    </rightSection>
    <runningUser>joannaelizabeth.campbell@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Win Rate</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
