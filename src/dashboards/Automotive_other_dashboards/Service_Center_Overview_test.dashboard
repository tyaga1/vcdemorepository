<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>Date range - 8/1/2016 to 7/31/2017</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows how many cases are being worked through each channel.</footer>
            <header>Service Center Case Summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Cases_Logged_by_Channel_test</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Cases by Channel</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the time frame cases were closed for the current MTD.</footer>
            <header>Service Center Case Summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Closed_Case_Handling_Times_test</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Close Case Handling Times</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component will show total cases and origin for the current fiscal year.</footer>
            <groupingColumn>TYPE</groupingColumn>
            <groupingColumn>BucketField_7742567</groupingColumn>
            <header>All Cases by Origin</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/All_Cases_by_Origin</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Cases by Origin</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows how the case types logged for each product being supported.</footer>
            <groupingColumn>TYPE</groupingColumn>
            <groupingColumn>BucketField_42031604</groupingColumn>
            <header>Cases by Product</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Top_Case_Types_by_Product_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top Case Types by Product</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows all cases that have been worked broken down by Type.  Note: Small groups are combined as Other.</footer>
            <groupingColumn>BucketField_65814680</groupingColumn>
            <groupingColumn>BucketField_43032794</groupingColumn>
            <header>All Case Summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/All_Cases_by_Type_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Cases Worked - By Type</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows how many cases are being worked through each channel broken out by each product being supported.</footer>
            <header>Service Center Case Summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Cases_Logged_By_Product_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Cases By Product</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component will show all new Infinity dealer contract submissions that have been setup.</footer>
            <header>Infinity Dealer Contracts</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Automotive_Client_Services_reports/Infinity_Contracts_Report_test</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Infinity Dealer Contracts</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_3216532</groupingColumn>
            <groupingColumn>BucketField_81347319</groupingColumn>
            <header>Consumer Cases Worked</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/All_Consumer_Cases_by_Type_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Consumer by Type</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component breaks out total cases created by phone or email broken out by commercial vs consumer.</footer>
            <header>Service Center Case Summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Case_Origin_Comms_vs_Consumer_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Case Origin - Commercial vs Consumer</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows total cases worked and their status</footer>
            <header>Case Count by Status</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Automotive_Client_Services_reports/AUT60_Case_count_by_status</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Cases by Status</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Monthly trends that show what types of cases are being opened each month for the current fiscal year.  Figures based on closed cases.</footer>
            <header>Case Trending by Type</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/Monthly_Trend_Cases_by_Type_test</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Case Trends by Type</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_61192748</groupingColumn>
            <groupingColumn>BucketField_14436169</groupingColumn>
            <header>Commercial by Type</header>
            <legendPosition>Bottom</legendPosition>
            <report>Automotive_Client_Services_reports/All_Comm_Cases_by_Type_test</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>diego.sanchez@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>AUT - Service Center Overview</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
