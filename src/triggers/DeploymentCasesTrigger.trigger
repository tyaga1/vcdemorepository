/**********************************************************************************
 * Experian
 * Name: DeploymentCasesTrigger
 * Description: Trigger for Deployment_Cases__c
 *             
 * Created Date: Oct 20th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified       Modified By                  Description of the update
 *********************************************************************************/
trigger DeploymentCasesTrigger on Deployment_Cases__c (after delete) {

  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
    
    if (Trigger.isAfter && Trigger.isDelete) {
      DeploymentCaseTriggerHandler.afterDelete(Trigger.oldMap);
    }
    
  }    

}