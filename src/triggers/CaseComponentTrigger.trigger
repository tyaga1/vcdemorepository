/**********************************************************************************
 * Experian
 * Name: CaseComponentTrigger
 * Description: Trigger for Case_Component__c
 *             
 * Created Date: Oct 21st, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified       Modified By                  Description of the update
 *********************************************************************************/
trigger CaseComponentTrigger on Case_Component__c (before delete, after delete) {
  
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
    
    if (Trigger.isBefore && Trigger.isDelete) {
      CaseComponentTriggerHandler.beforeDelete(Trigger.oldMap);
    }
    
    if (Trigger.isAfter && Trigger.isDelete) {
      CaseComponentTriggerHandler.afterDelete(Trigger.oldMap);
    }

  }
}