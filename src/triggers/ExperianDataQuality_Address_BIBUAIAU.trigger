/**=====================================================================
 * Created Date: Sept 9th, 2016
 * Created By: Kos Mitev (EDQ) 
 * 
 * EDQ v5 integration
 * Aptsys note: The trigger name is used hard-coded inside TriggerState.isActive function because we were unable to add it inside the Constants class.

 * Date Modified         Modified By                  Description of the update
 *  Nov 28th,2016       Sadar Yacob                  Added call to AddressTriggerHandler to support Experian global template changes from QAS V4
 *  Dec 5th, 2016       Sadar Yacob                  Moved code from here to Handler class to follow standards and streamline code
/**=====================================================================*/

trigger ExperianDataQuality_Address_BIBUAIAU on Address__c (before insert, before update, after insert, after update) {     
    if(!TriggerState.isActive('ExperianDataQuality_Address_BIBUAIAU')) return;
    
    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
       // Following code moved to AddressTriggerHandler -> Remove after verifying 12/05/16
       /* Address__c[] recordsToPassToTriggerNew = Trigger.New;
        Address__c[] recordsToPassToTriggerOld = Trigger.Old;
 
        if(isDataAdmin__c.getInstance().IsDataAdmin__c) {
            List<Address__c> recordsToPassToTriggerNewList = new List<Address__c>();
            List<Address__c> recordsToPassToTriggerOldList = new List<Address__c>();
            
            for(Integer i = 0; i < Trigger.New.size(); i++) {
                if(!Trigger.New[i].Validation_Status__c.endsWith('!')) continue;
   
                recordsToPassToTriggerNewList.add(Trigger.New[i]);
                if(Trigger.Old != null && Trigger.Old.size() > i)
                    recordsToPassToTriggerOldList.add(Trigger.Old[i]);
            }
            
            recordsToPassToTriggerNew = new Address__c[recordsToPassToTriggerNewList.size()];
            for(Integer i = 0; i < recordsToPassToTriggerNewList.size(); i++) {
                recordsToPassToTriggerNew[i] = recordsToPassToTriggerNewList[i];
            }
  
            recordsToPassToTriggerOld = new Address__c[recordsToPassToTriggerOldList.size()];
            for(Integer i = 0; i < recordsToPassToTriggerOldList.size(); i++) {
                recordsToPassToTriggerOld[i] = recordsToPassToTriggerOldList[i];
            }           
        }
        EDQ.DataQualityService.SetValidationStatus(recordsToPassToTriggerNew, recordsToPassToTriggerOld, Trigger.IsInsert, 2); */
        AddressTriggerHandler.beforeInsertUpdate(Trigger.new, Trigger.old);
        
    } else if(IsDataAdmin__c.getInstance().IsDataAdmin__c == false && Trigger.isAfter && Trigger.isInsert ) { 
        //EDQ.DataQualityService.ExecuteWebToObject(Trigger.New, 2, Trigger.IsUpdate); --> code moved to AddressTriggerHandler 12/5/16
        AddressTriggerHandler.afterInsert(Trigger.new);
    }  else if(IsDataAdmin__c.getInstance().IsDataAdmin__c == false && Trigger.isAfter &&  Trigger.isUpdate) {
        //EDQ.DataQualityService.ExecuteWebToObject(Trigger.New, 2, Trigger.IsUpdate); --> code moved to AddressTriggerHandler 12/5/16
         AddressTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    }       
}