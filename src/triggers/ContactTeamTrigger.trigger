/**=====================================================================
* Created Date: 14th Jan 2016 
* Name: ContactTeamTrigger
* Description: Trigger class for Contact_Team__c object
* Created by: James Wills
*
* Date Modified        Modified By            Description of the update
* 19 Jul 2016          Paul Kissick           CRM2:W-005406: Cleaned up class - Added holding methods
* Sep 9th, 2016        Paul Kissick           CRM2:W-005406: Moved IsDataAdmin out of the checks and into a class property.
*=====================================================================*/

trigger ContactTeamTrigger on Contact_Team__c (before insert, before update, before delete, after insert, after delete, after update, after undelete) {

  ContactTeamTriggerHandler.isDataAdmin = IsDataAdmin__c.getInstance().IsDataAdmin__c;

  if (Trigger.isBefore && Trigger.isInsert) {
    ContactTeamTriggerHandler.beforeInsert(Trigger.new); 
  }
  if (Trigger.isBefore && Trigger.isUpdate) {
    ContactTeamTriggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
  }
  if (Trigger.isBefore && Trigger.isDelete) {
    ContactTeamTriggerHandler.beforeDelete(Trigger.old);
  }
  if (Trigger.isAfter && Trigger.isInsert) {
    ContactTeamTriggerHandler.afterInsert(Trigger.new); 
  }
  if (Trigger.isAfter && Trigger.isUpdate) {
    ContactTeamTriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
  }
  if (Trigger.isAfter && Trigger.isDelete) {
    ContactTeamTriggerHandler.afterDelete(Trigger.old);
  }
  if (Trigger.isAfter && Trigger.isUndelete) {
    ContactTeamTriggerHandler.afterUndelete(Trigger.new);
  }


}