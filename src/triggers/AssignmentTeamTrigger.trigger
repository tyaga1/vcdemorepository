/**=====================================================================
 * Name: AssignmentTeamTrigger
 * Description: 
 * Created Date: May 25th, 2017
 * Created By: James Wills
 * 
 * Date Modified                Modified By                  Description of the update
  =====================================================================*/
trigger AssignmentTeamTrigger on Assignment_Team__c (before delete, after update) {
    
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false){
                                  //&& TriggerState.isActive(Constants.ACCOUNT_ASSIGNMENT_TEAM_TRIGGER)) {     
      //Before delete
      if (trigger.isBefore && trigger.isDelete) {
        AssignmentTeamTriggerHandler.beforeDelete(Trigger.oldMap);
      }
          
      //After update
      if (trigger.isAfter && trigger.isUpdate) {
        AssignmentTeamTriggerHandler.afterUpdate (Trigger.newMap, Trigger.oldMap);
      }   
    }
  }