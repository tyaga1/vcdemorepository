/**=====================================================================
 * Experian
 * Name: AccountStagingTrigger
 * Description: Trigger on Account_Staging__c 
 *
 * The AccountStagingTriggerHandler methods will not be called when the user is an isDataAdmin()
 *
   After Insert :
   After Update :                                 
   
 * Created Date: Dec. 15th 2016
 * Created By: James Wills for DRM Project
 * 27/01/2017                    James Wills                  DRM:W-006453:  No longer needed as these updates will be done manually by the DQ Team
 * 04/03/2017                    James Wills                  DRM:W-006452: Added after update section.
=====================================================================*/

trigger AccountStagingTrigger on Account_Staging__c (before update, after  update) {
                                    
  if (TriggerState.isActive(Constants.ACCOUNT_STAGING_TRIGGER)) {
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {

      //After Update
      if (Trigger.isBefore && Trigger.isUpdate) {
        AccountStagingTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        AccountStagingTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
      }
    } else {
    
      //After Update
      if (Trigger.isBefore && Trigger.isUpdate) {
        AccountStagingTriggerHandler.beforeUpdateIsDataAdmin(Trigger.new, Trigger.oldMap);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        AccountStagingTriggerHandler.afterUpdateIsDataAdmin(Trigger.new, Trigger.oldMap);
      }
    
    }
  }
}