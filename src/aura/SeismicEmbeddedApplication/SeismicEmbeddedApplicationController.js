({ 
    prepareBasicData : function(component, event, helper){
        if(!component.isValid()) return;
        helper.setIframeHeight(component);
        helper.getAppData(component);
    },
    
    jsLoaded : function(component, event, helper){
        if(!component.isValid()) return;
        
        var universalLinkQueryKey = "Link";
        
        /*if(component.get('v.environmentType') == 'Community' && !!window.location.hash){
            if(window.location.hash.indexOf(universalLinkQueryKey) < 0){
                //handle loading detail page in Salesforce Community
            	initCommunityDetailPage(component, helper);
            }else{
                //handle loading Universal Link In Salesforce Community
            	initUnivesalLink(component, helper);
            }
            
        }else{
            //handle loading Embedded App
            initEmbeddedApp(component, helper);
        }*/
        
        if(component.get('v.environmentType') == 'Community' && !!window.location.search && !!window.location.search.indexOf(universalLinkQueryKey) > -1){
            //redicrection for universal link
            redirectionForUniversalLink();
        }else if(component.get('v.environmentType') == 'Community' && !!window.location.hash){
            if(window.location.hash.indexOf(universalLinkQueryKey) < 0){
                //handle loading detail page in Salesforce Community
            	initCommunityDetailPage(component, helper);
            }else{
                //handle loading Universal Link In Salesforce Community
            	initUnivesalLink(component, helper);
            }
            
        }else{
            //handle loading Embedded App
            initEmbeddedApp(component, helper);
        }
        
        function redirectionForUniversalLink(){
            var linkId = getSearchObj().Link;
            var redirectURL = window.location.href.substring(0, window.location.href.indexOf("?" + universalLinkQueryKey)) + "#" + universalLinkQueryKey + "=" + linkId;
            
            //in community builder, window.location.href cannot be set
            try{
            	window.location.href = redirectURL;
            }catch(e){
            	console.log(e);
            }
        }
        
        function initUnivesalLink(component, helper){
            var timeCounter = 30;
            checkData();
            
            function checkData(){
                timeCounter--
                //handle error in community builder;
                if(!component.isValid()) return;
                
                var dataString = component.get('v.dataString');
                if(!!dataString){
            		var universalLinkInfo = setLinkIdForUniversalLink(); 
                    getParamsReadyToInit(component, helper, dataString, universalLinkInfo);
                }else if(timeCounter > 0){
                    setTimeout(function(){
                        checkData();
                    }, 500);
                }else{
                    alert('Init data failed, please refresh the page.');
                }
            }
        }
        
        function initCommunityDetailPage(component, helper){
            
            if(!window.location.hash || !component.isValid()) return;
            var transferData = JSON.parse($.base64.decode(window.location.hash.substring(1, window.location.hash.length)));
            var dataFromMainPage = transferData.dataFromMainPage;
            var mainPageDataJSON = JSON.parse(dataFromMainPage);
            
            var documentParam = !!transferData.documentParam ? JSON.parse(transferData.documentParam) : {};
            
            var initialData = mainPageDataJSON.initialData;
            var sObjectType = mainPageDataJSON.sObjectType;
            var sObjectId = mainPageDataJSON.sObjectId;
            var applicationName = mainPageDataJSON.applicationName;
            
            var contextInfo = initialData[0];
            var credentialInfo = initialData[1];
            var isFullPage = true;
            var notShowGoBack = false;
            var hideHeader = component.get('v.hideHeader');
            var openContentInline = false;
        	loadSeismicApp(component, helper, contextInfo, credentialInfo, sObjectType, sObjectId, applicationName, documentParam, isFullPage, notShowGoBack, hideHeader, openContentInline);
        }
        
        function initEmbeddedApp(component, helper){
            var timeCounter = 30;
            checkData();
            
            function checkData(){
                timeCounter--
                //handle error in community builder;
                if(!component.isValid())return;
                
                var dataString = component.get('v.dataString');
                if(!!dataString){
                    getParamsReadyToInit(component, helper, dataString);
                }else if(timeCounter > 0){
                    setTimeout(function(){
                        checkData();
                    }, 500);
                }else{
                    alert('Init data failed, please refresh the page.');
                }
            }
        }
        
        function getParamsReadyToInit(component, helper, dataString, universalLinkInfo){
            var backUrl = window.location.href;
            component.set('v.backUrl', backUrl);
            
            var dataJSON = JSON.parse(dataString);
            var contextInfo = dataJSON[0];
            var credentialInfo = dataJSON[1];
            var sObjectType = component.get('v.sObjectName');
            var sObjectId = component.get('v.recordId');
            if(typeof(sObjectType)=='undefined'){
            	sObjectType = '';
            }
            if(typeof(sObjectId)=='undefined'){
            	sObjectId = '';
            }
            var applicationName = component.get('v.applicationName');
            
            //get sObjectId in Community
            if(component.isValid() && component.get('v.environmentType') == 'Community'){
                sObjectId = window.location.href.split('detail/')[1];
            }
            
            //var isFullPage = component.isValid() && component.get('v.environmentType') == 'Community' && component.get('v.detailRedirectionPage') == 'None';
            var isFullPage = component.isValid() && ((component.get('v.environmentType') == 'Community' && component.get('v.detailRedirectionPage') == '') || (component.get('v.environmentType') == 'Standard' && component.get('v.openContentInline')));
            var notShowGoBack = true;
            var hideHeader = component.get('v.hideHeader');
            var openContentInline = component.get('v.openContentInline');
            loadSeismicApp(component, helper, contextInfo, credentialInfo, sObjectType, sObjectId, applicationName, null, isFullPage, notShowGoBack, hideHeader, openContentInline, universalLinkInfo);
        }
        
        function loadSeismicApp(component, helper, contextInfo, credentialInfo, sObjectType, sObjectId, applicationName, detailInfo, isFullPage, notShowGoBack, hideHeader, openContentInline, universalLinkInfo){
            var iframeDom = document.getElementById(component.getGlobalId()+'_seismicAppContainer');
            iframeDom.setAttribute('allowfullscreen', true);
            var config = {
                isFullPage: isFullPage,
                currentId: sObjectId,
                notShowGoBack: notShowGoBack,
                hideHeader: hideHeader,
                openContentInline: openContentInline
            };
            
            var context = {
                Salesforce: {
                    ServerBaseUrl: contextInfo.SFBaseUrl,
                    SessionId: contextInfo.SessionId,
                    ObjectType: sObjectType,
                    ObjectId: sObjectId,
                    ObjectName: contextInfo.SObjectName,
                    OrganizationId: contextInfo.SFOrganizationId,
                    UserId: credentialInfo.UserId
                }
            };
            var loginOptions = {
                RememberMeToken: credentialInfo.RememberMeToken,
                DisableRememberMe : credentialInfo.DisableRememberMe,
                DisableSSOAutoRedirection: credentialInfo.DisableSSOAutoRedirection,
                SSOState: credentialInfo.SSOState,
                CredentialsKey: credentialInfo.CredentialsKey
            };
            var embeddedApp = Seismic.IntegratedSDK.loadEmbeddedApplication(iframeDom, credentialInfo.ClientType, applicationName, contextInfo.ServerBaseUrl, detailInfo, config, loginOptions, context, universalLinkInfo);
            embeddedApp.on('action',function(data){
	            switch(data.type){
	        			case Seismic.IntegratedSDK.actions.showDetail:
	        				handleOpenDetailRequest(component, helper, data.data);
	        				break;
	    				case Seismic.IntegratedSDK.actions.goBack:
	    					handleGoBackRequest(component, helper, data.data);
	    					break;
						default:
							break;
	        		}
            });
        }
        
        function handleOpenDetailRequest(component, helper, data){
            if(!component.isValid())return;
            var environmentType = component.get('v.environmentType');
            var detailRedirectionPage = component.get('v.detailRedirectionPage');
            helper.navigateToDetail(environmentType, JSON.stringify(getMainPageData(component)), JSON.stringify(data), detailRedirectionPage);
        }
        
        function handleGoBackRequest(component, helper, data){
            if(!component.isValid() || component.get('v.environmentType') == 'Standard' || !window.location.hash) return;
            helper.navigateBack(component);
        }
        
        function getMainPageData(component){
            var initialData = JSON.parse(component.get('v.dataString'));
            var sObjectType = component.get('v.sObjectName');
            var sObjectId = component.get('v.recordId');
            var applicationName = component.get('v.applicationName');
            var backUrl = component.get('v.backUrl');
            return {
                initialData: initialData,
                backUrl: backUrl,
                sObjectType: sObjectType,
                sObjectId: sObjectId,
                applicationName: applicationName
            };
        } 
        
        function getAnchorObj(){
            var anchorString = window.location.hash.length > 0 ? window.location.hash.substr(1) : '';
            var args = {};  
            var items = anchorString.length > 0 ? anchorString.split('&') : [];
            var item = null, name = null, value = null, i = 0, len = items.length;
    
            for(i = 0;i < len; i++){
                item = items[i].split('=');
                name = decodeURIComponent(item[0]);
                value = decodeURIComponent(item[1]);
    
                if(name.length){
                    args[name] = value;
                }
            }
    		console.log(args);
            return args;
        } 
        
        function getSearchObj(){
            var queryString = window.location.search.length > 0 ? window.location.search.substr(1) : '';
            var args = {};  
            var items = queryString.length > 0 ? queryString.split('&') : [];
            var item = null, name = null, value = null, i = 0, len = items.length;
    
            for(i = 0;i < len; i++){
                item = items[i].split('=');
                name = decodeURIComponent(item[0]);
                value = decodeURIComponent(item[1]);
    
                if(name.length){
                    args[name] = value;
                }
            }
    		console.log(args);
            return args;
        } 
        
        function setLinkIdForUniversalLink(){
            if(!window.location.hash) return;
            var universalLinkInfo = {};
            var universalLinkId = getAnchorObj().Link;
            universalLinkInfo.linkId = universalLinkId;
            return universalLinkInfo;
        }
        
    } 
    
})