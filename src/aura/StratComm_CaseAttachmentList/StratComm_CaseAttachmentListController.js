({
	doInit : function(component, event, helper) {
        if (component.get("v.recordId") == null || component.get("v.recordId") == '' )
        {
            return;
        }

        component.set("v.url", window.location.origin + '/GlobalCustomerCommunity/servlet/servlet.FileDownload?file=');

        var checkCaseStatus = component.get("c.checkIfCaseIsClosed");
        checkCaseStatus.setParams({"caseID":component.get("v.recordId")});

        var action = component.get("c.findCaseAttachments");
        action.setParams({"caseID":component.get("v.recordId")});

        $A.enqueueAction(checkCaseStatus);
        $A.enqueueAction(action);

        var objAry = [];

        checkCaseStatus.setCallback(this, function(a){
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue();

                component.set("v.closedCase", respon);
            }
        });

        action.setCallback(this, function(a){
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue();

                for (var i in respon) {
                    var caseAttachmentObj = {
                        FileName: respon[i].FileName,
                        CreatorName: respon[i].CreatorName,
                        CreatedDate: respon[i].CreatedDate,
                        BodyLength: respon[i].BodyLength,
                        ParentCase: respon[i].ParentCase,
                        AttachmentId: respon[i].AttachmentId
                    }
                    objAry.push(caseAttachmentObj);
                }

                component.set("v.caseAttachmentList", objAry);
            }
            else if (component.isValid() && state === "ERROR") {
                alert('error');
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                }
            }
        });
    }
})