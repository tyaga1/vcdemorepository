({
    getAppData : function(component) {
        var action = component.get("c.getAppInfo");
        var sObjectType = component.get('v.sObjectName');
        var sObjectId = component.get('v.recordId');
        action.setParams({sObjectId: sObjectId, sObjectType: sObjectType, returnUrl: window.location.href});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var dataString = response.getReturnValue();
                component.set('v.dataString', dataString);
            }else if(state === "ERROR") {
                var error = response.getError();
                console.log(error)
            }else {
                console.log("unknown error");
            }
        });
        $A.enqueueAction(action);
    },
    
    setIframeHeight : function(component){
        var designHeight = component.get("v.appContainerHeight");
        var appContainerHeight = "";
        var appContainerWidth = "100%";
        if(designHeight == "100%"){
            appContainerHeight = "100%";
        }
        else{
            appContainerHeight = designHeight + "px";
        }
        var appContainerStyle = "border: 1px solid rgb(216, 221, 230);border-radius: 0 0 .25rem .25rem;height:" + appContainerHeight + ";width:" + appContainerWidth +";";
        component.set("v.appContainerStyle", appContainerStyle);
    }
})