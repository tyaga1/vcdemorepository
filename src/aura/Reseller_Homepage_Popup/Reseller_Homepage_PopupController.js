({
	doInit : function(component, event, helper) {
		helper.getUserAgreementFlag(component);
	},

	declineUserAgreement : function(component, event, helper) {
		// Reroute to a default page
	},

	acceptUserAgreement : function(component, event, helper) {
		helper.setUserAgreementFlag(component);	
	},
})