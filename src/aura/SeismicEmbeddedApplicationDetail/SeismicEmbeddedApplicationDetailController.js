({
    jsLoaded : function(component, event, helper){
        var dataFromMainPage = component.get('v.dataFromMainPage');
        var mainPageDataJSON = JSON.parse(dataFromMainPage);
        var initialData = mainPageDataJSON.initialData;
        var sObjectType = mainPageDataJSON.sObjectType;
        var sObjectId = mainPageDataJSON.sObjectId;
        var applicationName = mainPageDataJSON.applicationName;
        
        loadSeismicApp(component, helper, initialData, sObjectType, sObjectId, applicationName);
        
        function loadSeismicApp(component, helper, initialData, sObjectType, sObjectId, applicationName){
            var contextInfo = initialData[0];
            var credentialInfo = initialData[1];
            var documentParam = JSON.parse(component.get('v.documentParam'));

            var iframeDom = document.getElementById(component.getGlobalId()+'_seismicAppDetailContainer');
            iframeDom.setAttribute('allowfullscreen', true);
            var config = {
                isFullPage: true,
                currentId: sObjectId
            };
            var context = {
                Salesforce: {
                    ServerBaseUrl: contextInfo.SFBaseUrl,
                    SessionId: contextInfo.SessionId,
                    ObjectType: sObjectType,
                    ObjectId: sObjectId,
                    ObjectName: contextInfo.SObjectName,
                    OrganizationId: contextInfo.SFOrganizationId,
                    UserId: credentialInfo.UserId
                }
            };
            var loginOptions = {
                RememberMeToken: credentialInfo.RememberMeToken,
                DisableRememberMe : credentialInfo.DisableRememberMe,
                DisableSSOAutoRedirection: credentialInfo.DisableSSOAutoRedirection,
                SSOState: credentialInfo.SSOState,
                CredentialsKey: credentialInfo.CredentialsKey
            };
            var embeddedApp = Seismic.IntegratedSDK.loadEmbeddedApplication(iframeDom, credentialInfo.ClientType, applicationName, contextInfo.ServerBaseUrl, documentParam, config, loginOptions, context);
            embeddedApp.on('action',function(data){
	            switch(data.type){
	    				case Seismic.IntegratedSDK.actions.goBack:
	    					handleGoBackRequest(component, helper, data.data);
	    					break;
						default:
							break;
	        		}
            });
        }
        
        function handleGoBackRequest(component, helper, data){
        	helper.navigateBack(component);
        }
    }
})