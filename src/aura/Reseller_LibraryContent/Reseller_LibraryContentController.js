({
	doInit : function(component, event, helper) {
        console.log('init');
		var action = component.get("c.getLibraryContent");        
        
        action.setCallback(this, function(response) {
            console.log("inside getPrivateDocs action");
            var state = response.getState();            
            if (state === "SUCCESS") {   
                console.log("Success docs");
                var returnDocs = response.getReturnValue();
                component.set("v.documents", returnDocs);
                console.log(component.get("v.documents"));
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action);
	}
})