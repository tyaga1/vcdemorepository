({
    jsLoaded : function(component, event, helper){
        if(!window.location.hash || !component.isValid())return;
        var transferData = JSON.parse($.base64.decode(window.location.hash.substring(1, window.location.hash.length)));
        var dataFromMainPage = transferData.dataFromMainPage;
        var mainPageDataJSON = JSON.parse(dataFromMainPage);
        var documentParam = !!transferData.documentParam ? JSON.parse(transferData.documentParam) : {};
        var initialData = mainPageDataJSON.initialData;
        var sObjectType = mainPageDataJSON.sObjectType;
        var sObjectId = mainPageDataJSON.sObjectId;
        var applicationName = mainPageDataJSON.applicationName;
        
        loadSeismicApp(component, helper, initialData, sObjectType, sObjectId, applicationName);
        
        function loadSeismicApp(component, helper, initialData, sObjectType, sObjectId, applicationName){
            var contextInfo = initialData[0];
            var credentialInfo = initialData[1];
            var iframeDom = document.getElementById(component.getGlobalId()+'_seismicAppDetailContainer');
            iframeDom.setAttribute('allowfullscreen', true);        
            var config = {
                isFullPage: true,
                currentId: sObjectId
            };
            var context = {
                Salesforce: {
                    ServerBaseUrl: contextInfo.SFBaseUrl,
                    SessionId: contextInfo.SessionId,
                    ObjectType: sObjectType,
                    ObjectId: sObjectId,
                    ObjectName: undefined,
                    OrganizationId: contextInfo.SFOrganizationId,
                    UserId: credentialInfo.UserId
                }
            };
            var loginOptions = {
                RememberMeToken: credentialInfo.RememberMeToken,
                DisableRememberMe : credentialInfo.DisableRememberMe,
                DisableSSOAutoRedirection: credentialInfo.DisableSSOAutoRedirection,
                SSOState: credentialInfo.SSOState,
                CredentialsKey: credentialInfo.CredentialsKey
            };
            var embeddedApp = Seismic.IntegratedSDK.loadEmbeddedApplication(iframeDom, credentialInfo.ClientType, applicationName, contextInfo.ServerBaseUrl, documentParam, config, loginOptions, context);
            embeddedApp.on('action',function(data){
	            switch(data.type){
	    				case Seismic.IntegratedSDK.actions.goBack:
	    					handleGoBackRequest(component, helper, data.data);
	    					break;
						default:
							break;
	        		}
            });
        }
        
        function handleGoBackRequest(component, helper, data){
                if(!component.isValid())return;
                helper.navigateBack(component);
        }
    }
})