<aura:component controller="echosign_dev1.AgreementComponentController"
     implements="force:appHostable"
     description="Represents a document selector."
     extensible="true">
    <aura:registerEvent name="notifyDocumentsSelected" type="echosign_dev1:DocumentSelectedEvent"/>
    
    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    
    <aura:attribute name="settingsWrapper" type="echosign_dev1.SettingsWrapper" />
    <aura:attribute name="documentWrappers" type="echosign_dev1.DocumentFileWrapper[]" />
    <aura:attribute name="libraryDocumentWrappers" type="echosign_dev1.LibraryDocumentFileWrapper[]" />
    <aura:attribute name="contentWrappers" type="echosign_dev1.ContentFileWrapper[]" />
    <aura:attribute name="uploadedFileWrappers" type="echosign_dev1.UploadedFileWrapper[]" />
    
    <aura:attribute name="isContentEnabled" type="Boolean" default="true" />
    <aura:attribute name="isLoading" type="Boolean" default="true" />
    <aura:attribute name="errorMessage" type="String"/>
    
    <aura:attribute name="contentSearchTerm" type="String" />
    <aura:attribute name="documentSearchTerm" type="String" />
    <aura:attribute name="libraryDocumentSearchTerm" type="String" />
    
    <div aria-hidden="false" role="dialog" class="slds-modal slds-fade-in-open">
        <div class="esign-document-select-modal slds-modal__container">
            <div class="slds-modal__header">
                <h2 class="slds-text-heading--medium">{! $Label.echosign_dev1.Document_Selector_Header }</h2>
                <button onclick="{!c.onCancelAddFiles}" class="slds-button slds-modal__close">
                    <echosign_dev1:svgIcon class="slds-button__icon slds-button__icon--inverse slds-button__icon--large esign-icon-markup" svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/action-sprite/svg/symbols.svg#close'}" category="action" size="small" name="close" />
                    <span class="slds-assistive-text"></span>
                </button>
            </div>
            <div class="slds-modal__content">                   
                <aura:if isTrue="{! v.errorMessage != null }">
                    <div class="slds-grid slds-m-top--large slds-m-bottom--large">
                        <div class="slds-notify slds-notify--alert slds-theme--error slds-theme--inverse-text slds-theme--alert-texture slds-size--1-of-1" role="alert">
                            <span class="slds-assistive-text"></span>
                            <button onclick="{!c.dismissAlert}" class="slds-button slds-button--icon-inverse slds-notify__close">
                                <img src="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/action/close_60.png'}" alt="" class="slds-button__icon" />
                                <span class="slds-assistive-text"></span>
                            </button>
                            <h2>{! v.errorMessage }</h2>
                        </div>
                    </div>
                </aura:if>
                <aura:if isTrue="{! v.isLoading }">
                    <div class="slds-grid slds-grid--align-center">
                        <div class="slds-spinner--large">
                            <img src="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/images/spinners/slds_spinner_brand.gif'}" alt="" />
                        </div>
                    </div>
                </aura:if>
                <aura:if isTrue="{! !v.isLoading }">
                    <div class="slds-tabs--scoped">
                        <ul class="slds-tabs--scoped__nav" role="tablist">
                            <aura:if isTrue="{! and( !v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c, v.isContentEnabled ) }">
                                <li id="tab-content-item" class="slds-tabs__item slds-text-heading--label slds-active" title="Salesforce CRM Files" role="presentation"><a id="tab-content-item-link" class="esign-tab-selected-link" onclick="{! c.openTab }" role="tab" data-order="1" tabindex="1" aria-selected="true" aria-controls="tab-content">{! $Label.echosign_dev1.Document_Selector_Crm_Tab_Title }</a></li>
                            </aura:if>
                            <aura:if isTrue="{! !v.settingsWrapper.customSettings.echosign_dev1__Disable_Attach_Agreement_Attachments__c }">
                                <li id="tab-document-item" class="{! 'slds-tabs__item slds-text-heading--label' + if( or( !v.isContentEnabled, v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c ), ' slds-active', '' ) }" title="Salesforce Document Library" role="presentation"><a onclick="{! c.openTab }" role="tab" data-order="2" tabindex="2" aria-selected="false" aria-controls="tab-document">{! $Label.echosign_dev1.Document_Selector_Documents_Tab_Title }</a></li>
                            </aura:if>
                            <aura:if isTrue="{! v.settingsWrapper.customSettings.echosign_dev1__Enable_Library_Agreement_Attachments__c }">
                                <li id="tab-library-item" class="{! 'slds-tabs__item slds-text-heading--label' + if( and( or( !v.isContentEnabled, v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c ), v.settingsWrapper.customSettings.echosign_dev1__Disable_Attach_Agreement_Attachments__c ), ' slds-active', '' ) }" title="" role="presentation"><a onclick="{! c.openTab }" role="tab" data-order="3" tabindex="3" aria-selected="false" aria-controls="tab-library">{! $Label.echosign_dev1.Document_Selector_Library_Tab_Title }</a></li>
                            </aura:if>
                        </ul>
                        <aura:if isTrue="{! and( !v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c, v.isContentEnabled ) }">
                            <div id="tab-content" class="{! 'slds-tabs__content ' + if( !v.isContentEnabled, 'slds-hide', 'slds-show' ) }" role="tabpanel">
                                <div class="slds-grid slds-grid--align-left">
                                    <div class="slds-form-element slds-size--1-of-1 slds-p-bottom--large">
                                        <div class="slds-form-element__control">
                                            <ui:inputText value="{!v.contentSearchTerm}" class="slds-input" placeholder="{! $Label.echosign_dev1.Document_Selector_Search_Documents_Placeholder}" />
                                            <div class="slds-p-right--medium esign-doc-search-icon">
                                                <button onclick="{!c.onSearchContent}" class="slds-button slds-button--icon-bare slds-button-space-left slds-shrink-none">
                                                    <echosign_dev1:svgIcon otherClasses="esign-small-icon" class="slds-button__icon esign-invert-full-icon" svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/utility-sprite/svg/symbols.svg#search'}" category="utility" size="small" name="search" />
                                                    <span class="slds-assistive-text"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <table class="slds-table slds-table--bordered">
                                    <thead>
                                        <tr class="slds-text-heading--label">
                                            <th class="slds-cell-shrink" scope="col">
                                            </th>
                                            <th class="slds-is-sortable" scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_File_Name_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_Author_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_File_Size_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_Last_Modified_Tab_Title }</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <aura:iteration items="{!v.contentWrappers}" var="contentWrapper">
                                            <tr class="slds-hint-parent">
                                                <td class="slds-row-select">
                                                    <lightning:input value="{! contentWrapper.isSelected }" label=" " type="checkbox" aura:id="contentSelected"/>
                                                </td>
                                                <th data-label="content-name" role="row">
                                                    <div class="slds-media__figure">
                                                        <echosign_dev1:svgIcon svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/doctype-sprite/svg/symbols.svg#' + contentWrapper.docType}"  category="standard" size="small" name="{!contentWrapper.docType}" />
                                                        <a onclick="{! c.openDocument }" data-order="{! contentWrapper.content.Id }" class="esign-document-title slds-truncate">{! contentWrapper.title }</a>
                                                    </div>
                                                </th>
                                                <td data-label="content-author">{! contentWrapper.authorName }</td>
                                                <td data-label="content-size">
                                                    <span class="slds-truncate">{! contentWrapper.sizeFormatted }</span>
                                                </td>
                                                <td data-label="content-modified">
                                                    <span class="slds-truncate">{! contentWrapper.lastModifiedFormatted }</span>
                                                </td>
                                            </tr>
                                        </aura:iteration>
                                    </tbody>
                                </table>
                            </div>
                        </aura:if>
                        <aura:if isTrue="{! !v.settingsWrapper.customSettings.echosign_dev1__Disable_Attach_Agreement_Attachments__c }">
                            <div id="tab-document" class="{! 'slds-tabs__content ' + if( or( !v.isContentEnabled, v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c ), 'slds-show', 'slds-hide' ) }" role="tabpanel">
                                <div class="slds-grid slds-grid--align-left">
                                    <div class="slds-form-element slds-size--1-of-1 slds-p-bottom--large">
                                        <div class="slds-form-element__control">
                                            <ui:inputText value="{!v.documentSearchTerm}" class="slds-input" placeholder="{! $Label.echosign_dev1.Document_Selector_Search_Documents_Placeholder}" />
                                            <div class="slds-p-right--medium esign-doc-search-icon">
                                                <button onclick="{!c.onSearchDocument}" class="slds-button slds-button--icon-bare slds-button-space-left slds-shrink-none">
                                                    <echosign_dev1:svgIcon otherClasses="esign-small-icon" class="slds-button__icon esign-invert-full-icon" svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/utility-sprite/svg/symbols.svg#search'}" category="utility" size="small" name="search" />
                                                    <span class="slds-assistive-text"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table class="slds-table slds-table--bordered">
                                    <thead>
                                        <tr class="slds-text-heading--label">
                                            <th class="slds-cell-shrink" scope="col">
                                            </th>
                                            <th class="slds-is-sortable" scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_File_Name_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_Author_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_File_Size_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_Last_Modified_Tab_Title }</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <aura:iteration items="{!v.documentWrappers}" var="documentWrapper">
                                            <tr class="slds-hint-parent">
                                                <td class="slds-row-select">
                                                    <lightning:input value="{! documentWrapper.isSelected }" label=" " type="checkbox" aura:id="documentSelected"/>
                                                </td>
                                                <th data-label="document-name" role="row">
                                                    <div class="slds-media__figure">
                                                        <echosign_dev1:svgIcon svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/doctype-sprite/svg/symbols.svg#' + documentWrapper.docType}" category="standard" size="small" name="{!documentWrapper.docType}" />
                                                        <a onclick="{! c.openDocument }" data-order="{! documentWrapper.document.Id }" class="esign-document-title slds-truncate">{! documentWrapper.title }</a>
                                                    </div>
                                                </th>
                                                <td data-label="document-author">{! documentWrapper.authorName }</td>
                                                <td data-label="document-size">
                                                    <span class="slds-truncate">{! documentWrapper.sizeFormatted }</span>
                                                </td>
                                                <td data-label="document-modified">
                                                    <span class="slds-truncate">{! documentWrapper.lastModifiedFormatted }</span>
                                                </td>
                                            </tr>
                                        </aura:iteration>
                                    </tbody>
                                </table>
                            </div>
                        </aura:if>
                        <aura:if isTrue="{! v.settingsWrapper.customSettings.echosign_dev1__Enable_Library_Agreement_Attachments__c }">
                            <div id="tab-library" class="{! 'slds-tabs__content ' + if( and( or( !v.isContentEnabled, v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c ), v.settingsWrapper.customSettings.echosign_dev1__Disable_Attach_Agreement_Attachments__c ), 'slds-show', 'slds-hide' ) }" role="tabpanel">
                                <div class="slds-grid slds-grid--align-left">
                                    <div class="slds-form-element slds-size--1-of-1 slds-p-bottom--large">
                                        <div class="slds-form-element__control">
                                            <ui:inputText value="{!v.libraryDocumentSearchTerm}" class="slds-input" placeholder="{! $Label.echosign_dev1.Document_Selector_Search_Documents_Placeholder}" />
                                            <div class="slds-p-right--medium esign-doc-search-icon">
                                                <button onclick="{!c.onSearchLibraryDocument}" class="slds-button slds-button--icon-bare slds-button-space-left slds-shrink-none">
                                                    <echosign_dev1:svgIcon otherClasses="esign-small-icon" class="slds-button__icon esign-invert-full-icon" svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/utility-sprite/svg/symbols.svg#search'}" category="utility" size="small" name="search" />
                                                    <span class="slds-assistive-text"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table class="slds-table slds-table--bordered">
                                    <thead>
                                        <tr class="slds-text-heading--label">
                                            <th class="slds-cell-shrink" scope="col">
                                            </th>
                                            <th class="slds-is-sortable" scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_File_Name_Tab_Title }</span>
                                            </th>
                                            <th scope="col">
                                                <span class="slds-truncate">{! $Label.echosign_dev1.Document_Selector_Last_Modified_Tab_Title }</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <aura:iteration items="{!v.libraryDocumentWrappers}" var="libraryDocumentWrapper">
                                            <tr class="slds-hint-parent">
                                                <td class="slds-row-select">
                                                    <lightning:input value="{! libraryDocumentWrapper.isSelected }" label=" " type="checkbox" aura:id="librarySelected"/>
                                                </td>
                                                <th data-label="library-name" role="row">
                                                    <div class="slds-media__figure">
                                                        <echosign_dev1:svgIcon svgPath="{!$Resource.echosign_dev1__SLDS + '/SLDS/assets/icons/doctype-sprite/svg/symbols.svg#' + libraryDocumentWrapper.docType}" category="standard" size="small" name="{!libraryDocumentWrapper.docType}" />
                                                        <span class="esign-document-title">{! libraryDocumentWrapper.title }</span>
                                                    </div>
                                                </th>
                                                <td data-label="library-modified">
                                                    <span class="slds-truncate">{! libraryDocumentWrapper.lastModifiedFormatted }</span>
                                                </td>
                                            </tr>
                                        </aura:iteration>
                                    </tbody>
                                </table>
                            </div>
                        </aura:if>
                    </div>
                </aura:if>
            </div>                
            <aura:if isTrue="{! !v.isLoading }">
                <div class="slds-modal__footer">
                    <button onclick="{!c.onCancelAddFiles}" class="slds-button slds-button--neutral slds-float--left">{! $Label.echosign_dev1.Cancel_Button_Label }</button>
                    <aura:if isTrue="{! ! and( and( or( v.settingsWrapper.customSettings.echosign_dev1__Disable_Content_Agreement_Attachments__c, !v.isContentEnabled ), v.settingsWrapper.customSettings.echosign_dev1__Disable_Attach_Agreement_Attachments__c), !v.settingsWrapper.customSettings.echosign_dev1__Enable_Library_Agreement_Attachments__c ) }">
                        <button onclick="{!c.onAddFiles}" class="slds-m-left--small slds-button slds-button--neutral slds-button--brand slds-float--right">{! $Label.echosign_dev1.Add_Files_Button_Label }</button>
                    </aura:if>
                    <aura:if isTrue="{! !v.settingsWrapper.customSettings.echosign_dev1__Disable_Upload_Agreement_Attachments__c }">
                        <input onchange="{!c.onFilesUploaded}" type="file" name="file" id="files" class="esign-inputfile" />
                        <label for="files" class="slds-button slds-button--neutral slds-float--right">{! $Label.echosign_dev1.Document_Selector_Upload_Files_Label }</label>
                    </aura:if>
                </div>
            </aura:if>
        </div>
    </div>
    <div class="slds-modal-backdrop slds-modal-backdrop--open"></div>
</aura:component>