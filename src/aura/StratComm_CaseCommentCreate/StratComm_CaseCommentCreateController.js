({  
    doInit : function(component, event, helper) {
        if (component.get("v.recordId") == null || component.get("v.recordId") == '' )
        {
            return;
        }
        
        component.set("v.url", window.location.origin);

        var checkCaseStatus = component.get("c.checkIfCaseIsClosed");
        checkCaseStatus.setParams({"caseID":component.get("v.recordId")});

        $A.enqueueAction(checkCaseStatus);

        checkCaseStatus.setCallback(this, function(a){
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                var respon = a.getReturnValue();

                component.set("v.closedCase", respon);
            }

        });

    },
    createComment : function(component, event, helper) {
        helper.creatingComment(component, helper);

        /*
        var newComment = component.get("v.newComment");
        var action = component.get("c.saveComment");

        action.setParams({ 
            "newComment": newComment,
            "caseId" : component.get("v.recordId")
        });

        $A.enqueueAction(action)

        action.setCallback(this, function(actionResult){
            var returnedInteger = actionResult.getReturnValue();
            
            if(returnedInteger == 1){
                var myEvent = $A.get("e.c:StratComm_CaseCommentCreation_Event");
                //var myEvent = cmp.getEvent("commentListUpdateEvent");
                myEvent.setParams({"newComment": "True"});
                myEvent.fire();
            }
            
            helper.toggleSpinner(component);
        });

        helper.toggleSpinner(component);
        component.set("v.newComment", "");
        */
    },

    pressEnter : function(component, event, helper) {
        if (event.getParams().keyCode == 13)
        {
            helper.creatingComment(component, helper);
            /*
            event.preventDefault();
            var newComment = component.get("v.newComment");
            var action = component.get("c.saveComment");

            action.setParams({ 
                "newComment": newComment,
                "caseId" : component.get("v.recordId")
            });

            $A.enqueueAction(action);

            action.setCallback(this, function(actionResult){
                var returnedInteger = actionResult.getReturnValue();
                
                if(returnedInteger == 1){
                    var myEvent = $A.get("e.c:StratComm_CaseCommentCreation_Event");
                    //var myEvent = cmp.getEvent("commentListUpdateEvent");
                    myEvent.setParams({"newComment": "True"});
                    myEvent.fire();
                }
                
                helper.toggleSpinner(component);
            });

            helper.toggleSpinner(component);
            component.set("v.newComment", "");
            */
        }
    },

    onDragOver: function(component, event) {
        event.preventDefault();
    },

    onDrop: function(component, event, helper) {
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;

        $A.createComponent(
            "img",
            {
                "src": $A.get('$Resource.SLDS214') + "/assets/icons/doctype/ai_60.png"
            },
            function(newIcon, status, errorMessage){
                if (status === "SUCCESS")
                {
                    var body = component.get("v.body");
                    body.push(newIcon);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE")
                {
                    alert("incomplete");
                }
                else if (status === "ERROR")
                {
                    alert("error");
                }
            }
        );

        helper.addFiles(component, helper, files);
    },

    handleClick: function(component, event, helper) {
        var action = component.get("c.saveAttachments"); 
        action.setParams({
            caseId: component.get("v.recordId"),
            arrayJSON: component.get("v.attachmentArrayJSON")
        });

        component.set("v.message", "Uploading...");
        
        $A.enqueueAction(action); 

        action.setCallback(this, function(a) {
            var myEvent = $A.get("e.c:StratComm_CaseAttachmentCreation_Event");
            //var myEvent = cmp.getEvent("commentListUpdateEvent");
            myEvent.fire();

            component.set("v.message", "File uploaded!");
            helper.toggleSpinner(component);
        });

        helper.toggleSpinner(component);
    }

})