({
	doInit: function(cmp){
    var action = cmp.get("c.getProfileName");
    action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            cmp.set("v.ProfileName", response.getReturnValue());
         }
      });
       $A.enqueueAction(action);
     }
 
 })