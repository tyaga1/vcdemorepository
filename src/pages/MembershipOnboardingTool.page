<!--
/**=====================================================================
 * Experian
 * Name: MembershipOnboardingTool - Case 01222694
 * Description: Integrate a legacy excel spreadsheet into the Membership
 *  management/onboarding process for new clients.
 *
 * Created Date: 15 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By           Description of the update
 * 2nd  Aug 2016       Paul Kissick          Case 01222694: Changed columns to say 'Providing', from 'Done'.
 * 26th Jul 2017       Mauricio Murillo      Case 02425960: Added Info Message when Industry selected = 'Property Management/Owner' and
 *                                           Product to be sold = 'Data Furnishing - Consumer'
 * 22nd Sep 2017       Malcolm Russell       Added Document URL
 * =====================================================================*/
 -->
<apex:page standardController="Membership__c" title="Membership Onboarding Tool" extensions="MembershipOnboardingToolExt" action="{!loadPage}">
  
  <style>
  
  div.hide {
    display: none;
  }
  div.mask {
    position:           absolute;
    z-index:            9999;
    top:                0;
    left:               0;
    width:              100%;
    height:             100%;
    background-color:   rgba(0,0,0,0.15);
    text-align: center;
    display: block;
  }

  div.spinner {
    position: relative;
    width: 54px;
    height: 54px;
    display: inline-block;
    margin-top: 100px;
  }
    
  div.spinner div {
    width: 12%;
    height: 26%;
    background: #000;
    position: absolute;
    left: 44.5%;
    top: 37%;
    opacity: 0;
    -webkit-animation: fade 1s linear infinite;
    -webkit-border-radius: 50px;
    -webkit-box-shadow: 0 0 3px rgba(0,0,0,0.2);
  }
    
  div.spinner div.bar1 {-webkit-transform:rotate(0deg) translate(0, -142%); -webkit-animation-delay: 0s;}    
  div.spinner div.bar2 {-webkit-transform:rotate(30deg) translate(0, -142%); -webkit-animation-delay: -0.9167s;}
  div.spinner div.bar3 {-webkit-transform:rotate(60deg) translate(0, -142%); -webkit-animation-delay: -0.833s;}
  div.spinner div.bar4 {-webkit-transform:rotate(90deg) translate(0, -142%); -webkit-animation-delay: -0.75s;}
  div.spinner div.bar5 {-webkit-transform:rotate(120deg) translate(0, -142%); -webkit-animation-delay: -0.667s;}
  div.spinner div.bar6 {-webkit-transform:rotate(150deg) translate(0, -142%); -webkit-animation-delay: -0.5833s;}
  div.spinner div.bar7 {-webkit-transform:rotate(180deg) translate(0, -142%); -webkit-animation-delay: -0.5s;}
  div.spinner div.bar8 {-webkit-transform:rotate(210deg) translate(0, -142%); -webkit-animation-delay: -0.41667s;}
  div.spinner div.bar9 {-webkit-transform:rotate(240deg) translate(0, -142%); -webkit-animation-delay: -0.333s;}
  div.spinner div.bar10 {-webkit-transform:rotate(270deg) translate(0, -142%); -webkit-animation-delay: -0.25s;}
  div.spinner div.bar11 {-webkit-transform:rotate(300deg) translate(0, -142%); -webkit-animation-delay: -0.1667s;}
  div.spinner div.bar12 {-webkit-transform:rotate(330deg) translate(0, -142%); -webkit-animation-delay: -0.0833s;}

  @-webkit-keyframes fade {
    from {opacity: 1;}
    to {opacity: 0.25;}
  }
  
  .notRequired {
    text-align: center;
    font-size: 2em;
    padding-top: 1em;
    font-weight: bold;  
  }
      .docDetails {
        white-space: pre;
      }
  
  </style>
  <script>
  function loading(start) {
    if (start === true) {
      document.getElementById('pageMask').className = 'mask';
    }
    else {
      document.getElementById('pageMask').className = 'hide';
    }
  }
  function openAdminPage() {
    window.top.location.href = '{!URLFOR($Page.MembershipOnboardingToolMgmt)}';
  }
  </script>
  
  <apex:form >
    <apex:actionStatus id="savingChanges" onstart="loading(true);" onstop="loading(false);" />
    <apex:pageMessages />
    <apex:outputPanel id="wholePage">
      <apex:pageBlock mode="maindetail" rendered="{!currentRecord.Onboarding_Bypass__c==true}">
        <apex:pageBlockButtons location="top">
          <apex:commandButton action="{!bypassDisable}" value="Revert Bypass" rendered="{!$Permission.Membership_Onboarding_Manager}" rerender="wholePage" status="savingChanges" />
        </apex:pageBlockButtons>
        <apex:pageBlockSection columns="1">
          <apex:outputPanel layout="block" styleClass="notRequired" >
            Onboarding tool replaced with Opportunity Assessment.
          </apex:outputPanel>
        </apex:pageBlockSection>
      </apex:pageBlock>
      <apex:pageBlock mode="maindetail" rendered="{!AND(CheckAllSelected,NOT(EditMode),currentRecord.Onboarding_Bypass__c==false)}">
        <apex:pageBlockButtons location="top">
          <apex:commandButton action="{!enableEdit}" value="Edit" rerender="wholePage" status="savingChanges" />
          <apex:commandButton action="{!bypassEnable}" value="Bypass Tool" rendered="{!$Permission.Membership_Onboarding_Manager}" rerender="wholePage" status="savingChanges" />
          <apex:commandButton onclick="openAdminPage();" value="Manage Options" rendered="{!$Permission.Membership_Onboarding_Admin}" />
        </apex:pageBlockButtons>
        <apex:pageBlockSection columns="2">
          <apex:outputField label="{!$ObjectType.Membership__c.fields.Client_Industry__c.label}" value="{!currentRecord.Selected_Client_Industry__c}" />
          <apex:outputField label="{!$ObjectType.Membership__c.fields.Product_to_be_Sold__c.label}" value="{!currentRecord.Selected_Product_to_be_Sold__c}" />
          <apex:outputField label="{!$ObjectType.Membership__c.fields.Permissible_Purpose__c.label}" value="{!currentRecord.Selected_Permissible_Purpose__c}" />
          <apex:outputField label="{!$ObjectType.Membership__c.fields.Third_Party_Involvement__c.label}" value="{!currentRecord.Selected_Third_Party_Involvement__c}" />
        </apex:pageBlockSection>
        <apex:pageBlockSection columns="2">
          <apex:pageBlockSection columns="1" title="Client Documents" collapsible="false">
            <apex:pageBlockTable value="{!ClientDocuments}" var="c">
              <apex:column headerValue="Providing">
                <apex:inputCheckbox value="{!docDetails[c.Id].done}">
                  <apex:actionSupport action="{!buildJson}" event="onchange" rerender="wholePage" status="savingChanges" />
                </apex:inputCheckbox>
              </apex:column>
              <apex:column >
                <apex:facet name="header">On File /<br />Not Required</apex:facet>
                <apex:inputCheckbox value="{!docDetails[c.Id].onFile}">
                  <apex:actionSupport action="{!buildJson}" event="onchange" rerender="wholePage" status="savingChanges" />
                </apex:inputCheckbox>
              </apex:column>
              <apex:column headerValue="Details"><apex:outputText escape="false" value="{!c.Full_Detail__c}" /></apex:column>
              <apex:column >
                <apex:outputlink value="{!c.Document_URL__c}" target="_blank" rendered="{!IF(c.Document_URL__c =null,false,true)}">
                <apex:image id="theImage" value="/img/icon/documents24.png"/>                
                </apex:outputlink>
              </apex:column>
            </apex:pageBlockTable>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1" title="Supporting Documents" collapsible="false">
            <apex:pageBlockTable value="{!SupportingDocuments}" var="c">
              <apex:column headerValue="Providing">
                <apex:inputCheckbox value="{!docDetails[c.Id].done}">
                  <apex:actionSupport action="{!buildJson}" event="onchange" rerender="wholePage" status="savingChanges" />
                </apex:inputCheckbox>
              </apex:column>
              <apex:column >
                <apex:facet name="header">On File /<br />Not Required</apex:facet>
                <apex:inputCheckbox value="{!docDetails[c.Id].onFile}">
                  <apex:actionSupport action="{!buildJson}" event="onchange" rerender="wholePage" status="savingChanges" />
                </apex:inputCheckbox>
              </apex:column>
              <apex:column headerValue="Details"><apex:outputText escape="false" value="{!c.Full_Detail__c}" /></apex:column>
              <apex:column >
                <apex:outputlink value="{!c.Document_URL__c}" target="_blank" rendered="{!IF(c.Document_URL__c =null,false,true)}">
                <apex:image id="theImage" value="/img/icon/documents24.png"/>                
                </apex:outputlink>
              </apex:column>
            </apex:pageBlockTable>
          </apex:pageBlockSection>
        </apex:pageBlockSection>
        <apex:pageBlockSection columns="1">
          <apex:outputPanel >
            <p><b>Here are some expectation that will apply for this case. Keep in mind that these items are subject to the general note listed below.*</b><br />
              The Membership SLA to complete this request is 4 business days from the receipt of the properly completed agreements, 
              supporting documentation and all applicable setup fees. Ensure that clients are aware they will be contacted by our 
              third party vendor to schedule a physical inspection, as applicable. Follow this request and the associated Membership 
              activities to keep apprised of its status. 
            </p>
            <p>*Please note that exceptions will occur due to the unique circumstances of specific clients. While the above will be directionally correct, factors will influence the timeframes and information requirements noted.</p>
          </apex:outputPanel>
        </apex:pageBlockSection>
      </apex:pageBlock>
      <apex:pageBlock mode="edit"  rendered="{!AND(OR(NOT(CheckAllSelected),EditMode),currentRecord.Onboarding_Bypass__c==false)}">
        <apex:pageBlockButtons location="top">
          <apex:commandButton value="Clear All" action="{!clearSelectedIndustry}" rerender="wholePage" rendered="{!currentRecord.Onboarding_Bypass__c==false}" status="savingChanges" />
          <apex:commandButton action="{!bypassEnable}" value="Bypass Tool" rendered="{!$Permission.Membership_Onboarding_Manager}" rerender="wholePage" status="savingChanges" />
          <apex:commandButton onclick="openAdminPage();" value="Manage Options" rendered="{!$Permission.Membership_Onboarding_Admin}" />
        </apex:pageBlockButtons>
        <apex:pageBlockSection columns="1" >
          <apex:outputPanel id="myOptions">
            <apex:pageBlockSection columns="1">
              <apex:pageBlockSectionItem rendered="{!currentStep == 1}">
                <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Client_Industry__c.label}" />
                <apex:outputPanel >
                  <apex:selectList size="1" multiselect="false" value="{!currentRecord.Selected_Client_Industry__c}" disabled="{!NOT(ISNULL(Membership__c.Selected_Client_Industry__c))}">
                    <apex:selectOptions value="{!ClientIndustry}" />
                  </apex:selectList>
                  <apex:commandButton action="{!moveToStep2}" rerender="myOptions" value="Next >" status="savingChanges" />
                </apex:outputPanel>
              </apex:pageBlockSectionItem>
              <apex:pageBlockSectionItem rendered="{!currentStep > 1}">
                <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Client_Industry__c.label}" />
                <apex:outputPanel >
                  <apex:outputText value="{!currentRecord.Selected_Client_Industry__c}" />
                  &nbsp;&nbsp;&nbsp;<apex:commandButton action="{!clearSelectedIndustry}" rerender="wholePage" value=" Reset " status="savingChanges" />
                </apex:outputPanel>
              </apex:pageBlockSectionItem>
              <apex:pageBlockSectionItem rendered="{!currentStep == 2}" >
                <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Product_to_be_Sold__c.label}" />
                <apex:outputPanel >
                  <apex:selectList size="1" multiselect="false" value="{!currentRecord.Selected_Product_to_be_Sold__c}" disabled="{!NOT(ISNULL(Membership__c.Selected_Product_to_be_Sold__c))}">
                    <apex:selectOptions value="{!ProductToBeSold}" />
                  </apex:selectList>
                  <apex:commandButton action="{!moveToStep3}" rerender="myOptions" value="Next >" status="savingChanges" />
                </apex:outputPanel>
              </apex:pageBlockSectionItem>
              <apex:pageBlockSectionItem rendered="{!currentStep > 2}">
                <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Product_to_be_Sold__c.label}" />
                <apex:outputPanel >
                <apex:outputText value="{!currentRecord.Selected_Product_to_be_Sold__c}" />
                &nbsp;&nbsp;&nbsp;<apex:commandButton action="{!clearSelectedProduct}" rerender="wholePage" value=" Reset " status="savingChanges" />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{! (currentStep == 3 && showDataRepMembershipError)}" >
                 <apex:outputPanel >
                    <br/>
                    <apex:pageMessage summary="Property Management and Property Owners wishing to report consumer rental data will need to contact 
                                           Experian RentBureau for consumer data reporting membership.  
                                           Please contact the RentBureau team at rentbureau@experian.com" 
                                           severity="Info" strength="3" />
                    <br/>       
                   </apex:outputPanel> 
            </apex:pageBlockSectionItem>            
            <apex:pageBlockSectionItem rendered="{!(currentStep == 3 && !showDataRepMembershipError)}" >
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Permissible_Purpose__c.label}" />
              <apex:outputPanel >
                <apex:selectList size="1" multiselect="false" value="{!currentRecord.Selected_Permissible_Purpose__c}" disabled="{!NOT(ISNULL(Membership__c.Selected_Permissible_Purpose__c))}">
                  <apex:selectOptions value="{!PermissiblePurpose}" />
                </apex:selectList>
                <apex:commandButton action="{!moveToStep4}" rerender="myOptions" value="Next >" status="savingChanges" />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!currentStep > 3}">
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Permissible_Purpose__c.label}" />
              <apex:outputPanel >
                <apex:outputText value="{!currentRecord.Selected_Permissible_Purpose__c}" />
                &nbsp;&nbsp;&nbsp;<apex:commandButton action="{!clearSelectedPurpose}" rerender="wholePage" value=" Reset " status="savingChanges" />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!currentStep == 4}">
              <apex:outputLabel value="{!$ObjectType.Membership__c.fields.Third_Party_Involvement__c.label}" />
              <apex:outputPanel >
                <apex:selectList size="1" multiselect="false" value="{!currentRecord.Selected_Third_Party_Involvement__c}">
                  <apex:selectOptions value="{!ThirdParty}" />
                </apex:selectList>
                <apex:commandButton action="{!saveChanges}" rerender="wholePage" value="Save" status="savingChanges" />
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem />
            <apex:pageBlockSectionItem rendered="{!currentStep == 2}">
              <apex:outputPanel layout="block">
                <table width="100%">
                  <thead>
                    <tr><th colspan="6" style="text-align: center;">Product Group Reference</th></tr>
                    <tr>
                      <th width="16%">Acquisition Suite</th>
                      <th width="16%">Business Information</th>
                      <th width="17%">Collection Suite</th>
                      <th width="16%">Decision Analytics</th>
                      <th width="16%">Portfolio Suite</th>
                      <th width="17%">Prospecting Suite</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Credit Profile Reports<br />Employment Insight</td>
                      <td>Premier Profiles<br />Business Owner Profiles<br />Collection Research Tool</td>
                      <td>Collection Reports<br />Collection Advantage<br />Collection Triggers<br />Credit Profile Reports<br />First Sweep (don't forget additional document)<br />Metronet<br />True Trace</td>
                      <td>Precise ID<br />BizID<br />Commercial Fraud Insight</td>
                      <td>Account Review Advantage<br />Quest</td>
                      <td>iScreen<br />Instant Prescreen<br />Prescreen<br />Prospect Triggers</td>
                    </tr>
                  </tbody>
                </table>
              </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:outputPanel >
              <p>For all requests that fall outside theses available values, please submit a Membership Opportunity Assessment through Salesforce as your document needs may be unique.</p>
              <p>To ensure an accurate assessment provide the client's business model process flow outlining both the data and contracting flows and thoroughly complete all the assessment questions presented in the request form.</p>
            </apex:outputPanel>
            </apex:pageBlockSection>
            
          </apex:outputPanel>
        </apex:pageBlockSection>
      </apex:pageBlock>
    </apex:outputPanel>
  </apex:form>
  
  <div id="pageMask" class="hide">
    <div class="spinner">
      <div class="bar1"></div><div class="bar2"></div><div class="bar3"></div><div class="bar4"></div>
      <div class="bar5"></div><div class="bar6"></div><div class="bar7"></div><div class="bar8"></div>
      <div class="bar9"></div><div class="bar10"></div><div class="bar11"></div><div class="bar12"></div>
    </div>
  </div>
  
</apex:page>