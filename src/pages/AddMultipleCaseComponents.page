<!--
/**=====================================================================
 * Experian
 * Name: AddMultipleCaseComponents
 * Description: Search for and add case components
 * Created Date:
 * Created By: Ninie
 *
 * Date Modified      Modified By                  Description of the update
 * Apr 27th, 2016     Paul Kissick                 Fixing search because it's slow and has no feedback
  =====================================================================*/
-->
<apex:page id="thePage" controller="AddMultipleCaseComponentsCtrl" title="Add Multiple Case Components">
  <apex:form id="theForm">
  <!-- Start of SearchBlock -->
    <apex:pageBlock id="SearchBlock" title="Search Metadata Components">
      <apex:pageBlockButtons location="bottom">
        <apex:commandButton value="Search" action="{!search}" reRender="theResultBlock" status="searchingComps"/>
        <apex:outputPanel >
          <apex:actionStatus id="searchingComps">
            <apex:facet name="start"><apex:image value="/img/loading32.gif" width="16" height="16" /></apex:facet>
          </apex:actionStatus>
        </apex:outputPanel>
      </apex:pageBlockButtons>
      <apex:pageBlockSection >
        <apex:selectList size="1" value="{!searchCriteria.componentType}" label="Type">
          <apex:selectOptions value="{!componentTypeList}"/>
        </apex:selectList>
        <apex:inputText value="{!searchCriteria.componentName}" label="Name" onkeypress="return noenter(event);"/>
      </apex:pageBlockSection>
      <apex:actionFunction name="doSearchFunc" action="{!search}" reRender="theResultBlock" status="searchingComps" />
    </apex:pageBlock>
    <apex:pageBlock id="theResultBlock" title="Metadata Components">
      <apex:pageBlockButtons location="both">
        <apex:commandButton value="Add Component(s)" action="{!addComponent}" />
        <apex:commandButton value="Back" action="{!back}"/>
      </apex:pageBlockButtons>
      <apex:pageMessages />
      <!--
      <apex:pageBlockSection >
        <apex:selectList size="1" multiselect="false" value="{!searchCriteria.DeploymentType}" label="Deployment Type">
          <apex:selectOptions value="{!DeploymentTypeList}" />
          <apex:actionSupport event="onchange" reRender="theResultSet" />
        </apex:selectList>
        <apex:selectList size="1" multiselect="false" value="{!searchCriteria.action}" label="Action">
          <apex:selectOptions value="{!ActionList}" />
        </apex:selectList>
      </apex:pageBlockSection>
      -->
      <apex:pageBlockTable id="theResultSet" value="{!metadataComponentList}" var="comp">
        <apex:column width="5%" >
          <apex:facet name="header">
            <apex:inputCheckbox id="allChecker" onclick="checkAll(this, 'recordChecker');" rendered="{!IF(metadataComponentListCount==0,false,true)}" value="{!comp.selected}"/>
          </apex:facet>
          <apex:inputCheckbox id="recordChecker" onclick="check({!metadataComponentListCount});" value="{!comp.selected}"/>
        </apex:column>
        <apex:column width="15%" headerValue="Type" value="{!comp.obj.Component_Type__c}" />
        <apex:column width="15%" headerValue="Object API Name" value="{!comp.obj.Object_API_Name__c}" />
        <apex:column width="15%" headerValue="Name" value="{!comp.obj.Component_API_Name__c}" />
        <apex:column width="15%" headerValue="Action">
          <apex:selectList size="1" value="{!comp.selectedAction}">
            <apex:selectOptions value="{!actionList}"/>
          </apex:selectList>
        </apex:column>
        <apex:column width="15%" headerValue="Deployment Type">
          <apex:selectList size="1" value="{!comp.selectedDeploymentType}" >
            <apex:selectOptions value="{!deploymentTypeList}"/>
          </apex:selectList>
        </apex:column>
        <apex:column width="15%" headerValue="Configuration (Manual)">
          <apex:selectList size="1" value="{!comp.selectedConfigType}" >
            <apex:selectOptions value="{!configTypeList}"/>
          </apex:selectList>
        </apex:column>
        <apex:column width="20%" headerValue="Manual Step details">
          <apex:inputTextarea value="{!comp.manualSteps}"/>
        </apex:column>

      </apex:pageBlockTable>
      <apex:outputPanel layout="block" style="text-align:center">
        <apex:outputLabel value="No record found" rendered="{!metadataComponentListCount < 0}" />
      </apex:outputPanel>
      <apex:pageBlockSection id="pb2" rendered="{!metadataComponentListCount > 0}">
        <apex:outputPanel >
          <div style="align:center">
            <apex:commandButton status="fetchStatus" reRender="theResultBlock" value="|<" action="{!first}" disabled="{!!componentRecords.hasPrevious}" title="First Page"/>
            <apex:commandButton status="fetchStatus" reRender="theResultBlock" value="<" action="{!previous}" disabled="{!!componentRecords.hasPrevious}" title="Previous Page"/>
            <apex:commandButton status="fetchStatus" reRender="theResultBlock" value=">" action="{!next}" disabled="{!!componentRecords.hasNext}" title="Next Page"/>
            <apex:commandButton status="fetchStatus" reRender="theResultBlock" value=">|" action="{!last}" disabled="{!!componentRecords.hasNext}" title="Last Page"/>
          </div>
        </apex:outputPanel>
        <apex:outputPanel style="text-align:right">
          <apex:outputText >{!(componentRecords.pageNumber * pageSize)+1-pageSize}-{!IF((componentRecords.pageNumber * pageSize)>noOfRecords, noOfRecords,(componentRecords.pageNumber * pageSize))} of {!noOfRecords}</apex:outputText>
        </apex:outputPanel>
      </apex:pageBlockSection>
    </apex:pageBlock>
  </apex:form>
  <script type="text/javascript">
    function noenter (ev) {
      if (window.event && window.event.keyCode == 13 || ev.which == 13) {
        doSearchFunc();
        return false;
      }
      else {
        return true;
      }
    }
    function checkAll (cb,cbid) {
      var inputElem = document.getElementsByTagName("input");
      for (var i=0; i<inputElem.length; i++) {
        if (inputElem[i].id.indexOf(cbid)!=-1) {
          inputElem[i].checked = cb.checked;
        }
      }
    }

    </script>
</apex:page>