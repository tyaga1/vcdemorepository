<!--
/**=====================================================================
 * Experian
 * Name: ITCA_Home_Page
 * Description: ITCA_BuildProfile_Page
 * Created Date: July 9th 2017    
 * Created By:   Richard Joseph
 *
 * Date Modified      Modified By           Description of the update
 * 24/08/17           Alexander McCall      I1425
 * 06/09/17           Alexander McCall      ITCA I1466 Conditional Sub Header
 * 07/09/17           Alexander McCall      ITCA I1469 Skillset hyperlinks and submit button changes
 * =====================================================================*/
 -->
<apex:page controller="ITCA_Profile_Builder_controller" standardStylesheets="false" sidebar="false" docType="html-5.0" showHeader="false" action="{!ITCA_Profile_Builder_Load}">
    <title> ITCA </title>
    
    <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />

    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"/>

    <style>
      .ITCAFooter ul li,ol li{margin-left:1.5em;padding-left:0;}
      .ITCAFooter {font-size: 15px;    font-family: 'Roboto', sans-serif;}
      .header h2 {font-size: 70px;}
    </style> 


    <body class="home">
  <!-- Navigation -->
    
    <!-- Navigation -->
    <c:ITCA_Navigation_Banner currentActiveTab1="{!bannerInfoLocal}"/>
    <!-- ../Navigation-->
        

 <!-- AM Commented Out <div class="banner_container small_marquee" style="background:url(/global-images/hero/line-art-marquee-2000x250.png);filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( src='/global-images/hero/line-art-marquee-2000x250.png', sizingMethod='scale');"> -->
           <div>
              <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12 header">
                        <h2> Build your profile </h2>
                        <!-- ITCA I1466 Conditional Sub Header -->
                        <apex:outputPanel rendered="{!NOT(profileExists)}">
                            <h3> Getting started is easy! </h3>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!profileExists}">
                            <h3> Adding skills is easy! </h3>
                        </apex:outputPanel>

                        
                    </div>
                </div>
              </div>
           </div>
      <!-- AM Commented out  </div> -->
    <!-- ../Hero Image -->

    <!--Skill Sets Buckets-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p style="padding: 0px 0 30px;">
                        ITCA technical skill sets are aligned to Career Areas. To build an inventory of your primary skills (the ones you probably use most in your current role) start by exploring the skill sets in the Career Area that most closely describes the work you do today. 
                        You probably have skills from previous roles or experiences that you don’t use regularly in your role today.  These are your secondary skills.  To build an inventory of your secondary skills, explore the Career Areas that most closely describe previous experience.
                    </p>
                </div>
                
                <!--c:ITCA_Show_Career_Area_Skill_Sets/-->
                <apex:form >
                <div class="col-sm-12">
                    <div class="row is-flex">
                        <apex:repeat value="{!careerAreas}" var="careerArea">
                         
                       
                        <div class="col-md-3 col-sm-6">
                           
                            <div class="skill-set">
                                
                                <div class="title text-center"> 
                                    {!careerArea}
                                </div>
                                <div class="skills">
                                     <apex:repeat value="{!careerSkillSetDisplayList}" var="skillSets">  
                                      <apex:outputPanel rendered="{!IF(careerArea=skillSets.skillSetRecord.Career_Area__c,true,false)}">
                                    <div class="checkbox">
                                        
                                        <label>
                                           <apex:inputCheckbox value="{!skillSets.skillSetSelected}" id="checkedone"/>


                                            <!-- I1469<a href="../apex/ITCA_SkillSelect_Page?skillsetID={!skillSets.skillSetRecord.Id}">-->{!skillSets.skillSetRecord.Name}<!--</a>--><br/>

                                             
                                        </label>
                                    </div>
                                </apex:outputPanel>
                                </apex:repeat>
                                </div>    

                                
                            </div>
                            
                        </div>
                        
                        </apex:repeat>
                    </div>
                </div>
                
                <div class="col-sm-12">
                    <apex:commandLink styleClass="btn btn-experian-dark-blue" action="{!SkillSetSubmit}"> <!-- I1469 AM Commented OutSubmit-->Next</apex:commandLink>

                     
                </div>

                </apex:form>
                
                
            </div>
        </div>    



        <!--Footer-->
          <!--<c:ITCA_Page_Footer />-->
          <div class="ITCAFooter">
                      <apex:composition template="Community_template_footer"></apex:composition>
        </div>
        <!--../Footer-->


        <!-- Scripts -->
        <!--<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"></script>
        <script type="text/javascript" src="js/exp.navbar.js"></script>
        <script type="text/javascript" src="js/floatl.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        -->
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
        <!--.. /Scripts -->
        
    </body>

 

</apex:page>