<!--
/**=====================================================================
 * Experian
 * Name: NominationHome
 * Description: Nomination Home Page for Global
 * Created Date: Oct, 2016
 * Created By:   Tyaga Pati
 *
 * Date Modified      Modified By           Description of the update
 * 
 * =====================================================================*/
 -->
<apex:page tabStyle="Nomination__c" controller="NominationHomeController" action="{!initialisePage}" showHeader="true" docType="html-5.0" >
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <!-- THE ORDERING OF THESE FILES IS IMPORTANT -->
  <apex:includeScript value="{!$Resource.jquery_1_12_4}" />
  <script>
    var $j = jQuery.noConflict();
  </script>
  <apex:includeScript value="{!URLFOR($Resource.Bootstrap337, 'js/bootstrap.min.js')}" />
  
  <apex:stylesheet value="{!URLFOR($Resource.Bootstrap337, 'css/bootstrap-ns.min.css')}" />
  <apex:stylesheet value="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <apex:stylesheet value="//fonts.googleapis.com/css?family=Roboto" />
  
<style>
  .egc {
    font-family: "Roboto", Helvetica, Arial, sans-serif !important;
  }
  .experianDarkBlue {
    background-color: #26478d !important;
    border-color: #1e315a !important;
    color: #fff !important; 
  }
  .experianLightBlue {
    background-color: #406eb3 !important;
    color: #fff !important;
  }
  .experianViolet {
    background-color: #632678 !important;
    color: #fff !important;
  }
  .experianPink {
    background-color: #ba2f7d !important;
    border-color: #8e2f64 !important;  
    color: #fff !important;          
  }
  .experianGreen {
    background-color: #b6bd0d !important;
    border-color: #848a04 !important;
    color: #fff !important;
  }
  .experianPurple {
    background-color: #982881 !important;
    color: #fff !important;
  }
 
  .panel-table .panel-footer .col{
    line-height: 20px;
    height: 20px;
  }
  .exp-btn-group > input {
    margin-right: 20px;
  }
  .Level3HalfYearNominationTeam {
  }
  .Level3HalfYearNominationTeam::after {
    content: "L3 HY Team";
  }
  .Level2SpotAward::after {
    content: "L2 Spot";
  }
  .Level3HalfYearNominationIndividual::after {
    content: "L3 HY Indiv";
  }
  
  .Level4Elite::after {
    content: "L4 Elite";
  }
  .egc .label:empty {
    display: inline !important;
  }
  .label-exp-info {
    /* Dark Blue Colour */
    background-color: #26478d !important;
    font-size: 100% !important;
  }
</style>
  
<apex:form >

  <div class="egc container-fluid">
    <!-- PAGE HEADER -->
    <div class=" hidden-xs hidden-sm">
      <div class="page-header">
        <div class="row">
              <div class="col-md-3">
                <div class="media">
                  <div class="pull-left">
                    <img class="media-object" src="/img/icon/custom51_100/shield64.png" alt="{!$ObjectType.Nomination__c.label}" />
                  </div>
                  <div class="media-body">
                    <!-- TODO: Consider replacing these with labels -->
                    <h5><strong>One Experian Recognition</strong></h5><br />
                    <h2>Home</h2>                    
                  </div>
                </div>
              </div>
              <!-- Line up for the Buttons and Links -->
              <div class="col-md-7" style="width: 70%;">
                <div class="exp-btn-group" >
                  <apex:commandButton value="New Nomination/Award" 
                    styleClass="btn btn-default experianDarkBlue" 
                    alt="Reward Someone"
                    title="New Nomination/Reward"
                    action="{!navToNewRewardPage}" 
                  /><!--  $Permission.Recognition_Coordinator, -->
                  <apex:commandButton value="View Dashboard" 
                    styleClass="btn btn-default experianPurple" 
                    alt="Go To Dashboard"
                    title="View Dashboard"
                    rendered="{!OR($Permission.Administration_Profile,$Permission.Recognition_Coordinator,$User.Oracle_IsManager__c)}"
                    action="{!navToManagerDashboard}"
                  /><!--  $Permission.Recognition_Coordinator, -->
                  <apex:commandButton value="Pending Panel Report" 
                    styleClass="btn btn-default experianLightBlue " 
                    rendered="{!OR($Permission.Administration_Profile,$Permission.Recognition_Coordinator)}"
                    alt="Pending Panel Report"
                    title="Pending Panel Report"
                    action="{!navToRecognitionCoordinatorDashboard}"
                  /><!--  $Permission.Recognition_Coordinator, -->
                  <apex:outputLink value="{!$Label.Zoom_URL_for_Recognition}" 
                    styleClass="btn btn-default experianViolet "            
                    title="Need Help?"
                    target="_blank"
                  >Need Help?</apex:outputLink>
                  </div>
              </div>
        </div>  
      </div>
    </div>
    &nbsp;<br /> 
    <apex:outputPanel layout="block" styleClass="row" rendered="{!hasMsg}">
      <div class="col-xs-12">
         <div class="alert alert-success alert-block collapse in" role="alert" id="successMsgBlock">
           <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
           <span class="sr-only">Success:&nbsp;</span>{!msg}
          </div>
          <script>
            setTimeout(function(){
              $j('#successMsgBlock').collapse('hide');
            }, 10000);
          </script>
       </div>
    </apex:outputPanel>
    <div class="row">
    <apex:outputPanel layout="block" styleClass="col-sm-12" rendered="{!MyPendingApproval.NomRecords.size > 0}" id="myPendingApprovals" style="overflow-x: auto">
        <div class="panel panel-success ">
          <div class="panel-heading experianPink">
            <h3 class="panel-title">My Pending Approvals</h3>
          </div>
          <div>
          <apex:outputPanel layout="block">
            <div class="">
              <apex:dataTable value="{!MyPendingApproval.NomRecords}" var="m" styleClass="table table-striped">
                <apex:column headerValue="Action" >
                  <apex:outputLink value="{!URLFOR($Action.Nomination__c.View,m.Id)}">View</apex:outputLink>
                </apex:column>
                <apex:column headerValue="" style="width:100px">
                  <apex:image rendered="{!m.Nominee__c != null}" value="{!m.Nominee__r.SmallPhotoUrl}" style="width:20px; height:20px;" />
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Nominee__c.label} /{!$ObjectType.Nomination__c.fields.Team_Name__c.label}" style="width:100px">
                  <apex:outputField value="{!m.Nominee__c}"  rendered="{!m.Nominee__c != null}" />
                  <apex:outputField value="{!m.Team_Name__c}" rendered="{!m.Nominee__c == null && m.Team_Name__c != null}" />
                </apex:column>
                <!--
                // How does this work?
                // The class in the span is made up by removing the spaces, brackets and dashes from the Type field.
                // This declared above with a ::after css pseudo element with content to display a specific line of text.
                // e.g. Level 3 - Half Year Nomination (Team) -> Level3HalfYearNominationTeam -> L3 HY Team
                 -->
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Type__c.label}" style="width:120px">
                  <span class="label label-exp-info {!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(m.Type__c,'-',''),')',''),'(',''),' ','')}" title="{!m.Type__c}"></span>
                </apex:column>
                <apex:column headerValue="{!$Label.Recognition_Type_Label}" style="width:100px">
                  <apex:outputField style="vertical-align: middle;" value="{!m.Requestor__c}" />
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Justification__c.label}">
                  <apex:outputField style="vertical-align: middle;" value="{!m.Justification__c}" />
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.createddate.label}">
                  <apex:outputField style="vertical-align: middle;" value="{!m.createddate}" />
                </apex:column>
                
              </apex:dataTable>
            </div>
          </apex:outputPanel>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-xs-6">Page {!myPendingApproval.CurrentPage} of {!myPendingApproval.totalPages}</div>
            <div class="col-xs-6 text-right">{!myPendingApproval.totalSize} Record{!IF(myPendingApproval.totalSize!=1,'s','')}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination">
                <li class="{!IF(myPendingApproval.HasPrevious, '', 'disabled')}">
                  <apex:commandLink action="{!myPendingApproval.getPrevious}" status="updatePendingApprovals" rerender="myPendingApprovals" rendered="{!myPendingApproval.HasPrevious}">«</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myPendingApproval.hasPrevious)}">&laquo;</apex:outputPanel>
                </li>
                <apex:repeat value="{!myPendingApproval.pageList}" var="i">
                  <li class="{!IF(i == myPendingApproval.currentPage, 'active', '')}"><apex:commandLink action="{!myPendingApproval.gotoPage}" rerender="myPendingApprovals" status="updatePendingApprovals"><apex:param name="gotoMyPendingPagesParam" value="{!i}" assignTo="{!myPendingApproval.gotoPageNum}" />{!i}</apex:commandLink></li>
                </apex:repeat>
                <li class="{!IF(myPendingApproval.hasNext, '', 'disabled')}">
                  <apex:commandLink action="{!myPendingApproval.getNext}" status="updatePendingApprovals" rerender="myPendingApprovals"  rendered="{!myPendingApproval.hasNext}">»</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myPendingApproval.hasNext)}" >&raquo;</apex:outputPanel>
                </li>
              </ul>
              <apex:actionStatus id="updatePendingApprovals">
                <apex:facet name="start"><img src="/img/loading.gif" /></apex:facet>
              </apex:actionStatus>
            </div>
          </div>
        </div>
      </div>
    </apex:outputPanel>

    
  </div>
    
    
    
    
    <div class="row">
      <!-- This is the First Panel Pending Approval" -->

    <!-- End of First Panel Pending Approval" -->
    <!-- The Space betweent the First and Second Panel -->
    <!-- Begining of 2nd Panel - My Rewards-->  

    <apex:outputPanel layout="block" styleClass="col-sm-6" id="myRewards">
      <div class="panel panel-success">
        <div class="panel-heading experianDarkBlue">
          <h3 class="panel-title ">My Nominations/Awards</h3>
        </div>
        <div >
          <apex:outputPanel layout="block">
            <div class="">
              <apex:dataTable value="{!myRewards.NomRecords}" var="m" styleClass="table table-striped">
                <apex:column headerValue="Action">
                  <apex:outputLink value="{!URLFOR($Action.Nomination__c.View,m.Id)}">View</apex:outputLink>
                </apex:column>
                <!--<apex:column headerValue="{!$ObjectType.Nomination__c.fields.Recognition_Category__c.label}"> -->
                <apex:column headerValue="{!$Label.Recognition_Category_Label}">
                 <!-- <apex:image value="{!m.Badge__r.ImageUrl}" style="width: 20%;" /> -->
                  <apex:outputField style="vertical-align: middle;" value="{!m.Badge__r.Name}" />
                </apex:column>
                <apex:column headerValue="{!$Label.Recognition_Type_Label}">
                  <span class="label label-exp-info {!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(m.Type__c,'-',''),')',''),'(',''),' ','')}" title="{!m.Type__c}"></span>
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Requestor__c.label}">
                  <apex:outputField style="vertical-align: middle;" value="{!m.Requestor__c}" />
                </apex:column>
              </apex:dataTable>
            </div>
          </apex:outputPanel>       
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-xs-6">Page {!myRewards.CurrentPage} of {!myRewards.totalPages}</div>
            <div class="col-xs-6 text-right">{!myRewards.totalSize} Record{!IF(myRewards.totalSize!=1,'s','')}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination">
                <li class="{!IF(myRewards.HasPrevious, '', 'disabled')}">
                  <apex:commandLink action="{!myRewards.getPrevious}" status="updateMyRewards" rerender="myRewards" rendered="{!myRewards.HasPrevious}">«</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myRewards.hasPrevious)}">&laquo;</apex:outputPanel>
                </li>
                <apex:repeat value="{!myRewards.pageList}" var="i">
                  <li class="{!IF(i == myRewards.currentPage, 'active', '')}"><apex:commandLink action="{!myRewards.gotoPage}" rerender="myRewards" status="updateMyRewards"><apex:param name="gotoMyPendingPagesParam" value="{!i}" assignTo="{!myRewards.gotoPageNum}" />{!i}</apex:commandLink></li>
                </apex:repeat>
                <li class="{!IF(myRewards.hasNext, '', 'disabled')}">
                  <apex:commandLink action="{!myRewards.getNext}" status="updateMyRewards" rerender="myRewards"  rendered="{!myRewards.hasNext}">»</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myRewards.hasNext)}" >&raquo;</apex:outputPanel>
                </li>
              </ul>
              <apex:actionStatus id="updateMyRewards">
                <apex:facet name="start"><img src="/img/loading.gif" /></apex:facet>
              </apex:actionStatus>
            </div>
          </div>
        </div>
      </div>
    </apex:outputPanel>
 <!-- Panel 3 for the My Submissions data  -->
 
    <apex:outputPanel layout="block" styleClass="col-sm-6" id="mySubmissions" style="overflow-x: auto">
      <div class="panel panel-success" >
        <div class="panel-heading experianGreen">
          <h3 class="panel-title ">My Submitted Nominations/Awards</h3>
        </div>
        <div >
          <apex:outputPanel layout="block">
            <div class="">
              <apex:dataTable value="{!mySubmissions.NomRecords}" var="m" styleClass="table table-striped">
                <apex:column headerValue="Action" >
                  <apex:outputLink value="{!URLFOR($Action.Nomination__c.View,m.Id)}">View</apex:outputLink>
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Nominee__c.label} / {!$ObjectType.Nomination__c.fields.Team_Name__c.label}">
                  <apex:outputField value="{!m.Nominee__c}"  rendered="{!m.Nominee__c != null}" />
                  <apex:outputField value="{!m.Team_Name__c}" rendered="{!m.Nominee__c == null && m.Team_Name__c != null}" />
                </apex:column>
                <apex:column headerValue="{!$Label.Recognition_Type_Label}">
                  <span class="label label-exp-info {!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(m.Type__c,'-',''),')',''),'(',''),' ','')}" title="{!m.Type__c}"></span>
                </apex:column>
                <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Status__c.label}">
                  <apex:outputField style="vertical-align: middle;" value="{!m.Status__c}" />
                </apex:column>
              </apex:dataTable>
            </div>
          </apex:outputPanel>       
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-xs-6">Page {!mySubmissions.CurrentPage} of {!mySubmissions.totalPages}</div>
            <div class="col-xs-6 text-right">{!mySubmissions.totalSize} Record{!IF(mySubmissions.totalSize!=1,'s','')}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination">
                <li class="{!IF(mySubmissions.HasPrevious, '', 'disabled')}">
                  <apex:commandLink action="{!mySubmissions.getPrevious}" status="updateMySubmissions" rerender="mySubmissions" rendered="{!mySubmissions.HasPrevious}">«</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(mySubmissions.hasPrevious)}">&laquo;</apex:outputPanel>
                </li>
                <apex:repeat value="{!mySubmissions.pageList}" var="i">
                  <li class="{!IF(i == mySubmissions.currentPage, 'active', '')}"><apex:commandLink action="{!mySubmissions.gotoPage}" rerender="mySubmissions" status="updateMySubmissions"><apex:param name="gotoMyPendingPagesParam" value="{!i}" assignTo="{!mySubmissions.gotoPageNum}" />{!i}</apex:commandLink></li>
                </apex:repeat>
                <li class="{!IF(mySubmissions.hasNext, '', 'disabled')}">
                  <apex:commandLink action="{!mySubmissions.getNext}" status="updateMySubmissions" rerender="mySubmissions"  rendered="{!mySubmissions.hasNext}">»</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(mySubmissions.hasNext)}" >&raquo;</apex:outputPanel>
                </li>
              </ul>
              <apex:actionStatus id="updateMySubmissions">
                <apex:facet name="start"><img src="/img/loading.gif" /></apex:facet>
              </apex:actionStatus>
            </div>
            
          </div>
        </div>
      </div>
    </apex:outputPanel>
   
  </div>


&nbsp;<br /> 
<div class="row">
     <apex:outputPanel layout="block" styleClass="col-sm-6" rendered="{!myBadges.BadgeRecords.size > 0}"  id="MyBadges">
        <div class="panel panel-success ">
          <div class="panel-heading experianLightBlue">
            <h3 class="panel-title">Badges Received</h3>
          </div>
          <div>
          <apex:outputPanel layout="block">
            <div class="">
              <apex:dataTable value="{!myBadges.BadgeRecords}" var="m" styleClass="table table-striped">
  
                <apex:column headerValue="Badge" style="width:170px" >
                <apex:image value="{!m.ImageUrl}" style="width: 20%;" />
                   &nbsp;
                    &nbsp;
                  <apex:outputField style="vertical-align: middle;" value="{!m.DefinitionId}" />
                </apex:column>         
                <apex:column headerValue="Message" style="width:250px" >
                  <apex:outputField value="{!m.Message}"  rendered="{!m.Message!= null}" />
                </apex:column>
                <apex:column headerValue="Given By" style="width:250px">
                 <apex:outputField value="{!m.GiverId}"  rendered="{!m.GiverId!= null}" />
                </apex:column>
                
                
                <!--
                
                // How does this work?
                // The class in the span is made up by removing the spaces, brackets and dashes from the Type field.
                // This declared above with a ::after css pseudo element with content to display a specific line of text.
                // e.g. Level 3 - Half Year Nomination (Team) -> Level3HalfYearNominationTeam -> L3 HY Team
                 -->
               </apex:dataTable>
            </div>
          </apex:outputPanel>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-xs-6">Page {!myBadges.CurrentPage} of {!myBadges.totalPages}</div>
            <div class="col-xs-6 text-right">{!myBadges.totalSize} Record{!IF(myBadges.totalSize!=1,'s','')}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination">
                <li class="{!IF(myBadges.HasPrevious, '', 'disabled')}">
                  <apex:commandLink action="{!myBadges.getPrevious}" status="updatePendingApprovals1" rerender="MyBadges" rendered="{!myBadges.HasPrevious}">«</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myBadges.hasPrevious)}">&laquo;</apex:outputPanel>
                </li>
                <apex:repeat value="{!myBadges.pageList}" var="i">
                  <li class="{!IF(i == myBadges.currentPage, 'active', '')}"><apex:commandLink action="{!myBadges.gotoPage}" rerender="MyBadges" status="updatePendingApprovals"><apex:param name="gotoMyPendingPagesParam" value="{!i}" assignTo="{!myBadges.gotoPageNumTwo}" />{!i}</apex:commandLink></li>
                </apex:repeat>
                <li class="{!IF(myBadges.hasNext, '', 'disabled')}">
                  <apex:commandLink action="{!myBadges.getNext}" status="updatePendingApprovals1" rerender="MyBadges"  rendered="{!MyBadges.hasNext}">»</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(MyBadges.hasNext)}" >&raquo;</apex:outputPanel>
                </li>
              </ul>
              <apex:actionStatus id="updatePendingApprovals1">
                <apex:facet name="start"><img src="/img/loading.gif" /></apex:facet>
              </apex:actionStatus>
            </div>
          </div>
        </div>
      </div>
    </apex:outputPanel>

     <apex:outputPanel layout="block" styleClass="col-sm-6" rendered="{!myGivenBadges.BadgeRecords.size > 0}"  id="myGivenBadges">
        <div class="panel panel-success ">
          <div class="panel-heading experianViolet">
            <h3 class="panel-title">Badges Given</h3>
          </div>
          <div>
          <apex:outputPanel layout="block">
            <div class="">
              <apex:dataTable value="{!myGivenBadges.BadgeRecords}" var="m" styleClass="table table-striped">
                <apex:column headerValue="Badge" style="width:270px"  >
                  <apex:image value="{!m.ImageUrl}  " style="width: 20%;  " />
                    &nbsp;
                  <apex:outputField style="vertical-align: middle;" value="{!m.DefinitionId}" />
                </apex:column>
                <apex:column headerValue="Message" style="width:250px" >
                  <apex:outputField value="{!m.Message}"  rendered="{!m.Message!= null}" />
                </apex:column>
                <apex:column headerValue="Given To" style="width:200px" >
                  <apex:outputField value="{!m.RecipientId}"  rendered="{!m.RecipientId!= null}" />
                </apex:column>


                <!--
                
                // How does this work?
                // The class in the span is made up by removing the spaces, brackets and dashes from the Type field.
                // This declared above with a ::after css pseudo element with content to display a specific line of text.
                // e.g. Level 3 - Half Year Nomination (Team) -> Level3HalfYearNominationTeam -> L3 HY Team
                 -->
               </apex:dataTable>
            </div>
          </apex:outputPanel>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-xs-6">Page {!myGivenBadges.CurrentPage} of {!myGivenBadges.totalPages}</div>
            <div class="col-xs-6 text-right">{!myGivenBadges.totalSize} Record{!IF(myGivenBadges.totalSize!=1,'s','')}</div>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination">
                <li class="{!IF(myGivenBadges.HasPrevious, '', 'disabled')}">
                  <apex:commandLink action="{!myGivenBadges.getPrevious}" status="updatePendingApprovals1" rerender="myGivenBadges" rendered="{!myGivenBadges.HasPrevious}">«</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myGivenBadges.hasPrevious)}">&laquo;</apex:outputPanel>
                </li>
                <apex:repeat value="{!myGivenBadges.pageList}" var="i">
                  <li class="{!IF(i == myGivenBadges.currentPage, 'active', '')}"><apex:commandLink action="{!myGivenBadges.gotoPage}" rerender="myGivenBadges" status="updatePendingApprovals"><apex:param name="gotoMyPendingPagesParam" value="{!i}" assignTo="{!myGivenBadges.gotoPageNum}" />{!i}</apex:commandLink></li>
                </apex:repeat>
                <li class="{!IF(myGivenBadges.hasNext, '', 'disabled')}">
                  <apex:commandLink action="{!myGivenBadges.getNext}" status="updatePendingApprovals2" rerender="myGivenBadges"  rendered="{!myGivenBadges.hasNext}">»</apex:commandLink>
                  <apex:outputPanel rendered="{!NOT(myGivenBadges.hasNext)}" >&raquo;</apex:outputPanel>
                </li>
              </ul>
              <apex:actionStatus id="updatePendingApprovals2">
                <apex:facet name="start"><img src="/img/loading.gif" /></apex:facet>
              </apex:actionStatus>
            </div>
          </div>
        </div>
      </div>
    </apex:outputPanel>
</div>
</div>
</apex:form>
</apex:page>