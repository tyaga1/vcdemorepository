<!--
/**=====================================================================
 * Experian
 * Name: ITCA_Experian_Way
 * Description: ITCA_Experian_Way
 * Created Date: August 2017    
 * Created By:   Alexander McCall
 *
 * Date Modified      Modified By           Description of the update
 * 24/08/17           Alexander McCall      I1425
 * =====================================================================*/
 -->
<apex:page controller="ITCA_Homepage_Controller" docType="html-5.0" showHeader="false" standardStylesheets="false" sidebar="false"
     >
    <title> ITCA </title>
    
    <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />

    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"
    />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"
    />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"
    />

    <style>
    .the-experian-way i.icon {
        float: left;
        margin: 0 35px;
        margin-top: 20px;
    }

    .the-experian-way p {
        margin-left: 170px;
        font-weight: 500;
    }
    
    .the-experian-way ul {
        margin-left: 130px;
    }
    .ITCAFooter ul li,ol li{margin-left:1.5em;padding-left:0;}
    .ITCAFooter {font-size: 15px;    font-family: 'Roboto', sans-serif;}
    .header h2 {font-size: 70px;}
    </style>

    <body class="home">
        <!-- Navigation -->
        <!-- Navigation -->
        <c:ITCA_Navigation_Banner currentActiveTab1="{!bannerInfoLocal}"/>
        <!-- ../Navigation-->
        
        <!-- Banner-->
       <!-- AM Commented Out <div class="banner_container small_marquee" style="background:url(/global-images/hero/line-art-marquee-2000x250.png);filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( src='/global-images/hero/line-art-marquee-2000x250.png', sizingMethod='scale');"> -->
           <div>
              <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-xs-12 header">
                        <h2> The Experian Way </h2>                      
                    </div>
                </div>
              </div>
           </div>
      <!-- AM Commented out  </div> -->
       

        
    <!--Experian Way-->
    <div style="background-color:white">
       <div class="container the-experian-way" id="the-experian-way">
            <div class="row">
                <div class="col-sm-6">
                    <i class="icon i-primary-purple i-handshake" style="margin-top:5px;"></i> 
                    <div>
                        <h3>Delight Customers</h3>
                        <span>The Term 'Customers' refers to our 'Clients' (B2B) and 'Consumers' (B2C and B2B2C).</span>
                    </div>
                    <div>
                        <p><b>
                            At Experian, whether your role brings you into contact with customers directly or not, all of us contribute to meeting customer needs. At the heart of what we do are the relationships we invest in and nurture.
                        </b></p>
                        <ul class="list-bullets">
                            <li>
                                We are proactive in understanding customers' points of view, their needs and challenges so we can focus on delivering value to them with the right solutions. We proactively support each other internally to enable the best outcomes for customers.
                            </li>
                            <li>
                                We focus strongly on execution and deliver what we promise to our customers. We go the extra mile for our customers. But that doesn't mean saying â€˜yes' to everything. We first understand the commercial implications of our actions and decisions.
                            </li>
                            <li>
                                All of us take accountability and continue to improve our service, making it simple and easy. If things do go wrong, we focus on finding the solution and fixing the issues in a quick and professional manner, rather than assigning blame.
                            </li>
                        </ul>
                    </div>
                    <i class="icon i-primary-purple i-client" style="margin-top: 5px;"></i>
                    <div>
                        <h3>Collaborate To Win</h3>
                    </div>
                    <div>
                        <p><b>
                            'One Experian' mindset - we work as one united team and use the combined strengths and capabilities of our people, products and services across teams, functions and regions. This translates into seamless experiences for our customers.
                        </b></p>
                        <ul class="list-bullets">
                            <li>
                                We trust each other and proactively reach out to work together. We do this to solve problems better, driven by a clear focus on performance, results and outcomes.
                            </li>
                            <li>
                                We overcome barriers and work together across organisational boundaries, reporting lines and processes. We do this without slowing things down or losing individual ownership and accountability.
                            </li>
                            <li>
                                We embrace diversity and appreciate different perspectives and the unique value each of us brings. We are inquisitive to learn from each other.
                            </li>
                            <li>
                                We also work closely with our customers and partners to unlock greater value and help us develop better solutions.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <i class="icon i-primary-purple i-customer"></i>
                    <div>
                        <h3>Value Each Other</h3>
                    </div>
                    <div>
                        <p><b>
                            We make Experian a great place to work. We treat each other with respect, trust and integrity.
                        </b></p>
                        <ul class="list-bullets">
                            <li>
                                We support and invest in each other to help everyone achieve their potential and aspirations. We are prepared to speak up and take a leadership role.
                            </li>
                            <li>
                                We recognise and celebrate individual and team performances and achievements. We value those who lead by example and set high standards for performance, and share their learnings.
                            </li>
                            <li>
                                We promote a culture of inclusivity and value diversity of all kinds including in thinking, knowledge and experience. 
                            </li>
                            <li>
                                We work together to make a positive impact on the lives of others through our commitment to Corporate Social Responsibility and community involvement. 
                            </li>
                        </ul>
                    </div>
                    <i class="icon i-primary-purple i-security"></i>
                    <div>
                        <h3>Safeguard Our Future</h3>
                    </div>
                    <div>
                        <p><b>
                            At Experian, each one of us acts as a guardian for the protection of data, information, assets and our people to safeguard our future.
                        </b></p>
                        <ul class="list-bullets">
                            <li>
                                We understand and apply the highest standards and rigour and make sure the data we access and work with is secure. We make sure our partners apply the same standards and rigorous approach.
                            </li>
                            <li>
                                We act as a trusted steward of data. This builds greater confidence for our customers.
                            </li>
                            <li>
                                Each one of us takes responsibility to understand the risk and compliance obligations of our role. We plan for what might go wrong as we innovate and collaborate and make decisions in the right way.
                            </li>
                            <li>
                                We ensure that everyday security measures are always in place to protect ourselves and our assets â€“ screens locked, secured printing, organised and clutter free environment, sharing on social media.
                            </li>
                        </ul>
                    </div>
                    <i class="icon i-primary-purple i-idea" style="margin-top: 13px;"></i>
                    <div>
                        <h3>Innovate To Grow</h3>
                    </div>
                    <div>
                        <p><b>
                            At Experian, it's the responsibility of each one of us to find opportunities and improve the way we do things to help our business and our customers grow.
                        </b></p>
                        <ul class="list-bullets">
                            <li>
                                For us innovation is both transformative thinking and a continuous process of incremental improvements. It's not just for products and services, we're innovative in the way we approach and continue to do things better.
                            </li>
                            <li>
                                We understand that innovation requires taking the right risks. We welcome the opportunity to try new things, implement them fast and make them work. If we fail, we correct fast and learn from it to keep improving.
                            </li>
                            <li>
                                We scan the external environment and don't shy away from challenging things if we see a better way. We get out of our comfort zone, take ownership and find ways to share and nurture our ideas. We adapt our processes where they can be improved.
                            </li>
                            <li>
                                An idea without execution is worthless. So we prioritise, perform the required due diligence, test and execute in an agile way. 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>

    <!--../Experian Way-->

    

    <!--Template-->
    
    <!--Footer-->
        <!--<c:ITCA_Page_Footer />-->
        <div class="ITCAFooter">
          <apex:composition template="Community_template_footer"></apex:composition>
        </div>
    <!--../Footer-->


        <!-- Scripts -->
        <!--<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"></script>
        <script type="text/javascript" src="js/exp.navbar.js"></script>
        <script type="text/javascript" src="js/floatl.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        -->
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"
        />
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"
        />
        <!--.. /Scripts -->
    </body>

</apex:page>