<apex:page standardController="Opportunity_Plan_Competitor__c" extensions="OpportunityPlanCompetitorController">
    <style type="text/css">
      .inputfield { width: 350px; }
    </style>
    <apex:outputpanel >
        <apex:actionstatus id="entryStatus">
            <apex:facet name="start">
                <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb;
                       height: 100%;opacity:0.65;width:100%;"> 
                    <div class="waitingHolder" style="top: 74.2px; width: 91px;">
                        <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                        <span class="waitingDescription">Please Wait...</span>
                    </div>
                </div>
            </apex:facet>
        </apex:actionstatus>
    </apex:outputpanel>
    <apex:sectionHeader title="Opportunity Plan Competitor Edit" subtitle="{!pageTitle}"/>
    <apex:form >
        
        <apex:pageBlock mode="Edit">
            <apex:pageMessages id="errMsg"></apex:pageMessages>
            <apex:pageBlockButtons id="detailButtons" >
                  <apex:commandButton action="{!save}" value="Save" disabled="{!showSaveButton}"/>
                  <apex:commandButton action="{!saveAndNew}" value="Save & New" disabled="{!showSaveButton}"/>
                  <apex:commandButton action="{!cancel}" value="Cancel"/> 
            </apex:pageBlockButtons>
        
            <apex:pageblockSection columns="1" title="Opportunity Plan Competitor Edit">
                
                <!--  Begin Information section -->
                <apex:pageBlockSection title="Information">
                
                    <!-- name is an auto number. -->
                    <apex:outputField value="{!Opportunity_Plan_Competitor__c.Name}"/> 
                    <apex:outputText value=""/>
                    
                    <apex:inputField id="oppPlan" value="{!Opportunity_Plan_Competitor__c.Opportunity_Plan__c}" rendered="{!NOT(isEdit)}"/>
                    <apex:outputField value="{!Opportunity_Plan_Competitor__c.Opportunity_Plan__c}" rendered="{!isEdit}"/>
                    <apex:outputText value=""/>
                    
                    <apex:inputField value="{!Opportunity_Plan_Competitor__c.Competitor__c}"/>
                    <apex:outputText value=""/>
                
                </apex:pageBlockSection>
                <!-- End Information Section -->
                
                
                <!--  Begin Experian Advantages section -->
                <apex:pageBlockSection id="expAdvantage" title="Experian Advantages">
                    <apex:pageblockSection columns="1">
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Adv_1__c}" rendered="{!expAdvantage[0]}"/>
                    
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Adv_2__c}" rendered="{!expAdvantage[1]}"/>

                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Adv_3__c}" rendered="{!expAdvantage[2]}"/>
                    
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Adv_4__c}" rendered="{!expAdvantage[3]}"/>

                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Adv_5__c}" rendered="{!expAdvantage[4]}"/>
                    
                    
                    <apex:commandButton action="{!addEXPAdvantage}" style="margin-left:50%" value="Add Another Experian Advantage" status="entryStatus" rerender="expAdvantage, oppPlan, errMsg"/>
                    </apex:pageblockSection>
                
                </apex:pageBlockSection>
                <!-- End Experian Advantages Section -->
                
                
                
                
                <!--  Begin Competitor Advantages section -->
                <apex:pageBlockSection id="compAdvantage" title="Competitor Advantages">
                    <apex:pageblockSection columns="1">
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Adv_1__c}"  rendered="{!compAdvantage[0]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Adv_2__c}" rendered="{!compAdvantage[1]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Adv_3__c}" rendered="{!compAdvantage[2]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Adv_4__c}" rendered="{!compAdvantage[3]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Adv_5__c}" rendered="{!compAdvantage[4]}"/>
                
                    
                    <apex:commandButton action="{!addCompetitorAdvantage}" style="margin-left:48%" value="Add Another Competitor Advantage" status="entryStatus" rerender="compAdvantage, oppPlan, errMsg"/>
                    </apex:pageblockSection>
                </apex:pageBlockSection>
                <!-- End Competitor Advantages Section -->
                
                
                
                
                
                <!--  Begin Competitor Strategy section -->
                <apex:pageBlockSection id="compStrategy" title="Competitor Strategy">
                    <apex:pageblockSection columns="1">
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Strat_1__c}" rendered="{!compStrategy[0]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Strat_2__c}" rendered="{!compStrategy[1]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Strat_3__c}" rendered="{!compStrategy[2]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Strat_4__c}" rendered="{!compStrategy[3]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Comp_Strat_5__c}" rendered="{!compStrategy[4]}" />
                
                    
                    <apex:commandButton action="{!addCompetitorStrategy}" style="margin-left:50%" value="Add Another Competitor Strategy" status="entryStatus" rerender="compStrategy, oppPlan, errMsg"/>
                    </apex:pageblockSection>
                </apex:pageBlockSection>
                <!-- End Competitor Strategy Section -->
                
                
                
                
                
                <!--  Begin Experian Strategy section -->
                <apex:pageBlockSection id="expStrategy" title="Experian Strategy">
                    <apex:pageblockSection columns="1">
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Strat_1__c}" rendered="{!expStrategy[0]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Strat_2__c}" rendered="{!expStrategy[1]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Strat_3__c}" rendered="{!expStrategy[2]}"/>
                
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Strat_4__c}" rendered="{!expStrategy[3]}"/>
                    
                    <apex:inputField styleClass="inputfield" value="{!Opportunity_Plan_Competitor__c.Exp_Strat_5__c}" rendered="{!expStrategy[4]}"/>
                
                    
                    <apex:commandButton action="{!addEXPStrategy}" style="margin-left:52%" value="Add Another Experian Strategy" status="entryStatus" rerender="expStrategy, oppPlan, errMsg"/>
                    </apex:pageblockSection>
                </apex:pageBlockSection>
                <!-- End Experian Strategy Section -->
                
                
                
            </apex:pageblockSection>
        </apex:pageBlock>
        
    </apex:form>
     
</apex:page>