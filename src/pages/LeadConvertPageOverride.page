<!-- 
/**===================================================================== 
* Experian 
* Name: LeadConvertPageOverride 
* Description: Override of the Lead Convert screen to allow Primary Campaign Attribution 
* Case #580226 
* Created Date: May 20th, 2015 
* Created By: Paul Kissick 
* 
* Date Modified    Modified By           Description of the update 
* Feb 24th, 2016   Paul Kissick          Case 01816977:  Added check to convert the lead if access 
* Jul 18th, 2016   Manoj Gopu(QA)        CRM2.0W-005436: Displaying decision maker checkbox on lead conversion and even if there is no campaign for lead will display decision maker 
* Sep 12th, 2016   Manoj Gopu            CRM2.0W-005586: Commented out the code related to Issue(I266) in order not to display on VF Page.
* Oct 14th, 2016   Manoj Gopu            Case: 01143572 : Moved the pagemessage out of the output panel.
=====================================================================*/ 
--> 
<apex:page standardController="Lead" extensions="LeadConvertPageExtension" title="{!$Label.LeadConvert_Convert_Lead}" action="{!prepareCampaigns}"> 
    <apex:form > 
        <apex:outputPanel rendered="{!allCampaigns.size>0}">
            <apex:sectionHeader title="{!$Label.LeadConvert_Convert_Lead}" subtitle="{!$Label.LeadConvert_SelectPrimaryCampaign}" /> 
        </apex:outputPanel>
        <apex:outputPanel rendered="{!NOT(allCampaigns.size>0)}">
            <apex:sectionHeader title="{!$Label.LeadConvert_Convert_Lead}" /> 
        </apex:outputPanel>
        <apex:pageBlock id="wholePage"> 
            <apex:pageBlockButtons > 
                <apex:commandButton value="{!$Label.Next} >" action="{!save}" rendered="{!canConvert}" /> 
                <apex:commandButton value="{!$Label.Cancel}" action="{!cancel}" /> 
            </apex:pageBlockButtons> 
            <apex:pageBlockSection columns="1" rendered="{!canConvert}" > 
                <apex:pageBlockSectionItem > 
                    <apex:outputLabel value="{!$Label.LeadConvert_DoNotCreateOpp}" /> 
                    <apex:outputPanel > 
                        <apex:actionStatus id="pleaseWait"> 
                            <apex:facet name="start"><img src="/img/loading32.gif" width="16" height="16" alt="Please Wait..." /></apex:facet> 
                            <apex:facet name="stop"> 
                                <apex:inputCheckbox value="{!doNotCreateOpp}"> 
                                    <apex:actionSupport event="onchange" action="{!skipOpp}" rerender="wholePage" status="pleaseWait" /> 
                                </apex:inputCheckbox> 
                            </apex:facet> 
                        </apex:actionStatus> 
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!!doNotCreateOpp}">
                    <apex:outputLabel value="{!$Label.LeadConvert_Contact_Role}"/>
                    <apex:inputField value="{!objLead.Decision_Maker__c}"/><!--Added by Manoj-->
                </apex:pageBlockSectionItem> 
            </apex:pageBlockSection> 
            <apex:pageMessages />            
            <apex:outputPanel rendered="{!allCampaigns.size>0}">                
                <apex:pageBlockTable value="{!allCampaigns}" var="c" rendered="{!canConvert}" > 
                    <apex:column headerValue="Select"> 
                        <apex:actionStatus id="currentCheckbox"> 
                            <apex:facet name="start"><img src="/img/loading32.gif" width="16" height="16" alt="Please Wait..." /></apex:facet> 
                            <apex:facet name="stop"> 
                                   <apex:inputCheckbox value="{!c.selected}" disabled="{!doNotCreateOpp}" > 
                                    <apex:actionSupport event="onchange" action="{!checkTicks}" rerender="wholePage" status="currentCheckbox" /> 
                                </apex:inputCheckbox> 
                            </apex:facet> 
                        </apex:actionStatus> 
                    </apex:column> 
                    <apex:column value="{!c.camp.Name}" /> 
                    <apex:column value="{!c.camp.StartDate}" /> 
                    <apex:column value="{!c.camp.EndDate}" /> 
                    <apex:column value="{!c.camp.Type}" /> 
                    <apex:column value="{!c.camp.Status}" /> 
                </apex:pageBlockTable> 
                <apex:outputPanel rendered="{!canConvert}"> 
                    <apex:actionStatus id="showingMore"> 
                        <apex:facet name="start"><img src="/img/loading32.gif" width="16" height="16" alt="Please Wait..." /></apex:facet> 
                        <apex:facet name="stop"> 
                            <apex:commandLink action="{!showMoreCampaigns}" rerender="wholePage" status="showingMore" >{!$Label.AccountTeamMembersList_Show_More}</apex:commandLink> 
                        </apex:facet> 
                    </apex:actionStatus> 
                </apex:outputPanel> 
                <!-- <apex:pageBlockSection columns="1" rendered="{!canConvert}" > 
                    <apex:pageBlockSectionItem > 
                        <apex:outputLabel value="{!$Label.LeadConvert_DoNotAssignCampaign}" /> 
                        <apex:outputPanel > 
                            <apex:inputCheckbox value="{!doNotAssignCamp}" /> 
                        </apex:outputPanel> 
                    </apex:pageBlockSectionItem> 
                </apex:pageBlockSection> -->
            </apex:outputPanel>
        </apex:pageBlock> 
    </apex:form> 
</apex:page>