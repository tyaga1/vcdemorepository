/**=============================================================================
 * Experian plc
 * Name: ProcessBuilder_Unlock_Project_Record
 * Description: Case# 02308838 Serasa MS Project Process
 * Created Date: May 23rd, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

public class ProcessBuilder_Unlock_Project_Record
{
	@InvocableMethod(label='Unlock project record from Approval process')   
    public static void unlockProjectRecord(List<Project__c> projects)
    {
        for(Project__c prj : projects)
        {
         	if(Approval.isLocked(prj))
            {
                Approval.unlock(prj);
            }
        }        
    }    
}