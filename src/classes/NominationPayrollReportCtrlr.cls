/*=============================================================================
 * Experian
 * Name: NominationPayrollReportCtrlr
 * Description: W-005698: Report generator controller for nominations/rewards
                to send to payroll.
 * Created Date: 16 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 

 =============================================================================*/

public without sharing class NominationPayrollReportCtrlr {
  
  @testVisible private static String defaultLayout = 'Payroll_Report';
  
  public String xlsHeader {
    get {
      String strHeader = '';
      strHeader += '<?xml version="1.0"?>\n';
      strHeader += '<?mso-application progid="Excel.Sheet"?>\n';
      return strHeader;
    }
  }
  
  public String csvLineBreak {
    get {
      return '\n';
    }
  }
  
  public Boolean showNoGross {get{
    if (ApexPages.currentPage().getParameters().containsKey('nogross') && String.isNotBlank(ApexPages.currentPage().getParameters().get('nogross'))) {
      showNoGross = ('yes'.equals(ApexPages.currentPage().getParameters().get('nogross')));
    }
    return showNoGross;
  } set;}
  
  //===========================================================================
  // Return the FieldSet Payroll_Report for use on the page and the query below.
  // Note: Layout can be updated to another bunch of fields.
  //===========================================================================
  public List<Schema.FieldSetMember> reportFields {get{
    if (reportFields == null) {
      String reportLayout = defaultLayout;
      if (ApexPages.currentPage().getParameters().containsKey('layout') && 
          String.isNotBlank(ApexPages.currentPage().getParameters().get('layout')) && 
          SObjectType.Nomination__c.FieldSets.getMap().containsKey(ApexPages.currentPage().getParameters().get('layout'))) {
        reportLayout = ApexPages.currentPage().getParameters().get('layout');
      }
      reportFields = SObjectType.Nomination__c.FieldSets.getMap().get(reportLayout).getFields();
    }
    return reportFields;
  } set;}
  
  
  public String grossAmountColumn {get{
    if (grossAmountColumn == null) {
      String reportLayout = defaultLayout;
      if (ApexPages.currentPage().getParameters().containsKey('layout') && 
          String.isNotBlank(ApexPages.currentPage().getParameters().get('layout')) && 
          SObjectType.Nomination__c.FieldSets.getMap().containsKey(ApexPages.currentPage().getParameters().get('layout'))) {
        reportLayout = ApexPages.currentPage().getParameters().get('layout');
      }
      List<Schema.FieldSetMember> rFields = SObjectType.Nomination__c.FieldSets.getMap().get(reportLayout).getFields();
      Integer colNumber = 0;
      for (Schema.FieldSetMember f : rFields) {
        colNumber++;
        if (f.getFieldPath().equalsIgnoreCase(Nomination__c.Payroll_Gross_Amount__c.getDescribe().getLocalName())) {
          // FOUND IT...
          break;
        }
	    }
	    grossAmountColumn = String.fromCharArray(new Integer[]{colNumber+64});
    }
    return grossAmountColumn;
  } set;}
    
  public NominationPayrollReportCtrlr() {
  }
  
  //===========================================================================
  // Build the unique update URL using the NominationPayrollUpdate page.
  // Must pass the guid page parameter to work.
  //===========================================================================
  public String getUpdateUrl() {
    if (!ApexPages.currentPage().getParameters().containsKey('guid')) {
      return '';
    }
    String guid = (String)ApexPages.currentPage().getParameters().get('guid');
    if (String.isBlank(guid)) {
      return '';
    }
    PageReference pr = Page.NominationPayrollUpdate;
    pr.getParameters().put('guid', guid);
    return URL.getSalesforceBaseUrl().toExternalForm() + pr.getUrl();
  }
  
  //===========================================================================
  // Return the Nomination records from the supplied nomids parameter.
  // Note: Default sort order is by Id field.
  //===========================================================================
  public List<Nomination__c> getNomRecords () {
    //if (!ApexPages.currentPage().getParameters().containsKey('nomids')) {
    //  return new List<Nomination__c>();
    //}
    //String nomIdsJson = (String)ApexPages.currentPage().getParameters().get('nomids');
    //List<Id> suppliedIds = (List<Id>)JSON.deserialize(nomIdsJson, List<Id>.class);
    List<String> whereClauses = new List<String>();
    if (ApexPages.currentPage().getParameters().containsKey('guid')) {
      String guid = (String)ApexPages.currentPage().getParameters().get('guid');
      whereClauses.add(' Payroll_Batch_GUID__c = \''+String.escapeSingleQuotes(guid)+'\' ');
    }
    
    if (ApexPages.currentPage().getParameters().containsKey('payrollname') && ApexPages.currentPage().getParameters().containsKey('beforedate')) {
      String payrollName = (String)ApexPages.currentPage().getParameters().get('payrollname');
      Datetime beforeDate = (Datetime)JSON.deserialize(ApexPages.currentPage().getParameters().get('beforedate'), Datetime.class);
      
      whereClauses.add(' Oracle_Payroll_Name__c = \''+String.escapeSingleQuotes(payrollName)+'\' ');
      whereClauses.add(' Oracle_Payroll_Name__c != null ');
      whereClauses.add(' Payroll_Notified__c = true ');
      whereClauses.add(' Payroll_Notified_Date__c < :beforeDate ');
      whereClauses.add(' Nominee__c != null ');
      whereClauses.add(' Notify_Payroll__c = true ');
      
    }
    
    List<String> qFields = new List<String>();
    for (Schema.FieldSetMember f : reportFields) {
      qFields.add(f.getFieldPath()); 
    }
    
    return (List<Nomination__c>)Database.query(
         ' SELECT ' + String.join(qFields,',') 
       + ' FROM Nomination__c ' 
       + ' WHERE ' + String.join(whereClauses, ' AND ')
       + ' ORDER BY Employee_Number_Integer__c ASC '
    );
    
  }
  
}