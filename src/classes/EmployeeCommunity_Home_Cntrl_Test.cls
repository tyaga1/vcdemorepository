/**=====================================================================
 * Experian
 * Name: EmployeeCommunity_Home_Cntrl_test
 * Description: Test class to cover  EmployeeCommunity_Home_Cntrl
 * Created Date: Oct 14th , 2016
 * Created By: Richard Joseph
 *
 * Date Modified      Modified By                Description of the update
 * ====================================================================*/
@isTest
private class EmployeeCommunity_Home_Cntrl_Test{

private static testMethod void EmployeeCommunityHomeCntrlTest() {
    List<User> usrList = new List<User>();
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    system.runAs(thisUser) {
      Profile p = [SELECT Id FROM PROFILE WHERE Name = :Constants.PROFILE_SYS_ADMIN ];
      UserRole copsRole = [SELECT Id FROM UserRole WHERE Name = :Constants.ROLE_NA_COPS];
      User testUser1 = Test_Utils.createEDQUser(p, 'test1234@experian.com', 'test1');
      User testUser2 = Test_Utils.createEDQUser(p, 'test1235@experian.com', 'test2');
      testUser1.UserRoleId = copsRole.Id;
      testUser2.UserRoleId = copsRole.Id;
      /*testUser1.Oracle_Cost_Center__c = '11111';
      testUser2.Oracle_Cost_Center__c = '11111';*/ // Commented by RJ for EMERG fix
      
      usrList.add(testUser1);
      usrList.add(testUser2);
      insert usrList;
      testUser1.Manager = testUser2;
      update testUser1;
      
      Document documentRec;
      documentRec = new Document();
      documentRec.Body = Blob.valueOf('Some Text');
      documentRec.ContentType = 'image/jpeg';
      documentRec.DeveloperName = 'my_document';
      documentRec.IsPublic = true;
      documentRec.Name = 'My Document';
      documentRec.FolderId = UserInfo.getUserId();
      insert documentRec;
      
      WorkBadgeDefinition  wrkDef=new WorkBadgeDefinition(
      
        Name = 'Half Year Winner', 
        Description = 'Test Description', 
        ImageUrl = documentRec.Id, 
        IsActive = true
      );
   
      insert wrkDef ;
      
      WorkThanks  wrkThanks  = new WorkThanks();
    wrkThanks.GiverId       = UserInfo.getUserId();
    wrkThanks.Message    = 'Test Thanks';
    wrkThanks.OwnerId     = UserInfo.getUserId();
    Insert wrkThanks;
    system.debug('wrkThanks'+wrkThanks);
      
WorkBadge  wrkBadge = new WorkBadge ();
    wrkBadge.DefinitionId     = wrkDef.Id;
    wrkBadge.RecipientId      = UserInfo.getUserId();
    wrkBadge.SourceId         = wrkThanks.Id;
    Insert wrkBadge;
    system.debug('wrkBadge'+wrkBadge);
      
      List<Employee_Community_News__c>  testArticleLst = new List<Employee_Community_News__c> ();
      testArticleLst.add(new Employee_Community_News__c(Detail__c='Test', Headline__c='Test', isActive__c=true, isFeaturedArticle__c=true, Synopsis__c='Test'));
      
      testArticleLst.add(new Employee_Community_News__c(Detail__c='Test', Headline__c='Test', isActive__c=true, isFeaturedArticle__c=false, Synopsis__c='Test'));
      insert testArticleLst ;
      }
      
       Test.startTest();
       list<Employee_Community_News__c> testArticleLst = new list<Employee_Community_News__c>([Select id from Employee_Community_News__c limit 100]);
       EmployeeCommunity_Home_Cntrl  newTestCntrl = new EmployeeCommunity_Home_Cntrl (); 
       newTestCntrl.LoadHomePage();
       EmployeeCommunity_Home_Cntrl.loadThisWeekData();
       EmployeeCommunity_Home_Cntrl.loadThisMonthData();
       EmployeeCommunity_Home_Cntrl.loadThisQuaterData();
       ApexPages.currentPage().getParameters().put('id', testArticleLst[0].id);
       newTestCntrl.ReadActiveNewsDetail();
       newTestCntrl.ReadAllActiveNews();
       newTestCntrl.LoadRecognitionDate();
       newTestCntrl.closePopup();
       newTestCntrl.showPopup();
       
       
       system.debug(newTestCntrl.badgeList);
       system.assert(newTestCntrl.badgeList.size()>0);
       system.assert(newTestCntrl.featured.size()>0) ;
       system.assert(newTestCntrl.articles.size()>0);
       
       
       
        Test.stopTest();
      }
}