/*=============================================================================
 * Experian
 * Name: BatchOpportunityWinProbBuild
 * Description: W-005423 : Builds the Opportunity Win Probability values for any users.
 *              Only adds if the user has closed at least 50 opps.
 *              Schedule to run monthly, at any time really.
 * Created Date: 12 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 12 Jul 2016        Paul Kissick          CRM2:W-005423 - Opportunity Weightings should match owners closing ability
 * 28 Jul 2016        Paul Kissick          CRM2:W-005423 - Adding support to delete any inactive user settings, since these records are now useless
 * 29 Jul 2016        Paul Kissick          CRM2:W-005423 - Fixed miscalc on new split of New from Existing values
 * 30 Aug 2016        Paul Kissick          CRM2:W-005423 - Refactored to allow other 'types'
 =============================================================================*/

public class BatchOpportunityWinProbBuild implements Database.Batchable<sObject>, Database.Stateful {

  private List<String> errorsList;
  
  public Integer oppThreshold; // setting here allows for changes in future. Currently 50. Override by changing in schedule class, or here!
  
  // Hold count of opps reaching each stage.
  public class oppCount {
    public Decimal stage3 {get;set;}
    public Decimal stage4 {get;set;}
    public Decimal stage5 {get;set;}
    public Decimal stage6 {get;set;}
    public Decimal stage7 {get;set;}
    // Methods to return a 'clean' value for the probability of reaching each stage
    public Decimal getStage3Prob() {
      return cleanValue(stage3);
    }
    public Decimal getStage4Prob() {
      return cleanValue(stage4);
    }
    public Decimal getStage5Prob() {
      return cleanValue(stage5);
    }
    public Decimal getStage6Prob() {
      return cleanValue(stage6);
    }
    // Round down to 2 dpm then return prob from 0 to 100 (whole numbers)
    private Decimal cleanValue(Decimal inp) {
      Decimal prob = (inp > 0.0 && stage7 > 0.0) ? stage7.divide(inp, 2, System.RoundingMode.DOWN) : 0.0;
      return (prob > 1.0) ? 100.00 : (prob * 100);
    }
    // Init
    public oppCount() {
      stage3 = stage4 = stage5 = stage6 = stage7 = 0;
    }
  }
  
  // Maps to hold each type of opp counts  
  private Map<Id, oppCount> oppCountNFN; // New from New
  private Map<Id, oppCount> oppCountNFE; // New from Existing
  private Map<Id, oppCount> oppCountRen; // Renewal
  // To add more types, add a new map here.

  // Constructor
  public BatchOpportunityWinProbBuild () {
  }
  
  // Batch start
  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    // Initialise missing oppThreshold as 50, if set to null for some reason.
    if (oppThreshold == null) {
      oppThreshold = 50;
    }
    
    // Init the errorsList 
    errorsList = new List<String>();
    
    // Init the maps to hold the counts for each type of opp
    oppCountNFN = new Map<Id, oppCount>();
    oppCountNFE = new Map<Id, oppCount>();
    oppCountRen = new Map<Id, oppCount>();
    // To add more types, add here a new init
    
    // Return all closed opps, where they primary owner is still active, and was created in SFDC, and is one of the types we are tracking
    return Database.getQueryLocator([
      SELECT Id, OwnerId, Reached_Stage_3__c, Reached_Stage_4__c, Reached_Stage_5__c, Reached_Stage_6__c, Reached_Stage_7__c, Type 
      FROM Opportunity
      WHERE IsClosed = true 
      AND Owner.IsActive = true
      AND Experian_ID__c = null
      AND Type IN (
        :Constants.OPPTY_NEW_FROM_NEW, 
        :Constants.OPPTY_NEW_FROM_EXISTING, 
        :Constants.OPPTY_TYPE_RENEWAL
      )
    ]);
    // To add more types, add to the Type filter above.
  }
  
  //===========================================================================
  // Process all found opps to build the counts
  //===========================================================================
  public void execute (Database.BatchableContext bc, List<Opportunity> scope) {
    
    // Holding var of the current oppCount reference
    oppCount curOppCount;
    
    for (Opportunity o : scope) {
      if (o.Type.equals(Constants.OPPTY_NEW_FROM_NEW)) {
        // New from New, and check ownerId hasn't been added already, and initialise a new class reference.
        if (!oppCountNFN.containsKey(o.OwnerId)) {
         oppCountNFN.put(o.OwnerId, new oppCount());
        }
        curOppCount = oppCountNFN.get(o.OwnerId);
      }
      if (o.Type.equals(Constants.OPPTY_NEW_FROM_EXISTING)) {
        // New from Existing, and check ownerId hasn't been added already, and initialise a new class reference.
        if (!oppCountNFE.containsKey(o.OwnerId)) {
         oppCountNFE.put(o.OwnerId, new oppCount());
        }
        curOppCount = oppCountNFE.get(o.OwnerId);
      }
      if (o.Type.equals(Constants.OPPTY_TYPE_RENEWAL)) {
        // Renewal, and check ownerId hasn't been added already, and initialise a new class reference.
        if (!oppCountRen.containsKey(o.OwnerId)) {
         oppCountRen.put(o.OwnerId, new oppCount());
        }
        curOppCount = oppCountRen.get(o.OwnerId);
      }
      
      // Increment each stage count, if Reached field is true...
      curOppCount.stage3 += (o.Reached_Stage_3__c) ? 1.0 : 0;
      curOppCount.stage4 += (o.Reached_Stage_4__c) ? 1.0 : 0;
      curOppCount.stage5 += (o.Reached_Stage_5__c) ? 1.0 : 0;
      curOppCount.stage6 += (o.Reached_Stage_6__c) ? 1.0 : 0;
      curOppCount.stage7 += (o.Reached_Stage_7__c) ? 1.0 : 0;
      
      // Finally, store the current reference class back into the map
      if (o.Type.equals(Constants.OPPTY_NEW_FROM_NEW)) {
        oppCountNFN.put(o.OwnerId, curOppCount);
      }
      if (o.Type.equals(Constants.OPPTY_NEW_FROM_EXISTING)) {
        oppCountNFE.put(o.OwnerId, curOppCount);
      }
      if (o.Type.equals(Constants.OPPTY_TYPE_RENEWAL)) {
        oppCountRen.put(o.OwnerId, curOppCount);
      }
    }
  }
  
  //===========================================================================
  // For all the counts, insert or update the stored probabilities for all active
  // users, and then remove any inactive ones, to clean up legacy counts.
  //===========================================================================
  public void finish (Database.BatchableContext bc) {
    
    // Lists of the Opp Win Prob records to either insert or update.
    List<Opportunity_Win_Probability__c> toInsertList = new List<Opportunity_Win_Probability__c>();
    List<Opportunity_Win_Probability__c> toUpdateList = new List<Opportunity_Win_Probability__c>();
    
    Opportunity_Win_Probability__c owp;
    
    // Load the other 'type' groupings into a set of all the owner ids
    Set<Id> ownerIdSet = new Set<Id>();
    ownerIdSet.addAll(oppCountNFN.keySet());
    ownerIdSet.addAll(oppCountNFE.keySet());
    ownerIdSet.addAll(oppCountRen.keySet());
    
    for (Id userId : ownerIdSet) {
      
      // If no counts above the threshold are found for the current userId, leave since there is nothing to update or insert.
      if ((!oppCountNFN.containsKey(userId) || (oppCountNFN.containsKey(userId) && oppCountNFN.get(userId).stage3 < oppThreshold)) && 
          (!oppCountNFE.containsKey(userId) || (oppCountNFE.containsKey(userId) && oppCountNFE.get(userId).stage3 < oppThreshold)) &&
          (!oppCountRen.containsKey(userId) || (oppCountRen.containsKey(userId) && oppCountRen.get(userId).stage4 < oppThreshold))) {
        continue;
      }
      
      // Retrieve the current stored values... 
      owp = Opportunity_Win_Probability__c.getValues(userId);
      
      // And if there is none, create one referencing the current userId 
      if (owp == null) {
        owp = new Opportunity_Win_Probability__c(SetupOwnerId = userId);
      }
      
      // For New from New opps, get the probabilities, otherwise set to Null for each stage.
      owp.Stage_3__c = (oppCountNFN.containsKey(userId) && oppCountNFN.get(userId).stage3 >= oppThreshold) ? oppCountNFN.get(userId).getStage3Prob() : null;
      owp.Stage_4__c = (oppCountNFN.containsKey(userId) && oppCountNFN.get(userId).stage3 >= oppThreshold) ? oppCountNFN.get(userId).getStage4Prob() : null;
      owp.Stage_5__c = (oppCountNFN.containsKey(userId) && oppCountNFN.get(userId).stage3 >= oppThreshold) ? oppCountNFN.get(userId).getStage5Prob() : null;
      owp.Stage_6__c = (oppCountNFN.containsKey(userId) && oppCountNFN.get(userId).stage3 >= oppThreshold) ? oppCountNFN.get(userId).getStage6Prob() : null;
      
      // For New from Existing opps, get the probabilities, otherwise set to Null for each stage.
      owp.NFE_Stage_3__c = (oppCountNFE.containsKey(userId) && oppCountNFE.get(userId).stage3 >= oppThreshold) ? oppCountNFE.get(userId).getStage3Prob() : null;
      owp.NFE_Stage_4__c = (oppCountNFE.containsKey(userId) && oppCountNFE.get(userId).stage3 >= oppThreshold) ? oppCountNFE.get(userId).getStage4Prob() : null;
      owp.NFE_Stage_5__c = (oppCountNFE.containsKey(userId) && oppCountNFE.get(userId).stage3 >= oppThreshold) ? oppCountNFE.get(userId).getStage5Prob() : null;
      owp.NFE_Stage_6__c = (oppCountNFE.containsKey(userId) && oppCountNFE.get(userId).stage3 >= oppThreshold) ? oppCountNFE.get(userId).getStage6Prob() : null;
      
      // For Renewal opps, get the probabilities, otherwise set to Null for each stage.
      owp.Renewal_Stage_3__c = (oppCountRen.containsKey(userId) && oppCountRen.get(userId).stage4 >= oppThreshold) ? oppCountRen.get(userId).getStage3Prob() : null;
      owp.Renewal_Stage_4__c = (oppCountRen.containsKey(userId) && oppCountRen.get(userId).stage4 >= oppThreshold) ? oppCountRen.get(userId).getStage4Prob() : null;
      owp.Renewal_Stage_5__c = (oppCountRen.containsKey(userId) && oppCountRen.get(userId).stage4 >= oppThreshold) ? oppCountRen.get(userId).getStage5Prob() : null;
      owp.Renewal_Stage_6__c = (oppCountRen.containsKey(userId) && oppCountRen.get(userId).stage4 >= oppThreshold) ? oppCountRen.get(userId).getStage6Prob() : null;
      
      // If the Id of the OWP record is missing, add it to the list to insert, otherwise add to update.
      if (owp.Id == null) {
        toInsertList.add(owp);
      }
      else {
        toUpdateList.add(owp);
      }
    }
    
    // Then, insert the missing ones... 
    try {
      insert toInsertList;
    }
    catch (DMLException ex) {
      handleDmlException(ex);
    }
    // Update the existing ones....
    try {
      update toUpdateList;
    }
    catch (DMLException ex) {
      handleDmlException(ex);
    }
    
    // and finally, delete anyone who is now inactive - self cleaning!
    try {
      delete [
        SELECT Id 
        FROM Opportunity_Win_Probability__c 
        WHERE SetupOwner.IsActive = false
        AND SetupOwnerId IN (
          SELECT Id
          FROM User
          WHERE Date_Deactivated__c < LAST_N_DAYS:90
          AND IsActive = false
        )
      ];
      // Added 3 month deactivation window, to catch any over the 45 auto deactivation limit
    }
    catch (DMLException ex) {
      handleDmlException(ex);
    }
    
    // At the end, if there are any errors, send the email.
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchOpportunityWinProbBuild', false);
    
    if (!errorsList.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += String.join(errorsList, '\n');
    }
    
    bh.sendEmail();
  }
  
  // method to handle all dml exceptions, since it's duplicated many times.
  private void handleDmlException(DMLException ex) {
    apexLogHandler.createLogAndSave('BatchOpportunityWinProbBuild','finish', ex.getStackTraceString(), ex);
    errorsList.add(ex.getMessage());
    errorsList.add(ex.getStackTraceString());
  }
  
}