/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ApiDocumentRepository {
    global ApiDocumentRepository() {

    }
    webService static String Download(Integer documentId) {
        return null;
    }
    webService static String GetProperties(Integer documentId) {
        return null;
    }
}
