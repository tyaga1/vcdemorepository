/*=============================================================================
 * Experian
 * Name: ScheduleBatchContactRelationshipBULead
 * Description: Schedules the BatchContactRelationshipBULead process
 * Created Date: 2016-07-27
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 * 27 Jul 2016(QA)     Diego Olarte         CRM2:W-005406: Contact relationship BU Lead
 =============================================================================*/

global class ScheduleBatchContactRelationshipBULead implements Schedulable {

  global void execute(SchedulableContext sc) {
    
    Database.executeBatch(new BatchContactRelationshipBULead(), ScopeSizeUtility.getScopeSizeForClass('BatchContactRelationshipBULead')); 

  }
  
}