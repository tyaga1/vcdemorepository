/**=====================================================================
 * Experian
 * Name: SMXNACSClientSupportProcessCaseBatch 
 * Description: Finds cases related to CSDA support and creates survey nomination records
 * Created Date: 15/01/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 * Jan 26, 2016      Paul Kissick       Removed check for experian.com emails on contacts.
                                        Also found problem with search criteria
 * Apr 4, 2016       Paul Kissick       Case 01925676: Readded check for experian.com emails on contacts.
 =====================================================================*/

global class SMXNACSClientSupportProcessCaseBatch implements Database.Batchable<Case> {

  global static String strSurveyName = 'NA CS Client Support';
  global static String strSurveyId = 'EXPERIAN_108224';

  global Iterable<Case> start(database.batchablecontext BC) {
    
    return [
      SELECT Id, ContactId, Contact.Email, (SELECT Id FROM Survey__r)
      FROM Case 
      WHERE IsClosed = true 
      AND ClosedDate > YESTERDAY
      AND Status != 'Closed - Duplicate'
      AND Case_Owned_by_Queue__c = false
      AND Requester_Region__c = 'North America'
      AND Origin IN ('Email','Email - CIS Billing','Email - CIS Online','Email - CIS Partner','Email - CIS PS','Email - CIS Reseller','Email - CIS Sales','Email-CIS Sales','Phone','Web')
      AND Type NOT IN ('Consumer','Full Cancellation','Item Cancellation','Sales Support Internal')
      AND RecordTypeId IN (
        SELECT Id 
        FROM RecordType 
        WHERE Name IN ('CSDA CIS Support')
      ) 
      AND ContactId != null 
      AND ContactId != ''
    ];
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
 
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    
    Long lgSeed = System.currentTimeMillis();

    for(Case cs : scope){
      
      List<Feedback__c> lstSurvey = cs.Survey__r;

      if (lstSurvey.isEmpty() && String.isNotBlank(cs.Contact.Email) && !cs.Contact.Email.endsWithIgnoreCase('experian.com')) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Case__c = cs.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = cs.ContactId; //ContactName 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedbackList.add(feedback);        
      }
    }
    insert feedbackList;
  }

  global void finish(Database.BatchableContext info){
  }
    
  
}