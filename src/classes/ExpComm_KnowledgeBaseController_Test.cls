@isTest
private class ExpComm_KnowledgeBaseController_Test {
    
    @isTest static void test_page_initailization() {
        articlesSetup();
        ExpComm_ITServiceKnowledgePageController ctrl = new ExpComm_ITServiceKnowledgePageController();
    }
    
    @isTest
    private static void articlesSetup() {
        Test.startTest();
        ExpComm_ITServiceKnowledgePageController ctrl = new ExpComm_ITServiceKnowledgePageController();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;
            
            Test.setCurrentPageReference(new PageReference('ExpComm_IT_Service_Knowledge?u='));
            System.currentPageReference().getParameters().put('u', '5');
            
            Employee_Article__kav newEmpArti = new Employee_Article__kav (
                Title = 'testing article',
                UrlName = 'testing',
                Language = 'en_US'
            );

            insert newEmpArti;
            newEmpArti = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            String articleId = newEmpArti.KnowledgeArticleId;
            KbManagement.PublishingService.publishArticle(articleId, true);

            Employee_Article__DataCategorySelection artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;

            newEmpArti = new Employee_Article__kav (
                Title = 'testing article2',
                UrlName = 'testing2',
                Language = 'en_US'
            );

            insert newEmpArti;
            newEmpArti = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            articleId = newEmpArti.KnowledgeArticleId;
            KbManagement.PublishingService.publishArticle(articleId, true);

            artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;
            
            ctrl.displayItServices();
            ctrl.displayFac_Security();
            ctrl.displayBusinessResources();
            ctrl.displayHr();
            ctrl.displayFinance();
            ctrl.populateFinance();
            ctrl.populateHR();
            ctrl.populateFac_Security();
            ctrl.populateBusinessResources();
            
        }
    }
    
}