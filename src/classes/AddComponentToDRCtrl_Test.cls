/**=====================================================================
 * Experian
 * Name: AddComponentToDRCtrl_Test
 * Description: Case 01802324 - Adding test for AddComponentToDRCtrl
 * Created Date: 22 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Feb. 1st 2016                James Wills                  Case #01266714 Changed constant from Constants.CASE_REC_TYPE_CRM_REQUEST to Constants.CASE_REC_TYPE_SFDC_SUPPORT
 * Marc 8th 2016                Sadar Yacob                  Added support for Agile Accelerator Story
 * Apr 7th, 2016                Paul Kissick                 Case 01932085: Fixing Test User Email Domain 
 =====================================================================*/
@isTest
private class AddComponentToDRCtrl_Test {
  
  static String testUserEmail = 'test1234joiweghskdhg@experian.com';

  static testMethod void testNonProd() {
    
    Case c1 = [SELECT Id, Implementation_Status__c FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];
    // c1.Implementation_Status__c = AddComponentToDRCtrl.
    
    PageReference pr = Page.AddComponentToDRPage;
    Test.setCurrentPage(pr);
    ApexPages.currentPage().getParameters().put('Id',c1.Id);
    
    AddComponentToDRCtrl ac = new AddComponentToDRCtrl();
    ac.selectNone();
    ac.selectAll();
    ac.refresh();
    
    system.assertEquals(2, ac.allCaseComponents.size());
    system.assertEquals(1, ac.allDeploymentRequests.size());
    
    Deployment_Request__c drUat = [SELECT Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 1'];
    // ac.selectedDeploymentRequest = drUar.Id;
    system.assertEquals(null, ac.addToDeploymentRequest());
    // should return a null
    ac.selectedDeploymentRequest = drUat.Id;
    system.assertNotEquals(null, ac.addToDeploymentRequest());
    
    system.assertEquals(2, [SELECT COUNT() FROM Deployment_Component__c WHERE Slot__c = :drUat.Id]);
    
  }
  
  
  static testMethod void testProdRel() {
    
    Case c1 = [SELECT Id, Implementation_Status__c FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];
    c1.Implementation_Status__c = AddComponentToDRCtrl.UAT_SIGNED_OFF;
    update c1;
    
    PageReference pr = Page.AddComponentToDRPage;
    Test.setCurrentPage(pr);
    ApexPages.currentPage().getParameters().put('Id',c1.Id);
    
    AddComponentToDRCtrl ac = new AddComponentToDRCtrl();
    
    system.assertEquals(2, ac.allCaseComponents.size());
    system.assertEquals(2, ac.allDeploymentRequests.size());
    
    Deployment_Request__c drProd = [SELECT Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 2'];
    // ac.selectedDeploymentRequest = drUar.Id;
    system.assertEquals(null, ac.addToDeploymentRequest());
    ac.selectAll();
    // should return a null
    ac.selectedDeploymentRequest = drProd.Id;
    system.assertNotEquals(null, ac.addToDeploymentRequest());
    
    system.assertEquals(2, [SELECT COUNT() FROM Deployment_Component__c WHERE Slot__c = :drProd.Id]);
    
    system.assertEquals(AddComponentToDRCtrl.READY_FOR_PROD, [SELECT Id, Implementation_Status__c FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1].Implementation_Status__c);
    
  }
  
   static testMethod void testNonProdAgileStory() {
    
    agf__ADM_Work__c as1 = [SELECT Id, agf__Status__c FROM agf__ADM_Work__c WHERE agf__Subject__c  = 'Test Agile Story' LIMIT 1];
    // c1.Implementation_Status__c = AddComponentToDRCtrl.
    
    PageReference pr = Page.AddComponentToDRPage;
    Test.setCurrentPage(pr);
    ApexPages.currentPage().getParameters().put('Id',as1.Id);
    
    AddComponentToDRCtrl ac = new AddComponentToDRCtrl();
    ac.selectNone();
    ac.selectAll();
    ac.refresh();
    
    system.assertEquals(2, ac.allCaseComponents.size());
    system.assertEquals(1, ac.allDeploymentRequests.size());
    
    Deployment_Request__c drUat = [SELECT Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 1'];
    // ac.selectedDeploymentRequest = drUar.Id;
    system.assertEquals(null, ac.addToDeploymentRequest());
    // should return a null
    ac.selectedDeploymentRequest = drUat.Id;
    system.assertNotEquals(null, ac.addToDeploymentRequest());
    
    system.assertEquals(2, [SELECT COUNT() FROM Deployment_Component__c WHERE Slot__c = :drUat.Id]);
    
  }
  
  
  
  @testSetup
  static void setupTestData() {
    
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN LIMIT 1];
    User testUser1 = Test_Utils.createUser(p,testUserEmail,'test1');
    insert testUser1;
    
    // Need to Query permission set name 'Agile Accelerator Admin' to let the test class pass.
    system.runAs(new User(Id = UserInfo.getUserId())) {
      User tstUser = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
      PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Agile_Accelerator_Admin'];
    
      // Assign the above inserted user for the Agile Accelerator Admin Permission Set.
      PermissionSetAssignment psa = new PermissionSetAssignment();
      psa.AssigneeId =testUser1.id; // tstUser.Id;
      psa.PermissionSetId = ps.Id;
      insert psa;
    
    }

    system.runAs(testUser1) {         
      IsDataAdmin__c isDataAdmin = Test_Utils.insertIsDataAdmin(true);
    
      //create Account
      Account acc = Test_Utils.insertAccount();
      String crmRecordType = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.CASE_REC_TYPE_SFDC_SUPPORT);//Case #01266714 James Wills
    
      //Create Case
      Case RMACase = new Case();
      RMACase.RecordTypeId = crmRecordType;
      RMACase.Status = Constants.CASE_STATUS_IN_PROGRESS;
      RMACase.Implementation_Status__c = Constants.CASE_STATUS_IN_PROGRESS;
      RMACase.Type = Constants.CASE_TYPE_INCIDENT;
      RMACase.Account__c = acc.Id;
      RMACase.Subject = 'Test Case Subject';
      insert RMACase;
    
      // START OF TEST COVERAGE FOR AGILE ACCELERATOR CHANGES
      
      //Create Scrum Team
      agf__ADM_Scrum_Team__c newTeam  = new agf__ADM_Scrum_Team__c(Name = 'Test Team',agf__Active__c=true,agf__Cloud__c ='IT');
      insert newTeam;
    
      //Create Agile Product Tag
      agf__ADM_Product_Tag__c  newProductTag = new agf__ADM_Product_Tag__c(Name = 'GCSS Product Team',agf__Team__c =newTeam.Id,agf__Team_Tag_Key__c ='123', agf__Active__c=true );
      insert newProductTag;
      
      Id userStoryTypeId = DescribeUtility.getRecordTypeIdByName('agf__ADM_Work__c', 'User Story'); 
      //Create Agile Story
      agf__ADM_Work__c agileStory = new agf__ADM_Work__c();
      agileStory.agf__Assignee__c = testUser1.Id;
      agileStory.agf__Subject__c = 'Test Agile Story';
      agileStory.agf__Status__c = 'New';
      agileStory.RecordTypeId = userStoryTypeId;
      agileStory.agf__Description__c = 'Agile Description goes here';
      agileStory.agf__Product_Tag__c = newProductTag.Id;
      insert agileStory;
    
      //create metadata components
      Metadata_Component__c metaDataCom1 = new Metadata_Component__c(
        Component_API_Name__c = 'mc1_is_apexclass',
        Component_Type__c = 'ApexClass'
      );
      
      Metadata_Component__c metaDataCom2 = new Metadata_Component__c(
        Component_API_Name__c = 'mc2_is_apexclass',
        Component_Type__c = 'ApexClass'
      );
      
      insert new Metadata_Component__c[]{metaDataCom1,metaDataCom2};
      
      Case_Component__c cc1 = Test_Utils.createCaseComponent(RMACase.Id, metaDataCom1.Id);
      Case_Component__c cc2 = Test_Utils.createCaseComponent(RMACase.Id, metaDataCom2.Id);
      insert new List<Case_Component__c>{cc1,cc2};
      
      cc1.User_Story_AA__c = agileStory.id;
      cc2.User_Story_AA__c = agileStory.id;
      
      update cc1;
      update cc2;
      
      Salesforce_Environment__c src = new Salesforce_Environment__c(Name = 'Source', Platform__c = 'Salesforce');
      Salesforce_Environment__c trg = new Salesforce_Environment__c(Name = 'Target', Platform__c = 'Salesforce');
      insert new Salesforce_Environment__c[]{src,trg};
      
      Deployment_Request__c drUat = new Deployment_Request__c(
        Deployment_Date__c = Date.today(),
        Source__c = src.Id,
        Target__c = trg.Id,
        Release__c = 'Support Release 1',
        Deployment_Lead__c = UserInfo.getUserId(),
        Is_Production__c = false,
        Status__c = AddComponentToDRCtrl.NOT_STARTED
      );
      
      Deployment_Request__c drProd = new Deployment_Request__c(
        Deployment_Date__c = Date.today(),
        Source__c = src.Id,
        Target__c = trg.Id,
        Release__c = 'Support Release 2',
        Deployment_Lead__c = UserInfo.getUserId(),
        Is_Production__c = true,
        Release_Type__c = 'Standard',
        Status__c = AddComponentToDRCtrl.NOT_STARTED
      );
      
      insert new List<Deployment_Request__c>{drUat,drProd};
      
      delete isDataAdmin;
      
    }
  }
}