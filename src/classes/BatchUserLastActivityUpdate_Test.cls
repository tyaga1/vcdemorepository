/*======================================================================================
 * Experian Plc.
 * Name: BatchUserLastActivityUpdate_Test
 * Description: Test Class for BatchUserLastActivityUpdate
 * Created Date: July 30th, 2014
 * Created By: James Weatherall
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 4th, 2016                Paul Kissick                 Case 01927884: Added Improvements
 * Nov. 10th, 2016              James Wills                  Case 02108241: Updated Test Class to include test user who has never logged in
 * jun21st 2017                 manoj Gopu                   Fixed test class failure as a part of June 22nd Release
 =======================================================================================*/

@isTest
private class BatchUserLastActivityUpdate_Test {
  
  static string testUserEmail = 'asfkljhowhjrefoi987@experian.com';

  static testMethod void testBatch() {
  
    User testUserforInsert1 = Test_Utils.createUser('Standard User');
    testUserforInsert1.Email = testUserEmail;
    testUserforInsert1.Days_since_Activity__c = 1;
    insert testUserforInsert1;
    
    User testUserforInsert2 = Test_Utils.createUser('Standard User');   
    testUserforInsert2.Email = testUserEmail;
    insert testUserforInsert2;

    List<User> listTestUsers = [SELECT Id, Days_since_Activity__c FROM User WHERE Email = :testUserEmail];

    system.assert(listTestUsers[0].Days_since_Activity__c > 0);
        
    system.assert(listTestUsers[1].Days_since_Activity__c == null);

    Test.startTest();
    
    BatchUserLastActivityUpdate b = new BatchUserLastActivityUpdate();
    b.testUsers = listTestUsers;
    Database.executeBatch(b, 100);
    Test.stopTest();
    

    for(User userAssertAfter : [SELECT Id, Days_since_Activity__c FROM User where Id IN :listTestUsers]) {
      system.assert(userAssertAfter.Days_since_Activity__c == 0);
    }
    
  }
  
  
  static testMethod void testScheduler() {
    
    Test.startTest();
    
    List<User> listTestUsers = [SELECT Id, Days_since_Activity__c FROM User WHERE Email = :testUserEmail];
    
    ScheduleUserLastActivityUpdate.testUsers = listTestUsers;
    
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleUserLastActivityUpdate '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleUserLastActivityUpdate());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    test.stopTest();

    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  }
  
  @testSetup
  static void setupData() {
  
    User testUserforInsert1 = Test_Utils.createUser('Standard User');
    testUserforInsert1.Email = testUserEmail;
    testUserforInsert1.Days_since_Activity__c = 1;
    insert testUserforInsert1;
    
    User testUserforInsert2 = Test_Utils.createUser('Standard User');
    testUserforInsert2.Email = testUserEmail;
    insert testUserforInsert2;
  }
  
}