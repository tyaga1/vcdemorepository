/******************************************************************************
 * Name: expcommUserAssignPermission .cls
 * Created Date: 2/24/2017
 * Created By: Hay Win, UCInnovation
 * Description : Invocable method to use from Process Builder: User License Change 
 *                 to assing PermissionSetLicense for
 *                 active User with Salesforce Platform License
 * Change Log- 
 * Modified Date        Modified By            Comments
 * May 12th, 2017       Charlie Park           Just add Permission set to the new user, then the license will be created based on the permission set's license lookup

 ****************************************************************************/


public class expcommUserAssignPermission {

  @InvocableMethod(label='Assign permission' description='Assign or Unassigned permission set license assignment')  
  
  public static void assignPermissionLicense(List<String> userID) {
  
  Set<String> userSet = new Set<String>();
  //List<PermissionSetLicenseAssign> assignmentList = new List<PermissionSetLicenseAssign>();
  List<PermissionSetAssignment> permAssignmentList = new List<PermissionSetAssignment>();
  String permissionSetId;
  userSet.addAll(userID);
  
     
     //if user is in the permission set
     //remove from userSet
    for(PermissionSetAssignment assignment: [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSet.Label = 'Global Experian Employee Case Access' and AssigneeId IN: userID]) {      
         userSet.remove(assignment.AssigneeId);
    }
    
    if (permissionSetId == null ) {
        List<PermissionSet> permissions = [SELECT Id FROM PermissionSet WHERE Label = 'Global Experian Employee Case Access'];
        if (permissions.size() > 0)
        {
            permissionSetId = permissions[0].ID;
        }
        
    }
    
    // if it's not null
    if (permissionSetId != null ) {
    
        if (userSet.size() > 0) {
        
            for(String assignee: userSet) {
                PermissionSetAssignment permissionAssgn = new PermissionSetAssignment(AssigneeId = assignee, PermissionSetId = permissionSetId );
                permAssignmentList.add(permissionAssgn);
            }
        }
    
    }
    
    if (permAssignmentList.size() > 0) {
        insert permAssignmentList;
    }
    
    
  }
 
}