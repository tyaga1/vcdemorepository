/**=====================================================================
 * Name: CommunitynewCaseCntlr
 * Description: Create Community Case controller
 *
 * Created Date: ????
 * Created By: Richard Joseph
 *
 * Date Modified            Modified By              Description of the update
 * Oct 19th, 2016           Paul Kissick             Added header, formatted, and tested Assignment Rule Application
 ======================================================================*/
public with sharing class CommunitynewCaseCntlr {

  public List<SelectOption> accountAssetList {get{
    if (accountAssetList == null) {
      accountAssetList = new List<SelectOption>();
      for (Asset accAsset : [SELECT AccountId, Id, Name 
                             FROM Asset
                             WHERE Status__c = 'Live'
                             AND AccountId IN (
                               SELECT AccountId 
                               FROM User
                               WHERE Id = :UserInfo.getUserId())]) {
        accountAssetList.add(new SelectOption(accAsset.id, accAsset.Name));
      }
    }
    return accountAssetList;
  } set;}
  
  private Id getAccountEntitlement() {
    Id entId;
    for (Entitlement ent : [SELECT Id FROM Entitlement
                            WHERE AccountId IN (
                              SELECT AccountId 
                              FROM User 
                              WHERE Id = :UserInfo.getUserId()
                            )
                            AND AssetId = null
                            AND Status = 'Active'
                            ORDER BY CreatedDate DESC
                            LIMIT 1]) {
      entId = ent.Id;
    }
    return entId;
  }
  
  public String selectedAsset {get; set;}
  public Case newCommunitySupportCase {get; set;}
  
  public CommunitynewCaseCntlr() {
    newCommunitySupportCase = new Case();
  }
  
  public PageReference confirmSave() {
    newCommunitySupportCase.AssetId = (String.isNotBlank(selectedAsset) ? (Id)selectedAsset : null);
    newCommunitySupportCase.Status = 'New';
    newCommunitySupportCase.Origin = 'Global Community';
    newCommunitySupportCase.EntitlementId = getAccountEntitlement();
    
    Database.DMLOptions dmo = new Database.DMLOptions();
    dmo.AssignmentRuleHeader.useDefaultRule = true; // Make assignment rules fire!
    dmo.EmailHeader.triggerAutoResponseEmail = true; // Make auto response emails send!
    
    Database.insert(newCommunitySupportCase, dmo);
    
    //PageReference pageRef = new PageReference('{!$Site.Prefix}/apex/CommunityCaseDetailPage?id='+newCommunitySupportCase.id);
    PageReference pageRef = Page.CommunityCaseDetailPage;
    pageRef.getParameters().put('id', newCommunitySupportCase.Id);
    pageRef.setRedirect(true);

    return pageRef;
  }
}