/**
* @description A server-side controller for featured User Agreement Lightning Component
* Its main usage is to get/set the user's UserAgreement flag. 
*
* @author Alvin Pico, UC Innovation
*
* @date 06/23/2017
*/
public with sharing class Reseller_Homepage_Popup_Controller {
	
	@AuraEnabled
	public static Boolean getUserAgreementAlertFlag() {
		Id currentUserId = UserInfo.getUserId();
		User currentUser = [SELECT Id, UserAgreementAlert__c FROM User WHERE Id =: currentUserId];
		
		return currentUser.UserAgreementAlert__c;
	}

	@AuraEnabled
	public static void setUserAgreementAlertFlag(Boolean value) {
		try {
			Id currentUserId = UserInfo.getUserId();
			User currentUser = [SELECT Id, UserAgreementAlert__c FROM User WHERE Id =: currentUserId];
			currentUser.UserAgreementAlert__c = value;
			
			update currentUser;
		}
		catch (exception e) {
			System.debug('Exception thrown in setUserAgreementAlertFlag: ' + e.getMessage());	
		}
	}
}