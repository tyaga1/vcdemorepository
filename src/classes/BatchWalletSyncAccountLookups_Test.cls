/**=====================================================================
 * Experian
 * Name: BatchWalletSyncAccountLookups_Test
 * Description: 
 * Created Date: 20 Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Oct 1st, 2015      Paul Kissick               Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class BatchWalletSyncAccountLookups_Test {

  static testMethod void testAccountLookups() {
    
    List<WalletSync__c> wsObjs = new List<WalletSync__c>();
    
    List<Account> allAccs = [SELECT Id, CNPJ_Number__c FROM Account];
    
    for(Account a : allAccs) {
      wsObjs.add(new WalletSync__c(
        CNPJ_Number__c = a.CNPJ_Number__c, 
        Last_Processed_Date__c = Datetime.now().addHours(-1),
        LegacyCRM_Account_ID__c = 'CL'+a.CNPJ_Number__c
      ));
    }
    
    insert wsObjs;
    
    system.assertEquals(0,[SELECT COUNT() FROM WalletSync__c WHERE Account__c != null]);
    
    Test.startTest();
    
    BatchWalletSyncAccountLookups b = new BatchWalletSyncAccountLookups();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(5,[SELECT COUNT() FROM WalletSync__c WHERE Account__c != null]);
    
  }

  @testSetup
  static void setupTestData() {
    
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    List<User> tstUsrs = Test_Utils.createUsers(p,'test@experian.com','ZWQQ',1);
    insert tstUsrs;
    
    List<Account> newAccs = new List<Account>();
    User u = [SELECT Id FROM User WHERE Id = :tstUsrs[0].Id LIMIT 1];
    system.runAs(u) {
      Account acc1 = Test_utils.createAccount();
      acc1.CNPJ_Number__c = '62307946000152';
      newAccs.add(acc1);
      Account acc2 = Test_utils.createAccount();
      acc2.CNPJ_Number__c = '17240483000103';
      newAccs.add(acc2);
      Account acc3 = Test_utils.createAccount();
      acc3.CNPJ_Number__c = '43161803000130';
      newAccs.add(acc3);
      Account acc4 = Test_utils.createAccount();
      acc4.CNPJ_Number__c = '34895621000101';
      newAccs.add(acc4);
      Account acc5 = Test_utils.createAccount();
      acc5.CNPJ_Number__c = '18068852000186';
      newAccs.add(acc5);
      insert newAccs;
    }
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
  }
}