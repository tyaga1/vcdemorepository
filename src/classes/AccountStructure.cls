/**  Copyright (c) 2008, Matthew Friend, Sales Engineering, Salesforce.com Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
*  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
*  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
*  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**=====================================================================
* To adapt this to another Object simply search for "Change" to go to the places 
* where the sObject and query must be changed
* 
* Installed by: Nathalie Le Guay (Appirio)
* Installed Date: March 04, 2014
* 
* Date Modified         Modified By                 Description of the update
* March 07, 2014        Nathalie Le Guay            Added details about related Address
* Apr 24, 2014          Nathalie Le Guay            Replacing reference to Account.Type__c to Account.Account_Type__c
* Apr 24, 2016          Cristian Torres (UC In)     Enhanced to implement option to display truncated hierarchy for
*                                                   improved performance
* Oct 18th,2016         Manoj Gopu                  Getting the Active Contracts for each Account and displaying the count on Page.
=====================================================================*/
public with sharing class AccountStructure{

    //Declare variables
    public String currentId;
    public List<ObjectStructureMap> asm ;
    public Map<String, ObjectStructureMap> masm;
    public List<Integer> maxLevel;
    private Integer level               = 0;
    private List<ID> currentParent      = new List<ID>{};

    //By Cristian
    private Integer totalLevels = 0;
    private String ultimateAccountID;
    private Boolean flagLink;

    //passed from parameter to show Full structure no matter what if value is 1
    private Integer flagFullHierarchy;
    //if the currentID is an ultimate parent then we will show the children of the parent 
    private String childIDUltimate;

    public Boolean showFullStructure { get; set; }
    private Decimal maxSizeHierarchy;
    //Ends By Cristian
    public map<string,Integer> mapActiveContracts{get;set;}//Used to store the Active contracts count for each account

    /**
    * Contructor
    */
     public AccountStructure(ApexPages.StandardController controller) {
        //Starts by Cristian
        
        childIDUltimate = '';

        //CustomSettingName__c mc = CustomSettingName__c.getValues(data_set_name)
        String auxFlagFullHierarchy = ApexPages.currentPage().getParameters().get('flagFullHierarchy');
        if (auxFlagFullHierarchy != null)
        {
            flagFullHierarchy = Integer.valueOf(auxFlagFullHierarchy);
        }
        //Get max size value from custom setting
        //Account_Hierarchy_Settings__c hierarchySet = Account_Hierarchy_Settings__c.getValues('AccountHierarchy');
        //maxSizeHierarchy = hierarchySet.Max_Size_Tree__c;
        maxSizeHierarchy = Global_Settings__c.getValues('Global').Account_Hierarchy_Size__c;
        
        //Decimal testgdgdf= hierarchySet.Max_Size_Tree__c;
        //Ends by Cristian
        this.asm = new List<ObjectStructureMap>{};
        this.masm = new Map<String, ObjectStructureMap>{};
        this.maxLevel = new List<Integer>{};

        currentId = System.currentPageReference().getParameters().get( 'id' );
        currentParent.add( GetTopElement( currentId ) );
        checkCountofChildren();
         
        System.debug('Value of showFullStructure ' + showFullStructure);
    }

    
    /**
     * Contructor
     */
    public AccountStructure() {
        //Starts by Cristian
        
        childIDUltimate = '';
        
        String auxFlagFullHierarchy = ApexPages.currentPage().getParameters().get('flagFullHierarchy');
        if (auxFlagFullHierarchy != null)
        {
            flagFullHierarchy = Integer.valueOf(auxFlagFullHierarchy);
        }
        //Get max size value from custom setting
        //Account_Hierarchy_Settings__c hierarchySet = Account_Hierarchy_Settings__c.getValues('AccountHierarchy');
        //maxSizeHierarchy = hierarchySet.Max_Size_Tree__c;
        maxSizeHierarchy = Global_Settings__c.getValues('Global').Account_Hierarchy_Size__c;
        
        //Ends by Cristian
        this.asm = new List<ObjectStructureMap>{};
        this.masm = new Map<String, ObjectStructureMap>{};
        this.maxLevel = new List<Integer>{};

        currentId = System.currentPageReference().getParameters().get( 'id' );
        currentParent.add( GetTopElement( currentId ) );
        checkCountofChildren();
    }
    
    
    /* 
     * Cristian Torres
     * checks how many children the ultimate account has in total
    */
    public void checkCountofChildren()
    {
        //ultimateAccountID contains the ID of the Ultimate Parent for the account 
        String query;

        Integer childrenCount = 0;

        query = 'SELECT count() FROM Account WHERE Ultimate_Parent_Account_ID__c =: ultimateAccountID';
        childrenCount = database.countQuery(query);
        
        //ultimate account is in a 18 ID format and currentId is in a 15 ID format
        System.debug('Current ID: ' + currentId + ' Ultimate Account ' +  ultimateAccountID);
        System.debug('Total of children from Account: ' + ultimateAccountID + ' is: ' + childrenCount);
        
        
        if (childrenCount < maxSizeHierarchy)
        { 
            showFullStructure = true;
        
        } else {
            showFullStructure = false;
        }
          /*     
        //we use this from a parameter in the URL to force either view 
        //the full Hierarchy (1) or the Abbreviated Version (0)
        if ( flagFullHierarchy == 1 || ultimateAccountID.left(15) == currentId)
        {
            showFullStructure = true;
        } if ( flagFullHierarchy == 0 )
        {
            showFullStructure = false;
        }*/
        /*NEW EDIT BEINGS*/
        //we use this from a parameter in the URL to force either view 
        //the full Hierarchy (1) or the Abbreviated Version (0)
        if ( flagFullHierarchy == 1)
        {
            showFullStructure = true;
        } if ( flagFullHierarchy == 0 )
        {
            showFullStructure = false;
        }
        
        
        if (ultimateAccountID.left(15) == currentId && childrenCount > maxSizeHierarchy )
        { 
            Account acc;
                 
            try
            {
                acc = 
                [
                    SELECT ID 
                    FROM Account 
                    WHERE parentID =: currentID LIMIT 1
                ];
                
            } catch(Exception E)
            {
                System.debug('ERROR in the Account Query -- ' + E);
            }
            
            //currentID = acc.ID;\
            childIDUltimate = acc.ID;
            totalLevels = 2;
                
        }
        /*NEW EDIT ENDS*/
    }

    
    
    /**
    * Allow page to set the current ID
    */
    public void setcurrentId( String cid ){
        currentId = cid;
    }

    /**
    * Return ObjectStructureMap to page
    * @return asm
    */
    public List<ObjectStructureMap> getObjectStructure(){
        asm.clear();
        if ( currentId == null ) {
            currentId = System.currentPageReference().getParameters().get( 'id' );
        }
        
        System.assertNotEquals( currentId, null, 'sObject ID must be provided' );
        asm = formatObjectStructure( CurrentId );
        
        return asm;
    }



    /**
    * Query Account from top down to build the ObjectStructureMap
    * @param currentId
    * @return asm
    */
    public ObjectStructureMap[] formatObjectStructure( String currentId ){
    
        List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
        masm.clear();

        //Change below
        List<Account> al            = new List<Account>{};
        //List<ID> currentParent      = new List<ID>{};
        Map<ID, String> nodeList    = new Map<ID, String>{};
        List<String> nodeSortList   = new List<String>{};
        List<Boolean> levelFlag     = new List<Boolean>{};
        List<Boolean> closeFlag     = new List<Boolean>{};
        String nodeId               = '0';
        String nodeType             = 'child';
        Integer count               = 0;
       // Integer level               = 0;
        Boolean endOfStructure      = false;
        
        mapActiveContracts = new map<string,Integer>();//Added by Manoj           
        //by specific requirement if the account has more than a certain number of accounts then 
        //we will show a shorter version of the tree. the number is stored in a custom setting
        if (showFullStructure == true)
        {
            System.debug('Showing FULL STRUCTURE');
            //Loop though all children
            while ( !endOfStructure )
            {

                if( level == 0 )
                {
                    //Change below     
                    al = [ SELECT a.Account_Type__c, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id, a.Region__c,
                                  (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                                   FROM Account_Address__r
                                   WHERE Address_Type__c = 'Registered' LIMIT 1
                                   )
                           FROM Account a
                           WHERE a.id IN : CurrentParent ORDER BY a.Name 
                           ];

                } 
                else {
                    //Change below      
                    al = [ SELECT a.Account_Type__c, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id, a.Region__c,
                                  (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                                   FROM Account_Address__r
                                   WHERE Address_Type__c = 'Registered' LIMIT 1
                                   )
                           FROM Account a
                           WHERE a.ParentID IN : CurrentParent ORDER BY a.Name 
                         ];

                }
                
                
                if ( al.size() == 0 )
                {
                    endOfStructure = true;
                }
                else{
                    
                    currentParent.clear();
                    
                    for ( Integer i = 0 ; i < al.size(); i++ )
                    {
                        //Change below
                        Account a = al[i];                                            
                        nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.'+String.valueOf( i ) : String.valueOf( i );
                        masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, a ) );
                        currentParent.add( a.id );
                        nodeList.put( a.id,nodeId );
                        nodeSortList.add( nodeId );
                    }
                    
                    maxLevel.add( level );                
                    level++;
                }
            }       

        }
        
        //This means that the Ultimate parent has more than "maxSizeHierarchy" number of children
        if (showFullStructure == false)
        {

            System.debug('Showing ABREVIATED STRUCTURE');
            //This is going to keep a track to make sure we only display the maxnumber of accounts in total 
            Integer totalAccounts = 0;
            System.debug('After running GetTopElement totalLevels ' + totalLevels);

            
            //This IF takes care of the case in which we are trying to get an abbreviated structure from the Ultimate Parent
            //if the Ultimate Parent has more than 200 child accounts
            
            //if the field childIDUltimate was populated this means currentID is an ultimate parent with more than 200 children
            
            String temporaryID;
            
            if( childIDUltimate != '')
            {
                temporaryID = childIDUltimate;
                System.debug('Inside of the Ultimate child: ' + temporaryID + ' totalLevels: ' + totalLevels);    
                
            } else //childIDUltimate is null 
            {
                temporaryID = currentId;
            }
            
                //the reason why we start from 4 and go down is so that we can keep track of the number of levels
                //if level 1 is the highest (with no parents) then: 
                //level 4 for should have the following format 0.0.0.0, level 3 0.0.0, level 2 0.0 and the highest should simply be 0
                for (Integer i = totalLevels; i > 0; i--)
                {
                    List<Account> parAccs;   
                    Account a;
    
                    //gets the current account we will be working with 
                    a = 
                        [ 
                                SELECT Account_Type__c, Site, ParentId, OwnerId, Name, Industry, Id, Region__c,
                                      (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                                       FROM Account_Address__r
                                       WHERE Address_Type__c = 'Registered' LIMIT 1
                                       )
                                FROM Account
                                WHERE id =: temporaryID ORDER BY Name 
                        ];    
                    
                    if(a.ParentID != null)
                    {
                        //gets sibling accounts
                        parAccs = 
                            [
                                    SELECT Account_Type__c, Site, ParentId, OwnerId, Name, Industry, Id, Region__c,
                                          (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                                           FROM Account_Address__r
                                           WHERE Address_Type__c = 'Registered' LIMIT 1
                                           )
                                    FROM Account
                                    WHERE ParentID =: a.ParentID ORDER BY Name 
                            ];
                        
                        System.debug('Inside of IF ' + parAccs);
                    }
                    
                    System.debug(a);
                    //System.debug('Current AccountID ' + a.ID + ' and parentID ' + a.ParentID + ' total Siblings ' + parAccs.size());
                    System.debug('Current AccountID ' + a.ID + ' and parentID ' + a.ParentID);
                    
                    String levelString = '';
                    String siblingsLevelString = '';
                    
                    //bulding the string 0.0. (it is missing the last element we will add it later) 
                    for (Integer j = 0; j < (i - 1) ; j ++)
                    {
                        levelString += '0.';
                    }
    
                    
                    totalAccounts++;    // we are inserting one account to the structure so we add the counter  
                                        // (we need less than maxSizeHierarchy [custom setting] accounts in the short 
                                        // version of the structure)
                    
                    siblingsLevelString = levelString; 
                    levelString += '0';
                    System.debug('levelString ' + levelString);
                    nodeId = levelString;
                    
                    if (a.ID != currentId)
                    {
                        masm.put( nodeId, new ObjectStructureMap( nodeId, levelFlag, closeFlag, nodeType, false, false, a ) );
                        nodeList.put( a.id,nodeId );
                        nodeSortList.add( nodeId ); 
                    }
                    
                    //Ultimate Parent Case
                    if (childIDUltimate != '' && a.ID == currentID)
                    {
                        masm.put( nodeId, new ObjectStructureMap( nodeId, levelFlag, closeFlag, nodeType, false, false, a ) );
                        nodeList.put( a.id,nodeId );
                        nodeSortList.add( nodeId );
                    }
                      
                    
    
                    if (parAccs != null && totalAccounts < maxSizeHierarchy )
                    {
                        for (Integer j = 0; j < parAccs.size(); j++)
                        {
    
                            // we should only display the number of maxSizeHierarchy accounts 
                            if(totalAccounts >= maxSizeHierarchy)
                            {
                                break;
                            }
    
                            totalAccounts++; //increment the counter for another account inserted in the structure
    
                            Integer k = j + 1; //Example 0.0.0 is the root so we need 0.0.1 instead
                            nodeId = siblingsLevelString + k;
    
                            masm.put( nodeId, new ObjectStructureMap( nodeId, levelFlag, closeFlag, nodeType, false, false, parAccs[j] ) );
                            nodeList.put( parAccs[j].id,nodeId );
                            nodeSortList.add( nodeId );   
                        }
                    }     
                    
    
                    temporaryID = a.ParentID;
                }
                
            //} //end of else when childIDUltimate is null
            
        }   
            
            for(string strId:nodeList.keySet()){
                mapActiveContracts.put(strId,0);
            }
            
            //Get the Active contracts for each account - Added by Manoj 
            for(Contract__c contr:[select id,Account__c,Status__c from Contract__c where Account__c=:nodeList.keySet() AND Status__c='Active']){
                if(mapActiveContracts.containsKey(contr.Account__c)){
                    Integer contrCount = mapActiveContracts.get(contr.Account__c);
                    mapActiveContracts.put(contr.Account__c,contrCount+1);
                }
                else{
                    mapActiveContracts.put(contr.Account__c,1);
                }
            }
        
        
        //Account structure must now be formatted
        NodeSortList.sort();
        for( Integer i = 0; i < NodeSortList.size(); i++ ){
            List<String> pnl = new List<String> {};
            List<String> cnl = new List<String> {};
            List<String> nnl = new List<String> {};
            
            if ( i > 0 ){
                String pn   = NodeSortList[i-1];
                pnl         = pn.split( '\\.', -1 );
            }

            String cn   = NodeSortList[i];
            cnl         = cn.split( '\\.', -1 );

            if( i < NodeSortList.size()-1 ){
                String nn = NodeSortList[i+1];
                nnl = nn.split( '\\.', -1 );
            }
            
            ObjectStructureMap tasm = masm.get( cn );
            if ( cnl.size() < nnl.size() ){
                //Parent
                tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
            }
            else if( cnl.size() > nnl.size() ){
                tasm.nodeType   = 'child_end';
                tasm.closeFlag  = setcloseFlag( cnl, nnl, tasm.nodeType );
            }
            else{
                tasm.nodeType = 'child';
            }
            
            tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType ); 
            
            //Change below
            if ( tasm.account.id == currentId ) {
                tasm.currentNode = true;
            }
            asm.add( tasm );
        }
        if(asm.size()>0){
        asm[0].nodeType             = 'start';
        asm[asm.size()-1].nodeType  = 'end';
        }
        
        return asm;
    }


    /*
    public ObjectStructureMap[] formatObjectStructure( String currentId ){
    
        List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
        masm.clear();

        //Change below
        List<Account> al            = new List<Account>{};
        //List<ID> currentParent      = new List<ID>{};
        Map<ID, String> nodeList    = new Map<ID, String>{};
        List<String> nodeSortList   = new List<String>{};
        List<Boolean> levelFlag     = new List<Boolean>{};
        List<Boolean> closeFlag     = new List<Boolean>{};
        String nodeId               = '0';
        String nodeType             = 'child';
        Integer count               = 0;
       // Integer level               = 0;
        Boolean endOfStructure      = false;
        
        //Find highest level obejct in the structure
        currentParent.add( GetTopElement( currentId ) );

        //Loop though all children
        while ( !endOfStructure ){
        
        
   
            if( level == 0 ){
                //Change below     
                al = [ SELECT a.Account_Type__c, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id, a.Region__c,
                              (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                               FROM Account_Address__r
                               WHERE Address_Type__c = 'Registered' LIMIT 1
                               )
                       FROM Account a
                       WHERE a.id IN : CurrentParent ORDER BY a.Name 
                       ];
            } 
            else {
                //Change below      
                al = [ SELECT a.Account_Type__c, a.Site, a.ParentId, a.OwnerId, a.Name, a.Industry, a.Id, a.Region__c,
                              (SELECT Id, Address_Country__c, State__c, Address__r.County__c, Address__r.Province__c
                               FROM Account_Address__r
                               WHERE Address_Type__c = 'Registered' LIMIT 1
                               )
                       FROM Account a
                       WHERE a.ParentID IN : CurrentParent ORDER BY a.Name 
                     ];
            }
            

            if( al.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                for ( Integer i = 0 ; i < al.size(); i++ ){
                    //Change below
                    Account a = al[i];
                    nodeId = ( level > 0 ) ? NodeList.get( a.ParentId )+'.'+String.valueOf( i ) : String.valueOf( i );
                    masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, a ) );
                    currentParent.add( a.id );
                    nodeList.put( a.id,nodeId );
                    nodeSortList.add( nodeId );
                }
                
                maxLevel.add( level );                
                level++;
            }
        }
        
        //Account structure must now be formatted
        NodeSortList.sort();
        for( Integer i = 0; i < NodeSortList.size(); i++ ){
            List<String> pnl = new List<String> {};
            List<String> cnl = new List<String> {};
            List<String> nnl = new List<String> {};
            
            if ( i > 0 ){
                String pn   = NodeSortList[i-1];
                pnl         = pn.split( '\\.', -1 );
            }

            String cn   = NodeSortList[i];
            cnl         = cn.split( '\\.', -1 );

            if( i < NodeSortList.size()-1 ){
                String nn = NodeSortList[i+1];
                nnl = nn.split( '\\.', -1 );
            }
            
            ObjectStructureMap tasm = masm.get( cn );
            if ( cnl.size() < nnl.size() ){
                //Parent
                tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
            }
            else if( cnl.size() > nnl.size() ){
                tasm.nodeType   = 'child_end';
                tasm.closeFlag  = setcloseFlag( cnl, nnl, tasm.nodeType );
            }
            else{
                tasm.nodeType = 'child';
            }
            
            tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType ); 
            
            //Change below
            if ( tasm.account.id == currentId ) {
                tasm.currentNode = true;
            }
            asm.add( tasm );
        
        }

        asm[0].nodeType             = 'start';
        asm[asm.size()-1].nodeType  = 'end';
        
        return asm;
    }   */

    /**
    * Determin parent elements relationship to current element
    * @return flagList
    */
    public List<Boolean> setlevelFlag( List<String> nodeElements, String nodeType ){
        
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode     = '';
        Integer cn          = 0;
        

        for( Integer i = 0; i < nodeElements.size() - 1; i++ ){
            cn = Integer.valueOf( nodeElements[i] );
            cn++;
            searchNode  = workNode + String.valueOf( cn );
            workNode    = workNode + nodeElements[i] + '.';
            if ( masm.containsKey( searchNode ) ){
                flagList.add( true );
            }
            else {
                flagList.add( false );
            }
        }
        
        return flagList;
    }
    
    /**
    * Determin if the element is a closing element
    * @return flagList
    */
    public List<Boolean> setcloseFlag( List<String> cnl, List<String> nnl, String nodeType ){
        
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode     = '';
        Integer cn          = 0;
        
        for( Integer i = nnl.size(); i < cnl.size(); i++ ){
            flagList.add( true );
        }
        
        return flagList;
    }
    
    /**
    * Determin if Element is the bottom node  
    * @return Boolean
    */
    public Boolean isLastNode( List<String> nodeElements ){
        
        String searchNode   = '';
        Integer cn          = 0;
        
        for( Integer i = 0; i < nodeElements.size(); i++ ){
            if ( i == nodeElements.size()-1 ){
                cn = Integer.valueOf( nodeElements[i] );
                cn++;
                searchNode = searchNode + String.valueOf( cn );
            }
            else {
                searchNode = searchNode + nodeElements[i] + '.';
            }
        }
        if ( masm.containsKey( searchNode ) ){
            return false;
        }
        else{
            return true;
        }
    }
    
    /**
    * Find the top most element in Heirarchy  
    * @return objId
    */
    public String GetTopElement( String objId ){
        
        Boolean top = false;


        while ( !top ) {
            //Change below
            Account a = [ Select a.Id, a.Name, a.ParentId From Account a where a.Id =: objId limit 1 ];
            

            //Counts the total of levels
            totalLevels++;
            System.debug('totalLevels ' + totalLevels + ' Account Name ' + a.Name);

            if ( a.ParentID != null ) {
                objId = a.ParentID;
            }
            else {
                top = true;
                ultimateAccountID = a.ID;
            }
        }
        return objId ;
    }
    
    /**
    * Wrapper class
    */
    public with sharing class ObjectStructureMap{

        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        
        /**
        * @Change this to your sObject
        */
        public Account account;
        
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }


        /**
        * @Change this to your sObject
        */
        public Account getaccount() { return account; }
        /* 
         * Cristian Torres 04/23/16
         * Original Code I commented it to get the code coverage, didn't see any 
         * issues when commenting it 
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }
        */
        
        /**
        * @Change this to your sObject
        */
        /* 
         * Cristian Torres 04/23/16
         * Original Code I commented it to get the code coverage, didn't see any 
         * issues when commenting it 
         * public void setaccount( Account a ) { this.account = a; }
         */

        /**
        * @Change the parameters to your sObject
        */
        public ObjectStructureMap( String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, Account a ){
            
            this.nodeId         = nodeId;
            this.levelFlag      = levelFlag; 
            this.closeFlag      = closeFlag;
            this.nodeType       = nodeType;
            this.currentNode    = currentNode;

            //Change this to your sObject  
            this.account = a;
        }
    }
}