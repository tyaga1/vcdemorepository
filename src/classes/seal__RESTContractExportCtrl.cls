/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/contracts/*')
global class RESTContractExportCtrl {
    global RESTContractExportCtrl() {

    }
    @HttpGet
    global static List<Map<String,String>> getFiles() {
        return null;
    }
    @HttpPost
    global static Map<String,String> updateExportStatus(Map<String,Map<String,String>> contracts) {
        return null;
    }
}
