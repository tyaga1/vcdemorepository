/*******************************************************************************
 * Appirio, Inc
 * Name         : OrderTrigger_AccountSegmentation_Test
 * Created By   : Rohit B. (Appirio)
 * Purpose      : Test class of class "OrderTrigger_AccountSegmentation"
 * Created Date : April 07th, 2015
 *
 * Date Modified                Modified By         Description of the update
 * 15th Apr, 2015               Suminder Singh      T-378086: added methnod segmentCalculationPopulateOnOrderDelete_Test()
                                                    Delete and Undelete order test Unit
 * 01st Jul, 2015               Arpita Bose(Appirio)T-405525: Updated class to increase code-coverage and implement @testsetup 
 * 09th Sep, 2015               Parul Gupta         I-179537: Code coverage improvement for SegmentMaintenanceQueue                                                  
 * 10th Nov, 2015               Diego Olarte        Case 01135171: Added custom setting for Test class avoid a fail when running the UserTriggerHandler
 * Apr 7th, 2016                Paul Kissick        Case 01932085: Fixing Test User Email Domain
*******************************************************************************/

@isTest
private class OrderTrigger_AccountSegmentation_Test {
  public static Id readOnlyRTid = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_ORDER, Constants.READ_ONLY);
  public static Order__c testOrder;
  public static User usr;
  public static Account testAccount;
  public static List<Account_Segment__c> listAccSegments;
  
  //===========================================================================
  // Account Segmentation: Insert order test Unit
  //===========================================================================
  @isTest 
  static void segmentFieldPopulateOnOrderInsert_Test() {
    usr = [Select Id,Business_Line__c, Business_Unit__c, Global_Business_Line__c,
                  Country__c, Region__c FROM User where LastName = 'Testing'];
           
    system.runAs(usr) {
     //Quering inserted Order for assert check 
      Order__c odr = [SELECT Id, Owner_BL_on_Order_Create_Date__c, Owner_BU_on_Order_Create_Date__c,
                             Owner_GBL_on_Order_Create_Date__c, Owner_Country_on_Order_Create_Date__c, 
                             Owner_Region_on_Order_Create_Date__c
                             FROM Order__c where Name = 'test'];

      system.assertEquals(usr.Business_Line__c, odr.Owner_BL_on_Order_Create_Date__c);
      system.assertEquals(usr.Business_Unit__c, odr.Owner_BU_on_Order_Create_Date__c);
      system.assertEquals(usr.Global_Business_Line__c, odr.Owner_GBL_on_Order_Create_Date__c);
      system.assertEquals(usr.Country__c, odr.Owner_Country_on_Order_Create_Date__c);
      system.assertEquals(usr.Region__c, odr.Owner_Region_on_Order_Create_Date__c);
    }
    
  } //End static void test_method_one() {
  
  //===========================================================================
  // Account Segmentation: Update order test Unit
  //===========================================================================
  @isTest 
  static void segmentFieldPopulateOnOrderUpdate_Test() {
    //fetching profile Id of system admin
    usr = [Select Id,Business_Line__c, Business_Unit__c, Global_Business_Line__c,
                  Country__c, Region__c FROM User where LastName = 'Testing'];
    testAccount = [SELECT Id FROM Account WHERE Name = 'Test Account'];
    
    system.debug('testAccount>>>' +testAccount); 
    Order__c odrToUpdate;
    Double insertOrdAmount, updateOrdAmount;
    system.runAs(usr) {
      
        //Updating Order
        odrToUpdate = [SELECT Id, Contract_Start_Date__c, Contract_End_Date__c, Amount_Corp__c
                       FROM Order__c 
                       WHERE Name = 'test'];
        odrToUpdate.Contract_Start_Date__c = Date.Today()+5;
        odrToUpdate.Contract_End_Date__c = Date.Today()+5;
        
        List<Account_Segment__c> accSeg = [SELECT Total_Open_Pipeline__c, Total_Won__c
                                           FROM Account_Segment__c 
                                           WHERE Account__c =: testAccount.ID];
        system.debug('accSeg>>>' +accSeg);                                   
        insertOrdAmount = accSeg[0].Total_Won__c;
        odrToUpdate.Amount_Corp__c = 2050;
        
        Set<String> segNames = new Set<String>();
        listAccSegments = new List<Account_Segment__c>();
        for(Account_Segment__c accSegRec : [SELECT Id, Name, Value__c FROM Account_Segment__c]){
            listAccSegments.add(accSegRec);
            segNames.add(accSegRec.Value__c);
        }
        
        Test.startTest();
          OrderTrigger_AccountSegmentation.hasRunBatch = false;
          OpportunityTriggerHandler.isRunningOpportunityTrigger =  true;
          OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
          OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
          OrderLineItemTriggerHandler.hasRunAssetCreation = false;
          OrderLineItemTriggerHandler.hasRunMemoline = false;  
          OpportunityTrigger_OrderHelper.isExecuted = false;
          OrderTriggerHandler.hasRunAccountSegmentation = false;
          update odrToUpdate;
                    
        System.enqueueJob(new SegmentMaintenanceQueue(null, segNames));
        Test.stopTest();
      } //End system.runAs(usr) {
      

      //Quering inserted Order for assert check 
      Order__c odr = [SELECT Id, Owner_Business_Line__c, Owner_Business_Unit__c,
                              Owner_Global_Business_Line__c, Owner_Country__c, Owner_Region__c,
                              Segment_Business_Unit__r.Name, Segment_Global_Business_Line__r.Name, 
                              Segment_Country__r.Name, Segment_Region__r.Name, Segment_Business_Line__r.Name,
                              Owner_BL_on_Order_Create_Date__c, Owner_BU_on_Order_Create_Date__c,
                             Owner_GBL_on_Order_Create_Date__c, Owner_Country_on_Order_Create_Date__c, 
                             Owner_Region_on_Order_Create_Date__c
                      FROM Order__c WHERE Id =: odrToUpdate.Id];
      system.debug('listAccSegments>>>'+listAccSegments);
      system.debug('usr>>>'+usr);
      system.debug('odr>>>'+odr);
                      
      system.assertEquals(odr.Owner_BL_on_Order_Create_Date__c, usr.Business_Line__c);
      system.assertEquals(odr.Owner_BU_on_Order_Create_Date__c, usr.Business_Unit__c);
      system.assertEquals(odr.Owner_GBL_on_Order_Create_Date__c, usr.Global_Business_Line__c);
      system.assertEquals(odr.Owner_Country_on_Order_Create_Date__c, usr.Country__c);
      system.assertEquals(odr.Owner_Region_on_Order_Create_Date__c, usr.Region__c);

      List<Account_Segment__c> accSeg1 = [SELECT Total_Open_Pipeline__c, Total_Won__c
                                        FROM Account_Segment__c 
                                        WHERE Account__c =: testAccount.ID];
      updateOrdAmount = accSeg1[0].Total_Won__c;
     system.assert(updateOrdAmount > insertOrdAmount);
  } //End static void test_method_two() {

  //===========================================================================
  // T-378086: Account Segmentation: Delete and Undelete order test Unit
  //===========================================================================
  static testMethod void segmentCalculationPopulateOnOrderDelete_Test() {
    usr = [Select Id,Business_Line__c, Business_Unit__c, Global_Business_Line__c,
                  Country__c, Region__c FROM User where LastName = 'Testing'];
    testAccount = [SELECT Id FROM Account WHERE Name = 'Test Account'];
    testOrder = [SELECT Id, Owner_BL_on_Order_Create_Date__c, Owner_BU_on_Order_Create_Date__c,
                        Owner_GBL_on_Order_Create_Date__c, Owner_Country_on_Order_Create_Date__c, 
                        Owner_Region_on_Order_Create_Date__c
                        FROM Order__c where Name = 'test'];
    system.runAs(usr) {       
        Test.startTest();
        OpportunityTriggerHandler.isRunningOpportunityTrigger =  true;
        OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
        OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
        OrderLineItemTriggerHandler.hasRunAssetCreation = false;
        OrderLineItemTriggerHandler.hasRunMemoline = false;
        OpportunityTrigger_OrderHelper.isExecuted = false;
        OrderTriggerHandler.hasRunAccountSegmentation = false;
        //update testOpp;
        delete testOrder;
      
         //List to check total won and total pipeline segment amount
         List<Account_Segment__c> accSeg = [SELECT Total_Open_Pipeline__c, Total_Won__c
                                            FROM Account_Segment__c 
                                            WHERE Account__c =: testAccount.ID];
         System.assertEquals(0, accSeg[0].Total_Won__c) ;
         
         OpportunityTriggerHandler.isRunningOpportunityTrigger =  true;
         OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
         OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
         OrderLineItemTriggerHandler.hasRunAssetCreation = false;
         OrderLineItemTriggerHandler.hasRunMemoline = false; 
         OpportunityTrigger_OrderHelper.isExecuted = false;
         OrderTriggerHandler.hasRunAccountSegmentation = false;
         //Insert order to check undelete functionality on order undelete
          undelete testOrder;
         Test.stopTest();
         
     } //End system.runAs(usr) {
     List<Account_Segment__c> accSeg1 = [SELECT Total_Open_Pipeline__c, Total_Won__c
                                        FROM Account_Segment__c 
                                        WHERE Account__c =: testAccount.ID];
     //System.assertEquals(1050, accSeg1[0].Total_Won__c);
  } //End static void test_method_one() {
  
  //method to create test data
   @testSetup
  static void createData () {
    
    //DO Case 01135171: Custom Setting to avoid conflicts with userTriggerHandler
    Global_Settings__c setting = new Global_Settings__c();
    setting.Name = 'Global';
    setting.Experian_Cross_BU_Acc_Id__c = Userinfo.getOrganizationId();
    
    insert setting;

    //fetching profile Id of system admin
    Profile p = [SELECT id from profile where name =: 'System Administrator' ];
    //creating user with all necessary values for our use case like Bussiness Unit, Business Line, + 3
    usr = new User(alias = 'testUser', email='standarduser' + Math.random()  + '@experian.com',
                  emailencodingkey='UTF-8', firstName='test user', lastname='Testing', languagelocalekey='en_US',
                  localesidkey='en_US', profileid = p.ID, timezonesidkey='America/Los_Angeles', 
                  username='teststandarduser' + Math.random() + '@experian.global.test', IsActive=true, 
                  CompanyName = 'test Company', Business_Line__c = 'APAC Corporate', 
                  Business_Unit__c = 'APAC Corporate Finance', Global_Business_Line__c = 'Corporate', 
                  Country__c = 'India', Region__c = 'Global');
                  
    insert usr;
    
    
    Global_Settings__c settings = Global_Settings__c.getInstance(Constants.GLOBAL_SETTING);
    settings = new Global_Settings__c(name=Constants.GLOBAL_SETTING,Account_Team_Member_Default_Role__c= Constants.TEAM_ROLE_ACCOUNT_MANAGER);
    insert settings;
    
    // Create Account
    testAccount = new Account(Name = 'Test Account'); 
    insert testAccount;
       
    // Create an opportunity
    Opportunity testOpp = Test_Utils.createOpportunity(testAccount.ID);
    testOpp.Amount_Corp__c = 1000;
    insert testOpp;

    //Create a Contact
    Contact testCon = Test_Utils.insertContact(testAccount.ID);

    //Create an Order
    testOrder = Test_Utils.insertOrder(false, testAccount.ID, testCon.ID, testOpp.ID); 
    testOrder.Amount_Corp__c = 1050;
    testOrder.RecordTypeId = readOnlyRTid;
    testOrder.Owner_GBL_on_Order_Create_Date__c = 'Corporate';
    testOrder.Owner_BL_on_Order_Create_Date__c = 'APAC Corporate';
    testOrder.Owner_BU_on_Order_Create_Date__c = 'APAC Corporate Finance';
    testOrder.Owner_Region_on_Order_Create_Date__c = 'Global';
    testOrder.Owner_Country_on_Order_Create_Date__c = 'India'; 
    testOrder.Name = 'test';
    insert testOrder;
    system.debug('order>>>Insert'+testOrder); 
    
    createHierarchies();             
  }
  
  private static void createHierarchies() {
        List<Hierarchy__c> lstHierarchy = new List<Hierarchy__c>();
     Hierarchy__c hierarchy_BusinessUnit = 
     Test_Utils.insertHierarchy(false, null, 'APAC Corporate Finance', 'Business Unit');
     lstHierarchy.add(hierarchy_BusinessUnit);
     Hierarchy__c hierarchy_BusinessLine = 
     Test_Utils.insertHierarchy(false, null, 'APAC Corporate', 'Business Line');
     lstHierarchy.add(hierarchy_BusinessLine);
     Hierarchy__c hierarchy_Country = 
     Test_Utils.insertHierarchy(false, null, 'India', 'Country');
     lstHierarchy.add(hierarchy_Country);
     Hierarchy__c hierarchy_GlobalBusinessLine = 
     Test_Utils.insertHierarchy(false, null, 'Corporate', 'Global Business Line');
     lstHierarchy.add(hierarchy_GlobalBusinessLine);
     Hierarchy__c hierarchy_Region = 
     Test_Utils.insertHierarchy(false, null, 'Global', 'Region');
     lstHierarchy.add(hierarchy_Region);
     insert lstHierarchy;    //insertion of hierarchy records
     
      // Insert Account Segment    
    listAccSegments = new List<Account_Segment__c>();
    Account_Segment__c accSegment_BusinessLine = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_BusinessUnit.Id, null);
    listAccSegments.add(accSegment_BusinessLine);
    
    Account_Segment__c accSegment_BusinessUnit = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_BusinessLine.Id, null);
    listAccSegments.add(accSegment_BusinessUnit);
    
    Account_Segment__c accSegment_Country = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_Country.Id, null);
    listAccSegments.add(accSegment_Country);
    
    Account_Segment__c accSegment_GlobalBusinessLine = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_GlobalBusinessLine.Id, null);
    listAccSegments.add(accSegment_GlobalBusinessLine);
    
    Account_Segment__c accSegment_Region = Test_Utils.insertAccountSegment(false, testAccount.Id, hierarchy_Region.Id, null);
    listAccSegments.add(accSegment_Region);
    
    insert listAccSegments;
  }

} //End private class OrderTrigger_AccountSegmentation_Test