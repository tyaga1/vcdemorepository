/**=====================================================================
 * Experian
 * Name: BatchWalletSyncActivityOwners_Test
 * Description: 
 * Created Date: 6 Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Oct 1st, 2015      Paul Kissick               Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class BatchWalletSyncActivityOwners_Test {

  static testMethod void myUnitTest() {
    List<WalletSync__c> wsObjs = new List<WalletSync__c>();
    
    User usr1 = [SELECT Id FROM User WHERE Alias = 'ZWQQ0' LIMIT 1];
    User usr2 = [SELECT Id FROM User WHERE Alias = 'ZWQQ1' LIMIT 1];
    // Check how many open/closed opps are there...
    
    Integer tasksForUser1 = [SELECT COUNT() FROM Task WHERE OwnerId = :usr1.Id];
    Integer tasksForUser2 = [SELECT COUNT() FROM Task WHERE OwnerId = :usr2.Id];
    
    for(Account a : [SELECT Id, CNPJ_Number__c FROM Account]) {
	    WalletSync__c ws1 = new WalletSync__c(
	      Account_Owner__c = usr2.Id,
	      Previous_Account_Owner__c = usr1.Id,
	      CNPJ_Number__c = a.CNPJ_Number__c,
	      Account__c = a.Id,
	      LegacyCRM_Account_ID__c = 'ANYTHING1',
	      Last_Processed_Date__c = Datetime.now().addMinutes(-20)
	    );
	    wsObjs.add(ws1);
    }
    
    insert wsObjs;
    
    Test.startTest();
    
    BatchWalletSyncActivityOwners b = new BatchWalletSyncActivityOwners();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    Integer tasksForUser1Aft = [SELECT COUNT() FROM Task WHERE OwnerId = :usr1.Id];
    Integer tasksForUser2Aft = [SELECT COUNT() FROM Task WHERE OwnerId = :usr2.Id];
    
    system.assertNotEquals(tasksForUser1Aft,tasksForUser1);
    system.assertNotEquals(tasksForUser2Aft,tasksForUser2);
    
  }
  
  @testSetup
  static void setupTestData() {
    
    IsDataAdmin__c ida = new IsDataAdmin__c(IsDataAdmin__c = true, SetupOwnerId = UserInfo.getUserId());
    insert ida;
    
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    List<User> tstUsrs = Test_Utils.createUsers(p,'test@experian.com','ZWQQ',2);
    insert tstUsrs;
    
    User u = [SELECT Id FROM User WHERE Alias = 'ZWQQ0' LIMIT 1];

    system.runAs(u) {
      List<Account> newAccsList = new List<Account>();
      Account acc1 = Test_utils.createAccount();
      acc1.CNPJ_Number__c = '62307946000152';
      newAccsList.add(acc1);
      Account acc2 = Test_utils.createAccount();
      acc2.CNPJ_Number__c = '17240483000103';
      newAccsList.add(acc2);
      Account acc3 = Test_utils.createAccount();
      acc3.CNPJ_Number__c = '43161803000130';
      newAccsList.add(acc3);
      Account acc4 = Test_utils.createAccount();
      acc4.CNPJ_Number__c = '34895621000101';
      newAccsList.add(acc4);
      Account acc5 = Test_utils.createAccount();
      acc5.CNPJ_Number__c = '18068852000186';
      newAccsList.add(acc5);
      insert newAccsList;
    }
    
    List<Account> newAccs = [SELECT Id, CNPJ_Number__c FROM Account];
    
    List<Contact> newConts = new List<Contact>();
    for(Account a : newAccs) {
      newConts.add(Test_Utils.createContact(a.Id));
    }
    
    insert newConts;
    
    // quick map of account id to cont id...
    Map<Id,Id> accToContMap = new Map<Id,Id>();
    for(Contact c : newConts) {
      accToContMap.put(c.AccountId,c.Id);
    }
    
    
    // Then for each opp, add 1 open and 1 closed task...
    
    List<Task> newTasks = new List<Task>();
    for(Account a : newAccs) {
      Task openTask = new Task(
        ActivityDate = Date.today().addDays(1), 
        WhoId = accToContMap.get(a.Id),
        WhatId = a.Id,
        Status = 'In Progress',
        Subject = 'Call Back',
        OwnerId = u.Id
      );
      newTasks.add(openTask);
      Task closedTask = new Task(
        ActivityDate = Date.today().addDays(-1), 
        WhoId = accToContMap.get(a.Id), 
        WhatId = a.Id,
        Status = 'Completed',
        Subject = 'Called',
        OwnerId = u.Id
      );
      newTasks.add(closedTask);
    }
    insert newTasks;
    
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
    
    delete ida;
    
  }
}