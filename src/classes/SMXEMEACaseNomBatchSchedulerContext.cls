/**=====================================================================
 * Experian
 * Name: SMXProcessEMEACaseBatch 
 * Description: Schedule the SMXProcessEMEACaseBatch survey
 * Created Date: Sep 25th, 2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *                                         
 =====================================================================*/
 global class SMXEMEACaseNomBatchSchedulerContext implements Schedulable{
    global SMXEMEACaseNomBatchSchedulerContext(){}
    global void execute(SchedulableContext ctx){
        SMXProcessEMEACaseBatch b = new  SMXProcessEMEACaseBatch();
        if(!Test.isrunningTest()){
        database.executebatch(b); 
        }       
    }
    
}