/**=====================================================================
 * Appirio, Inc
 * Name: EmailMessageTriggerHandler_Test
 * Description: T-314309: Test class for EmailMessageTriggerHandler.cls
 * Created Date: Aug 25th, 2014
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sep 3rd, 2014                Arpita Bose(Appirio)         T-316539: Updated code to populate Sub_Origin__c with ToAddress    
 * Sep 19th, 2014               Mohit Parnami                T-318433: updated code to populate RecordTypeId, User_Requestor__c, Requestor__c, Requestor_Work_Phone__c                                                                                                                        
 * Oct 10th, 2014               Arpita Bose                  Updated method testPopulateSubOriginOnCase() to fix failure
 * Jul 10th, 2015               Paul Kissick                 Adding testEmailWithCPQQuote() to test quote linking
 * Jan 5th,  2016               James Wills                  Case 01266714: Updated Constant names and text to reflect updates to Case Record Types (see comments below).
 * Apr 7th, 2016                Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Dic 13th, 2016               Diego Olarte                 Case 01265676: Add method for EDQ Hard close case
  =====================================================================*/
@isTest
private class EmailMessageTriggerHandler_Test{
  
  static testmethod void testPopulateSubOriginOnCase(){
    // create test data
    Account testAcc = Test_Utils.insertAccount();
    Contact con = Test_Utils.insertContact(testAcc.Id);
    
    Id EDQGPDId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.RECORDTYPE_CASE_EDQ_GPD_SUPPORT);//James Wills Changed From RECORDTYPE_CASE_EDQ_GPD
    
    EMSSupportEmailAddresses__c supEmails = new EMSSupportEmailAddresses__c(
      Name = 'Default', 
      France_Emails__c = 'test-france@experian.com', 
      Germany_Emails__c = 'test-germany@experian.com', 
      Spain_Emails__c = 'test-spain@experian.com'
    );
    insert supEmails; 

    Case newCase = Test_Utils.insertCase(false, testAcc.Id);
    newCase.SuppliedEmail = 'test@experian.com';
    newCase.RecordTypeId = EDQGPDId;
    system.debug('--Rtype---'+newCase.RecordTypeId);
    insert newCase;

    Profile profile = [Select Id From Profile Where Name =: Constants.PROFILE_SYS_ADMIN];
    User usr = Test_Utils.createUser(profile, 'test@experian.com', 'LastName');
    usr.Phone = '9799559433';
    insert usr;


    EmailMessage em = new EmailMessage();
    em.FromName = 'Test User';
    em.FromAddress = 'test@example.com';
    em.Incoming = true;
    em.ToAddress = 'test-spain@experian.com';
    em.Subject = 'Test email';
    em.TextBody = 'Hello';
    em.ParentId = newCase.Id;
    em.CcAddress = 'test@test2.com';

    // start test
    Test.startTest();
    insert em;
    
    List<Case> caseResult = [SELECT Id, Sub_Origin__c, SuppliedEmail, RecordType.Name,
                             RecordTypeId, User_Requestor__c, Requestor__c, Requestor_Email__c, 
                             Requestor_Work_Phone__c
                             FROM Case WHERE Id =:newCase.Id];
    system.debug('case>>' +caseResult);
    List<EmailMessage> emLst = [SELECT Id, ToAddress, Subject, Status, ParentId, FromName, FromAddress
                                FROM EmailMessage
                                WHERE Id =:em.Id];
    system.debug('EmailMessage>>' +emLst);                                 
    // stop test
    Test.stopTest();
    // Assert to verify Sub Origin on Case equals to EmailMessage's ToAddress
    system.assertEquals(emLst.get(0).ToAddress+';test@test2.com', caseResult.get(0).Sub_Origin__c);
    system.assertEquals(EDQGPDId, caseResult.get(0).RecordTypeId);
    system.assertEquals(caseResult.get(0).SuppliedEmail, caseResult.get(0).User_Requestor__c);
    system.assertEquals(usr.Id, caseResult.get(0).Requestor__c);
    system.assertEquals(usr.Email, caseResult.get(0).Requestor_Email__c);
    system.assertEquals(usr.Phone, caseResult.get(0).Requestor_Work_Phone__c);
    
  }
  
  static testMethod void testEmailWithCPQQuote() {
    Account testAcc = Test_Utils.insertAccount();
    Contact con = Test_Utils.insertContact(testAcc.Id);
    Opportunity opp = Test_Utils.insertOpportunity(testAcc.Id);
    
    Id EDQGPDId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.RECORDTYPE_CASE_EDQ_GPD_SUPPORT);//James Wills Changed From RECORDTYPE_CASE_EDQ_GPD 

    Case newCase = Test_Utils.insertCase(false, testAcc.Id);
    newCase.SuppliedEmail = 'test@test.com';
    newCase.RecordTypeId = EDQGPDId;
    insert newCase;
    
    Case newCase2 = Test_Utils.insertCase(false, testAcc.Id);
    newCase2.SuppliedEmail = 'test@test.com';
    newCase2.RecordTypeId = EDQGPDId;
    insert newCase2;

    Quote__c cpqQuote = new Quote__c(Name = 'TestQuote00001', Opportunity__c = opp.Id);
    insert cpqQuote;

    EmailMessage em = new EmailMessage();
    em.FromName = 'Test User';
    em.FromAddress = 'test@example.com';
    em.Incoming = true;
    em.ToAddress = 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
    em.Subject = 'Approval Request for Quote 1';
    em.TextBody = 'Hello';
    em.HtmlBody = 'Blah blah http://ss.get/id='+cpqQuote.Id+'?test';
    em.ParentId = newCase.Id;


    // start test
    Test.startTest();
    insert em;
    
    Test.stopTest();
    Case checkCase = [SELECT Id, CPQ_Quote__c, Account__c, Opportunity__c FROM Case WHERE Id = :newCase.Id];
    system.assertEquals(cpqQuote.Id, checkCase.CPQ_Quote__c,'Quote not linked');
    system.assertEquals(testAcc.Id, checkCase.Account__c,'Account not linked');
    system.assertEquals(opp.Id, checkCase.Opportunity__c,'Opportunity not linked');
    
  }
  
  static testMethod void testAccountOnCaseFromEmail () {
     
    Account acc1 = Test_Utils.insertAccount();
    Account acc2 = Test_Utils.insertAccount();
    Account_Email_Address__c aea1 = new Account_Email_Address__c(Account__c = acc1.Id, Email_Domain__c = 'testdomain.com');
    Account_Email_Address__c aea2 = new Account_Email_Address__c(Account__c = acc2.Id, Email_Address__c = 'john.doe@newtest.com');
    insert new List<Account_Email_Address__c>{aea1, aea2};
    
    Id emsId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.RECORDTYPE_CASE_EMS);
    
    Case newCase = Test_Utils.insertCase(false, null);
    newCase.SuppliedEmail = 'anyaddress@testdomain.com';
    newCase.RecordTypeId = emsId;
    insert newCase;
    
    Case newCase2 = Test_Utils.insertCase(false, null);
    newCase2.SuppliedEmail = 'john.doe@newtest.com';
    newCase2.RecordTypeId = emsId;
    insert newCase2;

    EmailMessage em1 = new EmailMessage(
      FromName = 'Test User',
      FromAddress = 'anyaddress@testdomain.com',
      Incoming = true,
      ToAddress = 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com',
      Subject = 'Testing the email lookup to account',
      TextBody = 'Here is an email',
      HtmlBody = 'Here is an email',
      ParentId = newCase.Id
    );
    
    EmailMessage em2 = new EmailMessage(
      FromName = 'Test User',
      FromAddress = 'john.doe@newtest.com',
      Incoming = true,
      ToAddress = 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com',
      Subject = 'Testing the email lookup to account',
      TextBody = 'Here is an email',
      HtmlBody = 'Here is an email',
      ParentId = newCase2.Id
    );
    
    List<EmailMessage> emlList = new List<EmailMessage>{em1, em2};
    insert emlList;
    
    EmailMessageTriggerHandler.populateAccountOnCaseFromEmail(emlList);
    
    // update emlList;
    
    system.assertEquals(acc1.Id, [SELECT AccountId FROM Case WHERE Id = :newCase.Id].AccountId, 'AccountId incorrect for case 1');
    system.assertEquals(acc2.Id, [SELECT AccountId FROM Case WHERE Id = :newCase2.Id].AccountId, 'AccountId incorrect for case 2');
    
  }
  
  //===========================================================================
  //Case #01265676 - Add method for EDQ Hard close case - Added by Diego O
  //===========================================================================
  /*static testmethod void testcreateEDQCaseforHardClose(){
    // create test data
    Account testAcc = Test_Utils.insertAccount();
    Contact con = Test_Utils.insertContact(testAcc.Id);
    
    Id EDQTSId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.RECORDTYPE_CASE_EDQ_TECH_SUPPORT);
    String closeStatus = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.CASE_STATUS_CLOSED_COMPLETE);
        
    Case newCase = Test_Utils.insertCase(false, testAcc.Id);
    newCase.SuppliedEmail = 'test@experian.com';
    newCase.RecordTypeId = EDQTSId;
    newCase.Status = closeStatus;
    system.debug('--Rtype---'+newCase.RecordTypeId);
    insert newCase;
    
    EmailMessage em = new EmailMessage();
    em.FromName = 'Test User';
    em.FromAddress = 'test@example.com';
    em.Incoming = true;
    em.ToAddress = 'test-spain@experian.com';
    em.Subject = 'Test email';
    em.TextBody = 'Hello';
    em.ParentId = newCase.Id;
    em.CcAddress = 'test@test2.com';

    // start test
    Test.startTest();
    insert em;
    
    List<Case> caseResult = [SELECT Id, isClosed, RecordTypeId, Status
                             FROM Case WHERE Id =:newCase.Id];
    system.debug('case>>' +caseResult);
    List<EmailMessage> emLst = [SELECT Id, ParentId, Subject, TextBody, ToAddress, Parent.CaseNumber, Parent.Account.Name, Parent.Contact.Name
                                FROM EmailMessage
                                WHERE Id =:em.Id];
    system.debug('EmailMessage>>' +emLst);                                 
    // stop test
    Test.stopTest();
    // Assert to verify Correct case closure EmailMessage's ToAddress
    
    system.assertEquals(EDQTSId, caseResult.get(0).RecordTypeId);
    system.assertEquals(caseResult.get(0).Id, emLst.get(0).ParentId);
    
  }*/
}