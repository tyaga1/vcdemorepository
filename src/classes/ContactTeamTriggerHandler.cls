/**=====================================================================
 * Created Date: 14th Jan 2016
 * Name: ContactTeamTriggerHandler
 * Description: Handler class for ContactTeamTrigger (Case 01252420)
 * Created by: James Wills
 *
 * Date Modified        Modified By            Description of the update
 * 14 Jul 2016          Diego Olarte           CRM2:W-005406: Added method setUniqueBULead() in afterInsert()
 * 19 Jul 2016          Paul Kissick           CRM2:W-005406: Cleaned up class - Added holding methods
 * 19 Jul 2016          Paul Kissick           CRM2:W-005409: New method applyContactFields to add to contact - Also improved handling of contact updates
 * 28 Jul 2016          Diego Olarte           CRM2:W-005406: Contact relationship BU Lead
 * 01 Aug 2016          Paul Kissick           CRM2:W-005406: Fix for Null reference in applyContactFields
 * 03 Aug 2016          Diego Olarte           CRM2:W-005406: Contact relationship BU Lead set to update too
 * 09 Sep 2016          Paul Kissick           CRM2:W-005406: isDataAdmin is no longer ommitted to fix additions by the DQ Team 
 * 28 Sep 2016          Paul Kissick           W-005955: Added new method checkExistingRelationships, to verify the new relationship is unique. 
 * 29 Sep 2016          Diego Olarte           Case #02108074: Added Method setOwnerToRelationshipUser
 * =====================================================================*/

public without sharing class ContactTeamTriggerHandler {
  
  // Adding to support updating contacts in a single operation instead of twice!
  static Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();
  
  public static Boolean isDataAdmin = false;

  public static void beforeInsert(List<Contact_Team__c> newList) {
    checkExistingRelationships(newList);
    setOwnerToRelationshipUser(newList);
  }
  
  public static void beforeUpdate(List<Contact_Team__c> newList, Map<Id, Contact_Team__c> oldMap) {
    // Nothing
  }
  
  public static void beforeDelete(List<Contact_Team__c> oldList) {
    // Nothing
  }
  
  public static void afterInsert(List<Contact_Team__c> newList) {
    updateCounts(newList);
    applyContactFields(newList, true);
    
    // Must be called at the end of the method to update the contacts!
    updateContacts();
    
    if (isDataAdmin == false) {
      setUniqueBULead(newList);
    }
  }
  
  public static void afterUpdate(List<Contact_Team__c> newList, Map<Id,Contact_Team__c> oldMap) {
    updateCounts(newList);
    
    // Must be called at the end of the method to update the contacts!
    updateContacts();
    if (isDataAdmin == false) {
      setUniqueBULead(newList);
    }
  }
  
  public static void afterDelete(List<Contact_Team__c> oldList) {
    updateCounts(oldList);
    applyContactFields(oldList, false);
    
    // Must be called at the end of the method to update the contacts!
    updateContacts();
  }
  
  public static void afterUndelete(List<Contact_Team__c> newList) {
    // Nothing
  }
  
  //===========================================================================
  // setFieldToUpdate - Sets a specific field to update in a map of contacts
  //===========================================================================
  private static void setFieldToUpdate(Id contactId, String fieldName, Object fieldValue) {
    if (!contactsToUpdate.containsKey(contactId)) {
      contactsToUpdate.put(contactId, new Contact(Id = contactId));
    }
    Contact c = contactsToUpdate.get(contactId);
    c.put(fieldName, fieldValue);
    contactsToUpdate.put(contactId, c);
    return;
  }
  
  //===========================================================================
  // updateContacts - Updates any contacts that require field changes. Previously
  //  each method updated contacts after each execution. This way only 1 update 
  //  call is required.
  //===========================================================================
  private static void updateContacts() {
    // Force autoresponse email during update.
    List<Database.SaveResult> srList = Database.update(contactsToUpdate.values(), false);
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        system.debug('Failed to save.');
      }
    }
    contactsToUpdate.clear();
  }
  
  
  //===========================================================================
  // checkExistingRelationships - For all inserted contact relationships, throw
  // an error if the user is already on the contact team.
  //===========================================================================
  public static void checkExistingRelationships(List<Contact_Team__c> newList) {
    
    Set<Id> userIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    
    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null && ct.Contact__c != null) {
        userIds.add(ct.Relationship_Owner__c);
        contactIds.add(ct.Contact__c);
      }
    }
    
    if (userIds.isEmpty() && contactIds.isEmpty()) {
      return;
    }
    
    List<Contact_Team__c> existingContactTeams = [
      SELECT Id, Contact__c, Relationship_Owner__c
      FROM Contact_Team__c
      WHERE Contact__c IN :contactIds
      AND Contact__c != null
      AND Relationship_Owner__c IN :userIds
      AND Relationship_Owner__c != null
    ];
    
    if (existingContactTeams.isEmpty()) {
      return;
    }
    
    Set<String> matchingKeys = new Set<String>();
    
    for (Contact_Team__c ctExist : existingContactTeams) {
      if (ctExist.Relationship_Owner__c != null && ctExist.Contact__c != null) {
        matchingKeys.add(ctExist.Relationship_Owner__c+'|'+ctExist.Contact__c);
      }
    }
    
    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null && 
          ct.Contact__c != null &&
          matchingKeys.contains(ct.Relationship_Owner__c+'|'+ct.Contact__c)) {
        ct.Relationship_Owner__c.addError(system.label.Contact_Relationship_Duplicated);
      }
    }
    
  }

  //===========================================================================
  // Case 01252420: updateCounts
  // Update the Active_Contact_Relationship_Count__c Custom Field for all Contacts 
  // of all Inserted/Deleted/updated (Relationship_Owner__c Custom field only) 
  // Contact_Team__c records
  //===========================================================================
  public static void updateCounts(List<Contact_Team__c> newList) {
    
    List<Contact> contactList = getParentContactsList(newList);

    if (!contactList.isEmpty()) {
      Integer activeContactTeamCounter = 0;
      for (Contact cont : contactList) {

        //1. Update Contact.Total_Contact_Relationship_Count__c
        setFieldToUpdate(cont.Id, 'Total_Contact_Relationship_Count__c' ,cont.Contact_Teams__r.size());

        //2. Update Contact.Active_Contact_Relationship_Count__c
        for (Contact_Team__c contactTeam : cont.Contact_Teams__r) {
          if (contactTeam.Relationship_Owner__r.isActive) {
            activeContactTeamCounter++;
          }
        }
        setFieldToUpdate(cont.Id, 'Active_Contact_Relationship_Count__c' ,activeContactTeamCounter);
        // parentContact.Active_Contact_Relationship_Count__c = activeContactTeamCounter;
        activeContactTeamCounter = 0;
      }
      // Database.update(parentContactsList,false);
    }
  }

  //===========================================================================
  // getParentContactsList: Return a list of parent Contact records for inserted/deleted/updated
  // (Relationship_Owner__c Custom field only) Contact_Team__c records
  //===========================================================================
  public static List<Contact> getParentContactsList(List<Contact_team__c> listOfContactTeamRecords){

    List<Id> parentContactsOfContactTeamRecords = new List<Id>();
    List<Contact> parentContactsList = new List<Contact>();

    if (!listOfContactTeamRecords.isEmpty()) {
      for (Contact_Team__c contactTeamRecord : listOfContactTeamRecords) {
        parentContactsOfContactTeamRecords.add(contactTeamRecord.Contact__c);
      }
      parentContactsList = [
        SELECT Id, (
          SELECT Id, Relationship_Owner_is_Active__c, Relationship_Owner__r.IsActive FROM Contact_Teams__r
        )
        FROM Contact
        WHERE Id IN :parentContactsOfContactTeamRecords
      ];
    }
    return parentContactsList;
  }

  //===========================================================================
  // CRM2:W-005406: setUniqueBULead
  // When user is added system will check for existing BU Lead if exists it will replace
  // with new user and remove old otherwise set the new user
  //===========================================================================
  public static void setUniqueBULead (List<Contact_Team__c> newList) {

    Set<Id> primaryIdSet = new Set<Id>();
    Set<String> businessUnitSet = new Set<String>();
    Set<Id> contactIdSet = new Set<Id>();

    // Gets the BU and related contact for the Contact Team that will be later be used to filter existing records

    for (Contact_team__c newContactTeam : newList) {
      if (newContactTeam.Primary_User__c) {
        // Store a set of the inserted primary users....
        primaryIdSet.add(newContactTeam.Id);
      }
      if (newContactTeam.Business_Unit__c != null) {
        businessUnitSet.add(newContactTeam.Business_Unit__c);
      }
      if (newContactTeam.Contact__c != null) {
        contactIdSet.add(newContactTeam.Contact__c);
      }
    }

    if (!primaryIdSet.isEmpty()) {

      system.debug('Value for businessUnitSet:' + businessUnitSet);
      system.debug('Value for contactIdSet:' + contactIdSet);
  
      //List of Existing BU Leads on Contact
          
      List<Contact_Team__c> ctToUpdate = [
        SELECT Id, Primary_User__c, Business_Unit__c, Contact__c, Relationship_Owner__c
        FROM Contact_Team__c
        WHERE Primary_User__c = true
        AND Business_Unit__c IN :businessUnitSet
        AND Contact__c IN :contactIdSet
        AND Id NOT IN :primaryIdSet
      ];
  
      if (!ctToUpdate.isEmpty()) {
        for (Contact_Team__c ct : ctToUpdate) {
          ct.Primary_User__c = false;
        }
        try {
          update ctToUpdate;
        }
        catch (DMLException ex) {
          ApexLogHandler.createLogAndSave('ContactTeamTriggerHandler','setUniqueBULead', ex.getStackTraceString(), ex);
        }
      }
    }
  }
  
  //===========================================================================
  // CRM2:W-005409: applyContactFields
  // Add the user to the contact using the new Contact_Relationship_Team_IDs__c
  // to help filter contacts that user is on the team for
  //===========================================================================
  public static void applyContactFields(List<Contact_Team__c> newList, Boolean addEntry) {
    
    Set<Id> userIdSet = new Set<Id>();
    Set<Id> contactIdSet = new Set<Id>();
    
    // Build the sets to help get the contact fields needed and the user numbers
    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null && ct.Contact__c != null) {
        userIdSet.add(ct.Relationship_Owner__c);
        contactIdSet.add(ct.Contact__c);
      }
    }
    
    // Leave if any of these sets is empty. We cannot continue if they are!
    if (userIdSet.isEmpty() || contactIdSet.isEmpty()) {
      return;
    }
    
    Map<Id, User> usersToSet = new Map<Id, User>([
      SELECT Id, User_Hex_ID__c
      FROM User
      WHERE Id IN :userIdSet
    ]);
    
    Map<Id, Contact> contactsToCheck = new Map<Id, Contact>([
      SELECT Id, Contact_Relationship_Team_IDs__c
      FROM Contact
      WHERE Id IN :contactIdSet
    ]);
    
    String contString;
    String userNum;
    
    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null && ct.Contact__c != null) {
        // Get the unique number for the user of the inserted CT record
        userNum = usersToSet.get(ct.Relationship_Owner__c).User_Hex_ID__c;
        
        // If we have already 'seen' this contact, retrieve the current Contact_Relationship_Team_IDs__c field
        if (contactsToUpdate.containsKey(ct.Contact__c) && 
            String.isNotBlank(contactsToUpdate.get(ct.Contact__c).Contact_Relationship_Team_IDs__c)) {
          
          contString = contactsToUpdate.get(ct.Contact__c).Contact_Relationship_Team_IDs__c;
        }
        // Otherwise get the one from the query against the contact record.
        else {
          contString = contactsToCheck.get(ct.Contact__c).Contact_Relationship_Team_IDs__c;
        }
        if (addEntry) {
          // We are adding to the field...
          if (String.isEmpty(contString)) {
            // Always add to the field if it's empty.
            contString = userNum;
          }
          else {
            // Otherwise, check it doesn't ready have this users number
            if (!contString.contains(userNum)) {
              contString = (contString + ';' + userNum).replace(';;',';');
            }
          }
        }
        else {
          // We are removing the number from the field.
          if (String.isNotBlank(contString) && contString.contains(userNum)) {
            contString = contString.replace(userNum,'').replace(';;',';').removeEnd(';');
          }
        }
        setFieldToUpdate(ct.Contact__c, 'Contact_Relationship_Team_IDs__c',contString);
      }
    }
  } 
  
   //===========================================================================
  // Adding fix to set the owner of the inserted records to be the relationship
  // user, to allow them to be removed in future.
  //===========================================================================
  public static void setOwnerToRelationshipUser(List<Contact_Team__c> newList) {
    Set<Id> insertedUsers = new Set<Id>();

    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null && ct.OwnerId != ct.Relationship_Owner__c) {
        insertedUsers.add(ct.Relationship_Owner__c);
      }
    }

    if (insertedUsers.isEmpty()) {
      return;
    }
        
    Map<Id, User> checkActiveUsers = new Map<Id, User>([
      SELECT Id, IsActive
      FROM User
      WHERE Id IN :insertedUsers
    ]);

    for (Contact_Team__c ct : newList) {
      if (ct.Relationship_Owner__c != null) {
        if (checkActiveUsers.containsKey(ct.Relationship_Owner__c) && checkActiveUsers.get(ct.Relationship_Owner__c).IsActive) {
          ct.OwnerId = ct.Relationship_Owner__c;
        }
      }
    }
        
  }
  
  
}