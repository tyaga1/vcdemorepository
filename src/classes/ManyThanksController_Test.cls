/*=============================================================================
 * Experian
 * Name: ManyThanksController_Test
 * Description: 
 * Created Date: 19 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class ManyThanksController_Test {
  
  static testMethod void myUnitTest1() {
    
    ManyThanksController mtc = new ManyThanksController();
    PageReference vfPage = Page.ManyThanksAction;
    Test.setCurrentPage(vfPage);
    
    system.assertEquals(1, mtc.userIdMap.size());
    
    system.assertEquals(0, mtc.getUserIdMapSize());
    
    mtc.addAnother();
    system.assertEquals(1, mtc.userIdMap.size());
    
    mtc.addBadges();
    
    system.assert(ApexPages.getMessages().size() > 0); // errors with the user should be found. 
    
    mtc.userIdMap.put(0, UserInfo.getUserId());
    mtc.addAnother();
    system.assertEquals(2, mtc.userIdMap.size());
    system.assertEquals(1, mtc.getUserIdMapSize()); // This returns the last index key
    
    
    mtc.userIdMap.put(1, UserInfo.getUserId()); // Add the same user twice, should not save 2 badges!
    mtc.addBadges();
    
    system.assert(ApexPages.getMessages().size() > 0); // errors with the badge or message should be found. 
    
    system.assertEquals(1, mtc.allBadges.size()); // Should only find 1 badge
    
    mtc.badgeId = mtc.allBadges.get(0).Id;
    
    mtc.addBadges();
    
    system.assert(ApexPages.getMessages().size() > 0); // errors with the badge or message should be found.
    
    mtc.thanksMessage = 'Well done';
    
    mtc.addBadges();
    
    system.assertEquals(1, [SELECT COUNT() FROM WorkBadge WHERE RecipientId = :UserInfo.getUserId()]);
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    Document d = new Document(
      FolderId = UserInfo.getUserId(),
      Body = Blob.valueOf('this is actually an image!'),
      Description = 'Image',
      Name = 'Badge Image 1'
    );
    
    insert d;
    
    WorkBadgeDefinition badge1 = new WorkBadgeDefinition(
      Name = 'ABC (Always Be Closing!)',
      Description = 'Awesome Closer',
      IsCompanyWide = true,
      IsActive = true,
      ImageUrl = d.Id
    );
    
    insert badge1;
    
  }
  
}