/**=====================================================================
 * Appirio, Inc
 * Name: OrderSplitTriggerHandler
 * I-113908: Update Currency when inserting, currency should be same with parent
 * Created Date: Apr 30th, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * - COMMENTED OUT - 14th Sept. 2017              James Wills                  Case #13867785 Added code to create and update Order_Split_Product__c records.
 =====================================================================*/
public with sharing class OrderSplitTriggerHandler {
  //==========================================================================
  //On before insert trigger method
  //==========================================================================
  public static void onBeforeInsert(List<Order_Split__c> newList) {
    synchCurrencyWithParent(newList);
  }
  
  //==========================================================================
  //On after insert trigger method
  //==========================================================================
  public static void onAfterInsert(List<Order_Split__c> newList) {
    //createOrderSplitProducts(newList);
  }
  
  //==========================================================================
  //On after update trigger method
  //==========================================================================
  public static void onAfterUpdate(List<Order_Split__c> newList, Map<ID, Order_Split__c> oldMap) {
    //updateOrderSplitProducts(newList, oldMap);
  }
  
  

  //Update currencyISOCode
  private static void synchCurrencyWithParent(List<Order_Split__c> newList) {
    
    Set<ID> orderIDs = new Set<ID>();
    Map<ID, Order__c> orderMap = new Map<ID, Order__c>();
    //Adding orders in collection 
    for (Order_Split__c split : newList) {
        if (split.Order__c != null) {
            orderIDs.add(split.Order__c);
        }
    }
    
    //Querying orders (parent)
    for (Order__c ord : [SELECT ID, CurrencyISOCode FROM Order__c WHERE ID IN : orderIDs]) {
        orderMap.put(ord.ID, ord);
    }
    
    //Assigning currencyIsoCode form parent
    for (Order_Split__c split : newList) {
        if (orderMap.containsKey(split.Order__c) && split.CurrencyIsoCode != orderMap.get(split.Order__c).CurrencyISOCode) {
            split.CurrencyIsoCode = orderMap.get(split.Order__c).CurrencyISOCode;
        }
    }
  }
  
  /*public static void createOrderSplitProducts(List<Order_Split__c> newList){
    List<Order_Split_Product__c> osp_List = new List<Order_Split_Product__c>();
    List<id> orderIds = new List<id>();
    for(Order_Split__c os : newList){
      orderIds.add(os.Order__c);
    }
    
    Map<Id, Order__c> ord_Map = new Map<Id,Order__c>([SELECT id, (SELECT id, Product__r.id, Sales_Price__c FROM Order_Line_Items__r) FROM Order__c WHERE Id IN :orderIds]);
    
    for(Order_Split__c os : newList){      
              
      Order__c newOrder = ord_Map.get(os.Order__c);      

      if(!newOrder.Order_Line_Items__r.isEmpty()){      
        for(Order_Line_Item__c oli : newOrder.Order_Line_Items__r){
          Order_Split_Product__c newOsp = new Order_Split_Product__c();
        
          newOsp.Order__c           = os.Order__c;      
          newOsp.Product__c         = oli.Product__r.id;
          newOsp.Order_Split__c     = os.id;
          newOsp.Order_Line_Item__c = oli.id;

          osp_List.add(newOsp);
        }
      }
    }
    
    try{
      insert osp_List;
    } catch(Exception e){
    
    }
  }*/
  
  /*public static void updateOrderSplitProducts(List<Order_Split__c> newList, Map<ID, Order_Split__c>oldMap){
    List<Order_Split__c> newListForUpdate = new List<Order_Split__c>();    
    
    List<Order_Split_Product__c> ospid_List = [SELECT id, Order_Split__c FROM Order_Split_Product__c WHERE Order_Split__c IN :oldMap.KeySet()];
    Set<ID> ospid_Set = new Set<ID>();
    for(Order_Split_Product__c osp : ospid_List){
       ospid_Set.add(osp.Order_Split__c);
    }
    
    for(Order_Split__c os: newList){
      if(ospid_Set.contains(os.id)){
        //Do nothing
      } else {
        newListForUpdate.add(os);
      }
    }
    
    createOrderSplitProducts(newListForUpdate);
  }*/
  
  
  
}