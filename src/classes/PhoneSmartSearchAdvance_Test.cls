/**=====================================================================
 * Experian, Inc
 * Name: PhoneSmartSearchAdvance_Test
 * Description: Smart Search to Retrive Contacts Account and Leads using the Phone Number on them 
 * Created Date: Sep 11, 2015 
 * Created By: Paul Kissick
 * 
 * Date Modified                Modified By                  Description of the update
=====================================================================*/
@isTest
private class PhoneSmartSearchAdvance_Test {
  
  // First test lead searching
  static testMethod void testLeadPhoneSearch() {
    Lead l1 = Test_Utils.createLead();
    l1.Phone = '123 456 789';
    insert l1;
    
    PhoneSmartSearchAdvance ps = new PhoneSmartSearchAdvance();
    ps.searchstring = '123456789';
    ps.doQuery();
    system.assertEquals(1,ps.Led.size());
  }
  
  // Now test contact searching
  static testMethod void testContactPhoneSearch() {
    Account a1 = Test_Utils.insertAccount();
    Contact c1 = Test_Utils.createContact(a1.Id);
    c1.MobilePhone = '123 456 789';
    insert c1;
    
    PhoneSmartSearchAdvance ps = new PhoneSmartSearchAdvance();
    ps.searchstring = '123456789';
    ps.doQuery();
    system.assertEquals(1,ps.Con.size());
  }
  
  // Now test account searching
  static testMethod void testAccountPhoneSearch() {
    Account a1 = Test_Utils.createAccount();
    Account a2 = Test_Utils.createAccount();
    a1.Phone = '123 456 789';
    a2.Phone = '987 654 321';
    insert new List<Account>{a1,a2};
    
    PhoneSmartSearchAdvance ps = new PhoneSmartSearchAdvance();
    ps.searchstring = '123456789';
    ps.doQuery();
    system.assertEquals(1,ps.acnt.size());
  }
}