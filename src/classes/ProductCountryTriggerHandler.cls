/**=====================================================================
 *  * Name: ProductCountryTriggerHandler 
 * Description: Handler for Product_Country__c Trigger
 * Created Date: Oct 13th 2014
 * Created By: Richard
 * 
 * Business Case- To Sync Product Master details to Salesforce product2 and CPQ product objects.
 *
 * Date Modified                Modified By                  Description of the update
 * Feb. 13th 2017               James Wills                  W-006536 Generate/Delete Product Region record from Product Country
 * May 30th 2017                James Wills                  Case #02429766: Fix for when Product__c field not populated on new Region__c records.
 =====================================================================*/
 public class ProductCountryTriggerHandler {
 
  //===========================================================================
  // After Insert
  //===========================================================================
  public static void afterInsert(Map<ID, Product_Country__c> newMap) {
  
    processProductCountry(newMap.values());
      
    checkCreateProductRegion(newMap);
    
  }


  //===========================================================================
  //After Update
  //===========================================================================
  public static void afterUpdate(Map<ID, Product_Country__c> newMap, Map<ID, Product_Country__c> oldMap) {
  
    processProductCountry(newMap.values());
   
  }
 
  //===========================================================================
  //After Delete
  //===========================================================================
  public static void afterDelete(Map<ID, Product_Country__c> oldMap) {
  
    checkDeleteProductRegion(oldMap);
   
  }
 
  //===========================================================================
  //To run an update to SYNC product master data.
  //===========================================================================
  public static void processProductCountry(List<Product_Country__c > ProductCountryList){
 
    if(!ProductMasterUpdateHelper.isRecursive){
      set<Id> updateProductMasterIDSet = new set<Id>();
                  
      for(Product_Country__c ProductCountryRec :ProductCountryList){
        if(ProductCountryRec.Product_Master__c != null  ){
          updateProductMasterIDSet.add(ProductCountryRec.Product_Master__c);
        }
      }    
 
      if(updateProductMasterIDSet.size()>0){
        ProductMasterUpdateHelper.updateProductMaster(updateProductMasterIDSet);
      }        
    }
  }
 
  public static void checkCreateProductRegion(Map<ID, Product_Country__c> newMap){
    List<ID> pmidList = new List<ID>();
    
    for(Product_Country__c pc : newMap.values()){
      if(pc.Product_Master__c!=null){
        pmidList.add(pc.Product_Master__c);
      }
    }
   
    List<Product_Country__c> pcList = [SELECT id, Product__c, Product_Master__c, Product_Master__r.Drmid__c, Country__r.Region__c FROM Product_Country__c WHERE id IN :newMap.values()];
    
    Map<ID,Set<ID>> productMasterByProductRegionPotentials_Map = new Map<ID,Set<ID>>();
    Map<ID,Set<ID>> productMasterByProductRegionActuals_Map = new Map<ID,Set<ID>>();
    Map<ID,String> productMasterByDrmid_Map = new Map<ID,String>();
    Map<ID,ID> productByProductMaster_Map = new Map<ID,ID>();
    for(Product_Country__c pc : pcList){
      Set<ID> potentialsRegion_Set;
      if(productMasterByProductRegionPotentials_Map.get(pc.Product_Master__c)!=null){
        potentialsRegion_Set = productMasterByProductRegionPotentials_Map.get(pc.Product_Master__c);
        potentialsRegion_Set.add(pc.Country__r.Region__c);
      } else {
        potentialsRegion_Set = new Set<ID>{pc.Country__r.Region__c};
      }
      productMasterByProductRegionPotentials_Map.put(pc.Product_Master__c, potentialsRegion_Set);
      productMasterByDrmid_Map.put(pc.Product_Master__c, pc.Product_Master__r.Drmid__c);
      if(pc.Product__c!=null){
        productByProductMaster_Map.put(pc.Product_Master__c, pc.Product__c);
      }
    }
    productMasterByProductRegionActuals_Map = productMasterByProductRegionPotentials_Map;
   
   
    List<Product_Region__c> prList = [SELECT id, Product_Master__c, Region__c FROM Product_Region__c WHERE Product_Master__c IN :pmidList];        
    
    Map<ID,Set<ID>> productMasterByProductRegionSaved_Map = new Map<ID,Set<ID>>();
    for(Product_Region__c pr : prList){
      Set<ID> savedRegion_Set;
      if(productMasterByProductRegionSaved_Map.get(pr.Product_Master__c)!=null){
        savedRegion_Set = productMasterByProductRegionSaved_Map.get(pr.Product_Master__c);
        savedRegion_Set.add(pr.Region__c);
      } else {
        savedRegion_Set = new Set<ID>{pr.Region__c};
      }        
      productMasterByProductRegionSaved_Map.put(pr.Product_Master__c, savedRegion_Set);    
    }
    
    for(ID pmidForRegion: productMasterByProductRegionSaved_Map.keySet()){
      Set<ID> savedReg_Set = productMasterByProductRegionSaved_Map.get(pmidForRegion);
      Set<ID> actualsReg_Set = productMasterByProductRegionActuals_Map.get(pmidForRegion);
      for(ID reg : savedReg_Set){
        if(actualsReg_Set.contains(reg)){
          actualsReg_Set.remove(reg);
        }
      }
      productMasterByProductRegionActuals_Map.put(pmidForRegion, actualsReg_Set);
    }

    List<Product_Region__c> prInsertList = new List<Product_Region__c>();
    
    for(ID idForPRList : productMasterByProductRegionActuals_Map.keySet()){
      Set<ID> actualsRegSet2 = productMasterByProductRegionActuals_Map.get(idForPRList);
      for(ID reg2 : actualsRegSet2){

        Product_Region__c prtoAdd = new Product_Region__c(Drmid__c = productMasterByDrmid_Map.get(idForPRList),
                                                         Product_Master__c = idForPRList,
                                                         Region__c = reg2
                                                        );
        if(productByProductMaster_Map.containsKey(idForPRList)){
           prToAdd.Product__c = productByProductMaster_Map.get(idForPRList);//02429766
        }                                                
        prInsertList.add(prtoAdd);
      }
    }
    
    if(!prInsertList.isEmpty()){
      try{
        insert prInsertList;
      } catch(DMLException e){
        ApexLogHandler.createLogAndSave('ProductCountryTriggerHandler','checkCreateProductRegion', e.getStackTraceString(), e);
      }        
    }
    
  }
 

  public static void checkDeleteProductRegion(Map<ID, Product_Country__c> oldMap){
    List<ID> pmidList = new List<ID>();
    
    for(Product_Country__c pc : oldMap.values()){
      if(pc.Product_Master__c!=null){
        pmidList.add(pc.Product_Master__c);
      }
    }
       
    List<Product_Country__c> pcList = [SELECT id, Product_Master__c, Country__r.Region__c FROM Product_Country__c WHERE Product_Master__c IN :pmidList];
  
    Map<ID,Set<ID>> productRegionFromProdCountry_Map   = new Map<ID,Set<ID>>();        
    for(Product_Country__c pc: pcList){
      Set<ID> savedRegion_Set;
      if(productRegionFromProdCountry_Map.get(pc.Product_Master__c)!=null){
        savedRegion_Set = productRegionFromProdCountry_Map.get(pc.Product_Master__c);
        savedRegion_Set.add(pc.Country__r.Region__c);
      } else {
        savedRegion_Set = new Set<ID>{pc.Country__r.Region__c};
      }
      productRegionFromProdCountry_Map.put(pc.Product_Master__c, savedRegion_Set);
    }
    
    List<Product_Region__c> prList = [SELECT id, Region__c, Product_Master__c FROM Product_Region__c WHERE Product_Master__c IN :pmidList];
    
    List<Product_Region__c> productRegionToDelete_List = new List<Product_Region__c>();
    for(Product_Region__c pr: prList){
      if(productRegionFromProdCountry_Map.get(pr.Product_Master__c)!=null){
        Set<ID> validRegions =productRegionFromProdCountry_Map.get(pr.Product_Master__c);
        //If the Product Region has a region that is not in the list of valid regions
        if(validRegions.contains(pr.Region__c)!=true){
          productRegionToDelete_List.add(pr);
        }
        //The Product Region does not have any Product Countries - so add the region
      } else {
        productRegionToDelete_List.add(pr);
      }
    }
        
    if(!productRegionToDelete_List.isEmpty()){
      try{
        delete productRegionToDelete_List;
      } catch(DMLException e){
        ApexLogHandler.createLogAndSave('ProductCountryTriggerHandler','checkDeleteProductRegion', e.getStackTraceString(), e);
      }        
    }
    
  }

 
 }