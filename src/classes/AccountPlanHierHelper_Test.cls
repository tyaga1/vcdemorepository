/**=====================================================================
 * Appirio, Inc
 * Name: AccountPlanHierHelper_Test
 * Description: Test Class for AccountHierController.cls
 * Created Date: Apr 18th, 2016
 * Created By    : Tyaga Pati(Experian)
 * 
 * Date Modified      Modified By                 Description of the update
 * Apr 18th, 2016     Tyaga Pati                  
 ======================================================================**/
 @isTest
 public class AccountPlanHierHelper_Test{
 // Scenario 1: Test to Ensure the OpenOpps Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_HierarchyHtmlBuildFunction(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
         Test.startTest();
            //Step1: Insert Child Account
              Account Childacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Parent Account  
              Account Parentacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.  
            //Step3: Insert Ultimate Parent Account
              Account UltimateParacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
              UltimateParacc.Name = 'Ultimate Parent Account';
              Update UltimateParacc;
              Childacc.Parent = Parentacc;
              Childacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Childacc.Name = 'Child Account';
              Update Childacc;
              system.debug('Tyaga the ultimate Parent is 111 ' +Childacc.Ultimate_Parent_Account__c + ' and the account is ' + Childacc);
              Parentacc.Parent = UltimateParacc;
              Parentacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Parentacc.Name = 'Parent Account';
              Update Parentacc;
              //Following 4 statements are only needed for testign the highest level account function
              List<String> AccStringLst = New List<String>();
              AccStringLst.add(Childacc.Id);
              AccStringLst.add(Parentacc.Id);
              AccStringLst.add(UltimateParacc.Id); 
              //following statements are only needed for testing the save function
              String str1 ='1-'+ Childacc.Id + ',';
              String str2 ='2-'+ Childacc.Id + ',';
              String str3 = '3-'+ UltimateParacc.Id;
              String selectedAcnt = str1 + str2  + str3 ;
              List<Account> masterAcntList = New List<Account>();
              for(Account ac: [select Id ,Ultimate_Parent_Account__c,Region__C, Country_of_Registration__c, Ultimate_Parent_Account_ID__c, parentId, Name 
                                     from Account  
                                     where Id in :AccStringLst]){
              
                  masterAcntList.add(ac);   
              }   
              masterAcntList.sort();
              //Test Case 2: Test the Wrapper Class. 
              Account acnt = UltimateParacc;
              Integer Level = 1;
              List<Account> ChildAcnts = New List<Account>();
              List<Account> ChildAcntSet = New List<Account>();
              ChildAcnts.add(Parentacc);
              ChildAcnts.add(Childacc); 
              ChildAcntSet.add(Childacc);//just one child
              AccountPlanHierHelper HelperClass = new AccountPlanHierHelper();
              
              // Function Call for Test Case 1 : createPlanTreeHierarchy    
              String tempTree = AccountPlanHierHelper.createPlanTreeHierarchy(masterAcntList,UltimateParacc.Id); 
              // Function Call for Test Case 2 : AriaHierarchy 
              AccountPlanHierHelper.AriaHierarchy newWrapperClass = New AccountPlanHierHelper.AriaHierarchy(acnt, 1,ChildAcnts);
              //function Call for Test Case 3 : createMapTreeOrder
              Map<Id, Set<Id>> mapPlansRelationshipIds = new Map<Id, Set<Id>>();
              Set<Id> Idset1 = New Set<Id>();
              Idset1.add(Parentacc.Id);
              mapPlansRelationshipIds.put(UltimateParacc.Id,Idset1);
              Set<Id> Idset2 = New Set<Id>();
              Idset2.add(Childacc.Id);
              mapPlansRelationshipIds.put(Parentacc.Id,Idset2);
              Map<Id, Id> mapChildToParent = new Map<Id, Id>() ;
              mapChildToParent.put(Childacc.Id,Parentacc.Id);
              mapChildToParent.put(Parentacc.Id,UltimateParacc.Id);
              Map<Id, Account> mapIdToAccount = new Map<Id, Account>();
              for (Account ac : ([SELECT Id ,Ultimate_Parent_Account__c, Region__C, Country_of_Registration__c,Ultimate_Parent_Account_ID__c, parentId, Name 
                        FROM Account  
                        WHERE Id in :AccStringLst])) {
                          if (!mapIdToAccount.containsKey(ac.ID)) {
                            mapIdToAccount.put(ac.ID, ac);
                          }                                             
              }
              
              Map<Integer,List<AccountPlanHierHelper.AriaHierarchy>> MapForHtmlBuild = new Map<Integer,List<AccountPlanHierHelper.AriaHierarchy>> ();            
              MapForHtmlBuild = AccountPlanHierHelper.createMapTreeOrder(masterAcntList,mapPlansRelationshipIds,mapChildToParent,mapIdToAccount,UltimateParacc.Id);            
             //function Call for Test Case 4 : CreateTreeView
              Map<Id, Integer> mapOrder = new Map<Id, Integer>();
              String TempTreeView = AccountPlanHierHelper.CreateTreeView(MapForHtmlBuild,mapOrder,mapIdToAccount,mapPlansRelationshipIds);            
             //function Call for Test Case 5 : childTreeString
              String TempChildString = AccountPlanHierHelper.childTreeStringCreator(ChildAcntSet,mapIdToAccount);       
             //function Call for Test Case 5 : childTreeString
             List<Account> TestAcntList = AccountPlanHierHelper.createChildPlans(mapPlansRelationshipIds.get(Parentacc.Id),mapIdToAccount);    
         
         Test.stopTest(); 
         //Assert for Test case 1 :  createPlanTreeHierarchy function.   
         system.assertEquals(tempTree <> null, true);
         //Asset to test wrapper class
         system.assertEquals(newWrapperClass <> null, true);
         //Assert Statement for createMapTreeOrder function
         system.assertEquals(MapForHtmlBuild <> null, true);
         //Assert Statement for CreateTreeView function
         system.assertEquals(TempTreeView <> null, true);
         //Assert Statement for childTreeStringCreator function
         system.assertEquals(TempChildString <> null, true);
         //Assert Statement for createChildPlans function
         system.assertEquals(TestAcntList <> null, true);
      }
    }//End of test_OpenOpps_Prop

}