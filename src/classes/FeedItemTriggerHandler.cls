/**=====================================================================
 * Name: FeedItemTriggerHandler
 * Description:
 * Created Date: Aug 14th, 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified                Modified By       Description of the update
 /**=====================================================================*/
 
 public without sharing class FeedItemTriggerHandler {
 
    //Before Delete Call
    public static void beforeDelete (List<FeedItem> oldList, Boolean isDataAdmin) {
    
        if (isDataAdmin == false){
            ID currentUserId = UserInfo.getUserId();  
            ID currentUserProfId = UserInfo.getProfileId();
            Boolean isAdmin = false;
            
            String profileName = [SELECT Name FROM Profile WHERE Id = :currentUserProfId].Name;
            // Check if the User belongs to certain profiles and avoid the validations - Certified Sales Admin or Sales Effectiveness
            if(profileName == Constants.PROFILE_EXP_CERT_SALES_ADMIN || 
               profileName == Constants.PROFILE_EXP_SALES_ADMIN || 
               profileName == Constants.PROFILE_SYS_ADMIN || 
               profileName == Constants.PROFILE_EXP_DQ_ADMIN){
               isAdmin = true;      
            }
            
            if (!isAdmin){
                for(FeedItem fi : oldList){
                    if( currentUserId != fi.CreatedById){
                      fi.addError('You cannot delete a post you did not create.');
                    }
                }
            }
        }
    }
 
 }