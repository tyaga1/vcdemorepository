/******************************************************************************
 * Appirio, Inc
 * Name: BatchRenameAccountSegments_Test.cls
 * Description: 
 *
 * Created Date: Sep 9th, 2015.
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 *
 ******************************************************************************/
@isTest(seeAllData = false)
private class BatchRenameAccountSegments_Test {
  static Hierarchy__c hrchy1, hrchy2, hrchy3, hrchy4, hrchy5;
  
  public static testmethod void testBatch() {
    
    List<Hierarchy__c> listHierarchy = new List<Hierarchy__c>();
       
    hrchy1 = [SELECT Id, Apply_Name_Change_to_Account_Segments__c, Old_Value__c, Value__c
                             FROM Hierarchy__c WHERE Type__c =: 'Global Business Line'];
    hrchy1.Value__c = 'test 1';
    hrchy1.Apply_Name_Change_to_Account_Segments__c = true;
    listHierarchy.add(hrchy1);
    
    hrchy2 = [SELECT Id, Apply_Name_Change_to_Account_Segments__c, Old_Value__c, Value__c
                             FROM Hierarchy__c WHERE Type__c =: 'Country'];
    hrchy2.Value__c = 'test 2';
    hrchy2.Apply_Name_Change_to_Account_Segments__c = true;
    listHierarchy.add(hrchy2);
    
    update listHierarchy ;
    
    // start Test
    Test.startTest();
    BatchRenameAccountSegments batch = new BatchRenameAccountSegments();
    Database.executeBatch(batch);
    
    // stop test
    Test.stopTest();
    
    //Asserts
    List<Account_Segment__c> lstAccSeg = [SELECT Id, Account__c, Segment__r.Processed_Renaming__c, Segment__r.Old_Value__c, 
                                            Segment__r.Type__c, Segment__r.Value__c, Segment__c, Type__c, Value__c
                                          FROM Account_Segment__c 
                                          WHERE Segment__c IN :listHierarchy ];
    for (Account_Segment__c accSeg : lstAccSeg) {
      system.assertEquals(accSeg.Value__c, accSeg.Segment__r.Value__c);                                       
    }
    
    for (Hierarchy__c hrchy : listHierarchy){
      system.assertEquals(hrchy.Old_Value__c, null);
    }
  }
  
  @testSetup
  private static void createData () {
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getOrganizationId(), IsDataAdmin__c = true);
    insert ida;
    User newUsr = Test_Utils.createUser('System Administrator');
    newUsr.FirstName = 'testing User For AccSegment';
    newUsr.Country__c = 'test-Country';
    newUsr.Business_Line__c = 'test-BL';
    newUsr.Business_Unit__c = 'test-BU';
    newUsr.Region__c = 'test-Region';
    newUsr.Global_Business_Line__c = 'test-GBL';
    newUsr.CPQ_User_Type__c = 'CPQ Admin';
    insert newUsr;
    
    List<Account> accList = new List<Account>();
    Account acc = Test_Utils.createAccount();
    accList.add(acc);
    Account acc2 = Test_Utils.createAccount();
    accList.add(acc2);
    insert accList;

     List<Hierarchy__c> listHierarchies = new List<Hierarchy__c>();
     Hierarchy__c hierarchy_GlobalBusinessLine = Test_Utils.insertHierarchy(false, null, 'test-GBL', 'Global Business Line');
     hierarchy_GlobalBusinessLine.Name = hierarchy_GlobalBusinessLine.Type__c + '-' + hierarchy_GlobalBusinessLine.Value__c; // WF is turned off
     hierarchy_GlobalBusinessLine.Unique_Key__c = hierarchy_GlobalBusinessLine.Type__c + '-' + hierarchy_GlobalBusinessLine.Value__c; // WF is turned off
     listHierarchies.add(hierarchy_GlobalBusinessLine);
     Hierarchy__c hierarchy_BusinessUnit = Test_Utils.insertHierarchy(false, null, 'test-BU', 'Business Unit');
     hierarchy_BusinessUnit.Name = hierarchy_BusinessUnit.Type__c + '-' + hierarchy_BusinessUnit.Value__c; // WF is turned off
     hierarchy_BusinessUnit.Unique_Key__c = hierarchy_BusinessUnit.Type__c + '-' + hierarchy_BusinessUnit.Value__c; // WF is turned off
     listHierarchies.add(hierarchy_BusinessUnit);
     Hierarchy__c hierarchy_BusinessLine = Test_Utils.insertHierarchy(false, null, 'test-BL', 'Business Line');
     hierarchy_BusinessLine.Name = hierarchy_BusinessLine.Type__c + '-' + hierarchy_BusinessLine.Value__c; // WF is turned off
     hierarchy_BusinessLine.Unique_Key__c = hierarchy_BusinessLine.Type__c + '-' + hierarchy_BusinessLine.Value__c; // WF is turned off
     listHierarchies.add(hierarchy_BusinessLine);
     Hierarchy__c hierarchy_Country = Test_Utils.insertHierarchy(false, null, 'test-Country', 'Country');
     hierarchy_Country.Name = hierarchy_Country.Type__c + '-' + hierarchy_Country.Value__c; // WF is turned off
     hierarchy_Country.Unique_Key__c = hierarchy_Country.Type__c + '-' + hierarchy_Country.Value__c; // WF is turned off
     listHierarchies.add(hierarchy_Country);
     Hierarchy__c hierarchy_Region = Test_Utils.insertHierarchy(false, null, 'test-Region', 'Region');
     hierarchy_Region.Name = hierarchy_Region.Type__c + '-' + hierarchy_Region.Value__c; // WF is turned off
     hierarchy_Region.Unique_Key__c = hierarchy_Region.Type__c + '-' + hierarchy_Region.Value__c; // WF is turned off
     listHierarchies.add(hierarchy_Region);

     insert listHierarchies;

    for (Hierarchy__c hr: listHierarchies) {
      system.debug(hr);
    }

    // Insert Account Segment
    List<Account_Segment__c> listAccSegments = new List<Account_Segment__c>();
    Account_Segment__c accSegment_BusinessUnit = Test_Utils.insertAccountSegment(false, acc.Id, hierarchy_BusinessUnit.Id, null);
    accSegment_BusinessUnit.Value__c = 'test-BU';
    accSegment_BusinessUnit.Type__c = 'Business Unit';
    listAccSegments.add(accSegment_BusinessUnit);

    Account_Segment__c accSegment_BusinessLine = Test_Utils.insertAccountSegment(false, acc.Id, hierarchy_BusinessLine.Id, null);
    accSegment_BusinessLine.Value__c = 'test-BL';
    accSegment_BusinessLine.Type__c = 'Business Line';
    listAccSegments.add(accSegment_BusinessLine);

    Account_Segment__c accSegment_Country = Test_Utils.insertAccountSegment(false, acc.Id, hierarchy_Country.Id, null);
    accSegment_Country.Value__c = 'test-Country';
    accSegment_Country.Type__c = 'Country';
    listAccSegments.add(accSegment_Country);

    Account_Segment__c accSegment_GlobalBusinessLine = Test_Utils.insertAccountSegment(false, acc.Id, hierarchy_GlobalBusinessLine.Id, null);
    accSegment_GlobalBusinessLine.Value__c = 'test-GBL';
    accSegment_GlobalBusinessLine.Type__c = 'Global Business Line';
    listAccSegments.add(accSegment_GlobalBusinessLine);

    Account_Segment__c accSegment_Region = Test_Utils.insertAccountSegment(false, acc.Id, hierarchy_Region.Id, null);
    accSegment_Region.Value__c = 'test-Region';
    accSegment_Region.Type__c = 'Region';
    listAccSegments.add(accSegment_Region);
    
    insert listAccSegments;

  }

}