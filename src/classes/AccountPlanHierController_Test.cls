/**=====================================================================
 * Experian . Inc
 * Name: AccountPlanHierController_Test
 * Description: T-324658: Test Class for AccountPlanHierController.cls
 * Created Date: Apr 18th, 2016
 * Created By    : Tyaga Pati(Experian)
 * 
 * Date Modified      Modified By                 Description of the update
 * Apr 18th, 2016     Tyaga Pati                  
 * Jun 21st, 2016     Tyaga Pati              Case 01976432 : Test Edit Functionality.       
 ======================================================================**/
 @isTest
 public class AccountPlanHierController_Test{
 // Scenario 1: Test to Ensure the OpenOpps Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_TotalAccnt_Count(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
         Test.startTest();
            //Step1: Insert Child Account
              Account Childacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Parent Account  
              Account Parentacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.  
            //Step3: Insert Ultimate Parent Account
              Account UltimateParacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
              UltimateParacc.Name = 'Ultimate Parent Account';
              Update UltimateParacc;
              Childacc.ParentId = Parentacc.Id;
              Childacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Childacc.Name = 'Child Account';
              Update Childacc;
              system.debug('Tyaga the ultimate Parent is 111 ' +Childacc.Ultimate_Parent_Account__c + ' and the account is ' + Childacc);
              Parentacc.Parent = UltimateParacc;
              Parentacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Parentacc.Name = 'Parent Account';
              Update Parentacc;
              //Following 4 statements are only needed for testign the highest level account function
                  List<String> AccStringLst = New List<String>();
                  AccStringLst.add(Childacc.Id);
                  AccStringLst.add(Parentacc.Id);
                  AccStringLst.add(UltimateParacc.Id); 
                  
              //following statements are only needed for testing the save function
              String str1 ='1-'+ Childacc.Id + ',';
              String str2 ='2-'+ Childacc.Id + ',';
             // String str3 = '3-'+ UltimateParacc.Id;
             // String selectedAcnt = str1 + str2  + str3 ;
              String selectedAcnt = str1 + str2 ;
                 
              //Test for the Total Account in Hier Count Function
              ApexPages.currentPage().getParameters().put('selectedAcnts',selectedAcnt);  
              ApexPages.currentPage().getParameters().put('Id', Childacc.Id); 
              ApexPages.standardController testController = New ApexPages.standardController(Childacc);
              AccountPlanHierController controller1 = new AccountPlanHierController(testController);    
              Integer AccountPlanCount = controller1.getTotalCountOfElements();
              
              //Test for Hierarchy Html Count Function
              String AccountHierTree = controller1.getAcntTreeHierarchy();
              
              //Test for Highest Level Account Function
              Account HighestLvlAccnt = controller1.FindHighestLeveAcntSelected(AccStringLst,Childacc.Id);   
              
              //Test the Save Function to generate the Account plan
             PageReference pr = controller1.saveAccountsForPlan(); 
              
              //Test the Back Button 
             PageReference pr1 = controller1.GoBacktoAccountPage(); 
              
             Test.stopTest(); 

         //Assert for the Testing the Count function   
         System.assertEquals(AccountPlanCount >0, true);
         //Assert for the Hierarchy funciton
         System.assertEquals(AccountHierTree !=null, true);
         //Assert for the Highest Level Account function
         System.assertEquals(HighestLvlAccnt <> null, true);
         //Assert for the Save function
        // System.assertEquals(pr <> null, true);
         //Assert for the Back Button 
        // System.assertEquals(pr1 <> null, true);
      }
    }//End of test_OpenOpps_Prop
// Scenario 2: Test to Ensure the OpenOpps Prop has the Open Opportunities List Inside it.
    @isTest//(SeeAllData=true)
      public static void test_AcntPlan_Update(){
       
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
         Test.startTest();
            //Step1: Insert Child Account
              Account Childacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step2: Insert Parent Account  
              Account Parentacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.  
            //Step3: Insert Ultimate Parent Account
              Account UltimateParacc = Test_Utils.insertAccount();//Account Created with Name as Test Account appended with a Randon Number.
            //Step4: Replacement Accunt To Test Edit Functionality.
              Account ReplaceAcnt = Test_Utils.insertAccount(); 
              UltimateParacc.Name = 'Ultimate Parent Account';
              Update UltimateParacc;
              Childacc.ParentId = Parentacc.Id;
              Childacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Childacc.Name = 'Child Account';
              Update Childacc;
              system.debug('Tyaga the ultimate Parent is 111 ' +Childacc.Ultimate_Parent_Account__c + ' and the account is ' + Childacc);
              Parentacc.Parent = UltimateParacc;
              Parentacc.Ultimate_Parent_Account__c = UltimateParacc.Id;
              Parentacc.Name = 'Parent Account';
              Update Parentacc;
              //Following 4 statements are only needed for testign the highest level account function
              List<String> AccStringLst = New List<String>();
              AccStringLst.add(Childacc.Id);
              AccStringLst.add(Parentacc.Id);
              AccStringLst.add(UltimateParacc.Id);     
              //following statements are only needed for testing the save function
              String str1 ='1-'+ Childacc.Id + ',';
              String str2 ='2-'+ Parentacc.Id + ',';
              String str3 = '3-'+ UltimateParacc.Id;
              //String selectedAcnt = str1 + str2  + str3 ;
              String selectedAcnt = str1;
              //Test for the Total Account in Hier Count Function
              ApexPages.currentPage().getParameters().put('selectedAcnts',selectedAcnt);
              ApexPages.currentPage().getParameters().put('Id', Childacc.Id); 
              ApexPages.standardController testController = New ApexPages.standardController(Childacc);
              AccountPlanHierController controller1 = new AccountPlanHierController(testController);
              //Test the Save Function to generate the Account plan
              PageReference pr = controller1.saveAccountsForPlan();
             //String newAcntPlanIdstr = pr.getParameters().get('id');
              Id newAcntPlanId = [select Id from Account_plan__c where Id = :pr.getParameters().get('id')].Id;
             //Now Run the Edit plan Code:
              String selectedAcnt1 = Childacc.Id + ','+ Parentacc.Id;
              //String selectedAcnt2 = str1 + str2;
              String selectedAcnt2 = str1;
              ApexPages.currentPage().getParameters().put('selectedAcnt',selectedAcnt2);
              ApexPages.currentPage().getParameters().put('existingAcntLstOnPlan',selectedAcnt1);
              ApexPages.currentPage().getParameters().put('acntPlanIdtoUpdate', newAcntPlanId);
             //Save the Edited plan
              PageReference pr1 = controller1.saveAccountsForPlan();
             //Test the Back Button
             //PageReference pr2 = controller1.GoBacktoAccountPage();
              List<Account_Account_Plan_Junction__c> acPlanAcntList = [select Id from Account_Account_Plan_Junction__c where Account_plan__C = :pr1.getParameters().get('id')];
              Test.stopTest();
          System.assertEquals(acPlanAcntList.size()==1, true);


      }
    }//End of test_AcntPlan_Update

}