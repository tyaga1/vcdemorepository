/******************************************************************************
 * Name: BatchCreateNewSICrossReferenceRecords
 * Description: This Class will create SalesInsight_Cross_Reference__c records and associate them with matched Accounts where an equivalent record does not exist
 *              following the DQ matching process in the Account Staging table.
 * Created Date: 5th April , 2017
 * Created By: James Wills
 *
 * Date Modified                Modified By                  Description of the update
 * 05/04/2017                   James Wills                  DRM:W-007488: Created.
 * 29/05/2017                   James Wills                  Updated.
 ******************************************************************************/
global class BatchCreateNewSICrossReferenceRecords implements Database.Batchable<sObject>, Database.Stateful {
      
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    
    return Database.getQueryLocator([SELECT id, Name, Assignment_Team__c, Account__c FROM Account_Assignment_Team__c]);
    
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
 
    Map<ID,ID> assignmentTeamMap_BIS = new Map<ID,ID>();
    Map<ID,ID> assignmentTeamMap_CIS = new Map<ID,ID>();
    Map<ID,String> accountMap_BIS = new Map<ID,String>();
    Map<ID,String> accountMap_CIS = new Map<ID,String>();
    Map<ID,String> accountMapForBISAccountExecutives = new Map<ID,String>();
    Map<ID,String> accountMapForCISAccountExecutives = new Map<ID,String>();
    
    List<ID> aatIDList = new List<ID>();
    for(Account_Assignment_Team__c aat : (List<Account_Assignment_Team__c>)scope){
      aatIDList.add(aat.Assignment_Team__c);      
    }
    
    List<Assignment_Team__c> atList = [SELECT id, Name, Account_Executive__c
                                      FROM Assignment_Team__c 
                                      WHERE id IN :aatIDList
                                      AND ((Name LIKE 'BIS - Growth%' OR Name LIKE 'BIS - Inside%') OR
                                           (Name LIKE 'CIS - Growth%' OR Name LIKE 'CIS - Inside%'))];   
    
    
    
    for(Assignment_Team__c at : atList){
      if(at.Name.LEFT(3)=='BIS'){
        assignmentTeamMap_BIS.put(at.id, at.Account_Executive__c);
      } else if(at.Name.LEFT(3)=='CIS'){
        assignmentTeamMap_CIS.put(at.id, at.Account_Executive__c);
      }
    }

    Map<ID, User> accountExecutiveList = new Map<ID,User>([SELECT id, UserRole.Name FROM User WHERE id IN :assignmentTeamMap_BIS.values() OR id IN :assignmentTeamMap_CIS.values()]);

    for(Account_Assignment_Team__c aat : (List<Account_Assignment_Team__c>)scope){
      for(User u : accountExecutiveList.values()){
        if(u.id==assignmentTeamMap_BIS.get(aat.Assignment_Team__c)){
           accountMap_BIS.put(aat.Account__c, u.UserRole.Name);
           accountMapForBISAccountExecutives.put(aat.Account__c, u.id);
           break;                 
        } else if(u.id==assignmentTeamMap_CIS.get(aat.Assignment_Team__c)){
           accountMap_CIS.put(aat.Account__c, u.UserRole.Name); 
           accountMapForCISAccountExecutives.put(aat.Account__c, u.id);
           break;
        }
      }
    }


    if(accountMap_BIS.isEmpty()==false){
      List<Account> accList1 = [SELECT id, AE_Assignment_Team_Role_BIS__c FROM Account WHERE id IN :accountMap_BIS.keySet()];
      for(Account acc : accList1){
        //acc.AE_Assignment_Team_Role_BIS__c = accountMap_BIS.get(acc.id);
        acc.BIS_Account_Executive__c = accountMapForBISAccountExecutives.get(acc.id);
      }
        
      try{
        update accList1;
      } catch(Exception e){
        system.debug('\n[AssignmentTeamMemberTriggerHandler: updateAssignmentTeamMemberRoleFieldsOnAccount]: ['+e.getMessage()+']]');
        apexLogHandler.createLogAndSave('AssignmentTeamMemberTriggerHandler','updateAssignmentTeamMemberRoleFieldsOnAccount', e.getStackTraceString(), e);
      } 
    }

    if(accountMap_CIS.isEmpty()==false){
      List<Account> accList2 = [SELECT id, AE_Assignment_Team_Role_CIS__c FROM Account WHERE id IN :accountMap_CIS.keySet()];
      for(Account acc : accList2){
        //acc.AE_Assignment_Team_Role_CIS__c = accountMap_CIS.get(acc.id);
        acc.CIS_Account_Executive__c = accountMapForCISAccountExecutives.get(acc.id);
      }
      
      try{
        update accList2;
      } catch(Exception e){
        system.debug('\n[AssignmentTeamMemberTriggerHandler: updateAssignmentTeamMemberRoleFieldsOnAccount]: ['+e.getMessage()+']]');
        apexLogHandler.createLogAndSave('AssignmentTeamMemberTriggerHandler','updateAssignmentTeamMemberRoleFieldsOnAccount', e.getStackTraceString(), e);
      } 
    }
   
  }
  
 
  
  
  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    
  }

}