/**=====================================================================
 * Experian, Inc
 * Name: ControllerCustomSettings_Test
 * Description: Test class for ControllerCustomSettings
 *
 * Created Date: Feb 3rd, 2016
 * Created By: Tyaga Pati (Experian)
 *
 * Date Modified            Modified By              Description of the update
 ======================================================================*/
@isTest
public class ControllerCustomSettings_Test {
    @isTest//(SeeAllData=true)
    public static void test_TotalAccnt_Count(){
        // create Admin Settings data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        system.runAs(testUser1) {
        Test.startTest();
           //Step1: Insert Child Account
            AdminMessages__c AdmSettings1 = new AdminMessages__c();
            AdmSettings1.Is_Message_Active__c= true;
            AdmSettings1.Header__c='Test Header';
            AdmSettings1.Admin_Message__c='A test message'; 
            Insert AdmSettings1;      
            ControllerCustomSettings controller1 = new ControllerCustomSettings();
            Boolean IsActiveFlag;
            String Msghdr ;
            String DisplayMessage ;
            PageReference pr = controller1.save();
            Test.stopTest(); 
                
         //Assert for the Testing the Count function   
         System.assertEquals(controller1.msghdr !=null, true);
         System.assertEquals(controller1.DisplayMessage !=null, true);
         System.assertEquals(controller1.IsActiveFlag !=null, true);
      }
    }//End of test_OpenOpps_Prop






}