/**
* @description A page controller for IT Service Knowledge visualforce page
* This controller controls what languages of IT Service articles will be displaying to the currently logged in user
* This controller also group articles by their selected date category and sort them by article title alphabetically within 
* each sub-category under IT Service
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/

public with sharing class ExpComm_ITServiceKnowledgePageController {

    /* Private Controler properties */

    //private final Map<String, Wrapper> nodeMap;
    private final String DATA_CATEGORY_GROUPNAME = 'Experian_Internal';
    private final Integer MAX_NUMBER_OF_COLOR = 5;
    
    private Map<String, String> dataCategoryAPINameAndLabelMap;
    private Map<String, String> dataCategoryAPINameAndLabelMapFS;
    private Map<String, String> dataCategoryAPINameAndLabelMapBR;
    private Map<String, String> dataCategoryAPINameAndLabelMapHR;
    private Map<String, String> dataCategoryAPINameAndLabelMapFinance;
    
    private Map<String, CategoryArticleWrapper> catApiToWrapperMap {get; set;}
    private Map<String, CategoryArticleWrapper> catApiToWrapperMapFS {get; set;}
    private Map<String, CategoryArticleWrapper> catApiToWrapperMapBR {get; set;}
    private Map<String, CategoryArticleWrapper> catApiToWrapperMapHR {get; set;}
    private Map<String, CategoryArticleWrapper> catApiToWrapperMapFinance {get; set;}
    
    public String renderCategories {get;set;}
    
    private Map<String, Wrapper> nodeMap = new Map<String, Wrapper>();
    private Map<String, Wrapper> nodeMapFS = new Map<String, Wrapper>();
    private Map<String, Wrapper> nodeMapBR = new Map<String, Wrapper>(); 
    private Map<String, Wrapper> nodeMapHR = new Map<String, Wrapper>(); 
    private Map<String, Wrapper> nodeMapFinance = new Map<String, Wrapper>(); 
    
    public List<CategoryArticleWrapper> dataCatAndArticlesList {get {
            return catApiToWrapperMap.values();
        } set;}
        
    public List<CategoryArticleWrapper> dataCatAndArticlesListFS {get {
            return catApiToWrapperMapFS.values();
        } set;}
        
    public List<CategoryArticleWrapper> dataCatAndArticlesListBR {get {
            return catApiToWrapperMapBR.values();
        } set;}

    public List<CategoryArticleWrapper> dataCatAndArticlesListHR {get {
            return catApiToWrapperMapHR.values();
        } set;}
        
    public List<CategoryArticleWrapper> dataCatAndArticlesListFinance {get {
            return catApiToWrapperMapFinance.values();
        } set;}    
        
    public ExpComm_ITServiceKnowledgePageController() {
        
        String currentCategory = ApexPages.currentPage().getParameters().get('u');
        //system.debug('currentCategory' + currentCategory);
        if(currentCategory == null || currentCategory == '' || currentCategory == '1') {
            renderCategories = '1';
            populateItServices();
        }    
        else if(currentCategory == '2') {
            renderCategories = currentCategory;
            populateFac_Security();
        }
        else if(currentCategory == '3') {
            renderCategories = currentCategory; 
            populateBusinessResources();
        }
        else if(currentCategory == '4') {
            renderCategories = currentCategory;
            populateHR();
        }
        else if(currentCategory == '5') {
            renderCategories = currentCategory;
            populateFinance();
        }
        
        /*populateItServices();
        populateFac_Security();
        populateBusinessResources();
        populateHR();
        populateFinance();*/
    }
     
    public void displayItServices() {
        renderCategories = '1';
        
        if(nodeMap.isEmpty())
        populateItServices();
       
    }
    
    public void displayFac_Security() {
        renderCategories = '2';

        if(nodeMapFS.isEmpty())
        populateFac_Security();
    }
    
    public void displayBusinessResources() {
        renderCategories = '3';
        
        if(nodeMapBR.isEmpty())
        populateBusinessResources();
    }
    
    public void displayHr() {
        renderCategories = '4';
        
        if(nodeMapHR.isEmpty())
        populateHR();
    }
    
    public void displayFinance() {
        renderCategories = '5';
        
        if(nodeMapFinance.isEmpty())
        populateFinance();
    }
    
    public void populateItServices(){
     
        //nodeMap = new Map<String, Wrapper>();
        
        //if(nodeMap.isEmpty()){
        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW IT_Services__c LIMIT 10000';

        List<Employee_Article__kav> articleVersionList = Database.query(queryStr);
        
        dataCategoryAPINameAndLabelMap = new Map<String, String>();

        catApiToWrapperMap = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMap);

        // Remove the top level, only need the sub levels
        nodeMap.remove('IT_Services');

        Integer colorCount = 0;
        for (String key : nodeMap.keySet()) {
            dataCategoryAPINameAndLabelMap.put(nodeMap.get(key).apiName, nodeMap.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMap.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMap.get(key).label;
            newCatArtWrp.categoryImageIcon = 'it_service_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMap.put(nodeMap.get(key).apiName, newCatArtWrp);

            colorCount++;
        }

        groupingArticles(catApiToWrapperMap, articleVersionList);
       
    }
    
    public void populateHR(){
        
        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW HR__c LIMIT 10000';
                                        
        List<Employee_Article__kav> articleVersionListHR = Database.query(queryStr);
        dataCategoryAPINameAndLabelMapHR = new Map<String, String>();
        
        catApiToWrapperMapHR = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMapHR);
        
        nodeMapHR.remove('HR');
        
        Integer colorCount = 0;
        for (String key : nodeMapHR.keySet()) {
            dataCategoryAPINameAndLabelMapHR.put(nodeMapHR.get(key).apiName, nodeMapHR.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMapHR.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMapHR.get(key).label;
            newCatArtWrp.categoryImageIcon = 'hr_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMapHR.put(nodeMapHR.get(key).apiName, newCatArtWrp);

            colorCount++;
        }
        
        groupingArticles(catApiToWrapperMapHR, articleVersionListHR);
    }
    
    public void populateFinance(){
        
        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW Finance__c LIMIT 10000';
                                        
        List<Employee_Article__kav> articleVersionListFinance = Database.query(queryStr);
        dataCategoryAPINameAndLabelMapFinance = new Map<String, String>();
        
        catApiToWrapperMapFinance = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMapFinance);
        
        nodeMapFinance.remove('Finance');
        
        Integer colorCount = 0;
        for (String key : nodeMapFinance.keySet()) {
            dataCategoryAPINameAndLabelMapFinance.put(nodeMapFinance.get(key).apiName, nodeMapFinance.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMapFinance.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMapFinance.get(key).label;
            newCatArtWrp.categoryImageIcon = 'finance_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMapFinance.put(nodeMapFinance.get(key).apiName, newCatArtWrp);

            colorCount++;
        }
        
        groupingArticles(catApiToWrapperMapFinance, articleVersionListFinance);}
     
    public void populateFac_Security(){
        
        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW Facilities_Security__c LIMIT 10000';
                                        
        List<Employee_Article__kav> articleVersionListFS = Database.query(queryStr);
        dataCategoryAPINameAndLabelMapFS = new Map<String, String>();
        
        catApiToWrapperMapFS = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMapFS);
        
        nodeMapFS.remove('Facilities_Security');
        
        Integer colorCount = 0;
        for (String key : nodeMapFS.keySet()) {
            dataCategoryAPINameAndLabelMapFS.put(nodeMapFS.get(key).apiName, nodeMapFS.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMapFS.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMapFS.get(key).label;
            newCatArtWrp.categoryImageIcon = 'facilities_security_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMapFS.put(nodeMapFS.get(key).apiName, newCatArtWrp);

            colorCount++;
        }
        
        groupingArticles(catApiToWrapperMapFS, articleVersionListFS);
    }
    
    public void populateBusinessResources(){
        
        
        // Get current user(viewer)'s USER INFO
        Id currentUserId = UserInfo.getUserId();
        // Query for the current user's locale setting
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        // Query all the articles that matches current user's language
        String queryStr = 'SELECT ArticleNumber, ArticleType, Id, IsLatestVersion, KnowledgeArticleId, Title, UrlName'
                                        + ' , (SELECT Id, DataCategoryName FROM DataCategorySelections) FROM Employee_Article__kav'
                                        + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentUser.LanguageLocaleKey + '\' AND IsLatestVersion = true'
                                        + ' WITH DATA CATEGORY Experian_Internal__c BELOW Business_Resources__c LIMIT 10000';
                                        
        List<Employee_Article__kav> articleVersionListBR = Database.query(queryStr);
        dataCategoryAPINameAndLabelMapBR = new Map<String, String>();
        
        catApiToWrapperMapBR = new Map<String, CategoryArticleWrapper>();
        
        getDescribeDataCategoryGroupStructureResults(nodeMapBR);
        
        nodeMapBR.remove('Business_Resources');
        
        Integer colorCount = 0;
        for (String key : nodeMapBR.keySet()) {
            dataCategoryAPINameAndLabelMapBR.put(nodeMapBR.get(key).apiName, nodeMapBR.get(key).label);
            CategoryArticleWrapper newCatArtWrp = new CategoryArticleWrapper();
            newCatArtWrp.categoryApiName = nodeMapBR.get(key).apiName;
            newCatArtWrp.categoryLable = nodeMapBR.get(key).label;
            newCatArtWrp.categoryImageIcon = 'business_resources_' + newCatArtWrp.categoryApiName + '.png';
            newCatArtWrp.setColorOption(Math.mod(colorCount, MAX_NUMBER_OF_COLOR));

            catApiToWrapperMapBR.put(nodeMapBR.get(key).apiName, newCatArtWrp);

            colorCount++;
        }
        
        groupingArticles(catApiToWrapperMapBR, articleVersionListBR);
    }

    /**
     * A helper method that helps group each article into their category group and sort them by title afterward.
     *
     * @param   catApiToWrapperMap           A map that contains the mapping of (category API name and CategoryArticleWrapper) pair
     * @param   articleVersionList           The list that contains all the articles.
     * @return  None.
     *
     */
    private void groupingArticles(Map<String, CategoryArticleWrapper> catApiToWrapperMap, List<Employee_Article__kav> articleVersionList) {
        for (Employee_Article__kav oneArticle : articleVersionList) {
            if (oneArticle.DataCategorySelections != null && oneArticle.DataCategorySelections.size() > 0) {
                
                for (Employee_Article__DataCategorySelection dataCatType : oneArticle.DataCategorySelections) {
                    
                    //Employee_Article__DataCategorySelection dataCatType = oneArticle.DataCategorySelections.get(0);                   
                    
                    if (dataCatType.DataCategoryName == 'IT_Services') {
                        // Found an article belongs to Main Category, add it too all sub-category under main-category
                        //for (String key : catApiToWrapperMap.keySet()) {
                        //    catApiToWrapperMap.get(key).addArticleToList(oneArticle);
                        //}
                    }
                    else {
                        // Add to sub-category list if it is under any of the direct sub-category
                        if (catApiToWrapperMap.get(dataCatType.DataCategoryName) != null) {                         
                            catApiToWrapperMap.get(dataCatType.DataCategoryName).addArticleToList(oneArticle);                          
                        }
                    }
                }
            }
        }

        // Sort the articles by title in each sub-category
        for (String key : catApiToWrapperMap.keySet()) {
            List<Employee_Article__kav> oneSubCatArticles = catApiToWrapperMap.get(key).articleVersionList;

            if (oneSubCatArticles != null && oneSubCatArticles.size() > 1) {
                // Only sort if it contains more than 1 article in this sub-category

                List<ArticleSorter> artSortList = new List<ArticleSorter>();
                for (Employee_Article__kav oneArti : oneSubCatArticles) {
                    artSortList.add(new ArticleSorter(oneArti));
                }

                artSortList.sort();

                // clear the original list
                catApiToWrapperMap.get(key).clearArticleList();

                // re-add the article in order
                for (ArticleSorter sortedArti : artSortList) {
                    catApiToWrapperMap.get(key).addArticleToList(sortedArti.articleVersion);
                }
            }
        }
    }

    /**
     * A helper method that helps to build the data category structure map
     *
     * @param   nodeMap           the nodeMap that passed in and used to store the structure information.
     * @return  None.
     *
     */
    private void getDescribeDataCategoryGroupStructureResults(Map<String, Wrapper> nodeMap){
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        try {
            //Making the call to the describeDataCategoryGroups to
            //get the list of category groups associated
            List<String> objType = new List<String>();
            objType.add('KnowledgeArticleVersion');

            describeCategoryResult = Schema.describeDataCategoryGroups(objType);

            //Creating a list of pair objects to use as a parameter
            //for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

            //Looping throught the first describe result to create
            //the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }

            //describeDataCategoryGroupStructures()
            describeCategoryStructureResult = 
            Schema.describeDataCategoryGroupStructures(pairs, false);

            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
                //Get name of the associated Sobject
                singleResult.getSobject();

                //Get the name of the data category group
                singleResult.getName();

                //Get the name of the data category group
                singleResult.getLabel();

                //Get the description of the data category group
                singleResult.getDescription();

                //Get the top level categories
                DataCategory [] toplevelCategories = 
                singleResult.getTopCategories();

                //Recursively get all the categories
                List<DataCategory> allCategories = getAllCategories(toplevelCategories);

                List<DataCategory> categoryToIterate = new List<DataCategory>();


                // For this controller's visualforce page, only interested in 'IT_Services' Data Category
                for(DataCategory category: allCategories){
                    if(category.getName().equalsIgnoreCase('IT_Services') && renderCategories == '1' ){

                        categoryToIterate.add(category);
                    }
                    
                    if(category.getName().equalsIgnoreCase('Facilities_Security') && renderCategories == '2'){

                        categoryToIterate.add(category);
                    }
                    
                    if(category.getName().equalsIgnoreCase('Business_Resources') && renderCategories == '3'){

                        categoryToIterate.add(category);
                    }
                    
                    if(category.getName().equalsIgnoreCase('HR') && renderCategories == '4'){

                        categoryToIterate.add(category);
                    }
                    
                    if(category.getName().equalsIgnoreCase('Finance') && renderCategories == '5'){

                        categoryToIterate.add(category);
                    }
                }

                buildCategoryStructureTree(nodeMap, categoryToIterate, null);
            }
        } 
        catch (Exception e){
        }
    }
    
    /**
     * A helper method that helps the above 'getDescribeDataCategoryGroupStructureResults' method
     */
    private DataCategory[] getAllCategories(DataCategory [] categories){
        if(categories.isEmpty()){
            return new DataCategory[]{};
        } else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }

    /**
     * A helper method that use the data category structure map to build a tree structure of the data category
     *
     * @param   nodeMap             the nodeMap that passed in and used to store the structure information.
     * @param   allCategories       All child categories of their parent node
     * @param   parentNode          Parent node of the child categories
     * @return  None.
     *
     */
    private void buildCategoryStructureTree(Map<String, Wrapper> nodeMap, List<DataCategory> allCategories, Wrapper parentNode) {
        for (DataCategory oneCategory : allCategories) {

            Wrapper currentNode = new Wrapper();
            currentNode.label = oneCategory.getLabel();
            currentNode.apiName = oneCategory.getName();

            if (parentNode != null) {
                currentNode.parent = parentNode;
            }

            nodeMap.put(oneCategory.getName(), currentNode);

            List<DataCategory> children = oneCategory.getChildCategories();

            if (children.isEmpty() == false) {
                buildCategoryStructureTree(nodeMap, children, currentNode);
            }
        }
    }

    /**
     * A Wrapper class that helps to build the tree structure of the data categories
     *
     */
    public class Wrapper {
        public String label {get; set;}
        public String apiName {get; set;}
        public Wrapper parent {get; set;}
        public List<Wrapper> children {get; set;}

        public Wrapper() {
            children = new List<Wrapper>();
        }
    }


    /**
     * A Wrapper class that helps to show all sub data categories on visualforce page
     *
     */
    public class CategoryArticleWrapper {
        public String categoryApiName {get; set;}
        public String categoryLable {get; set;}
        public List<Employee_Article__kav> articleVersionList {get; set;}
        public Integer colorOption {get; set;}
        public String categoryImageIcon {get; set;}

        public CategoryArticleWrapper() {
            articleVersionList = new List<Employee_Article__kav>();
        }

        public void addArticleToList(Employee_Article__kav oneArticle) {
            articleVersionList.add(oneArticle);
        }

        public void setColorOption(Integer color) {
            colorOption = color;
        }

        public void clearArticleList() {
            articleVersionList.clear();
        }
    }

    /**
     * A Helper class that helps to sort the article list by article title
     *
     */
    public class ArticleSorter implements Comparable {
        public final Employee_Article__kav articleVersion;
        public final String recordTitle;

        public ArticleSorter(Employee_Article__kav articleVersion) {
            this.articleVersion = articleVersion;

            if (articleVersion.Title == null) {
                recordTitle = '';
            }
            else {
                recordTitle = articleVersion.Title;
            }
        }

        public Integer compareTo(Object compareTo) {
            ArticleSorter cmt = (ArticleSorter) compareTo;

            String articleTitle = cmt.recordTitle;
            
            if (articleTitle == null) {
                articleTitle = '';
            }

            return recordTitle.compareTo(articleTitle);
        }
    }
}