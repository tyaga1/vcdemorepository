/**=====================================================================
 * Experian
 * Name: ScheduleWalletSyncStart_Test
 * Description: 
 * 
 * Created Date: 12th August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class ScheduleWalletSyncStart_Test {

  static testMethod void testNormalStart() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.WalletIntegn_StartDate__c = system.now().addMinutes(-15);
    gs.WalletIntegn_CompletionDate__c = system.now().addMinutes(-13);
    gs.Name = 'Global';
    insert gs;
    
    Test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleWalletSyncStartTEST1', CRON_EXP, new ScheduleWalletSyncStart());
    // Get the information from the CronTrigger API object
    Test.stopTest();  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    // Verify the Schedule has been scheduled with the same time specified  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run, but scheduled  
    System.assertEquals(0, ct.TimesTriggered);
  }
  
  static testMethod void testAbnormalStart() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.WalletIntegn_StartDate__c = system.now().addMinutes(-15);
    gs.WalletIntegn_CompletionDate__c = null;
    gs.Name = 'Global';
    insert gs;
    
    Test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleWalletSyncStartTEST1', CRON_EXP, new ScheduleWalletSyncStart());
    // Get the information from the CronTrigger API object
    Test.stopTest();  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    // Verify the Schedule has been scheduled with the same time specified  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run, but scheduled  
    System.assertEquals(0, ct.TimesTriggered);
  }
  
  
}