/**=====================================================================
 * Name: AccountTeamMembersList_Full_Test
 * Description: Case #01192655Show the Business Unit alongside each AE under Account Team
 * This is the test class for the controller extension class for the page of the same name 
 * that was written to allow for the customisation of the list of AccountTeamMember records. 
 *
 * Created Date: Mar. 8th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Mar. 8th, 2016        James Wills           Case #1192655: Show the Business Unit alongside each AE under Account Team: Created
 * June 22nd, 2016       James Wills           Case #1192655: Further updates
 * Aug. 30th, 2016       James Wills           CRM2:W-005718: Account Team View - Job Title
  =====================================================================*/

 @isTest
 public class AccountTeamMembersList_Full_Test {
 
 
  @isTest
  public static void testAccountTeamMembersListFull_1(){
    
    Account testAccount1 = Test_Utils.insertAccount();
    
    AccountTeamMember newATM                   = new AccountTeamMember(AccountId = testAccount1.id);
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testAccount1);
    AccountTeamMembersList_Full controller     = new AccountTeamMembersList_Full(stdController);
    
    
    Test.startTest();  
    
      Boolean hasDisplayAccess = controller.getDisplayAccess();
    
      String userAccessType = controller.accessType;
      Boolean hasEdit       = controller.userHasEditAccess;
      Boolean hasDelete     = controller.userHasDeleteAccess;
    
      controller.fetchExistingAccountTeamMembers_Full();

    
      //1. Test the re-ordering of the Account Team Member Wrapper list by the Name of the User, first Ascending then Descending
      controller.atmListOrderParam = Label.AccountTeamMembersList_Team_Member;
      String newListOrder          = controller.atmListOrder;//Set the ORDER BY clause of the ATM list to the Name of the team member
      controller.atmListAscDesc    = 'ASC';                  //Set the sort order of the ATM list to 'ASC'
      List<AccountTeamMembersList_Full.AccountTeamWrapper> atwList = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Name <= atwList[atwlist.size()-1].member.User.Name, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Name properly. '+ atwList[0].member.User.Name + atwList[atwlist.size()-1].member.User.Name);
  
      controller.atmListReorder();//Set the sort order of the ATM list to 'DESC'
      atwList = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Name >= atwList[atwlist.size()-1].member.User.Name, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Name properly. ' + atwList[0].member.User.Name + atwList[atwlist.size()-1].member.User.Name );
    
      //2. Test the re-ordering of the Account Team Member Wrapper list by Account Team Member Role
      controller.atmListOrderParam = Label.AccountTeamMembersList_Job_Title;//Set the ORDER BY clause of the ATM list to the Role of the team member
      controller.atmListAscDesc    = 'ASC';                                 //Set the sort order of the ATM list to 'ASC'
      newListOrder                 = controller.atmListOrder;      
      atwList                      = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Title <= atwList[atwlist.size()-1].member.User.Title, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Job Title properly. ');
    
      //3. Test the re-ordering of the Account Team Member Wrapper list by Account Team Member Role
      controller.atmListOrderParam = Label.AccountTeamMembersList_Team_Role;//Set the ORDER BY clause of the ATM list to the Role of the team member
      controller.atmListAscDesc    = 'ASC';                                 //Set the sort order of the ATM list to 'ASC'
      newListOrder                 = controller.atmListOrder;      
      atwList                      = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.TeamMemberRole <= atwList[atwlist.size()-1].member.TeamMemberRole, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Role properly. ');
    
      //4. Test the re-ordering of the Account Team Member Wrapper list by Account Team Member Business Unit
      controller.atmListOrderParam = Label.AccountTeamMembersList_Team_Member_Business_Unit;
      newListOrder                 = controller.atmListOrder;               //Set the ORDER BY clause of the ATM list to the Business Unit of the team member
      atwList                      = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Business_Unit__c <= atwList[atwlist.size()-1].member.User.Business_Unit__c, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Business Unit properly. ');
    
      //5. Test the re-ordering of the Account Team Member Wrapper list by Account Team Member Sales Team
      controller.atmListOrderParam = Label.AccountTeamMembersList_Sales_Team;
      newListOrder                 = controller.atmListOrder;               //Set the ORDER BY clause of the ATM list to the Sales Team of the team member
      atwList                      = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Sales_Team__c <= atwList[atwlist.size()-1].member.User.Sales_Team__c, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Sales Team properly. ');
    
      //6. Test the re-ordering of the Account Team Member Wrapper list by Account Team Member Sub-Team
      controller.atmListOrderParam = Label.AccountTeamMembersList_Sales_Sub_Team;
      newListOrder                 = controller.atmListOrder;                //Set the ORDER BY clause of the ATM list to the Sub-Team of the team member
      atwList                      = controller.fetchExistingAccountTeamMembers_Full();
      System.assert(atwList[0].member.User.Sales_Sub_Team__c <= atwList[atwlist.size()-1].member.User.Sales_Sub_Team__c, 'AccountTeamMembersList_Full_Test: Account Team Member list not sorted by Sales Sub Team properly. ');
    
      controller.seeMoreATMs();
    
      //Test deletion of Account Team Member
      List<AccountTeamMember> atmList = [SELECT id FROM AccountTeamMember WHERE UserId =:UserInfo.getUserId()];

      Integer atmListSizePreDelete = atmList.size();
      controller.selectedID = atmList[0].ID;
      controller.doDelete();
      atmList = [SELECT id FROM AccountTeamMember WHERE UserId =:UserInfo.getUserId()];
      Integer atmListSizePostDelete = atmList.size();
    
      System.assertNotEquals(atmListSizePreDelete, atmListSizePostDelete, 'AccountTeamMembersList_Full_Test: Account Team Member not deleted.');
    
      controller.backToAccount();

    Test.stopTest();
    
  }
  
  
  @testSetup
  private static void setupTestData(){
    User thisUser = [SELECT Id FROM User WHERE Id =:UserInfo.getUserId()];
    
    //Create data
    Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    
    List<User> userList = new List<User>();
    Integer i=0;
    Integer numberUsersToCreate = 150;
    for(i=0;i<numberUsersToCreate;i++){    
      User user1 = Test_Utils.createUser(profile1, 'test@experian.com', 'ATM' + i);
      user1.Business_Unit__c = 'Test' + i;
      user1.Sales_Team__c = '';
      user1.Sales_Sub_Team__c = '';
      userList.add(user1);
    }
    
    //Now set some default values that will be sorted in the opposite direction to Name and Business_Unit__c
    for(i=userList.size();i<0;i--){    
      userList[i].Sales_Team__c = 'Test' + i;
      userList[i].Sales_Sub_Team__c = 'Test' + i;
    }    
    
    Account testAccount1 = Test_Utils.createAccount(); 

    
    System.runAs(thisUser){
      insert userList;
      insert testAccount1;
    }
    
    //This record is required in order for Confidential_Information_Share records to be created
    Confidential_Information__c conInfoCustom = new Confidential_Information__c();
    conInfoCustom.Account__c = testAccount1.Id;
    conInfoCustom.Synch_Account_Team_Members__c = true;
    insert conInfoCustom;
    
    ApexPages.Standardcontroller std = new ApexPages.Standardcontroller(testAccount1);
    AccountTeamMembersListEdit editController = new AccountTeamMembersListEdit(std);
    AccountTeamMembersList viewController = new AccountTeamMembersList(std);
    
    List<AccountTeamMembersListEdit.AccountTeamWrapper> atmListEdit = new List<AccountTeamMembersListEdit.AccountTeamWrapper>();
    
    for(i=0;i<numberUsersToCreate;i++){
      // Call the methods and create new team member
      editController.addTeamMembers();//This method resets the editController.newAccountTeamMembers list
      editController.newAccountTeamMembers[0].member.UserId = userList[i].id;
      editController.newAccountTeamMembers[0].member.TeamMemberRole = Constants.TEAM_ROLE_SALES_MANAGER;
      editController.newAccountTeamMembers[0].accountAccess = Label.AccountTeamMembersList_Edit;
      editController.newAccountTeamMembers[0].opportunityAccess = Label.AccountTeamMembersList_Read;
      editController.newAccountTeamMembers[0].caseAccess = Label.AccountTeamMembersList_Read;
      
      atmListEdit.add(editController.newAccountTeamMembers[0]);
      
    }
    editController.newAccountTeamMembers.clear();
    
    //The line below is required to allow for editController.saveAndMore() to be called outside of the loop above, thus preventing DML from being called inside the loop.
    //(editController.addTeamMembers() resets the editController.newAccountTeamMembers list when it is called).
    editController.newAccountTeamMembers = atmListEdit;
    
    editController.saveAndMore();
    
    List<AccountTeamMember> atmList = [SELECT id FROM AccountTeamMember LIMIT :numberUsersToCreate];
    
    System.assertEquals(atmList.size(), userList.size(), 'AccountTeamMembersList_Full_Test:Account Team Members have been not been created for all Users.');
        
    List<User> userIDList = [SELECT id FROM User WHERE Alias LIKE 'ATM%'];    
    
    List<Confidential_Information__Share> coninfo = [ SELECT Id
                                                      FROM Confidential_Information__Share
                                                      WHERE RowCause = :Constants.ROWCAUSE_ACCOUNT_TEAM
                                                      AND Parent.Account__c = :testAccount1.Id
                                                      AND UserOrGroupId IN :userIDList];

    System.assertEquals(coninfo.size(), atmList.size(), 'AccountTeamMembersList_Full_Test: Confidential_Information_Share records have not been created for all Account Team Members.');
    
  
  } 
  
 
 }