/**=====================================================================
 * Experian
 * Name: ITCA_Team_Member_Controller
 * Description:
 * Created Date: June 2017
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 29th July 2017     James Wills           ITCA:W-008961 - Build list of Employees for Manager - removed to VF Component ITCA_Navigation_Banner
 * 30th July          Richard Joseph        Added employeeSkillDisplay, employeeSkillDisplaySort, compareSkillSet. compareCareerArea
 * 2nd August 2017    Alexander McCall      Updated working to 'Skill Set' and 'Career Area' on drop downs
 * 3rd August 2017    James Wills           ITCA:W-008961 - Updated for new requirements.
 * 8th August 2017    James Wills           Tidying up code.
 * 11th August 2017   James Wills           ITCA:W-009246 Updated to allow editing technical skills.
 * 31st August 2017   Malcolm Russell       Added modal code and split technical and sifa skill list
 * 1st September 2017 Malcolm Russwll       Commented out code that is not required.
 * =====================================================================*/
 
global without sharing class ITCA_Team_Member_Controller{

  public list<Career_Architecture_User_Profile__c> currentUserProfile        {get;set;}
  public list<Career_Architecture_Skills_Plan__c> currentUserSkills          {get;set;}
  //public list<Career_Architecture_Skills_Plan__c> currentUserSkillsLevel     {get;set;}
  public list<Skill_Set_to_Skill__c>              currentUserSkillsSelection {get;set;}
  //public list<Skill_Set_to_Skill__c>              selectedSkillsetSkills     {get;set;}
  //public list<Skill_Set_to_Skill__c>              skillSetSkillsList         {get;set;}
  
  public Map<String, Decimal> currentUserSkillLevelMap                       {get;set;}  
  public Map<String, Decimal> maxSkillLevels                                 {get;set;}
  //public Map<String, Decimal> maxSelectedSkillsLevel                         {get;set;}
  //public String[] maxLevels                                                  {get;set;}
  
  public list<AggregateResult> currentUserMatchedSkillsSelection             {get;set;}   

  //public String level1_selectedCareerArea                                    {get;set;}
  //public String level2_selectedSkillSet                                      {get;set;}
  
  public class userSkillInfo{
    public ID skillID          {get;set;}
    public String skillName    {get;set;}
    public String skillSet     {get;set;}
    public String currentSkillValue {get;set;}
    public String currentLevel {get;set;}
    public Integer nextSkillvalue {get;set;}
    
    public Map<String,String> skillLevels_Map{get;set;}
    
    public String nextLevel    {get;set;}
     
  }
  
  public String selectedSkillId                                                  {get;set;}
  
  public String selectedSkillLevel{
    get{
      if(selectedSkillId!=null && selectedSkillId.contains('skillName')){
        return 'true';
      } else {
        return 'false';
      }
    
    }set;
  
  }
  
  public employeeSkillDisplay selectedESDS {get;set;}
  
  
  public Map<ID,ProfileSkill> skillMaxLevels_Map {get{ return new Map<ID, ProfileSkill>([SELECT id, Name, Max_Skill_Level__c FROM ProfileSkill]);} set;}
  
  public String userProfileCurrentLevel {get{ return itcaTeamSummaryController.getProfileLevel(casp_List, skillMaxLevels_Map);} set;}
  
  public String selectedCareerProfileID{get{ 
    return ApexPages.CurrentPage().GetParameters().get('selectedCareerProfileID');}  
  set;}
  
  public String employeeNameForProfile {get{
      if(!currentUserProfile.isEmpty()){
        return currentUserProfile[0].Employee__r.Name + '\'s';
      }
      return null;
    }
  set;}
  
  public String profileStatus {get{
      if(!currentUserProfile.isEmpty()){
        return currentUserProfile[0].Status__c;
      }
      return null;
    }
  set;}
  
  public Datetime dateDiscussedWithEmployee {get{
      if(!currentUserProfile.isEmpty()){
        return currentUserProfile[0].Date_Discussed_with_Employee__c;
      }
      return null;
    }
    set;    
  }
  
  public List<Career_Architecture_Skills_Plan__c> casp_List {get{ return new list<Career_Architecture_Skills_Plan__c>([SELECT Id, Skill__r.Id, Skill__r.Name, Skill__r.sfia_Skill__c,
                                                                                                      Skill__r.Level1__c,Skill__r.Level2__c,
                                                                                                      Skill__r.Level3__c,skill__r.Level4__c,Skill__r.Level5__c,
                                                                                                      Skill__r.Level6__c,Skill__r.Level7__c,
                                                                                                      Level__c, Experian_Skill_Set__r.Name, Experian_Skill_Set__r.Career_Area__c,
                                                                                                      Career_Architecture_User_Profile__c,
                                                                                                      Skill__r.Max_Skill_Level__c
                                                                                                      FROM Career_Architecture_Skills_Plan__c 
                                                                                                      WHERE Career_Architecture_User_Profile__c 
                                                                                                      IN :currentUserProfile
                                                                                                      ORDER BY Skill__r.Name]);} set;}    
  
  
  public String managerComments {get; set;}
  
 //RJ added
  public List<employeeSkillDisplay> employeeSkillDisplayList {get; set;}
  public List<employeeSkillDisplay> TechnicalSkillDisplayList {get; set;}
  public list<employeeSkillDisplaySort> employeeSkillDisplaySortList {get; set;} 
  public enum activePanel {isSummary, isMatchSKills, isCompareSKillSet, isCompareCareerArea}

  
  public String currentActiveTab {get;set;}
 
  public ITCABannerInfoClass bannerInfoLocal {get;set;}

  //Compare Skillsets
  /* Removed Malcolm Russell
  public Set<String> level1_SelectedCareerAreas_Set{
    get{
      if(level1_selectedCareerArea==null || level1_selectedCareerArea=='Select Career Area'){      
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel : level1_CareerAreaOptions){
          options_Set.add(sel.getLabel());
        }      
        return options_Set;
      }
      return new Set<String>{level1_selectedCareerArea};
    }        
  set;}
        
  public Set<String> level2_selectedSkillSets_Set{
    get{
       if(level2_selectedSkillSet==null || level2_selectedSkillSet=='Select Skill Set'){
         Set<String> options_Set = new Set<String>();
         for(SelectOption sel : level2_skillSetOptions){
           options_Set.add(sel.getValue());
         }      
         return options_Set;
       }    
       return new Set<String>{level2_selectedSkillSet};
     }           
  set;}    
          
  public List<SelectOption> level1_CareerAreaOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();
      
      Schema.DescribeFieldResult fieldResult = Experian_Skill_Set__c.Career_Area__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();     
      selList.add(new SelectOption('Select Career Area', 'Select Career Area'));     
      for( Schema.PicklistEntry f : ple){
        selList.add(new SelectOption(f.getValue(), f.getLabel()));
      }                  
      return selList;
    }          
    set;
  }
          
  public List<SelectOption> level2_skillSetOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();                                  
      //List<Experian_Skill_Set__c> skillOptions_List = [SELECT id, Name FROM Experian_Skill_Set__c WHERE Career_Area__c IN :level1_SelectedCareerAreas_Set]; RJ Commented
      List<Experian_Skill_Set__c> skillOptions_List = [SELECT id, Name FROM Experian_Skill_Set__c];
      selList.add(new SelectOption('Select Skill Set', 'Select Skill Set'));                
      for(Experian_Skill_Set__c ess : skillOptions_List){
        SelectOption sel = new SelectOption(ess.id, ess.Name);
        selList.add(sel);
      }      
      return selList;
    }               
    set;
  }
              
  public set<string> skillSetSkills{                                                         
    get{ 
      List<skill_set_to_skill__c> skillOptions_List = [SELECT skill__r.id, skill__r.Name FROM Skill_Set_to_Skill__c WHERE Experian_Skill_Set__r.id IN :level2_selectedSkillSets_Set];                                                                      
      Set<string> skillOptionsSet = new Set<String>();
      for (skill_set_to_skill__c c : skillOptions_List){
        skillOptionsSet.add(c.skill__r.id);}                  
        return skillOptionsSet;                              
      }
      set;
  }               
  
  //Get all compare skills information    
  public map<string,decimal> skillSetSkillsMap{
    get{
      maxSelectedSkillsLevel = new Map<string, decimal>();         
      selectedSkillsetSkills= new list<Skill_set_to_skill__c>(
        [select id, skill__r.id,skill__r.Max_Skill_Level__c,skill__r.Minimum_Skill_Level__c,level__c, skill__r.name from Skill_set_to_skill__c where Skill_set_to_skill__c.skill__r.id in :skillSetSkills]);
                
      //Get the current users skills form their current profile
      currentUserSkillsLevel= new list<Career_Architecture_Skills_Plan__c>
         ([select skill__r.id, skill__r.name, Current_Level__c from Career_Architecture_Skills_Plan__c where Career_Architecture_Skills_Plan__c.skill__r.id in :skillSetSkills]);  
                                      
      return maxSelectedSkillsLevel;
    } 
  set;}  
  */   
  
  public Boolean isManagerOfEmployee {get{
    Boolean isManager = false;
      if(selectedCareerProfileID!=null){
        isManager=true;
      } else {
        isManager=false;
      }
    return isManager;
    }
  set;}
  
  
 ////////////////////////////////////////////////////////////////////////////////////////////

  public void ITCA_Homepage_Load(){   

    maxSkillLevels = new Map<String, Decimal>();
    //minSkillLevels = new Map<String, Decimal>();    
    //maxFutureSkillLevels = new Map<String, Decimal>(); 
    
    if(ApexPages.CurrentPage().GetParameters().get('currentActiveTab')!= null){    
       currentActiveTab= ApexPages.CurrentPage().GetParameters().get('currentActiveTab');   
    } else {
     currentActiveTab='isSummary';
    }
    
   
        
    if(selectedCareerProfileID== null){
      //selectedCareerProfileID = ApexPages.CurrentPage().GetParameters().get('selectedCareerProfileID');   
    }
        
    if (selectedCareerProfileID != null){
      currentUserProfile= new List<Career_Architecture_User_Profile__c>([SELECT id, Employee__r.Name, role__c, State__c, Status__c, Manager_Comments__c, Date_Discussed_with_Employee__c 
                                                                         FROM Career_Architecture_User_Profile__c 
                                                                         WHERE State__c = 'Current' AND id = :selectedCareerProfileID LIMIT 1]);
    } else {                   
      currentUserProfile= new List<Career_Architecture_User_Profile__c>([SELECT id, Employee__r.Name, role__c, State__c, Status__c, Manager_Comments__c, Date_Discussed_with_Employee__c
                                                                         FROM Career_Architecture_User_Profile__c 
                                                                         WHERE State__c = 'Current' AND Employee__c = :UserInfo.getUserId() LIMIT 1]);
    }
    
    if(!currentUserProfile.isEmpty()){
      //employeeNameForProfile    = currentUserProfile[0].Employee__r.Name;
      //profileStatus             = currentUserProfile[0].Status__c;
      dateDiscussedWithEmployee = currentUserProfile[0].Date_Discussed_with_Employee__c;     
      managerComments           = currentUserProfile[0].Manager_Comments__c;    
    }
        
         
    //Get the current users skills form their current profile
    currentUserSkills= new list<Career_Architecture_Skills_Plan__c>([SELECT skill__r.id, Level_In_Number__c, skill__r.name, Skill__r.sfia_Skill__c, Current_Level__c 
                                                                     FROM Career_Architecture_Skills_Plan__c 
                                                                     WHERE Career_Architecture_User_Profile__c IN :currentUserProfile]);        
        
    //Create a set and Map to list all the skill__r.id from currentUserSkills
    Set<String> currentUserSkillsId = new Set<String>();
    currentUserSkillLevelMap        = new Map<String,Decimal> ();
    
    for (Career_Architecture_Skills_Plan__c currentUserSkillRec : currentUserSkills){
      currentUserSkillsId.add(currentUserSkillRec.skill__r.id);
      currentUserSkillLevelMap.put((String)currentUserSkillRec.skill__r.id , (Decimal) currentUserSkillRec.Level_In_Number__c);            
    }
            
    //Get all information             
    currentUserskillsSelection = new list<Skill_set_to_skill__c>(
                                          [SELECT id, skill__r.id,skill__r.Max_Skill_Level__c,skill__r.Minimum_Skill_Level__c,level__c, skill__r.name 
                                           FROM Skill_set_to_skill__c 
                                           WHERE Skill_set_to_skill__c.skill__r.id IN :currentUserSkillsId 
                                           ORDER BY Skill_set_to_skill__c.skill__r.name ]);         
        
        
    //Get the maximum and minimum Levels     
    for(Skill_set_to_skill__c currentSkillsSelectRec: currentUserskillsSelection){               
       //Get the minimum Levels   
       //minSkillLevels.put(currentSkillsSelectRec.Skill__r.name, currentSkillsSelectRec.skill__r.Minimum_Skill_Level__c);// RJ Needs to remove this
       //minSkillLevels.put(currentSkillsSelectRec.Skill__r.id, currentSkillsSelectRec.skill__r.Minimum_Skill_Level__c);
       //Get the maximum and minimum Levels    
       //maxSkillLevels.put(currentSkillsSelectRec.Skill__r.name, currentSkillsSelectRec.skill__r.Max_Skill_Level__c);// RJ Need to remove this// JW - done
       maxSkillLevels.put(currentSkillsSelectRec.Skill__r.Id, currentSkillsSelectRec.skill__r.Max_Skill_Level__c);
    }
    
    for(Career_Architecture_Skills_Plan__c casp : casp_List){
      if(casp.Skill__r.sfia_Skill__c==false){
        maxSkillLevels.put(casp.Skill__r.id, casp.Skill__r.Max_Skill_Level__c);
      }
    }
    
    system.debug(maxSkillLevels.values());
        
    //Set<String> minLevels = minSkillLevels.keySet();
    //Set<String> maxLevels = maxskillLevels.keySet();
        
        
    //RJ Added
    employeeSkillDisplaySortList = new List<employeeSkillDisplaySort >();
    for (Career_Architecture_Skills_Plan__c currentUserSkillRec : currentUserSkills){
      //Instantiate employeeSkillDisplay with constructor #1 = (Career_Architecture_Skills_Plan__c careerArchSkillPlan, Map<String, Decimal> maxSkillLevels
      employeeSkillDisplaySortList.add(new employeeSkillDisplaySort(new employeeSkillDisplay(currentUserSkillRec,maxskillLevels) ));       
    }
    employeeSkillDisplaySortList.sort();
            
    employeeSkillDisplayList = new List<employeeSkillDisplay>();
     technicalSkillDisplayList = new List<employeeSkillDisplay>();
            
    for(employeeSkillDisplaySort employeeSkillDisplaySortRec :employeeSkillDisplaySortList){ 
      if(employeeSkillDisplaySortRec.employeeSkillDisplayRec.sfiaSkill){    
      employeeSkillDisplayList.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
      }else{
      technicalSkillDisplayList.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
      }
    }
    //employeeSkillDisplayList.sort(); - JW cant be sorted as elements are not comparable
             
    //employeeSkillDisplayList = new List<employeeSkillDisplay>(employeeSkillDisplaySortList);
        
    //RJ -Ends
        
    

       
    //Get Matched Skills
    currentUserMatchedSkillsSelection = new list<AggregateResult>([SELECT Count(ID),Skill__r.Name Skill,Experian_Skill_Set__r.Name 
                                                                   FROM Skill_Set_to_Skill__c 
                                                                   WHERE Skill__r.ID IN :currentUserSkillsId 
                                                                   GROUP BY Skill__r.Name,Experian_Skill_Set__r.Name]);
      
  }//End of ITCA_Homepage_Load()
       
        
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
   
   
  public PageReference discussedEmployeeProfile(){  
    if(!currentUserProfile.isEmpty()){
      currentUserProfile[0].Status__c                       = 'Discussed with Employee';
      currentUserProfile[0].Manager_Comments__c             = managerComments;
      currentUserProfile[0].Date_Discussed_with_Employee__c = Datetime.Now();
      try{
        update currentUserProfile;
      } catch(Exception e){
        ApexPages.Message dmlWarn = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
        ApexPages.addMessage(dmlWarn);
      }
    }
    return null;
  }
   
  /*  removed Malcolm Russell                    
  public void resetSkillList(){
    level2_selectedSkillSet=null;
    level2_skillSetOptions.clear();
  }
  */        

  
  public class employeeSkillDisplay{
  
    public String  skillName           {get;set;}
    public String  skillId             {get;set;}
    public Boolean sfiaSkill           {get;set;}
    public Integer curentSkillValue    {get;set;}
    public Integer maxSkillValue       {get;set;}
    public Double  skillAccPercentage  {get;set;}
    public Integer endorsementReceived {get;set;}
    public Boolean ismanagerApproved   {get;set;}
        
    //default method to formate current user skill.
    public employeeSkillDisplay(Career_Architecture_Skills_Plan__c careerArchSkillPlan, Map<String, Decimal> maxSkillLevels){
    
      this.skillId              = careerArchSkillPlan.Skill__r.Id;
      this.skillName            = careerArchSkillPlan.Skill__r.Name;
      this.sfiaSkill            = careerArchSkillPlan.Skill__r.sfia_Skill__c;//ITCA:W-009246
      this.curentSkillValue     = (Integer)careerArchSkillPlan.Level_In_Number__c;
      this.maxSkillValue        = (Integer) maxSkillLevels.get(careerArchSkillPlan.Skill__r.Id);
      
      if (maxSkillValue > 0){
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100);
      } else {
        this.skillAccPercentage = 0;
      }
    }    
        
    //used in SkillSet and CareerArea select options 
    /** Removed Malcolm Russell
    public employeeSkillDisplay(AggregateResult skillforCurrentSkillSelRec, Map<String, Decimal> currentUserSkillMap){
    
      this.skillId              = (String)skillforCurrentSkillSelRec.get('Id');
      this.skillName            = (String)skillforCurrentSkillSelRec.get('name');
      this.maxSkillValue        = (Integer)((decimal) skillforCurrentSkillSelRec.get('expr0') );
      
      if (currentUserSkillMap.containsKey((string)skillforCurrentSkillSelRec.get('Id')) && maxSkillValue >0){
        this.curentSkillValue   = (Integer)currentUserSkillMap.get((string)skillforCurrentSkillSelRec.get('Id'));
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100) ;
      } else {
        this.curentSkillValue   = 0;
        this.skillAccPercentage = 0;
      }

    }    
    */    
    //User in match top 5  skill set.
    /** Removed Malcolm Russell
    public employeeSkillDisplay(AggregateResult skillforCurrentSkillSelRec, Map<String, Integer> currentUserSkillMap){
      
      this.skillId              = (String)skillforCurrentSkillSelRec.get('Id');
      this.skillName            = (String)skillforCurrentSkillSelRec.get('name');
      this.curentSkillValue     = (Integer)((Decimal) skillforCurrentSkillSelRec.get('expr0') );
      this.maxSkillValue        = currentUserSkillMap.get((String)skillforCurrentSkillSelRec.get('name'));
            
      if(maxSkillValue > 0){
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100);
      } else {
        this.skillAccPercentage = 0;
      }
    }    
     */ 
  }
  
  //Compare Starts  
  global class employeeSkillDisplaySort implements Comparable {

    public employeeSkillDisplay employeeSkillDisplayRec;
    
    // Constructor
    public employeeSkillDisplaySort (employeeSkillDisplay empDispRec) {
        employeeSkillDisplayRec = empDispRec ;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {

      // Cast argument to OpportunityWrapper
      employeeSkillDisplaySort compareToemployeeSkillDisplayRec = (employeeSkillDisplaySort)compareTo;
        
      // The return value of 0 indicates that both elements are equal.
      Integer returnValue = 0;
      
      //if(employeeSkillDisplayRec.sfiaSkill==true){//ITCA:W-009246
        if(employeeSkillDisplayRec.skillAccPercentage > compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage){
          // Set return value to a positive value.
          returnValue = -1;
        } else if (employeeSkillDisplayRec.skillAccPercentage < compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage ) {
          // Set return value to a negative value.
          returnValue = 1;
        }

      return returnValue;       
    }
  }
  //Compare Ends
  public List<userSkillInfo> skillOverview_List{get{

      List<userSkillInfo> skillOverview_List = new List<userSKillInfo>();
      List<Career_Architecture_Skills_Plan__c> casp_List = [SELECT id, Level__c, Skill__r.Name, Skill__r.Level1__c, Skill__r.Level2__c, Skill__r.Level3__c, Skill__r.Level4__c, Skill__r.Level5__c,
                                                            Skill__r.Level6__c, Skill__r.Level7__c, Skill__r.Max_Skill_Level__c,
                                                            Experian_Skill_Set__r.Name
                                                            FROM Career_Architecture_Skills_Plan__c
                                                            WHERE Skill__c = :selectedSkillId AND
                                                            Career_Architecture_User_Profile__c IN :currentUserProfile
                                                            ];
      system.debug('casp_List::'+casp_List);
      system.debug('selectedSkillId::'+selectedSkillId) ; 
      system.debug('currentUserProfile::'+currentUserProfile)  ;                                                     
      if(!casp_List.isEmpty()){                                                     
        for(Career_Architecture_Skills_Plan__c casp : casp_list){
          userSkillInfo usi     = new userSkillInfo();
          usi.skillID           = casp.id;
          usi.skillName         = casp.Skill__r.Name;
          usi.skillSet          = casp.Experian_Skill_Set__r.Name;
          usi.currentLevel      = getCurrentSkillLevel(casp);
          usi.currentSkillValue = casp.Level__c.substring(casp.Level__c.length()-1,casp.Level__c.length());
          //usi.nextLevel         = getNextSkillLevel(casp);
          if(Integer.valueOf(usi.currentSkillValue) < casp.Skill__r.Max_Skill_Level__c){
            usi.nextSkillValue  = Integer.valueOf(usi.currentSkillValue)+1;
          } else {
            usi.nextSkillValue  = Integer.valueOf(usi.currentSkillValue);
          }
          usi.skillLevels_Map   = buildSkillLevelsMapforModal(casp);

         skillOverview_List.add(usi);
        }
      } else {
        List<ProfileSkill> ps_List = [SELECT id, Name, Level1__c, Level2__c, Level3__c, Level4__c, Level5__c,Level6__c, Level7__c, Max_Skill_Level__c
                                      FROM ProfileSkill
                                      WHERE id = :selectedSkillId];
        if(!ps_list.isEmpty()){
          for(ProfileSkill ps : ps_List){
            userSkillInfo usi   = new userSkillInfo();
            usi.skillID         = ps.id;
            usi.skillName       = ps.Name;
            //usi.skillSet      = level2_selectedSkillSet;
          
            usi.skillLevels_Map = buildSkillLevelsMapforModal_FromSkill(ps);

            skillOverview_List.add(usi);
          }
        } else {
           List<Experian_Skill_Set__c> es_List = [SELECT id, Name FROM Experian_Skill_Set__c WHERE id =:selectedSkillId];

           for(Experian_Skill_Set__c es : es_List){
             userSkillInfo usi   = new userSkillInfo();
             usi.skillID         = es.id;
             usi.skillName       = es.Name;
             
             usi.skillLevels_Map = buildSkillLevelsMapforModal_FromExpSkillSet(es);
             
             skillOverview_List.add(usi);
           }           
        }
      }
      return skillOverview_List;
    }
    set;
    
  }  
  
  public String getCurrentSkillLevel(Career_Architecture_Skills_Plan__c casp){
    String currentLevel;
    if(casp.Level__c=='Level1'){
      return casp.Skill__r.Level1__c;
    }else if(casp.Level__c=='Level2'){
      return casp.Skill__r.Level2__c;
    }else if(casp.Level__c=='Level3'){
      return casp.Skill__r.Level3__c;
    }else if(casp.Level__c=='Level4'){
      return casp.Skill__r.Level4__c;
    }else if(casp.Level__c=='Level5'){
      return casp.Skill__r.Level5__c;
    }else if(casp.Level__c=='Level6'){
      return casp.Skill__r.Level6__c;
    }else if(casp.Level__c=='Level7'){                 
      return casp.Skill__r.Level7__c;
    }
    
    return currentLevel;
  }
  
  public Map<String,String> buildSkillLevelsMapForModal(Career_Architecture_Skills_Plan__c casp){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    if(casp.Skill__r.Level1__c!=null){
      skillLevels_Map.put('1', casp.Skill__r.Level1__c);
    }
    if(casp.Skill__r.Level2__c!=null){
      skillLevels_Map.put('2', casp.Skill__r.Level2__c);
    }
    if(casp.Skill__r.Level3__c!=null){
      skillLevels_Map.put('3', casp.Skill__r.Level3__c);
    }
    if(casp.Skill__r.Level4__c!=null){
      skillLevels_Map.put('4', casp.Skill__r.Level4__c);
    }
    if(casp.Skill__r.Level5__c!=null){
      skillLevels_Map.put('5', casp.Skill__r.Level5__c);
    }
    if(casp.Skill__r.Level6__c!=null){
      skillLevels_Map.put('6', casp.Skill__r.Level6__c);
    }
    if(casp.Skill__r.Level7__c!=null){
      skillLevels_Map.put('7', casp.Skill__r.Level7__c);
    }    
    return skillLevels_Map;
  }
  
  public Map<String,String> buildSkillLevelsMapForModal_FromSkill(ProfileSkill ps){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    if(ps.Level1__c!=null){
      skillLevels_Map.put('1', ps.Level1__c);
    }
    if(ps.Level2__c!=null){
      skillLevels_Map.put('2', ps.Level2__c);
    }
    if(ps.Level3__c!=null){
      skillLevels_Map.put('3', ps.Level3__c);
    }
    if(ps.Level4__c!=null){
      skillLevels_Map.put('4', ps.Level4__c);
    }
    if(ps.Level5__c!=null){
      skillLevels_Map.put('5', ps.Level5__c);
    }
    if(ps.Level6__c!=null){
      skillLevels_Map.put('6', ps.Level6__c);
    }
    if(ps.Level7__c!=null){
      skillLevels_Map.put('7', ps.Level7__c);
    }
    
    return skillLevels_Map;
  }
  
  public Map<String,String> buildSkillLevelsMapForModal_FromExpSkillSet(Experian_Skill_Set__c es){
    Map<String,String> skillLevels_Map = new Map<String,String>();
    
    List<Career_Architecture_Skills_Plan__c> casp_List = [SELECT id, Skill__r.Name FROM Career_Architecture_Skills_Plan__c
                                                          WHERE Experian_Skill_Set__c = :es.id AND
                                                          Career_Architecture_User_Profile__c IN :currentUserProfile
                                                          ];
    Integer i=0;
    for(Career_Architecture_Skills_Plan__c casp : casp_List){
      i++;
      skillLevels_Map.put(String.valueOf(i), casp.Skill__r.Name);
    }
    return skillLevels_Map;
  }
   
}