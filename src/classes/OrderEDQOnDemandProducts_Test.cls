/**********************************************************************************************
 * Experian 
 * Name         : OrderEDQOnDemandProducts_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleOrderEDQOnDemandProducts" & OrderEDQOnDemandProducts
 * Created Date : September 9th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Dec 4th, 2015                Paul Kissick                Fixed failing test (testSchedulable (System.ListException: Row with duplicate Name at index: 1))
***********************************************************************************************/

@isTest
private class OrderEDQOnDemandProducts_Test {
    
  static testMethod void testSchedulable() {
      
    test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('Order EDQ On Demand Products Test', CRON_EXP, new ScheduleOrderEDQOnDemandProducts());
      
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                      FROM CronTrigger 
                      WHERE id = :jobId];     
    test.stopTest();
    // Verify the expressions are the same  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run  
    System.assertEquals(0, ct.TimesTriggered);
  }
    
    
  private static testMethod void testAccountEDQDependencyC2() {
    Test.startTest();
    OrderEDQOnDemandProducts batchToProcess = new OrderEDQOnDemandProducts();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM Order_Line_Item__c WHERE EDQ_On_Demand_Product__c = true]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(false);
    tstAcc.EDQ_Dependency_Exists__c = true;
    tstAcc.SaaS__c = false;
    insert new List<Account>{tstAcc};
    
    Contact tstCon = Test_utils.insertEDQContact(tstAcc.ID,true);
    
    update new List<Contact>{tstCon};
    
    User tstUser = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Business_Unit__c = 'NA MS Data Quality';
    
    insert new List<User>{tstUser};
    IsDataAdmin__c IsdataAdmin = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    
    insert IsdataAdmin;
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.OwnerId = tstUser.Id;
    tstOpp.StageName = Constants.OPPTY_STAGE_7;
        
    insert new List<Opportunity>{tstOpp};
    
    Order__c tstOrd = Test_utils.insertOrder(true,tstAcc.ID,tstCon.ID,tstOpp.ID);
    tstOrd.OwnerId = tstUser.Id;
                
    update new List<Order__c>{tstOrd};
    
    Billing_Product__c tstBillP = Test_utils.createBillingProduct(false);
                
    insert new List<Billing_Product__c>{tstBillP};
    
    Order_Line_Item__c tstOrdli = Test_utils.insertOrderLineItems(true,tstOrd.ID,tstBillP.ID);
    
    tstOrdli.EDQ_On_Demand_Product__c = true;
                
    update new List<Order_Line_Item__c>{tstOrdli};
    
    delete IsdataAdmin;
    
  }
  
}