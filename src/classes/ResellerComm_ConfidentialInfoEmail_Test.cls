@isTest
private class ResellerComm_ConfidentialInfoEmail_Test {
    
    @isTest static void test_method_one() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {

        List<Profile> stratProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Strategic'];
        List<Profile> resellerProfileList = [Select Id From Profile Where Name = 'Xperian Global Community Resellers'];

        

        Global_Settings__c setting = new Global_Settings__c();
        setting.Name = 'Global';
        setting.Account_Team_Member_Default_Role__c = 'Account Manager';
        insert setting;

        Account acc =  new Account(Name = 'Test Account');
        Insert acc;

        Contact cont1 = new Contact(firstName = 'FirstNameTest',
                                    lastName = 'Experian',
                                    email = 'tt@experian.com',
                                    Is_Community_User__c = true,
                                    AccountId = acc.Id);
        insert cont1;

        Contact cont2 = new Contact(firstName = 'testTryFirstName2',
                                    lastName = 'testLastNametry',
                                    email = 'tt2@experian.com',
                                    Is_Community_User__c = true,
                                    AccountId = acc.Id);
        insert cont2;

        User user1 = new User(
                    ProfileId = stratProfileList[0].Id,
                    Username = System.now().millisecond() + 'test2@test.com',
                    Alias = 'batman',
                    Email='tt@experian.com',
                    EmailEncodingKey='UTF-8',
                    Lastname='testLastName',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago',
                    ContactId = cont1.Id
                    );
        insert user1;

        User user2 = new User(
                    ProfileId = resellerProfileList[0].Id,
                    Username = System.now().millisecond() + 'test2@test.com',
                    Alias = 'man',
                    Email='tt2@experian.com',
                    EmailEncodingKey='UTF-8',
                    Lastname='testLastName2',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago',
                    ContactId = cont2.Id
                    );
        insert user2;

        Confidential_Information__c confInfo1 = new Confidential_Information__c(Account__c = acc.Id,
                                                    Document_Type__c = 'Strat Clients Document');
        insert confInfo1;

        Confidential_Information__c confInfo2 = new Confidential_Information__c(Account__c = acc.Id,
                                                    Document_Type__c = 'BIS Community Document');
        insert confInfo2;

        ApexPages.StandardController stdCtrl1 = new ApexPages.StandardController(confInfo1);
        PageReference sendEmailPage1 = new PageReference('/apex/ResellerComm_ConfidentialInfoEmail');
        test.SetCurrentPage(sendEmailPage1);
        ResellerComm_ConfidentialInfoEmail pageController1 = new ResellerComm_ConfidentialInfoEmail(stdCtrl1);
        pageController1.finalConfInfo = confInfo1.Id;
        pageController1.sendEmail();

        ApexPages.StandardController stdCtrl2 = new ApexPages.StandardController(confInfo2);
        PageReference sendEmailPage2 = new PageReference('/apex/ResellerComm_ConfidentialInfoEmail');
        test.SetCurrentPage(sendEmailPage2);
        ResellerComm_ConfidentialInfoEmail pageController2 = new ResellerComm_ConfidentialInfoEmail(stdCtrl2);
        pageController2.finalConfInfo = confInfo2.Id;
        pageController2.sendEmail();

        }

    }
    
    
    
}