/**=====================================================================
 * Experian
 * Name: OrderSplitTriggerHandler_Test
 * Description: Test Class for OrderSplitTriggerHandler
 *
 * Created Date: Sept. 15th 2017
 * Created By: James Wills
 * 
 * Date Modified        Modified By           Description of the update
 *
=====================================================================*/
@isTest
private class OrderSplitTriggerHandler_Test{

 private static testMethod void test_ITCA_Homepage_Controller_1(){
            
    Test.startTest();
  
      Order_Split__c os1 = new Order_Split__c();
      os1.Order__c = [SELECT id FROM Order__c LIMIT 1].id;
      
      insert os1;  
      
      //System.assert([SELECT id FROM Order_Split_Product__c].size()>0, 'No Order_Split_Products__c have been created.');
      
      os1.Split_Note__c = 'Test';
      
      update os1;      
      
    Test.stopTest();
  
  }
  

  @testSetup
  private static void createData(){
  
    Order__c ord1 = new Order__c();
        
    insert ord1;
        
    Product2 prd1 = new Product2();
    prd1.Name='Test1';
    
    insert prd1;
    
    Order_Line_Item__c oli1 = new Order_Line_Item__c();
    oli1.Order__c   = ord1.id;
    oli1.Product__c = prd1.id;
    
    insert oli1;
        
  }



}