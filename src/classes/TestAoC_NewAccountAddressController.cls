/**=====================================================================
 * Appirio Inc
 * Name: TestAoC_NewAccountAddressController.cls
 * Description: 
 * Created Date: Nov 11, 2013
 * Created By: Mohammed Irfan (Appirio)
 *
 * Date Modified         Modified By          Description of the update
 * January 28th, 2014    Nathalie Le Guay     Adding duplicate check tests
 * Mar 04th, 2014        Arpita Bose(Appirio) T-243282: Added Constants in place of String
 * Aprl 22nd, 2014       Aditi Bhardwaj  - Modify myUnitTest method to increase coverage  
 * Sep 11th, 2014        Naresh kr Ojha       Test class fix for issue: I-130073 
 * Aug 5th, 2015         Paul Kissick         Case #1084152: Duplicate Management (changes from controller to extension)
 * Apr 15th,2016         Sadar Yacob          Case 2376: State and Country Pick lists implementation
 * Dec 6th ,2016         Sadar Yacob          Remove QAS v4 code components  
 * Jun 06th, 2017        Manoj Gopu           Fixed test method Failure testNewAccountAddressAndNewContactExisting  Inserted Custom Setting
 =====================================================================*/
@isTest
private class TestAoC_NewAccountAddressController {
  
  
  static testMethod void myUnitTest() {
    
    Account acc = [SELECT Id,Name,BillingCountry FROM Account WHERE Name = '00TestAccount0' LIMIT 1];
    
    ApexPages.StandardController sc = new ApexPages.StandardController(acc); 
    
    
    Address__c addr = [
      SELECT Id, Address_1__c, Address_2__c, Address_3__c, State__c, City__c, Zip__c, Country__c 
      FROM Address__c 
      WHERE Address_1__c = '818 40th Avenue' 
      LIMIT 1
    ];
    
    Account_Address__c accAddrRel = new Account_Address__c(Account__c=acc.id,Address__c=addr.id);
    insert accAddrRel;
       
    ApexPages.currentPage().getParameters().put('accName', 'Test Acc');
    AddOrCreateNewAccountAddressController cntInit = new AddOrCreateNewAccountAddressController(sc);
       
    ApexPages.currentPage().getParameters().put('accId',acc.id);
    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_AddAddress);
    ApexPages.currentPage().getParameters().put('addrId',accAddrRel.Id+';'+addr.Id); 
        
    AddOrCreateNewAccountAddressController cnt = new AddOrCreateNewAccountAddressController(sc);
    
    cnt.address.Validation_Status__c='test';
    cnt.performSave();

    ApexPages.currentPage().getParameters().put('addrId',accAddrRel.id+';'+addr.id); 
    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_AddAddress);
    cnt.performSave();

    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_NewAccountAddress);
    cnt.performSave();
    cnt.cancel();
    cnt.blankCall();
        
  }
  
  
  static testMethod void performDuplicateChecks() {
    // Assumes duplicate checking is enabled!
    Account acc1 = [SELECT Id,Name,BillingCountry, BillingStreet, BillingCity, BillingState, BillingPostalCode FROM Account WHERE Name = '00TestAccount0' LIMIT 1];
    
    Address__c addr = [SELECT Id, Address_1__c, Address_2__c, City__c, Zip__c, State__c, Country__c FROM Address__c WHERE Address_1__c = '818 40th Avenue' LIMIT 1];
    
    Account_Address__c accAddrRel = new Account_Address__c(Account__c=acc1.id,Address__c=addr.id);
    insert accAddrRel;
    
    acc1.BillingStreet = addr.Address_1__c;
    acc1.BillingCity = addr.City__c;
    acc1.BillingState = addr.State__c;
    acc1.BillingCountry = addr.Country__c;
    acc1.BillingPostalCode = addr.Zip__c;
    update acc1;
    
    Test.startTest();
    
    Account acc = new Account();
    
    ApexPages.StandardController sc = new ApexPages.StandardController(acc); 
       
    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_NewAccountAddress);
    // ApexPages.currentPage().getParameters().put('bypassQAS','0');
    
    AddOrCreateNewAccountAddressController cnt = new AddOrCreateNewAccountAddressController(sc);
    
    cnt.account.Name = '00TestAccount0';
    cnt.address.Address_1__c = addr.Address_1__c;
    cnt.address.Address_2__c = addr.Address_2__c;
    cnt.address.City__c = addr.City__c;
    cnt.address.Zip__c = addr.Zip__c;
    cnt.address.State__c = addr.State__c;
    cnt.address.Country__c = addr.Country__c;
    cnt.address.Validation_Status__c='Registered';
    cnt.accountAddress.Address_Type__c = 'Registered';
    
    cnt.performSave();
    
    // Commenting this out below until api v35 which has isactive column on the duplicaterule object!
    // Integer ruleCount = [SELECT COUNT() FROM DuplicateRule WHERE SobjectType = 'Account' AND IsActive = true];
    //if (ruleCount > 0) {
      // Duplicate rules are enabled, so we can check assertions.
    //}
    
    // system.assert(ApexPages.getMessages().get(0).getDetail().contains(Label.New_Record_Duplicates_Found));
    
    cnt.account.Name = 'Another Account Name';
    
    cnt.performSave();
    
    // system.assert(ApexPages.getMessages().get(0).getDetail().contains(Label.New_Record_Possible_Duplicates));
    
    cnt.performSaveAnyway();
    
    //cnt.performSaveAnyway();
    
    Test.stopTest();         
  }
  
  
  static testMethod void testNewAccountAddressAndNewAccountExisting() {
    
    
    Account acc = [SELECT Id,Name,BillingCountry FROM Account WHERE Name = '00TestAccount0' LIMIT 1];
    
    ApexPages.StandardController sc = new ApexPages.standardController(acc);
    

    ApexPages.currentPage().getParameters().put('accId',acc.id);
    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_AddAddress);

    AddOrCreateNewAccountAddressController cnt = new AddOrCreateNewAccountAddressController(sc);

    // Assign the address selected as being unique
    cnt.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='test';
    
    // This should create an Account, an Address record and an Account_Address__c junction record
    cnt.performSave();
    
    
    
    // Unique Address which we want to verify will not be created as a duplicate
    Address__c addressVerif = [SELECT Id, Address_Id__c FROM Address__c WHERE Id =: cnt.address.Id];
    
    Account_Address__c accountAddressFirst = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                              FROM Account_Address__c
                                              WHERE Account__c =: cnt.account.Id AND Address__c =: cnt.address.Id];

    // Address_Id__c is the generated key
    System.assertNotEquals(null, addressVerif.Address_Id__c);
    
    // Checks on the junction object
    System.assertEquals(addressVerif.Id, accountAddressFirst.Address__c);
    
  }
  
  static testMethod void testNewAccountAddressAndNewContactExisting() {
    
    Account acc = [SELECT Id,Name,BillingCountry FROM Account WHERE Name = '00TestAccount0' LIMIT 1];
    
    ApexPages.StandardController sc = new ApexPages.standardController(acc);

    ApexPages.currentPage().getParameters().put('accId',acc.id);
    ApexPages.currentPage().getParameters().put('action',Label.ASS_Operation_AddAddress);

    AddOrCreateNewAccountAddressController cnt = new AddOrCreateNewAccountAddressController(sc);

    // Assign the address selected as being unique
    cnt.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cnt.address.Validation_Status__c='test';

    // This should create an Account, an Address record and an Account_Address__c junction record
    cnt.performSave();
    
    // Unique Address which we want to verify will not be created as a duplicate
    Address__c addressVerif = [SELECT Id, Address_Id__c FROM Address__c WHERE Id =: cnt.address.Id];
    Account_Address__c accountAddressFirst = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                              FROM Account_Address__c
                                              WHERE Account__c =: cnt.account.Id AND Address__c =: cnt.address.Id];

    // Address_Id__c is the generated key
    System.assertNotEquals(null, addressVerif.Address_Id__c);
    
    // Checks on the junction object
    System.assertEquals(addressVerif.Id, accountAddressFirst.Address__c);
    
    // Specifying that a new address needs to be created
    //cntContact.action = Label.CSS_Operation_NewContactAddress;
    ApexPages.currentPage().getParameters().put('action', Label.CSS_Operation_NewContactAddress);
    
    ApexPages.StandardController scCnt = new ApexPages.standardController(new Contact());
    
    Serasa_User_Profiles__c objCustStt = new Serasa_User_Profiles__c();
    objCustStt.Name = 'Serasa Customer Care Profiles';
    objCustStt.Profiles__c = 'test,test1';
    insert objCustStt;
    
    AddOrCreateNewContactAddressController cntContact = new AddOrCreateNewContactAddressController(scCnt);
    
    // Create new Contact
    cntContact.contact = new Contact(FirstName= '00TestContact0',LastName = 'Sweden');

    // Assign the same address as for the Account
    cntContact.address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    cntContact.address.Validation_Status__c='test';

    Test.StartTest();
    // This should use the existing address record
    cntContact.performSave();
    Test.StopTest();

    // Getting the created Contact_Address__c junction record. We want to verify it links to the existing Address__c record
    Contact_Address__c contactAddressSecond = [SELECT Id, Address__r.Id, Address__r.Address_Id__c
                                               FROM Contact_Address__c
                                               WHERE Contact__c =: cntContact.contact.Id AND Address__c =: cntContact.address.Id];
    System.assertEquals(addressVerif.Id, contactAddressSecond.Address__c);
  }
  

  @testSetup
  private static void createTestData() {
    Global_Settings__c custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING,Smart_Search_Query_Limit__c=250);
    insert custSettings;
    Account acc = new Account(Name = '00TestAccount0',BillingCountry = 'Sweden');
    insert acc;
    Address__c address = new Address__c(Address_1__c='818 40th Avenue', Address_2__c='',Address_3__c='',
                          State__c='California',City__c='San Francisco',zip__c='94121',Country__c='United States of America');
    insert address;
  }
}