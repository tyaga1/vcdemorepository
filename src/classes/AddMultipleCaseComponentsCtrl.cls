/**=====================================================================
 * Name: AddMultipleCaseComponentsCtrl
 * Description: This class is used to associate multiple case components to a Deployment Request
 * Created Date: July  2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * March 8th 2016               Sadar Yacob                  Case 01825428: Added support for Agile Accelerator Stories
=====================================================================**/

public class AddMultipleCaseComponentsCtrl {

  public String objectId {get;set;}
  
  public String casePrefix = Case.sObjectType.getDescribe().getKeyPrefix();
  public String storyPrefix = Story__c.sObjectType.getDescribe().getKeyPrefix();
  public String agileStoryPrefix = agf__ADM_Work__c.sObjectType.getDescribe().getKeyPrefix();
  
  public String myIdPrefix;
  private String queryString;
  public Integer noOfRecords {get;set;}
  public List<ObjectWrapper> metadataComponentList;
  
  private Map<Id, ObjectWrapper> selectedComponentsMap = new Map<Id, ObjectWrapper>();

  
  public final static Integer PAGE_SIZE = 100;
  public Boolean firstLoaded = true;
  
  public Boolean canProceed {get{if (canProceed == null) canProceed = true; return canProceed;}set;}
  public Boolean showTable {get{if (showTable == null) showTable = true; return showTable;}set;}
  
  public List<SelectOption> componentTypeList { 
    get {
      if (componentTypeList == null) {
        componentTypeList = new List<SelectOption>();
        List<Schema.PicklistEntry> componentTypeFieldPickList = Metadata_Component__c.Component_Type__c.getDescribe().getPicklistValues();
        componentTypeList.add(new SelectOption('', '--None--'));
        for (Schema.PicklistEntry pl : componentTypeFieldPickList) {
          componentTypeList.add(new SelectOption(pl.getValue(), pl.getLabel()));
        }
      }
      return componentTypeList;
    } 
    set;
  }
  
  public List<SelectOption> deploymentTypeList {
    get {
      if (deploymentTypeList == null) {
        deploymentTypeList = new List<SelectOption>();
        List<Schema.PicklistEntry> plVals = Case_Component__c.Deployment_Type__c.getDescribe().getPicklistValues();
        deploymentTypeList.add(new SelectOption('', '-- Choose --'));
        for (Schema.PicklistEntry pl : plVals) {
          deploymentTypeList.add(new SelectOption(pl.getValue(), pl.getLabel()));
        }
      }
      return deploymentTypeList;
    }
    set;
  }
  
  public List<SelectOption> actionList {
    get {
      if (actionList == null) {
        actionList = new List<SelectOption>();
        List<Schema.PicklistEntry> plVals = Case_Component__c.Action__c.getDescribe().getPicklistValues();
        actionList.add(new SelectOption('', '--Choose--'));
        for (Schema.PicklistEntry pl : plVals) {
          actionList.add(new SelectOption(pl.getValue(), pl.getLabel()));
        }
      }
      return actionList;
    }
    set;
  }
  
  public List<SelectOption> configTypeList {
    get {
      if (configTypeList == null) {
        configTypeList = new List<SelectOption>();
        List<Schema.PicklistEntry> plVals = Case_Component__c.Configuration_Type__c.getDescribe().getPicklistValues();
        configTypeList.add(new SelectOption('', '--Choose--'));
        for (Schema.PicklistEntry pl : plVals) {
          configTypeList.add(new SelectOption(pl.getValue(), pl.getLabel()));
        }
      }
      return configTypeList;
    }
    set;
  }
  
  
  
  public SearchCriteria searchCriteria {get;set;}
  public List<Case_Component__c> selectedComponentList;
  
  public class NoSelectedItemException extends Exception {}
  
  public class ObjectWrapper {
    public Boolean selected { get; set; }
    public Metadata_Component__c obj { get; set; }
    public String selectedDeploymentType {get; set;}
    public String selectedAction {get; set;}
    public String manualSteps {get; set;}
    public String selectedConfigType {get; set;}

    public ObjectWrapper (Metadata_Component__c metadataComponent, ObjectWrapper wrapper, Boolean s) {
      obj = metadataComponent;
      if (wrapper != null) {
        selectedDeploymentType = wrapper.selectedDeploymentType;
        selectedAction = wrapper.selectedAction;
        manualSteps = wrapper.manualSteps;
        selectedConfigType = wrapper.selectedConfigType;
      }
      selected = s;
    }
  }
  
  public class SearchCriteria {
    public String componentName {get;set;}
    public String componentType {get;set;}
    public String DeploymentType {get;set;}
    public String action {get;set;}
  }

  //===========================================================================
  // Constructor
  //===========================================================================
  public AddMultipleCaseComponentsCtrl() {
    try {
      objectId = ApexPages.currentPage().getParameters().get('Id');
      myIdPrefix = String.valueOf(objectId).substring(0,3);
      initializeSearchCriteria();
    }
    catch (Exception e) {
      handleError(e);
    }
  }
  
  //===========================================================================
  //
  //===========================================================================
  public List<ObjectWrapper> getMetadataComponentList() {
    if (firstLoaded) {
      metadataComponentList = new List<ObjectWrapper>();
      return metadataComponentList;
    }

    List<Metadata_Component__c> metadataTempComponentList = componentRecords.getRecords();
    system.debug('###Record' +componentRecords.getRecords());
    
    metadataComponentList = new List<ObjectWrapper>();
    for (Metadata_Component__c mc : metadataTempComponentList) {
      
      ObjectWrapper wrapper = selectedComponentsMap.get(mc.Id);
      metadataComponentList.add(
        new ObjectWrapper(
          mc, 
          wrapper, 
          selectedComponentsMap.containsKey(mc.Id)
        )
      );
    }
    return metadataComponentList;

  }

  public void setMetadataComponentList(List<ObjectWrapper> metadataComponentList) {
    this.metadataComponentList = metadataComponentList;
  }

  public Integer getPageSize() {
    return PAGE_SIZE;
  }

  public Integer getMetadataComponentListCount() {
    return metadataComponentList.size();
  }
  
  private void clearSearchCriteria() {
    searchCriteria = new SearchCriteria();
  }

  private void clearSelectedComponents() {
    selectedComponentsMap.clear();
  }

  // To get picklist value of MetadataType
  private void initializeSearchCriteria() {
    showTable = false;
    searchCriteria = new SearchCriteria();
  }

  private void querySearchComponent() {
    componentRecords = null;
    firstLoaded = false;
    showTable = true;
    queryString = 'SELECT Id, Name, Component_Type__c, Object_API_Name__c, Component_API_Name__c ' +
                  'FROM Metadata_Component__c ';

    List<String> fieldSearchStringList = new List<String>();
    if (String.isNotBlank(searchCriteria.componentType)) {
      fieldSearchStringList.add('Component_Type__c = \'' + searchCriteria.componentType + '\'');
    }

    if (String.isNotBlank(searchCriteria.componentName)) {
      fieldSearchStringList.add('Name LIKE \'%' + searchCriteria.componentName + '%\'');
    }
    
    if (!fieldSearchStringList.isEmpty()) {
      queryString += ' WHERE ' + String.join(fieldSearchStringList, ' AND ');
    }
    queryString += ' ORDER BY Component_Type__c, Object_API_Name__c, Component_API_Name__c LIMIT 10000';
    
    system.debug(queryString);
  }

  public ApexPages.StandardSetController componentRecords {
    get {
      system.debug('###queryString :  ' + queryString);
      if (componentRecords == null) {
        system.debug('###Re-searching the data and persists the records');
        componentRecords = new ApexPages.StandardSetController(
          Database.getQueryLocator(queryString)
        );
        componentRecords.setPageSize(PAGE_SIZE);
        noOfRecords = componentRecords.getResultSize();
      }
      return componentRecords;
    }
    private set;
  }

  //
  public PageReference search() {
    try {
      //clearSelectedComponents();
      showTable = true;
      querySearchComponent();
    }
    catch (Exception e) {
      handleError(e);
    }
    return null;
  }

  public PageReference addComponent() {
    try {
      keepSelectedComponentIntoMemory();
      addComponentIntoDb();
      clearSelectedComponents();
      clearSearchCriteria();
      querySearchComponent();
    }
    catch (Exception e) {
      handleError(e);
      return null;
    }
    return back();
  }

  private void handleError(Exception e) {
    String errMsg;
    system.debug('### error : ' + e.getStackTraceString());
    system.debug('### error Message: ' + e.getMessage());
    if (e instanceOf NoSelectedItemException) {
      createError('Please select at least one component to be added.'); //TO create custom label
      return;
    }
    createError('An unexpected error has occurred. Please contact the administrator.'); //TO create custom label
  }

  private void createError(String errMsg) {
    ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, errMsg));
  }

  private void createInfo(String msg) {
    ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.CONFIRM, msg));
  }

  public PageReference back() {
    return new PageReference('/' + objectId+'#'+ objectId+'_00Ne0000001JP8n_target'); //Need to update as need to get the object ID
  }

  //Method to add component into Metadata Component object.
  private void addComponentIntoDb() {
    List<Case_Component__c> lstCaseComponents = new List<Case_Component__c>();
    for (ObjectWrapper wrapper : selectedComponentsMap.values()) {
      if (wrapper.selected) {
        Case_Component__c cc = new Case_Component__c(
          Component_Name__c = wrapper.obj.Id, 
          Deployment_Type__c = wrapper.selectedDeploymentType, 
          Action__c = wrapper.selectedAction, 
          Manual_Step_Details__c = wrapper.manualSteps,
          Configuration_Type__c = wrapper.selectedConfigType
        );
        if (myIdPrefix == storyPrefix) {
          cc.Story__c = objectId;
        }
        else if (myIdPrefix == casePrefix) {
          cc.Case_Number__c = objectId;
        }
        else if (myIdPrefix == agileStoryPrefix) {
          cc.User_Story_AA__c = objectId;
        }
        lstCaseComponents.add(cc);
      }
    }
    if (lstCaseComponents.isEmpty()) {
      throw new NoSelectedItemException();
    }
    else {
      insert lstCaseComponents;
    }
  }

  //Method to keep all the selected metadata into memory therefore selection will not unchecked if user go to next page.
  private void keepSelectedComponentIntoMemory() {
    for (ObjectWrapper wrapper : metadataComponentList) {
      if (wrapper.selected) {
        selectedComponentsMap.put(wrapper.obj.Id, wrapper);
      }
      else {
        selectedComponentsMap.remove(wrapper.obj.Id);
      }
    }
  }

  //TO KEEP SELECTION INTO MEMORY
  public PageReference previous() {
    try {
      keepSelectedComponentIntoMemory();
      componentRecords.previous();

    }
    catch (Exception e) {
      handleError(e);
    }
    return null;
  }

  public PageReference next() {
    try {
      keepSelectedComponentIntoMemory();
      componentRecords.next();
    }
    catch (Exception e) {
      handleError(e);
    }
    return null;
  }

  public PageReference first() {
    try {
      keepSelectedComponentIntoMemory();
      componentRecords.first();
    }
    catch (Exception e) {
      handleError(e);
    }
    return null;
  }

  public PageReference last() {
    try {
      keepSelectedComponentIntoMemory();
      componentRecords.last();
    }
    catch (Exception e) {
      handleError(e);
    }
    return null;
  }

  


}