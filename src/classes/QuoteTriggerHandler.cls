/*=============================================================================
 * Experian
 * Name: QuoteTriggerHandler
 * Description: Handler class for QuoteTrigger
 * Created Date: 14th Jan 2016
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 14th Jan, 2016     James Wills           Case 01059650: Added updateQuoteRelationshipCounts to afterInsert, afterDelete
 * 6th Jun, 2016      Esteban Castro        Case 01989024: Added validateOpportunityReferenceNotChanged method for Quote__c validation to prevent update Opportunity Reference
 * 28th Jul, 2016     Diego Olarte          CRM2:W-005496: Added new method primaryQuoteApproved to afterUpdate
 * 1st Aug, 2016      Paul Kissick          Fixed Comment header to match Dev Standards. Also tidied up the code a bit
 =============================================================================*/
public without sharing class QuoteTriggerHandler {    
  
  public static void afterInsert(List<Quote__c> newList) {
    updateQuoteRelationshipCounts(newList);           
  }
    
  public static void afterDelete(List<Quote__c> oldList) {
    updateQuoteRelationshipCounts(oldList);    
  }
    
  public static void beforeUpdate(List<Quote__c> newList, Map<Id, Quote__c> oldMap) {
    validateOpportunityReferenceNotChanged(newList, oldMap);
  }
  
  public static void afterUpdate(List<Quote__c> newList, Map<Id, Quote__c> oldMap) {
    primaryQuoteApproved(newList);
  }
    
  //Purpose of Method: Update the Quote_Count__c Custom Field for all Opportunity Parents of all Inserted/Deleted/updated Quote__c records   
  public static void updateQuoteRelationshipCounts(List<Quote__c> updatedQuoteList){    
    List<Opportunity> parentOpportunitiesList = getParentOpportunitiesList(updatedQuoteList);    

    if (!parentOpportunitiesList.isEmpty()) {
      for (Opportunity parentOpportunity : parentOpportunitiesList) {
        parentOpportunity.Quote_Count__c = parentOpportunity.Quotes__r.size();
      }
      update parentOpportunitiesList;
    }
  }

  //Purpose of Method: Return a list of parent Contact records for inserted/deleted/updated(Relationship_Owner__c Custom field only) Contact_Team__c records
  public static List<Opportunity> getParentOpportunitiesList(List<Quote__c> listOfQuoteRecords){
    List<ID> parentOpportunitiesOfQuoteRecords = new List<ID>();
    List<Opportunity> parentOpportunitiesList = new List<Opportunity>();    

    if (!listOfQuoteRecords.isEmpty()) {  
      for (Quote__c quoteRecord : listOfQuoteRecords) {
        parentOpportunitiesOfQuoteRecords.add(quoteRecord.Opportunity__c);
      }    
      parentOpportunitiesList = [SELECT Id, (SELECT Id FROM Quotes__r) FROM Opportunity WHERE Id IN :parentOpportunitiesOfQuoteRecords];
    }    
    return parentOpportunitiesList;
  }
  
  //Purpose of Method: validate the old Opportunity__c equals new Opportunity__c; else fail.
  // PK: Fixed reference to newMap, and used newList instead - for consistency 
  public static void validateOpportunityReferenceNotChanged(List<Quote__c> newList, Map<Id, Quote__c> oldMap){
    Quote__c qOld;
    for (Quote__c q : newList){
      qOld = oldMap.get(q.Id);

      if (qOld.Opportunity__c != NULL && q.Opportunity__c != qOld.Opportunity__c) {
        throw new QuoteTriggerException('Quote Trigger: Quote opportunity reference update not allowed. Old opportunity Id: ' + 
                                        qOld.Opportunity__c + '; New Opportunity ID: ' + q.Opportunity__c);
      }
    }
  }
  
  //Purpose: General Exception class
  public class QuoteTriggerException extends Exception{}
  
  //Purpose of Method: Set the Opportunity.Primary_Quote_has_been_Approved__c to true when quote has been approved
  public static void primaryQuoteApproved(List<Quote__c> newQuoteList){
    
    Set<Id> opportunityIdSet = new Set<Id>();
    
    for (Quote__c pQ : newQuoteList){
      if (pQ.Opportunity__c != null && (pQ.Status__c == System.Label.CPQ_Completed_Status || pQ.Status__c == System.Label.CPQ_Approved_Status)){
        opportunityIdSet.add(pQ.Opportunity__c);
      }
    }
    
    if (!opportunityIdSet.isEmpty()) {
    
      List<Opportunity> oppToUpdate = [
        SELECT Id, Primary_Quote_has_been_Approved__c
        FROM Opportunity
        WHERE Id IN :opportunityIdSet
      ];
      
      if (!oppToUpdate.isEmpty()) {
        for (Opportunity opp : oppToUpdate) {
          opp.Primary_Quote_has_been_Approved__c = true;
        }
        try {
          update oppToUpdate;
        }
        catch (DMLException ex) {
          ApexLogHandler.createLogAndSave('QuoteTriggerHandler','primaryQuoteApproved', ex.getStackTraceString(), ex);
        }
      }
    }
  } 
}