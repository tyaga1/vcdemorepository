/**=====================================================================
 * Experian
 * Name: SalesSupportRequestTriggerHandler_Test
 * Description: Test class for SalesSupportRequestTriggerHandler
 * Created Date: Unknown
 * Created By: Unknown
 *
 * Date Modified      Modified By   Description of the update
 * Sep 22nd, 2015     Paul Kissick  I-179463 Duplicate Management Rule Failures
 * Dec 13th, 2016     Manoj Gopu    Case 02196955: Updated the test method testSetOwnerManager to Increase the code coverage.
 * Jan 12th, 2016     Diego Olarte  Case 02184830: Created the test method testCreateSalesSupportRequest to Increase the code coverage.
 * Jan 31st, 2017     Sanket Vaidya Case #02265406: Added test methods for addPreSales_NA_EDQ_OwnerToOpportunityTeam()
 * Mar 7th, 2017      Sanket Vaidya Case #02300472: Added test method for CreateSalesSupportRequest when decision-maker does not exist.
 * Aug 08th, 2017     Manoj Gopu    Case #1340777:  Fixed test class Failure and added required Fields 
 *===================================================================== */
@isTest
public class SalesSupportRequestTriggerHandler_Test {

  @isTest
  public static void testCreateSalesSupportClientEcos() {
    Integer i;
    Integer count = 0;
    List<Competitor__c> lstCompetitors = new List<Competitor__c>();
    List<OpportunityContactRole> lstOppContactRoles = new List<OpportunityContactRole>();

    // Create Test Data
    Account testAccount = Test_Utils.insertAccount();

    Account testCompetitorAccount = Test_Utils.createAccount();
    testCompetitorAccount.Is_Competitor__c = true;
    testCompetitorAccount.FUSE_Competitor_Name__c= '11111111';
    Database.insert(testCompetitorAccount,Test_Utils.duplicateRuleOverride());

    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);
    Contact testContact = Test_Utils.insertContact(testAccount.Id);

    Competitor__c testCompetitor = new Competitor__c();
    while(count < 5) {
      // Contact Roles
      OpportunityContactRole testOppContactRole = Test_Utils.insertOpportunityContactRole(false, testOpportunity.Id, testContact.Id, Constants.OPPTY_CONTACT_ROLE_PURCHASE_LEDGER, true);
      lstOppContactRoles.add(testOppContactRole);
      // Competitors
      testCompetitor = Test_Utils.createCompetitor(testOpportunity.Id);
      testCompetitor.Account__c = testCompetitorAccount.Id;
      testCompetitor.Incumbent__c = true;
      lstCompetitors.add(testCompetitor);
      count++;
    }
    insert lstOppContactRoles;
    insert lstCompetitors;

    Test.startTest();

    Sales_Support_Request__c testSSR = new Sales_Support_Request__c(Opportunity__c = testOpportunity.Id,Proposition_Type__c = 'Data Clean', Description__c = 'SSR Pre Sales Request');
    insert testSSR;

    Test.stopTest();
    // Competitors. Should be 5.
    i = 0;
    for(Sales_Support_Competitor__c SSC : [select Id, Name from Sales_Support_Competitor__c where Sales_Support_Request__c =: testSSR.Id]) {
      i = i+1;
    }
    System.assertEquals(5, i);
    // Key Contacts. Should only be 1.
    i = 0;
    for(Sales_Support_Key_Contact__c SSC : [select Id, Name from Sales_Support_Key_Contact__c where Sales_Support_Request__c =: testSSR.Id]) {
      i = i+1;
    }
    System.assertEquals(1, i);
  }

  @isTest
  public static void testSetOwnerManager() {

    // Create Test Data
    Account testAccount = Test_Utils.insertAccount();

    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);
    Contact testContact = Test_Utils.insertContact(testAccount.Id);
    User testUser = Test_Utils.insertUser(Constants.PROFILE_EXP_SALES_EXEC);
    testUser.ManagerId = UserInfo.getUserId();
    update testUser;

    Test.startTest();

    Sales_Support_Request__c testSSR = new Sales_Support_Request__c(Opportunity__c = testOpportunity.Id, Proposition_Type__c = 'Data Clean',Description__c = 'SSR Pre Sales Request', OwnerId = testUser.Id);
    insert testSSR;

    testSSR.Assign_Owner_to_Opportunity_Team__c = true;
    update testSSR;

    Test.stopTest();

    for(Sales_Support_Request__c ssr : [select Id, Name, Owner_Manager__c from Sales_Support_Request__c where Id =: testSSR.Id])
    {
      System.assertEquals(ssr.Owner_Manager__c, UserInfo.getUserId());
    }
  }

  @isTest
  public static void CreateSalesSupportRequest_When_OppContactRole_As_DecisionMaker_Exists()
  {
    Id automotiveSSR_RecordTypeId = Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get(label.Automotive_Support_Request).getRecordTypeId();
    Account testAccount = Test_Utils.insertAccount();

    Test.startTest();

    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);
    Contact testContact = Test_Utils.insertContact(testAccount.Id);
    OpportunityContactRole testOppContactRole = Test_Utils.insertOpportunityContactRole(true, testOpportunity.Id, testContact.Id, Constants.OPPTY_CONTACT_ROLE_DECISION_MAKER, true);
    User testUser = Test_Utils.insertUser(Constants.PROFILE_EXP_SALES_EXEC);
    testUser.ManagerId = UserInfo.getUserId();
    update testUser;

    string contactid = testContact.id;

    Sales_Support_Request__c testSSR2 = new Sales_Support_Request__c();
      testSSR2.Opportunity__c = testOpportunity.Id;
      testSSR2.Description__c = 'Automotive Support Request';
      testSSR2.OwnerId = UserInfo.getUserId();
      testSSR2.SOW_Status__c = 'In Progress';
      testSSR2.RecordtypeId = automotiveSSR_RecordTypeId;
      testSSR2.Proposition_Type__c = 'Data Clean';
    insert testSSR2;

    for(Sales_Support_Request__c ssr : [select Id, Client_Contact__c from Sales_Support_Request__c where Id =: testSSR2.Id])
    {
      System.Debug('SSR Created: '+ ssr.Id + '; Client contact:'+ssr.Client_Contact__c);
      System.assertEquals(contactid , ssr.Client_Contact__c);
    }

    Test.stopTest();
  }

  @isTest
  public static void CreateSalesSupportRequest_When_OppContactRole_As_DecisionMaker_Does_Not_Exist() {

    // Create Test Data
    Id automotiveSSR_RecordTypeId = Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get(label.Automotive_Support_Request).getRecordTypeId();

    Account testAccount = Test_Utils.insertAccount();

    Test.startTest();

    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);
    Contact testContact = Test_Utils.insertContact(testAccount.Id);
    User testUser = Test_Utils.insertUser(Constants.PROFILE_EXP_SALES_EXEC);
    testUser.ManagerId = UserInfo.getUserId();
    update testUser;

    string contactid = testContact.id;

    Sales_Support_Request__c testSSR2 = new Sales_Support_Request__c();
      testSSR2.Opportunity__c = testOpportunity.Id;
      testSSR2.Description__c = 'Automotive Support Request';
      testSSR2.OwnerId = UserInfo.getUserId();
      testSSR2.SOW_Status__c = 'In Progress';
      testSSR2.Proposition_Type__c = 'Data Clean';
      testSSR2.RecordtypeId = automotiveSSR_RecordTypeId;
    insert testSSR2;

    for(Sales_Support_Request__c ssr : [select Id, Client_Contact__c from Sales_Support_Request__c where Id =: testSSR2.Id])
    {
      System.assertEquals(null, ssr.Client_Contact__c, 'Client contact is empty because decision maker didn\'t exist.');
    }

    Test.stopTest();
  }

  @isTest(SeeAllData=true) //For Record Type IDs
  public static void addPreSales_NA_EDQ_OwnerToOpportunityTeam_When_Opportunity_Exists()
  {
     // Create Test Data
    Id recordTypeId_preSalesNA_EDQ = Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get('Presales Request NA EDQ').getRecordTypeId();

    Account testAccount = Test_Utils.insertAccount();

    Test.startTest();

    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);

    User newUser = [SELECT NAME FROM USER WHERE PROFILE.NAME = 'System Administrator' AND ID != :UserInfo.GetUserId() LIMIT 1];
    System.Debug('new User : ' + newUser.ID + ' ,  ' + newUser.Name);

    Integer beforeDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: newUser.ID];
    System.Debug('Before: Does User exist in Opportunity Team : [0-No / 1-Yes] ' + beforeDoesUserExistInOppTeam);

    Sales_Support_Request__c testSSR = new Sales_Support_Request__c();
    testSSR.Opportunity__c = testOpportunity.Id;
    testSSR.OwnerId = newUser.Id;
    testSSR.Proposition_Type__c = 'Data Clean';
    testSSR.RecordtypeId = recordTypeId_preSalesNA_EDQ;
    insert testSSR;

    Sales_Support_Request__c retrievedSSR = [SELECT Owner.Name from Sales_Support_Request__c WHERE Id =: testSSR.Id];
    System.Debug('The owner of SSR was updated by a WorkFlow. The Owner is : ' + retrievedSSR.Owner.Name);
    System.assertEquals('NA EDQ Sales Engineering', retrievedSSR.Owner.Name);

    retrievedSSR.OwnerId = newUser.ID;
    update retrievedSSR;

    Integer afterDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: newUser.Id];
    System.Debug('After: Does User exist in Opportunity Team : [0-No / 1-Yes] ' + afterDoesUserExistInOppTeam);

    System.assertNotEquals(beforeDoesUserExistInOppTeam, afterDoesUserExistInOppTeam);

    Test.stopTest();
  }
   
   @isTest(SeeAllData=true) //For Record Type IDs
  public static void addPreSales_NA_EDQ_OwnerToOpportunityTeam_Bulk_TeamMember_Already_Exists()
  {
     // Create Test Data
    Id recordTypeId_preSalesNA_EDQ = Record_Type_Ids__c.getInstance().SSR_Presales_Request_NA_EDQ__c;

    Account testAccount = Test_Utils.insertAccount();
    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);

    List<User> listUsers = [SELECT NAME FROM USER WHERE PROFILE.NAME = 'System Administrator' AND ID != :UserInfo.GetUserId() LIMIT 200];

    OpportunityTeamMember teamMember1 = new OpportunityTeamMember();
    teamMember1.OpportunityId = testOpportunity.Id;
    teamMember1.UserId = listUsers[0].Id;
    teamMember1.TeamMemberRole = Constants.TEAM_ROLE_PRE_SALES_CONSULTANT;

    OpportunityTeamMember teamMember2 = new OpportunityTeamMember();
    teamMember2.OpportunityId = testOpportunity.Id;
    teamMember2.UserId = listUsers[1].Id;
    teamMember2.TeamMemberRole = Constants.TEAM_ROLE_PRE_SALES_CONSULTANT;

    Insert teamMember1;
    Insert teamMember2;


    Test.startTest();

    List<Sales_Support_Request__c> listSSR = new List<Sales_Support_Request__c>();
    for(Integer i=0; i < 200; i++)
    {
      Sales_Support_Request__c testSSR = new Sales_Support_Request__c();
      testSSR.Opportunity__c = testOpportunity.Id;
      testSSR.Proposition_Type__c = 'Data Clean';
      //testSSR.OwnerId = ; Owner will be a Queue
      testSSR.RecordtypeId = recordTypeId_preSalesNA_EDQ;
      listSSR.add(testSSR);
    }
    Insert listSSR;


    Integer beforeDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: listUsers[0].ID];
    System.Debug('Before: How many number of times does user exist in Opportunity Team : ' + beforeDoesUserExistInOppTeam);

    for(Integer i=0; i < 200; i++)
    {
      listSSR[i].OwnerId = listUsers[0].Id;
    }
    Update listSSR;

    Integer afterDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: listUsers[0].Id];
    System.Debug('After: After updating 200 records with the same User, how many times does user exist in Opportunity Team : ' + afterDoesUserExistInOppTeam);

    System.Debug('User should exist only once.');
    System.assertEquals(beforeDoesUserExistInOppTeam, afterDoesUserExistInOppTeam);

    Test.stopTest();
  }

  @isTest(SeeAllData=true) //For Record Type IDs
  public static void addPreSales_NA_EDQ_OwnerToOpportunityTeam_Bulk_TeamMember_Does_Not_Exist()
  {
     // Create Test Data
    Id recordTypeId_preSalesNA_EDQ = Record_Type_Ids__c.getInstance().SSR_Presales_Request_NA_EDQ__c;

    Account testAccount = Test_Utils.insertAccount();
    Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);

    List<User> listUsers = [SELECT NAME FROM USER WHERE PROFILE.NAME = 'System Administrator' AND ID != :UserInfo.GetUserId() LIMIT 5];

    OpportunityTeamMember teamMember1 = new OpportunityTeamMember();
    teamMember1.OpportunityId = testOpportunity.Id;
    teamMember1.UserId = listUsers[0].Id;
    teamMember1.TeamMemberRole = Constants.TEAM_ROLE_PRE_SALES_CONSULTANT;

    OpportunityTeamMember teamMember2 = new OpportunityTeamMember();
    teamMember2.OpportunityId = testOpportunity.Id;
    teamMember2.UserId = listUsers[1].Id;
    teamMember2.TeamMemberRole = Constants.TEAM_ROLE_PRE_SALES_CONSULTANT;

    Insert teamMember1;
    Insert teamMember2;

    for(OpportunityTeamMember tm : [Select UserID, USER.Name from OpportunityTeamMember where OpportunityId=:testOpportunity.Id])
    {
      System.Debug('Opp Team Member: ' + tm.User.Name);
    }

    Test.startTest();

    List<Sales_Support_Request__c> listSSR = new List<Sales_Support_Request__c>();
    for(Integer i=0; i < 200; i++)
    {
      Sales_Support_Request__c testSSR = new Sales_Support_Request__c();
      testSSR.Opportunity__c = testOpportunity.Id;
      //testSSR.OwnerId = ; Owner will be a Queue
      testSSR.Proposition_Type__c = 'Data Clean';
      testSSR.RecordtypeId = recordTypeId_preSalesNA_EDQ;
      listSSR.add(testSSR);
    }
    Insert listSSR;


    Integer beforeDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: listUsers[3].Id];
    System.Debug('Before: How many number of times does user *' + listUsers[3].Name  + '* exist in Opportunity Team : ' + beforeDoesUserExistInOppTeam);

    for(Integer i=0; i < 200; i++)
    {
      listSSR[i].OwnerId = listUsers[3].Id;
    }
    Update listSSR;

    Integer afterDoesUserExistInOppTeam = [SELECT count() FROM OpportunityTeamMember WHERE OpportunityId =:testOpportunity.Id AND UserID =: listUsers[3].Id];
    System.Debug('After: After updating 200 records with the same User, how many times does user exist in Opportunity Team : ' + afterDoesUserExistInOppTeam);

    System.Debug('User should exist only once.');
    System.assertNotEquals(beforeDoesUserExistInOppTeam, afterDoesUserExistInOppTeam);

    Test.stopTest();
  }

  /*@isTest(SeeAllData=true) //For Record Type IDs
  public static void create_Project_when_SSR_Is_Saved_As_Evaluation_Complete()
  {
       Boolean testResult = false;

       Account testAccount = Test_Utils.insertAccount();
       Opportunity testOpportunity = Test_Utils.insertOpportunity(testAccount.Id);

       String recordTypeId_preSalesNA_EDQ = Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get('Presales Request NA EDQ').getRecordTypeId();

       Sales_Support_Request__c ssr = new Sales_Support_Request__c(Opportunity__c = testOpportunity.Id);
       ssr.Status__c = 'Evaluation Complete';
       ssr.Proposition_Type__c = 'Data Clean';
       ssr.Due_Date__c = Date.today();
       ssr.Type__c = 'Billable';
       //ssr.RecordTypeId = recordTypeId_preSalesNA_EDQ; //Record_Type_Ids__c.getInstance().SSR_Presales_Request_NA_EDQ__c;

       INSERT ssr;

       List<Project__c> listPrj = [SELECT ID, Sales_Support_Request__c FROM Project__c where Sales_Support_Request__c =:ssr.Id];

       if(listPrj.size() > 0)
       {
           testResult = true;
       }

       System.debug('• • ' + Schema.SObjectType.Sales_Support_Request__c.getRecordTypeInfosByName().get('Presales Request NA EDQ').getRecordTypeId());

       System.assert(testResult);
  }*/
}