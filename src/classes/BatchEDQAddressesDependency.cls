/**=====================================================================
 * Experian
 * Name: BatchEDQAddressesDependency
 * Description: Case 01890211
                Find all orders with EDQ related fields set and addresses not already marked as EDQ dependent, and
                set the address EDQ Dependency field as appropriate.
 * Created Date: 14 Mar 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
global class BatchEDQAddressesDependency implements Database.Batchable<sObject>, Database.Stateful {
  
  global List<String> updateErrors;
  
  global EDQ_Address_Dependency__mdt edqAddrDepDetails;
  
  global List<String> edqAddrDepsList;
  global String edqAddrDepName;
  
  global Database.Querylocator start ( Database.Batchablecontext bc ) {
    
    if (updateErrors == null) updateErrors = new List<String>();
    
    if (edqAddrDepsList != null && edqAddrDepsList.size() > 0) {
      edqAddrDepName = edqAddrDepsList[0];
    }
    
    edqAddrDepDetails = [
      SELECT Address_Field__c, SObject__c, Where_AND_1__c
      FROM EDQ_Address_Dependency__mdt
      WHERE DeveloperName = :edqAddrDepName
    ];
    
    return Database.getQueryLocator(
      'SELECT '+ edqAddrDepDetails.Address_Field__c +
      ' FROM '+ edqAddrDepDetails.SObject__c + 
      ' WHERE '+ edqAddrDepDetails.Where_AND_1__c
    );
    
  }
  
  global void execute (Database.BatchableContext bc, List<sObject> scope) {
    
    Map<Id,Address__c> addressMap = new Map<Id,Address__c>();
    
    for(sObject obj : scope) {
      addressMap.put(
        (Id)obj.get(edqAddrDepDetails.Address_Field__c), 
        new Address__c(
          Id = (Id)obj.get(edqAddrDepDetails.Address_Field__c), 
          EDQ_Dependency__c = true
        )
      );
    }
    
    List<Database.SaveResult> saveResultsList = Database.update(addressMap.values(),false);
    for (Database.SaveResult sr : saveResultsList) {
      if (!sr.isSuccess()) {
        updateErrors.addAll(BatchHelper.parseErrors(sr.getErrors()));
      }
    }
  }
  
  global void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchEDQAddressesDependency', false);
    
    String emailBody = '';
    if (updateErrors.size() > 0 || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      
      emailBody += '\nThe following errors were observed when updating records:\n';
      emailBody += String.join(updateErrors,'\n');
      
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
    
    if (edqAddrDepsList != null && edqAddrDepsList.size() > 0) {
      edqAddrDepsList.remove(0);
      if (edqAddrDepsList.size() > 0 || Test.isRunningTest()) {
        BatchEDQAddressesDependency b = new BatchEDQAddressesDependency();
        b.edqAddrDepsList = edqAddrDepsList;
        if (!Test.isRunningTest()) {
          system.scheduleBatch(b, 'BatchEDQAddressesDependency'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchEDQAddressesDependency'));
        }
      }
    }
    
    
  }
    
}