public class Account_Competitor_Info_Controller
{
    private final Account acct; 
    private Set<Id> setOpportunities;
    
    private List<Account_Competitor_Info> listAcctComp{get; set;}
    
    public Account_Competitor_Info_Controller(ApexPages.StandardController controller)
    {
        this.acct = (Account)controller.getRecord(); 		       
    }      
    
    public List<Account_Competitor_Info> getCompetitors()
    {
       
        listAcctComp = new List<Account_Competitor_Info>();
        
        setOpportunities = (new Map<Id, Opportunity>([Select Id from Opportunity where AccountId = :this.acct.Id])).keySet();
        
        List<Competitor__c> listCompetitors = [Select ID, 
                                               			Name, 
                                               			Opportunity__c, 
                                               			Opportunity__r.Name,
                                               			Opportunity__r.Id,
                                               			GBL__c,
                                               			BU__c,
                                               			Lost_To__c
                                               From Competitor__c 
                                               Where Account__c = :this.acct.Id 
                                               Order by LastModifiedDate DESC, BU__c ASC
                                               LIMIT 100
                                              ];
        
        for(Competitor__c c : listCompetitors)
        {
            Account_Competitor_Info aci = new Account_Competitor_Info();
            aci.CompetitorID = c.ID;
            aci.CompetitorName = c.Name;
            aci.CompetingFor = c.Opportunity__R.Name;
            aci.CompetingForID = c.Opportunity__c;
            aci.GBL = c.GBL__c;
            aci.BU = c.BU__c;
            aci.LostTo = c.Lost_To__c;
            aci.IsAttachedToOppBelongingToAccount = setOpportunities.contains(c.Opportunity__c);
            listAcctComp.add(aci);
        }   
        
        return listAcctComp;
    }

}