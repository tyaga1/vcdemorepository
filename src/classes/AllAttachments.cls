/*===================================================================== 
* Experian 
* Name: AllAttachments
* Description: This Class is used to get all the attachments for the specified Account 
* 
* Created Date: July 13th, 2016 
* Created By: Manoj (Experian) 
* 
* Date Modified          Modified By             Description of the update 
* July 13th 2016         Manoj Gopu              CRM2:W-005491 - Consolidated view of attachments on the account record.
* Sep  12th 2016         Manoj Gopu              CRM2:W-005491 - Updated code to display the Contract Name 
* Jan  26th 2017		 Sanket Vaidya			 Case#02165224 - Order by create date [Added sorting for Type, Record associated to, Created Date, Created By, Creator's Region]
======================================================================*/ 

global with sharing class AllAttachments{
    
    public list<AllAttachmentsWrapper> lstAttachments{get;set;}  
    public list<AllAttachmentsWrapper> lstAttachmentsData{get;set;}     
    public string accId{get;set;}    
    private String sortExp = 'Object Name';
    private String sortDirection = 'ASC';
    public Integer pageNumber;//used for pagination
    public Integer pageSize;//used for pagination
    public Integer totalPageNumber;//used for pagination
    public Integer PrevPageNumber {get;set;}//used for pagination
    public Integer NxtPageNumber {get;set;}//used for pagination
    public Integer NlistSize {get;set;}//used for pagination    
    public Integer totallistsize{get;set;}//total size of list for pagination     
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
             sortDirection = 'ASC';
           sortExp = value;
        }
    }

    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
    }
    
    public AllAttachments(ApexPages.StandardController controller) {
        accId=ApexPages.currentPage().getParameters().get('id');          
        if(!Test.isRunningTest())            
        displayAttachments();           
    }
    
    public void displayAttachments()
    {        


        lstAttachments=new list<AllAttachmentsWrapper>();
        list<AllAttachmentsWrapper> lstAttachmentsTemp=new list<AllAttachmentsWrapper>();
        set<string> setUserIds=new set<string>();
        list<Account> lstAcc=[select id,Name, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Account where id=:accId];
        for(Account acc:lstAcc)
        {
            if (acc.CombinedAttachments != null && acc.CombinedAttachments.size() > 0) 
            {               
                for (CombinedAttachment ca : acc.CombinedAttachments) 
                {
                    setUserIds.add(ca.CreatedById);

                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, acc.Name,Account.sObjectType.getDescribe().getLabel(), '', '', sortExpression, SortDirection));
                }
            }
        }
        
        list<Opportunity> lstOpp=[select id,Name, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Opportunity where AccountId=:accId];
        for(Opportunity opp:lstOpp)
        {
            if (opp.CombinedAttachments != null && opp.CombinedAttachments.size() > 0) 
            {               
                for (CombinedAttachment ca : opp.CombinedAttachments) 
                {  
                    setUserIds.add(ca.CreatedById);

                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, opp.Name,Opportunity.sObjectType.getDescribe().getLabel(), '', '', sortExpression, SortDirection));
                }
            }
        }
        
        list<Account_segment__c> lstAccSeg=[select id,Name, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Account_segment__c where Account__c=:accId];
        for(Account_segment__c accSeg:lstAccSeg)
        {
            if (accSeg.CombinedAttachments != null && accSeg.CombinedAttachments.size() > 0) 
            {               
                for (CombinedAttachment ca : accSeg.CombinedAttachments) 
                {  
                    setUserIds.add(ca.CreatedById);

                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, accSeg.Name,Account_segment__c.sObjectType.getDescribe().getLabel(), '', '', sortExpression, SortDirection));
                }
            }
        }
        
        list<Confidential_Information__c> lstConf=[select id,Name,Contract_Name__c,Opportunity__c,Membership__c,Contract__c, Case__c, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Confidential_Information__c where Account__c=:accId];
        for(Confidential_Information__c confInfo:lstConf)
        {
            if (confInfo.CombinedAttachments != null && confInfo.CombinedAttachments.size() > 0) 
            {    
                string objName=Confidential_Information__c.sObjectType.getDescribe().getLabel();
                if(confInfo.Opportunity__c!=null)
                    objName+='('+Opportunity.sObjectType.getDescribe().getLabel()+')';
                if(confInfo.Membership__c!=null)
                    objName+='('+Membership__c.sObjectType.getDescribe().getLabel()+')';
                if(confInfo.Contract__c!=null)
                    objName+='('+Contract.sObjectType.getDescribe().getLabel()+')';
                if(confInfo.Case__c!=null)
                    objName+='('+Case.sObjectType.getDescribe().getLabel()+')';
                string recordName=confInfo.Contract__c!=null?confInfo.Contract_Name__c:confInfo.Name;
                for (CombinedAttachment ca : confInfo.CombinedAttachments) 
                {      
                    setUserIds.add(ca.CreatedById);                 
                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, recordName,objName, '', '', sortExpression, SortDirection));
                }
            }
        }
        
        list<Membership__c> lstMem=[select id,Name, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Membership__c where Account__c=:accId];
        for(Membership__c memb:lstMem)
        {
            if (memb.CombinedAttachments != null && memb.CombinedAttachments.size() > 0) 
            {               
                for (CombinedAttachment ca : memb.CombinedAttachments) 
                {    
                    setUserIds.add(ca.CreatedById);

                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, memb.Name, Membership__c.sObjectType.getDescribe().getLabel(), '', '', sortExpression, SortDirection));
                }
            }
        }
        
        list<Account_Plan__c> lstAccPl=[select id,Name, (select Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, Parent.Name, CreatedBy.Name from CombinedAttachments) from Account_Plan__c where Account__c=:accId];
        for(Account_Plan__c accplan:lstAccPl)
        {
            if (accplan.CombinedAttachments != null && accplan.CombinedAttachments.size() > 0) 
            {               
                for (CombinedAttachment ca : accplan.CombinedAttachments) 
                {     
                    setUserIds.add(ca.CreatedById);

                    lstAttachmentsTemp.add(new AllAttachmentsWrapper(ca, accplan.Name,Account_Plan__c.sObjectType.getDescribe().getLabel(), '', '', sortExpression, SortDirection));
                }
            }
        }
        map<Id,User> mapUsers=new map<Id,User>([select id,Name,Region__c,Business_Unit__c from User where id IN: setUserIds]);//Get the Users list in map to get the User Region and Business Unit values
        
        lstAttachments=new list<AllAttachmentsWrapper>();
        for(AllAttachmentsWrapper att:lstAttachmentsTemp)
        {   
            string userRegion=mapUsers.containsKey(att.attach.CreatedById)?(mapUsers.get(att.attach.CreatedById).Region__c!=null && mapUsers.get(att.attach.CreatedById).Region__c!=''?mapUsers.get(att.attach.CreatedById).Region__c:'-'):'-';
            string userBusiness=mapUsers.containsKey(att.attach.CreatedById)?(mapUsers.get(att.attach.CreatedById).Business_Unit__c!=null && mapUsers.get(att.attach.CreatedById).Business_Unit__c!=''?mapUsers.get(att.attach.CreatedById).Business_Unit__c:'-'):'-';
            lstAttachments.add(new AllAttachmentsWrapper(att.attach, att.recordName,att.objectName, userRegion, userBusiness, att.sortExpression, att.SortDirection));
        }       
        
        lstAttachmentsTemp.clear();
        lstAttachments.sort();
        totallistsize=lstAttachments.size();// this size for pagination
        pageSize = 50;// default page size will be 50
        if(totallistsize==0)
        {
            BindData(0);// this method is for pagination, if no records found then passing zero to the pagination method
            NxtPageNumber=lstAttachmentsData.size();
            PrevPageNumber=0;
        }
        else
        {
            BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
            NxtPageNumber=lstAttachmentsData.size();
            PrevPageNumber=1; 
        }
    }
    
    global class AllAttachmentsWrapper implements Comparable{
        public CombinedAttachment attach{get;set;}
        public string objectName{get;set;}
        public string userRegion{get;set;}
        public string userBusiness{get;set;}
        public string recordName{get;set;}
        public string sortExpression;//used for grid
        public string sortDirection;//used for grid
        
        global AllAttachmentsWrapper(CombinedAttachment att,string rName,string oName,string uRegion,string uBusiness,String sortexp,String sortdir)
        {
            this.attach=att;
            this.objectName=oName;
            this.userRegion=uRegion;
            this.recordName=rName;
            this.userBusiness=uBusiness;    
            this.sortExpression=sortexp;
            this.sortDirection=sortdir;         
        }
        
        global Integer compareTo(Object ObjToCompare)
        {
            if(sortDirection=='ASC')
            {
                if(sortExpression=='Object Name')
                    return objectName.CompareTo(((AllAttachmentsWrapper)ObjToCompare).objectName);
                else if(sortExpression=='Business Unit')
                    return userBusiness.CompareTo(((AllAttachmentsWrapper)ObjToCompare).userBusiness); 
                else if(sortExpression=='Title')
                    return attach.Title.CompareTo(((AllAttachmentsWrapper)ObjToCompare).attach.Title);                  
                else if(sortExpression=='Type')
                    return attach.RecordType.CompareTo(((AllAttachmentsWrapper)ObjToCompare).attach.RecordType);                  
                else if(sortExpression=='RecordAssociatedTo')
                    return recordName.CompareTo(((AllAttachmentsWrapper)ObjToCompare).recordName);                  
                else if(sortExpression=='CreatedDate')                    
                	return sortByCreatedDate(objToCompare);
                else if(sortExpression=='CreatedBy')
                    return attach.CreatedBy.Name.CompareTo(((AllAttachmentsWrapper)ObjToCompare).attach.CreatedBy.Name);   
                else if(sortExpression=='CreatorRegion')
                    return userRegion.CompareTo(((AllAttachmentsWrapper)ObjToCompare).userRegion);   
            }
            else
            {
                if(sortExpression=='Object Name')
                    return ((AllAttachmentsWrapper)ObjToCompare).objectName.CompareTo(objectName);
                else if(sortExpression=='Business Unit')
                    return ((AllAttachmentsWrapper)ObjToCompare).userBusiness.CompareTo(userBusiness);  
                else if(sortExpression=='Title')
                    return ((AllAttachmentsWrapper)ObjToCompare).attach.Title.CompareTo(attach.Title);
                else if(sortExpression=='Type')
                    return ((AllAttachmentsWrapper)ObjToCompare).attach.RecordType.CompareTo(attach.RecordType);
                else if(sortExpression=='RecordAssociatedTo')
                    return ((AllAttachmentsWrapper)ObjToCompare).recordName.CompareTo(recordName);
                else if(sortExpression=='CreatedDate')                
                	return sortByCreatedDate(objToCompare);
                else if(sortExpression=='CreatedBy')
                    return ((AllAttachmentsWrapper)ObjToCompare).attach.CreatedBy.Name.CompareTo(attach.CreatedBy.Name);
                 else if(sortExpression=='CreatorRegion')
                    return ((AllAttachmentsWrapper)ObjToCompare).userRegion.CompareTo(userRegion);
            }
            return null;
        }
        
       private Integer sortByCreatedDate(Object objToCompare)
       {
           if(sortDirection=='ASC')
           {
               if(this.attach.CreatedDate > ((AllAttachmentsWrapper)objToCompare).attach.CreatedDate)
                   return 1;               
               else if(this.attach.CreatedDate < ((AllAttachmentsWrapper)objToCompare).attach.CreatedDate)
                   return -1;
               else               
                   return 0;
           } 
           else
           {
               if(this.attach.CreatedDate > ((AllAttachmentsWrapper)objToCompare).attach.CreatedDate)
                   return -1;               
               else if(this.attach.CreatedDate < ((AllAttachmentsWrapper)objToCompare).attach.CreatedDate)
                   return 1;
               else               
                   return 0;
           }     
           return null;
       }
        
    }
    
    // pagination
    public PageReference nextBtnClick() 
    {
        
        BindData(pageNumber + 1);
        pageData(pageNumber + 1);
        
        return null;
    }
    // Below method fires when user clicks on previous button of pagination.
    public PageReference previousBtnClick() 
    {       
        pageData(pageNumber - 1);
        BindData(pageNumber - 1);
        
        return null;
    }
    /// Below method fires when user clicks on next button of pagination.
    public Integer getPageNumber()
    {
        return pageNumber; 
    }
    /// Below method is for getting pagesize how many records per page .
    public Integer getPageSize()
    {
        return pageSize;
    }
    /// Below method is for enabling and disabling the previous button of pagination.
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    /// Below method is for enabling and disabling the nextbutton of pagination.
    public Boolean getNextButtonDisabled()
    {
        if (totallistsize ==0) 
            return true;
        else
            return ((pageNumber * pageSize) >= totallistsize);
    }
    /// Below method gets the total no.of pages.
    public Integer getTotalPageNumber()
    {       
        totalPageNumber = 0;
        if (totalPageNumber == 0 && totallistsize!=0)
        {
            

            totalPageNumber = totallistsize / pageSize;
            Integer mod = totallistsize - (totalPageNumber * pageSize);
            if (mod > 0)
            totalPageNumber++;
        }
        
        return totalPageNumber;
    }
    /// Below method is the main method of pagination if have atleast one record then we pass 1 to this method otherwise zero.
    public void BindData(Integer newPageIndex)
    {
       
        lstAttachmentsData =new list<AllAttachmentsWrapper>();
        Transient Integer counter = 0;
        Transient Integer min = 0;
        Transient Integer max = 0;
        
        if (newPageIndex > pageNumber)
        {
            min = pageNumber * pageSize;
            max = newPageIndex * pageSize;
        }
        else
        {
            max = newPageIndex * pageSize;
            min = max - pageSize;
        }
        if(lstAttachments!=NULL)
        {
            for(AllAttachmentsWrapper b : lstAttachments)
            {
                counter++;
                if (counter > min && counter <= max) 
                {
                    lstAttachmentsData.add(b);// here adding files list
                }
            }
        }
        pageNumber = newPageIndex;
        NlistSize=lstAttachmentsData.size();
        
    }
    /// Below method bnds the values for Next and previous buttons.
    public void pageData(Integer newPageIndex)
    {   
        if (newPageIndex > pageNumber)
        {
            PrevPageNumber=PrevPageNumber+pagesize;   
            NxtPageNumber=NxtPageNumber+NlistSize; 
        }
        if (newPageIndex < pageNumber)               
        {                                 
            PrevPageNumber=PrevPageNumber-pagesize;               
            NxtPageNumber=NxtPageNumber-NlistSize;      
        }
    }
    /// Below method binds the data for last button.
    public void LastpageData(Integer newPageIndex)
    {        
        lstAttachmentsData=new list<AllAttachmentsWrapper>();
        Transient Integer counter = 0;
        Transient Integer min = 0;
        Transient Integer max = 0;          
        min = pageNumber * pageSize;
        max = newPageIndex * pageSize;    
        for(AllAttachmentsWrapper a : lstAttachments)
        {
            counter++;
            if (counter > min && counter <= max) 
            {
                lstAttachmentsData.add(a);// here adding the folders list      
            }
        }
        pageNumber = newPageIndex;
        NlistSize=lstAttachmentsData.size();
        //PrevPageNumber=totallistsize - NlistSize +1;   
        PrevPageNumber=((pageNumber - 1)*pageSize) +1;   
        NxtPageNumber=totallistsize;
        
    }
    /// Below method bnds the value for last button.
    public PageReference LastbtnClick() 
    {       
        BindData(totalpagenumber - 1);
        LastpageData(totalpagenumber);
        
        return null;
    }
    /// Below method bnds the value for first button.
    public PageReference FirstbtnClick() 
    {       
        BindData(1);// this method is for pagination, if atleast one record found then passing 1 to the pagination method
        NxtPageNumber=NlistSize;
        PrevPageNumber=1;
        
        return null;
    } 

}