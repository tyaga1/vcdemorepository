/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityStageIndicatorHelper
 * Description: 
 * Created Date: January 2014 (reused from topCoder challenge)
 * Created By: Nathalie Le Guay (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 30th, 2014               Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Feb 13th, 2014               Jinesh Goyal(Appirio)        T-232763: Added Exception Logging
 * Feb 22th, 2014               Naresh kr Ojha (Appirio)     T-251145: updated to remove ref Has_Completed_Task__c
 * Feb 27th, 2014               Nathalie Le Guay (Appirio)   Class cleanup
 * March 3rd, 2014              Nathalie Le Guay             Updated opp query
 * Apr 15th, 2014               Arpita Bose (Appirio)        T-271695: Removed reference to Below_Review_Thresholds__c field
 * Apr 16th, 2014               Arpita Bose (Appirio)        T-271695: Renamed fields Has_Stage_4_Approval__c to Has_Stage_3_Approval__c 
 *                                                           and Has_Stage_5_Approval__c to Has_Senior_Approval__c
 * Apr 16th, 2014               Nathalie Le Guay             T-272404: Adding Approval Process fields to Opportunity query
 * Jul 28th, 2014               Nathalie Le Guay             S-252919 - Added Outcomes__c to the Opp query (constructor)
 * Nov 13th, 2014               Arpita Bose                  T-333541 - Added method edqUKIUser()
 * Mar 18th, 2015               Paul Kissick                 Case #591965 - Fix for Chinese not rendering properly.
 * Sep 23rd, 2015               Paul Kissick                 I-181512 - Fixes for translated picklists to work correctly
 * Feb 25th, 2016               Paul Kissick                 Case 01872263: Fixed query to return archived tasks
 * Apr 5th, 2016                Paul Kissick                 Case 01028611: Adding new requirement to stage 4
 * Aug 9th, 2016                Paul Kissick                 CRM2:W-005495: Removing fields no longer used
 * Aug 16th, 2016               Paul Kissick                 CRM2:W-005534: Adding support for clicking the stages and updating the stage
 =====================================================================*/
public without sharing class OpportunityStageIndicatorHelper {
    
  public Opportunity opp {get;set;}
  public List<wrapperStage> lstStages {get;set;}
  public Boolean hasContactRole {get;set;}
  public Boolean hasCompletedTask {get;set;}
  public Integer NUM_500000 {get;set;}
  public Integer NUM_250000 {get;set;}
  
  public String newStage {get;set;}
  public Boolean reloadPage {get{if(reloadPage == null) reloadPage = false; return reloadPage;}set;}
  public PageReference changeStageTo() {
    if (String.isNotBlank(newStage) && opp.IsClosed == false) {
      if (newStage != Constants.OPPTY_STAGE_7) {
        opp.StageName = newStage;
        try {
          update opp;
          reloadPage = true;
        }
        catch (Exception e) {
          system.debug(e);
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
          // PK: Not displaying because its done my the trigger!
        }
      }
      else {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.OECS_Change_to_Execute_Below));
      }
    }
    return null;
  }

  /*
   *   Constructor taking standard controller as a parameter
   */
  public OpportunityStageIndicatorHelper(ApexPages.StandardController controller) {
    NUM_500000 = Integer.valueOf(Label.NUM_500000);
    NUM_250000 = 250000; // TODO: Make label?
    if (!String.isBlank(controller.getId())) {
      Set<String> allowedStages = new Set<String>();

      for (Opportunity_Sales_Process__c osp : [SELECT Sales_Process_Name__c, Name
                                               FROM Opportunity_Sales_Process__c
                                               WHERE Sales_Process_Name__c =: Constants.OPPTY_SALES_PROCESS_STANDARD]) {
        allowedStages.add(osp.Name);
      } 
      // PK Case 01872263: Fixed query to return archived tasks
      opp = [SELECT Id, Turn_Off_Contact_Role_Criteria_Check__c, toLabel(StageName), Starting_Stage__c, Has_Stage_3_Approval__c,
                    //Below_Review_Thresholds__c,  T-271695: Removed reference to Below_Review_Thresholds__c field
                    Opportunity_Products_Count__c, Has_Senior_Approval__c, Senior_Approver__c,
                    Competitor_Count__c, Amount, Budget__c, isClosed, Owner.Region__c,
                    /* Is_There_Commercial_Risk__c, Is_There_Delivery_Risk__c,
                    Is_There_Financial_Risk__c, Is_There_Legal_Risk__c,  
                    Has_There_Been_Significant_Change__c,*/ IsWon, Stage_3_Approver__c, OwnerId,
                    Tech_Support_Maintenance_Tiers__c, Type,
                    Owner_s_Business_Unit__c, Risk_Tool_Output__c, Risk_Tool_Output_Code__c, // CRM2:W-005495
                    (SELECT Id, Type_of_Sale__c, PricebookEntry.Product2.Global_Business_Line__c, PricebookEntry.Product2.Business_Line__c, PricebookEntry.Product2.Types_of_Sale__c 
                     FROM OpportunityLineItems WHERE IsDeleted = false),
                    (SELECT Id, Role, IsPrimary FROM OpportunityContactRoles WHERE Role =: Constants.DECIDER AND IsDeleted = false),
                    (SELECT Id, Type, Status, Outcomes__c
                     FROM Tasks WHERE Status =: Constants.STATUS_COMPLETED AND IsDeleted = false)
             FROM Opportunity
             WHERE Id = :controller.getId() ALL ROWS];
      String sObjectName = controller.getRecord().getSObjectType().getDescribe().getName();
      lstStages = new List<wrapperStage>();
      hasContactRole = opp.OpportunityContactRoles.size() > 0;
      hasCompletedTask = opp.Tasks.size() > 0;
      Schema.DescribeFieldResult f = Schema.sObjectType.Opportunity.fields.StageName;
      // Get the field describe result from the token
      f = f.getSObjectField().getDescribe();
      List <Schema.PicklistEntry> picklistValues = f.getPicklistValues();  
      System.debug('----allowedStages---'+allowedStages);
      for (Integer x = 0; x < picklistValues.size(); x++) {
        //if (!pickListValuesToRemove.contains(picklistValues[x].getLabel().toLowerCase())) {  // PK Case #591965 - Removed as this seems pointless
          System.debug('=======>'+picklistValues[x].getLabel());
          if (allowedStages.contains(picklistValues[x].getValue())) {  // PK Case #591965 - Changed to value.
            lstStages.add(new wrapperstage(picklistValues[x].getLabel(), picklistValues[x].getValue())); // PK I-181512 - Store both system and translated picklist values
          }
        //}
      }

      for (Integer i = 0; i < lstStages.size(); i++) {
        if (opp.StageName == lstStages[i].strStageName) {
          lstStages[i].bolCompleted = true;
          lstStages[i].bolCurrent = true;
          if (i > 1) {
            lstStages[i - 1].bolCurrent = false;
          }
          break;
        } 
        else if (opp.IsClosed && !opp.IsWon) { // I-181512 Checking for not won instead of against constant // == Constants.OPPTY_CLOSED_LOST) { 
          lstStages[i].bolCompleted = false;
          break;
        } 
        else {
          lstStages[i].bolCompleted = true;
        }
      }
    }
  }

  //=============================================================
  // Booleans used by the VF page for one of the Exit Criteria
  //=============================================================
  public Boolean hasSignedContract {
    get {
     if (hasSignedContract == null && opp != null) {
       hasSignedContract = Opportunity_ExitCriteriaUtility.hasRequiredTask(opp, Constants.ACTIVITY_TYPE_SIGNED_CONTRACT);
     }
     return hasSignedContract;
    }
    set;
  }

  public Boolean hasSelectionConfirmed {
    get {
      if (hasSelectionConfirmed == null & opp != null) {
        hasSelectionConfirmed = Opportunity_ExitCriteriaUtility.hasRequiredTask(opp, Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED);
      }
      return hasSelectionConfirmed;
    }
    set;
  }

  public Boolean hasQuoteDelivered {
    get {
      if (hasQuoteDelivered == null && opp != null) {
        hasQuoteDelivered = Opportunity_ExitCriteriaUtility.hasRequiredTask(opp, Constants.ACTIVITY_TYPE_QUOTE_DELIVERED);
      }
      return hasQuoteDelivered;
    }
    set;
  }
  
  public Boolean hasTechSuppFieldRequirement {
    get {
      if (hasTechSuppFieldRequirement == null) {
        hasTechSuppFieldRequirement = Opportunity_ExitCriteriaUtility.hasTechSuppFieldRequired(opp);
      }
      return hasTechSuppFieldRequirement;
    }
    set;
  }

  /*
   *   Wrapper class 
   */
  public class wrapperStage {
    public string strStageName {get;set;}
    public string sysStageName {get;set;}
    public boolean bolCompleted {get;set;}
    public boolean bolCurrent {get;set;} 
    public wrapperStage(string y, String x) {
      strStageName = y; 
      sysStageName = x;
    }
  }
    
    //=================================================================================
    // T-333541: UK&I Exit Criteria: Approvals exit criteria do not apply to EDQ Users
    //=================================================================================
    
  public Boolean edqUKIUser {
    get {
      edqUKIUser = false;
      User currentUser = [SELECT Id,Region__c 
                          FROM User
                          WHERE Id = : opp.OwnerId];
      String groupName = BusinessUnitUtility.getBusinessUnit(userinfo.getUserId()) ;
      if (String.isNotBlank(groupName) && 
          groupName.equalsIgnoreCase(Constants.EDQ) && 
          currentUser.Region__c == Constants.REGION_UKI) {
        edqUKIUser = true;
      }
      return edqUKIUser;
    }
    set;
  }

}