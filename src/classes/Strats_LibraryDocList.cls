public class Strats_LibraryDocList {
        
    @AuraEnabled
    public static Map<String,List<ContentVersion>> getLibraryDocs(){
        /*
        List<ContentWorkSpace> libraryList = [Select Id, Name From ContentWorkSpace];
        system.debug('List of LIbraries: ' +  libraryList);
        //List<Id> libraryIdList = new List<Id>();
        Map<Id, String> libraryNameMap = new Map<Id, String>();

        for(ContentWorkSpace cws : libraryList){
                //libraryIdList.add(cws.Id);
                libraryNameMap.put(cws.Id,cws.Name);
        }
        //system.debug('Library Id List: ' + libraryIdList); 


        List<ContentDocument> docList = [Select Id, Title, ParentId, LastModifiedDate From ContentDocument Where ParentId IN :libraryNameMap.keySet()];
        system.debug('All Documents: '+docList);
        Map<String,List<ContentDocument>> libraryDocMap = new Map<String,List<ContentDocument>>();
        for(ContentDocument cd : docList){
            if(!libraryDocMap.KeySet().Contains(libraryNameMap.get(cd.ParentId))){
                libraryDocMap.put(libraryNameMap.get(cd.ParentId) , new List<ContentDocument>());
            }
            libraryDocMap.get(libraryNameMap.get(cd.ParentId)).add(cd);
        }
        system.debug('The returned Map: ' +  libraryDocMap);
        return libraryDocMap;
        */

        List<ContentWorkSpace> libraryList = [Select Id, Name, (Select Id,ContentDocumentId From ContentDocumentLinks) From ContentWorkSpace];
        List<Id> DocumentIdList = new List<Id>();

        for(ContentWorkSpace cws : libraryList){
            for(ContentDocumentLink cdl : cws.ContentDocumentLinks){
                DocumentIdList.add(cdl.ContentDocumentId);
            }
        }

        List<ContentDocument> docList = [Select Id, Title, ParentId, LastModifiedDate,
                                         (Select Id, Title, LastModifiedDate From ContentVersions Where isLatest = true)
                                         From ContentDocument Where Id IN :DocumentIdList];
        /*
        Map<String, ContentDocument> docMap = new Map<String, ContentDocument>();
        for(ContentDocument cd : docList){
            docMap.put(cd.Id,cd);
        }
        */
        Map<String, ContentVersion> docMap = new Map<String, ContentVersion>();
        for(ContentDocument cd : docList){
            docMap.put(cd.Id,cd.ContentVersions[0]);
        }

            
        //Map<String, List<ContentDocument>> libraryDocMap = new Map<String, List<ContentDocument>>();
        Map<String, List<ContentVersion>> libraryDocMap = new Map<String, List<ContentVersion>>();

        /*
        for(ContentWorkSpace cws : libraryList){

            libraryDocMap.put(cws.Name, new List<ContentDocument>());
                
            for(ContentDocumentLink cdl : cws.ContentDocumentLinks){
                
                libraryDocMap.get(cws.Name).add(docMap.get(cdl.ContentDocumentId));
            }
        }
        */
        for(ContentWorkSpace cws : libraryList){

            libraryDocMap.put(cws.Name, new List<ContentVersion>());
                
            for(ContentDocumentLink cdl : cws.ContentDocumentLinks){
                
                libraryDocMap.get(cws.Name).add(docMap.get(cdl.ContentDocumentId));
            }
        }
        system.debug('The returned Map: ' +  libraryDocMap);

        return libraryDocMap;
        


    }
    
}