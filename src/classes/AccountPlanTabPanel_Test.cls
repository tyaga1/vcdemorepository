/**=====================================================================
 * Name: AccountPlanTabPanel_Test
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Apr. 15th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By            Description of the update
 * Apr. 15th, 2016       James Wills            Case #01848189: Created
 * May 6th, 2016         James Wills            Changed modifiers on class and test method 
 * May 20th, 2016        James Wills            Case 01959211: AP - Saving Child Objects - Return to correct tab
 * Feb. 21st, 2017       James Wills            Case #01999757: Updated test for openAccountPlanPDFGenerator()
  =====================================================================*/
@isTest
private class AccountPlanTabPanel_Test{

   private static testmethod void test_backToAccountMethod(){
   
    //Create data
    Account testAccount1 = Test_Utils.insertAccount();  
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);  
    insert newAccountPlan;
    
    Account_Plan__c newAccountPlan2 = new Account_Plan__c(Account__c = testAccount1.id);  
    insert newAccountPlan2;
    
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);    
    ApexPages.StandardController stdController2 = new ApexPages.StandardController((sObject)newAccountPlan2);    
    
    AccountPlanTabPanel controller             = new AccountPlanTabPanel(stdController);
    

    Test.startTest();
      PageReference pageRef = Page.AccountPlanTabPanel; 
      Test.setCurrentPage(pageRef);
      
      System.CurrentPageReference().getParameters().put('tabLastVisited', Label.ACCOUNTPLANNING_ClientOverviewTab);
      
      controller.currentUserAccountPlanTab = Label.ACCOUNTPLANNING_StrategyAndActionTab;      
      
      //Now switch to a different Account Plan
      controller = new AccountPlanTabPanel(stdController2);
      
      controller.setAccountPlanCookies();
      
      controller.openAccountPlanPDFGenerator();
      
      PageReference mainpage = controller.backToAccount();
      
    Test.stopTest();
  
  
  }
  
  
}