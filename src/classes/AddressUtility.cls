/**=====================================================================
 * Appirio Inc
 * Name: AddressUtility.cls
 * Description: Utility class to support Address Smart Searches
 * Created Date: 21 January 2014
 * Created By: Nathalie Le Guay (Appirio)
 *
 * Date Modified       Modified By            Description of the update
 * Feb 13th, 2014      Jinesh Goyal(Appirio)  T-232763: Added Exception Logging
 * Mar 14th, 2014			 Naresh K Ojha (Appirio)T-251968: made findMatchingaddress method public.
 * Apr 07th, 2014      Arpita Bose (Appirio)  T-269372: Added addError()in try-catch block
 * May 19th, 2014      Nathalie Le Guay       : created hasDuplicateAddresses() which checks if the a
 * Aug 26th, 2015      Arpita Bose            Added Bar_Code__c in query to fix issue
 * Sep 17th, 2015      Paul Kissick           Brazil address fix (Lines 3 & 4 should not be used for Brazil addresses)
 * May 24th, 2016      Paul Kissick           Case 01996442: Changing exception type in checkDuplicateAddress from QueryException to Exception
 * Sep 21st, 2016      Paul Kissick           Case 02141360: Adding fix to trim the space around the strings when building the key.
 =====================================================================*/
public without sharing class AddressUtility {
  private static List<String> addressFields = new List<String>{
                                                   'Address_1__c',
                                                   'Address_2__c',
                                                   'Address_3__c',
                                                   'Address_4__c',
                                                   'City__c',
                                                   'Country__c',
                                                   'CEDEX__c',
                                                   'Codiga_Postal__c',
                                                   'County__c',
                                                   'District__c',
                                                   'Emirate__c',
                                                   'Floor__c',
                                                   'Partofterritory__c',
                                                   'POBox__c',
                                                   'Postcode__c',
                                                   'Prefecture__c',
                                                   'Province__c',
                                                   'SortingCode__c',
                                                   'State__c',
                                                   'Suite__c',
                                                   'Zip__c'};
  /**
   * Perform a database check to find an address match
   */
 
  /*===========================================================================
   * Adding new method to clean up the address fields to remove certain (known)
   * alternative characters often added by MS Word/Outlook
  ===========================================================================*/
  private static Address__c cleanUpAddressData (Address__c add) {
    String fValue;
    
    // I've encoded the chars below to make sure they transfer properly
    Map<String, String> knownChars = new Map<String, String> {
      EncodingUtil.urlDecode('%E2%80%99','UTF-8') => '\'',
      EncodingUtil.urlDecode('%E2%80%98','UTF-8') => '\''
    };
    
    for (String fName : addressFields) {
      fValue = (String)add.get(fName);
      if (String.isNotBlank(fValue)) {
        for (String knownChar : knownChars.keySet()) {
          fValue = fValue.replace(knownChar, knownChars.get(knownChar)).trim(); // Case 02141360 - Added .trim()
        }
        add.put(fName, fValue);
      }
    }
    return add; 
  }
 
  public static Address__c checkDuplicateAddress(Address__c address) {
    String addressString = '';
    addressFields.sort();
    
    // Adding fix for Brazil addressing as lines 3 and 4 from the QAS tool are duplicated of city and postal code
    if (String.isNotBlank(address.Address_3__c)) {
      if (address.Address_3__c.equals(address.City__c) || 
          address.Address_3__c.equals(address.County__c) || 
          address.Address_3__c.equals(address.State__c) ||
          address.Address_3__c.equals(address.Postcode__c) ||
          address.Address_3__c.equals(address.Zip__c)) {
        address.Address_3__c = null;
      }
    }
    if (String.isNotBlank(address.Address_4__c)) {
      if (address.Address_4__c.equals(address.City__c) ||
          address.Address_4__c.equals(address.County__c) ||
          address.Address_4__c.equals(address.State__c) ||
          address.Address_4__c.equals(address.Postcode__c) ||
          address.Address_4__c.equals(address.Zip__c)) {
        address.Address_4__c = null;
      }
    }
    
    address = cleanUpAddressData(address);

    addressString = DescribeUtility.buildKey(addressFields, (SObject) address, 'Address__c').mid(0,255);

    if (String.isNotEmpty(addressString)) {
      try {
        List<Address__c> databaseAddressCheck = [
          SELECT Id, Address_1__c, Address_2__c, Address_3__c, Address_4__c, Address_Id__c, City__c, Country__c, CEDEX__c,
                 Codiga_Postal__c, County__c, District__c, Emirate__c, Floor__c, Partofterritory__c, POBox__c, Postcode__c,
                 Prefecture__c, Province__c, SortingCode__c, State__c, Suite__c, Zip__c, Last_Validated__c, Validation_Status__c,
                 Bar_Code__c
          FROM Address__c
          WHERE Address_Id__c = :addressString
        ];
        Address__c addressMatch;

        if (!databaseAddressCheck.isEmpty()) {
          system.debug('\n~AddressUtility: Found one or many match(es): '+ databaseAddressCheck);
          addressMatch = findMatchingAddress(address, databaseAddressCheck);
          // This address will be used when creating the Account_Address__c record
        }
        if (addressMatch != null) {
          system.debug('\n~AddressUtility: Found existing entry for: ' + addressString);
          address = addressMatch;
        } 
        else {
          system.debug('\n~AddressUtility: No duplicate was found for entry: ' + addressString);
          address.Address_Id__c = addressString;
        }
        return address;
      }
      catch(Exception e) { 
        // Case 01996442
        system.debug('\n~AddressUtility: No duplicate was found for entry: ' + addressString);
        // Assigning the key in the Address_Id__c field
        address.Address_Id__c = addressString;
        ApexLogHandler.createLogAndSave('AddressUtility','checkDuplicateAddress', e.getStackTraceString(), e);
        address.addError(e.getMessage());
      }
    }
    return address;
  }

  public static Boolean hasDuplicateAddresses(List<Address__c> addresses, Set<Id> recordIds, String objectName) {
    if (recordIds == null || recordIds.isEmpty()) {
      return false;
    }
    
    String addressString = '';
    // List to store all the addressKey generated
    List<String> addressKeys = new List<String>();

    // Map to store the key of each address
    Map<String, String> addressKeyByAddressId = new Map<String, String>();

    // Map to store the results of key search
    Map<String, List<Address__c>> addressesByKey = new Map<String, List<Address__c>>();
    addressFields.sort();

    for (Address__c address: addresses) {
      addressString = DescribeUtility.buildKey(addressFields, (SObject)address, 'Address__c').mid(0,255);
      addressKeys.add(addressString);
      addressKeyByAddressId.put(address.Id, addressString);
    }

    if (!addressKeys.isEmpty()) {
      for (Address__c address : [SELECT Id, Address_1__c,Address_2__c,Address_3__c,Address_4__c,Address_Id__c,
                                        City__c,Country__c ,CEDEX__c,Codiga_Postal__c,County__c,District__c,Emirate__c,Floor__c,
                                        Partofterritory__c,POBox__c,Postcode__c,Prefecture__c,Province__c,SortingCode__c,State__c,Suite__c,Zip__c,
                                        Last_Validated__c,Validation_Status__c, Bar_Code__c,
                                        (SELECT Id, Contact__c FROM Contact_Addresses__r),
                                        (SELECT Id, Account__c FROM Account_Address__r)
                                 FROM Address__c
                                 WHERE Address_Id__c IN :addressKeys]) {
        if (!addressesByKey.containsKey(address.Address_Id__c)) {
          addressesByKey.put(address.Address_Id__c, new List<Address__c>());
        }
        addressesByKey.get(address.Address_Id__c).add(address);
      }
    }
    
    Address__c addressMatch;
    for (Address__c address: addresses) {
      String addressKey = addressKeyByAddressId.get(address.Id);
      system.debug('\n[AddressUtility : checkDuplicateAddresses] Address Key: ' + addressKey);

      List<Address__c> addressesMatch = addressesByKey.get(addressKey);
      if (addressesMatch != null && addressesMatch.size() > 0) {
        system.debug('\n~AddressUtility: Found one or many match(es): '+ addressesMatch);
        addressMatch = findMatchingAddress(address, addressesMatch);
      }
      if (addressMatch != null) {
        if (objectName.equalsIgnoreCase(Constants.SOBJECT_CONTACT)) {
          for (Contact_Address__c contactAddress: addressMatch.Contact_Addresses__r) {
            if (recordIds.contains(contactAddress.Contact__c)) {
              system.debug('\nFOUND DUPLICATE with '+ contactAddress.Id);
              return true;
            }
          }
        }
        if (objectName.equalsIgnoreCase(Constants.SOBJECT_ACCOUNT)) {
          for (Account_Address__c accountAddress: addressMatch.Account_Address__r) {
            if (recordIds.contains(accountAddress.Account__c)) {
              system.debug('\nFOUND DUPLICATE with '+ accountAddress.Id);
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  /**
   * This method will check the full key string, given the 255 characters limitations given by the external Id
   */
  public static Address__c findMatchingAddress(Address__c address, List<Address__c> addressesToCompare) {
    String databaseEntryAddress = '';
    String userInputAddress = DescribeUtility.buildKey(addressFields, (SObject) address, 'Address__c');
    for (Address__c addressToCompare: addressesToCompare) {
      databaseEntryAddress = DescribeUtility.buildKey(addressFields, (SObject) addressToCompare, 'Address__c');
      system.debug('\n~AddressUtility: \nComparing: ' + databaseEntryAddress + ' with: ' + userInputAddress);
      if (databaseEntryAddress == userInputAddress) {
        system.debug('~AddressUtility: Match found: ' + addressToCompare);
        return addressToCompare;
      }
    }
    return null;
  }
}