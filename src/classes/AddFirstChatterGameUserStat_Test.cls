/**********************************************************************************************
 * Experian 
 * Name         : AddFirstChatterGameUserStat_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleAddFirstChatterGameUserStat" & AddFirstChatterGameUserStat
 * Created Date : Sep 9, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Apr 7th, 2016                Paul Kissick                Removed scheduleable test
***********************************************************************************************/

@isTest
private class AddFirstChatterGameUserStat_Test {
    
  /*
  // Removed this schedulable class - not required 
  static testMethod void testSchedulable() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = '';
    insert gs;
      
    test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('Add Active Chatter Game', CRON_EXP, new ScheduleAddFirstChatterGameUserStat());
      
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                      FROM CronTrigger 
                      WHERE id = :jobId];     
    test.stopTest();
    // Verify the expressions are the same  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run  
    System.assertEquals(0, ct.TimesTriggered);
  }
  */
    
    
  private static testMethod void testBatchGame1() {
    Test.startTest();
    AddFirstChatterGameUserStat batchToProcess = new AddFirstChatterGameUserStat();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM User_Chatter_Stats__c WHERE Chatter_Game__c != '' AND First_Chatter_Stat_for_Game__c != '' AND Game_Fist_Stat__c = false]);
  }
  
  @testSetup
  private static void setupData() {
    
    /*
    BatchSchedulingIDstorage__c setting = new BatchSchedulingIDstorage__c();
    setting.BSIDS04__c = Userinfo.getOrganizationId();
    setting.BSIDS05__c = Userinfo.getOrganizationId();
    insert setting;
    */
    
    //Create test user
       
    User tstUser = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Region__c = 'APAC';
    tstUser.Business_line__c = 'APAC Corporate';
    tstUser.Function__c = 'Sales operations';

    insert new List<User>{tstUser};
    
    Chatter_Game__c game = new Chatter_Game__c(
      Name = 'Test Game 1',
      Game_Region__c = 'APAC',
      Business_line__c = 'APAC Corporate',
      Function__c = 'Sales operations',
      Unique_Active_Game_name__c = 'APACAPAC CorporateSales operations',
      Start_Date__c = date.today().addDays(-10),
      Active__c = true
    );
    
    insert new list<Chatter_Game__c>{game};
    
    User_Chatter_stats__c stat1 = new User_Chatter_stats__c(
      User__c = tstUser.Id,
      Cut_Off_date__c = date.today().addDays(-10),
      Chatter_Game__c = game.id
    );
    
    User_Chatter_stats__c stat2 = new User_Chatter_stats__c(
      User__c = tstUser.Id,      
      Chatter_Game__c = game.id
    );
    
    insert new List<User_Chatter_stats__c>{stat1,stat2};
    
    User_Chatter_stats__c stat2update;
    
    stat2update = [SELECT Cut_Off_date__c FROM User_Chatter_Stats__c WHERE id =: stat2.id ];
        
        stat2update.Cut_Off_date__c = date.today();
        
    update new list<User_Chatter_stats__c>{stat2update};
    
  }
  
}