/**********************************************************************************
 * Experian, Inc
 * Name: BatchConStatusUpdOneTimeSchedule 
 * Description: This is the schedulable interface for the Batch job BatchContactStatusUpdateOnlyGoLive
 * Created Date: Aug 23rd, 2016
 * Created By: Tyaga Pati (Experian)
 *
 * Summary:
 * - Schedulable interface for batch job BatchContactStatusUpdateOnlyGoLive
 *
 * Date Modified                Modified By                  Description of the update

 **********************************************************************************/

/*
 * This is the schedule that will allow the class BatchDQFlagPopulation to run on a schedule
*/
global class BatchConStatusUpdOneTimeSchedule implements Schedulable{
    
    global void execute(SchedulableContext SC) 
    {
        BatchContactStatusUpdateOnlyGoLive obj;
        obj = new BatchContactStatusUpdateOnlyGoLive();
        database.executeBatch(obj, 5000);
     }

    /*  
     * This code should be run in order to schedule the job:
     * 
     * //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
     * BatchDQFlagPopulationSchedule b = new BatchDQFlagPopulationSchedule();
     * String sch = '0 0 * * * ? *' ; // This is just like a regular cron job 
     * system.schedule('Populate DQ Flag, Decision Maker', sch, b);
     */

}