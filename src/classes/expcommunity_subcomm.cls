/******************************************************************************
 * Name: expcommunity_subcomm.cls
 * Created Date: 2/14/2017
 * Created By: Hay Win, UCInnovation
 * Description : Controller for expcommunity_subcommuApps component for new Exp Community
 * Change Log- 
 ****************************************************************************/

public with sharing class expcommunity_subcomm{ 
   
    public List<ExpCommunity_Apps__c> commList{get;set;}
    public boolean featured {get;set;}
    
    public expcommunity_subcomm() {
        commList = new List<ExpCommunity_Apps__c>();
    }
    
     public boolean getfindSubComm(){
         String featureString = '';
         if (featured == true) {
             featureString =  ' AND Feature__c =: featured';
         }
         System.debug('featured ' + featured );
         System.debug('featureString ' + featureString );         
         
         String commQuery = 'Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = true' + featureString + ' Order by Order__c asc';
         //commList = [Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = true AND Feature__c =: featured Order by Order__c asc];
         commList = Database.query(commQuery);
         System.debug('commQuery ' + commQuery );   
         System.debug('commList ' + commList );   
         
         return true;
     }
   
}