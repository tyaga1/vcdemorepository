/*==================================================================
 * Experian
 * Created By: Paul Kissick
 * Created Date: Oct 1st, 2015
 * 
 * Description: T-437698 Extension to page to intercept the New button on Opportunity tab
 *   and redirect only if presented with the conid parameter, referenced in URLRedirect class/page
 *  Caused by Winter16 changes.
 * 
 * Modified Date          By                      Description Of Change
 * 
 *==================================================================*/
public class OpportunityNewPageExtension {
  
  ApexPages.StandardController stdCon;

  public OpportunityNewPageExtension(ApexPages.StandardController con) {
    stdCon = con;
  }
    
  public PageReference prepareToRedirect() {
    // Hold all current url parameters in this variable
    Map<String,String> allParams = ApexPages.currentPage().getParameters();
    if (allParams.containsKey('conid')) {
      // conid url param is presented so, redirect to the normal edit/new page.
      allParams.put('nooverride','1'); // Hack to override the VF page
      allParams.remove('save_new'); // Remove this to prevent errors when redirecting
      PageReference pr = new PageReference('/006/e'); // Standard new/edit page
      pr.getParameters().putAll(allParams);
      pr.setRedirect(true);
      return pr;
    }
    else {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, system.label.Alert_Creating_from_Opportunity_Tab));
    }
    return null;
  }
        
}