public class ResellerHomePage_Cntrl {
//Use @AuraEnabled to enable client- and server-side access to the method
  @AuraEnabled
  public static String getProfileName() {
      	List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
    return PROFILE[0].Name;
  }
}