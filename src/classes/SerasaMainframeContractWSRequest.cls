/**=====================================================================
 * UC Innovation
 * Name: SerasaMainframeContractWSRequest
 * Description:  Class to do http callout to Serasa mainframe web service 
 * and parse results to display to a page
 *
 * Created Date: 8 May, 2017
 * Created By: Charlie Park
 =====================================================================*/

public class SerasaMainframeContractWSRequest {

	// Class to store results of webservice call
	public class contractResult {
	    public String ContractNumber {get;set;} 
	    public String ContractStartDate {get;set;}  
	    public String ContractDescription {get;set;}
	    public String ContractSource {get;set;}
	    public String StatusCode {get;set;}
    	public String ErrorMessage {get;set;}
	    public String returnStr() {
     		return ContractNumber +';'+ContractStartDate +';'+ContractDescription +';';
    	}
  	}   

  	public class webserviceResponse {
	    public Boolean success {get;set;}
	    public Boolean offline {get;set;}
	    public String errorMsg {get;set;}
	    public List<contractResult> contractResults {get;set;}
	    public webserviceResponse() {
	      success = false;
	      offline = false;
	      errorMsg = '';
	      contractResults = new List<contractResult>();
	    }
  	}

  	private static String webserviceName = 'SerasaMainframe-Contract';

  	private static Webservice_Endpoint__c smfSettings {
	    get {
	      	smfSettings = [SELECT Id, Name, URL__c, Username__c, Password__c, Timeout__c, Key__c, Active__c FROM Webservice_Endpoint__c 
      			WHERE Name = : 'SerasaMainframe-Contract' 
      			LIMIT 1];

	      	return smfSettings;
	    }
	}

	private Static String buildXmlRequest(String rValue, String userName, String password) {
    
	    String requestBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://services.experian.com.br/EBO/Comum/v1" xmlns:v11="http://services.experian.com.br/EBS/Contrato/v1" xmlns:v12="http://services.experian.com.br/EBO/Pessoa/v1">'; 
	    requestBody += '<soapenv:Header>';
	    requestBody += '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
	    requestBody +=  '<wsse:UsernameToken wsu:Id="UsernameToken-4">'; 
	    requestBody +=   '<wsse:Username>' + userName +'</wsse:Username>';
	    requestBody +=   '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+ password + '</wsse:Password>';
	    requestBody +=   '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">' + 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password)) +'</wsse:Nonce>';
	    requestBody +=   '<wsu:Created>' +Datetime.now().formatGmt('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '</wsu:Created>';
	    requestBody +=  '</wsse:UsernameToken>';
	    requestBody += '</wsse:Security>';
	    requestBody += '</soapenv:Header>';
	    
	    requestBody += '<soapenv:Body>';
	    
	    requestBody += '<v11:ConsultarPorDocumentoRequest>';
	    requestBody +=  '<v11:documento>';
	    requestBody +=   '<v12:numero>' + rValue+ '</v12:numero>';
	    requestBody +=  '</v11:documento>';
	    requestBody += '</v11:ConsultarPorDocumentoRequest>';

	    requestBody += '</soapenv:Body>';
	    requestBody += '</soapenv:Envelope>';
    	return requestBody;
  	}

  	private static HttpRequest buildHttpRequest() {
	    HttpRequest hr = new HttpRequest();
	    hr.setEndpoint(smfSettings.URL__c);  //'callout:SerasaMainframeWS'
	    hr.setTimeout(Integer.valueOf(smfSettings.Timeout__c * 1000));
	    
	    hr.setHeader('Authorization','Basic ' + EncodingUtil.base64Encode(Blob.valueOf(smfSettings.Username__c + ':' + smfSettings.Password__c)));
	            
	    hr.setHeader('Content-Type', 'text/xml; charset=utf-8');
	    hr.setHeader('Connection','keep-alive');
	    
	    hr.setMethod('POST');
	    return hr;
	}

	public static webserviceResponse CallGetContractsWS(String cnpjNumVal) {
		webserviceResponse wsresp = new webserviceResponse();

		if (String.isBlank(smfSettings.URL__c)) {
	      	wsresp.success = false;
	      	wsresp.errorMsg = webserviceName+' not found.';
	      	return wsresp;
	    }
	    
	    if (smfSettings.Active__c == false) {
		    wsresp.offline = true;
		    wsresp.success = false;
		    wsresp.errorMsg = '';
	        return wsresp;
	    }

	    Http http = new Http();
	    HttpRequest req = buildHttpRequest();

	    try {                
	      	req.setBody(buildXmlRequest(
	        cnpjNumVal,
	        smfSettings.Username__c,
	        smfSettings.Password__c
	     	));

	     	//system.debug('body xml' + req.getBody());

		    HttpResponse resp = http.send(req);

		    //system.debug('response' + resp);

		    String wsStatusCode = string.valueof(resp.GetStatusCode());

		    //PARSE USING DOM
	        XmlDom doc = new XmlDom(resp.getBody());
	        XmlDom.Element[] recs = doc.getElementsByTagName('ConsultarPorDocumentoResponse'); // pessoaJuridica'); //pessoa

	        //system.debug('recs' + recs);

	        if (recs != null && resp.getStatusCode() == 200) {
	          	for (XMlDom.Element rec : recs) {
	          		for (XmlDom.Element eachContract : rec.getElementsByTagName('Contrato')) {
						contractResult rContract = new contractResult();

			            rContract.ContractNumber = eachContract.getValue('identificadorContrato');
			            rContract.ContractStartDate = eachContract.getValue('dataRegistro');
			            rContract.ContractDescription = eachContract.getValue('descricao');  

			            if (eachContract.getValue('SistemaOrigem') == 'L')
		                {
		                    rContract.ContractSource = 'Legacy CRM';
		                }
		                else if(eachContract.getValue('SistemaOrigem') == 'S')
		                {
		                    rContract.ContractSource = 'Siebel on Premise';
		                }
            
			            rContract.StatusCode = wsStatusCode;
			            rContract.ErrorMessage = '';

			            wsresp.contractResults.add(rContract); 
	          		}
	        	} 

		        wsresp.success = true;
	      	} 
	      	else if (resp.getStatusCode() != 200) {
		        system.debug('update response = '+resp.getBody()); 
		        // responseVal = resp.toString();
		        // system.debug('update response2 = '+responseVal );  
		        wsresp.errorMsg = wsStatusCode+' : '+parseErrors(resp.getBody());
		        wsresp.success = false;
	      	}
	      
		    //lets check if the cnpjResult has anything in it
		      
		    //system.Debug('Return from Class :'+ wsresp.contractResults);
		      
		    return wsresp;

		} catch(System.CalloutException e)
		{
			wsresp.errorMsg = '0:'+e.getmessage();
		    wsresp.success = false;

		    //system.debug('exception caught' + e.getmessage());
		    return wsresp;
		}

		return wsresp;
	}

	private static String parseErrors(String respBody) {
	    String errorMsg = '';
	    XmlDom errdoc = new XmlDom(respBody);
	    XmlDom.Element[] errrecs = errdoc.getElementsByTagName('BusinessFault'); 
	    String errMessage;
	    if (errrecs != null) {
	      	for (XMlDom.Element rec1 : errrecs) {
	        	errMessage = rec1.getValue('mensagem');
	        	//system.Debug('Error Message:'+ errMessage); 
	        	//responseVal = wsStatusCode + ';' + errMessage;
	        	errorMsg = errMessage;
	      	}
	    }
	    XmlDom.Element[] techErrs = errdoc.getElementsByTagName('TechnicalFault'); 
	    if (techErrs != null) {
	      	for (XMlDom.Element rec1 : techErrs) {
		        errMessage = rec1.getValue('mensagem');
		        //system.Debug('Error Message:'+ errMessage); 
		        //responseVal = wsStatusCode + ';' + errMessage;
		        errorMsg = errMessage + ' - Please contact CRMSupport@experian.com';
	      	}
	    }
	    return errorMsg;
	}

	public class MockCalloutContract implements HttpCalloutMock {

	      String goodResponseBody = 
	      	'<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'
			+'<S:Header>'
			+'<wsse:Security S:mustUnderstand="0">'
			+'<wsu:Timestamp wsu:Id="_1" xmlns:ns14="http://docs.oasis-open.org/ws-sx/ws-secureconversation/200512" xmlns:ns13="http://www.w3.org/2003/05/soap-envelope">'
			+'<wsu:Created>2017-05-15T21:30:56Z</wsu:Created>'
			+'<wsu:Expires>2017-05-15T21:35:56Z</wsu:Expires>'
			+'</wsu:Timestamp>'
			+'</wsse:Security>'
			+'</S:Header>'
			+'<S:Body>'
			+'<ns3:ConsultarPorDocumentoResponse xmlns:ns3="http://services.experian.com.br/EBS/Contrato/v1" xmlns:ns11="http://services.experian.com.br/EBO/Cliente/v1" xmlns:ns10="http://services.experian.com.br/EBO/Fornecedor/v1" xmlns:ns9="http://services.experian.com.br/EBO/Telefone/v1" xmlns:ns8="http://services.experian.com.br/EBO/Endereco/v1" xmlns:ns7="http://services.experian.com.br/EBO/Comum/v1" xmlns:ns6="http://services.experian.com.br/EBO/Fault/v1" xmlns:ns5="http://services.experian.com.br/EBO/GarantiaFinanciamento/v1" xmlns:ns4="http://services.experian.com.br/EBO/Pessoa/v1" xmlns:ns2="http://services.experian.com.br/EBO/ParametroMultaFidelizacao/v1" xmlns="http://services.experian.com.br/EBO/Contrato/v1">'
			+'<ns3:contratos>'
			+'<Contrato>'
			+'<identificadorContrato>0012</identificadorContrato>'
			+'<dataRegistro>2014-12-18-02:00</dataRegistro>'
			+'<descricao>PLATAFORMAS MS</descricao>'
			+'</Contrato>'
			+'<Contrato>'
			+'<identificadorContrato>0013</identificadorContrato>'
			+'<dataRegistro>2014-12-18-02:00</dataRegistro>'
			+'<descricao>INFOCLEAN PME</descricao>'
			+'</Contrato>'
			+'<Contrato>'
			+'<identificadorContrato>0016</identificadorContrato>'
			+'<dataRegistro>2016-07-13-03:00</dataRegistro>'
			+'<descricao>ME AVISE CORPORATE</descricao>'
			+'</Contrato>'
			+'</ns3:contratos>'
			+'</ns3:ConsultarPorDocumentoResponse>'
			+'</S:Body>'
			+'</S:Envelope>';

	    public HTTPResponse respond(HTTPRequest req) {
	      HttpResponse res = new HttpResponse();
	      res.setHeader('Content-Type', 'application/xml');
	      res.setBody(goodResponseBody);
	      res.setStatusCode(200);
	      return res;
	    }
  	}
  
  	public class MockBadCalloutContract implements HttpCalloutMock {
    
	    String badResponseBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fault="http://services.experian.com.br/EBO/Fault/v1">'
		+'<soapenv:Body>'
		+'<soapenv:Fault>'
		+'<faultcode>soapenv:Server</faultcode>'
		+'<faultstring>Ocorreu um erro na execução do serviço</faultstring>'
		+'<faultactor>POST https://serviceshomologa.serasaexperian.com.br:8871/EBS/Contratov1 HTTP/1.1</faultactor>'
		+'<detail>'
		+'<fault:TechnicalFault>'
		+'<fault:codigo>3</fault:codigo>'
		+'<fault:descricao>Erro na execução do serviço - ESB</fault:descricao>'
		+'<fault:mensagem>/ Node throwing exception / br.com.services.experian.contrato.flow.Contrato.SOAP Extract / problem creating SOAP tree from bitstream / XML Parsing Errors have occurred / A schema validation error has occurred while parsing the XML document / 6012 / 1 / 12 / 34 / cvc-enumeration-valid: The value "" is not valid with respect to the enumeration facet for type "tipo". It must be a value from the enumeration. / /XMLNSC/http://schemas.xmlsoap.org/soap/envelope/:Envelope/http://schemas.xmlsoap.org/soap/envelope/:Body/http://services.experian.com.br/EBS/Contrato/v1:ConsultarPorDocumentoRequest/http://services.experian.com.br/EBS/Contrato/v1:documento/http://services.experian.com.br/EBO/Pessoa/v1:tipo</fault:mensagem>'
		+'</fault:TechnicalFault>'
		+'</detail>'
		+'</soapenv:Fault>'
		+'</soapenv:Body>'
		+'</soapenv:Envelope>';
	    
	    public HTTPResponse respond(HTTPRequest req) {
	      HttpResponse res = new HttpResponse();
	      res.setHeader('Content-Type', 'application/xml');
	      res.setBody(badResponseBody);
	      res.setStatusCode(500);
	      return res;
	    }
  	}
}