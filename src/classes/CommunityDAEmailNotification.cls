public class CommunityDAEmailNotification {

    @InvocableMethod(label='DA Knowledge Article email Notification')
	public static void AddDebug(list<Id> artcleIds) {
    
   List<Guide__kav> guideArticleList = [SELECT ArticleType,Product__c,Id,Description__c,Title,urlname,Group_Name__c,KnowledgeArticleId FROM Guide__kav where id in :artcleIds ];

    set<string> groupNameSet = new set<string>();
    	for (Guide__kav artcleGuide:guideArticleList){
    	    
    	        
    	    groupNameSet.add((artcleGuide.Group_Name__c).trim());
    	}
        
        
       List<GroupMember> groupMembers = [
    Select GroupId, UserOrGroupId ,Group.Name,group.DeveloperName
    From GroupMember
    Where Group.DeveloperName In :groupNameSet
];

map<string,set<String>> groupNameMapIds = new map<string,set<String>> ();

for (GroupMember GroupMemberRec:groupMembers){
    If(groupNameMapIds.containskey(GroupMemberRec.Group.DeveloperName))
    {
    Set<string> userIdSet = groupNameMapIds.get(GroupMemberRec.Group.DeveloperName);    
    userIdSet.add(GroupMemberRec.UserOrGroupId);
    groupNameMapIds.put(GroupMemberRec.Group.DeveloperName,userIdSet);
    }
    else
    
    {
        Set<string> userIdSet = new Set<string>();
        userIdSet.add(GroupMemberRec.UserOrGroupId);
    groupNameMapIds.put(GroupMemberRec.Group.DeveloperName,userIdSet );
    
    }
}

		for (Guide__kav artcleGuide:guideArticleList){
			OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress where displayname ='Customer Community' limit 1];
EmailTemplate templateId = [Select id from EmailTemplate where Developername = 'DA_Software_New_Guide_Release' Limit 1];
List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//list<string> emailIds = new list<string>('richard.joseph@experian.com');


mail.setTemplateID(templateId.Id); 
//mail.setHtmlBody('<H1> Test <h1>');
mail.setSaveAsActivity(false);
mail.setTargetObjectId([select id from contact where name ='Experian Community Contact' limit 1].id); 
mail.setOrgWideEmailAddressId(owa.id); 
mail.setTreatBodiesAsTemplate(False);
mail.setWhatId(artcleGuide.Id);   
//mail.setWhatId( [select id from contact where id ='0031b000005C9YV' limit 1].id );
//mail.toAddresses= new String[] { 'William.Hemmerick@experian.com' };
//mail.setBccAddresses( new String[] {'richard.joseph@experian.com' });
String[] userIdStringArray =new List<string>(groupNameMapIds.get(artcleGuide.Group_Name__c));
mail.setBccAddresses(userIdStringArray);
allmsg.add(mail);
KbManagement.PublishingService.publishArticle(artcleGuide.KnowledgeArticleId, true); 

Messaging.sendEmail(allmsg);

		}		
	}
}