/******************************************************************************
 * Name: ExpComm_CaseList_cmpt_test .cls
 * Created Date: 3/15/2017
 * Created By: Hay Win, UCInnovation
 * Description : Test class for ExpComm_CaseList_cmpt_Controller
 * Change Log- 
 ****************************************************************************/
@isTest
private class ExpComm_CaseList_cmpt_test {
    
    @isTest static void caseListApexControllerTest() {
        /*Account_Staging_Action_Code__c ac = new Account_Staging_Action_Code__c();
        ac.Name = '1';
        ac.isActive__c = True;
        ac.Description__c = 'Test';
        insert ac;*/
        Account testAccount1 = Test_Utils.createAccount();
        testAccount1.ownerId = userinfo.getuserId();
        insert testAccount1;
        List<Case> caseList = new List<Case>();
        String platformSupportId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Platform Support').getRecordTypeId();
 
        Case c1 = new Case(
                        RecordTypeId = platformSupportId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1');
        caseList.add(c1);

        Case c2 = new Case(
                        RecordTypeId = platformSupportId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 2',
                        Description = 'Test Description 2');
        caseList.add(c2);
        insert caseList;

        /****Test ServiceNow CaseList call out****/
        String mockJSON = '';
        ExpComm_MockHttp_ServiceNowCaseList fakeResponse = new ExpComm_MockHttp_ServiceNowCaseList(200,'Complete',
            '[{"parent": "","request": {"link": "https://experiantest.service-now.com/api/now/table/sc_request/359a130adbffae00417e3f3ffe961966","value": "359a130adbffae00417e3f3ffe961966"},"opened_at": "2016-12-21 16:49:53","short_description": "Request Access/Deactivation","closed_at": "","request.number": "REQ0010042","stage": "waiting_for_approval","sys_updated_on": "2016-12-21 16:49:53"}]',
            null);
 
        Test.setMock(HttpCalloutMock.class, fakeResponse);


        Test.startTest();
        List<ExpComm_CaseList_cmpt_Controller.caseWrapper> returnedList = ExpComm_CaseList_cmpt_Controller.findAll('Status','DESC');
        Test.stopTest();        
    }

    @isTest static void caseListApexControllerTest_altStatus() {
       /* Account_Staging_Action_Code__c ac = new Account_Staging_Action_Code__c();
        ac.Name = '1';
        ac.isActive__c = True;
        ac.Description__c = 'Test';
        insert ac;*/

        Account testAccount1 = Test_Utils.createAccount();
        testAccount1.ownerId = userinfo.getuserId();
        insert testAccount1;
        List<Case> caseList = new List<Case>();
        String platformSupportId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Platform Support').getRecordTypeId();
 
        Case c1 = new Case(
                        RecordTypeId = platformSupportId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1');
        caseList.add(c1);

        Case c2 = new Case(
                        RecordTypeId = platformSupportId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 2',
                        Description = 'Test Description 2');
        caseList.add(c2);
        insert caseList;

        /****Test ServiceNow CaseList call out****/
        String mockJSON = '';
        ExpComm_MockHttp_ServiceNowCaseList fakeResponse = new ExpComm_MockHttp_ServiceNowCaseList(100,'Complete',
            '[{"parent": "","request": {"link": "https://experiantest.service-now.com/api/now/table/sc_request/359a130adbffae00417e3f3ffe961966","value": "359a130adbffae00417e3f3ffe961966"},"opened_at": "2016-12-21 16:49:53","short_description": "Request Access/Deactivation","closed_at": "","request.number": "REQ0010042","stage": "waiting_for_approval","sys_updated_on": "2016-12-21 16:49:53"}]',
            null);
 
        Test.setMock(HttpCalloutMock.class, fakeResponse);

        try{
            Test.startTest();
            List<ExpComm_CaseList_cmpt_Controller.caseWrapper> returnedList = ExpComm_CaseList_cmpt_Controller.findAll('Status','DESC');
            Test.stopTest();
        }
        catch(exception e){
            
        }
                
    }
}