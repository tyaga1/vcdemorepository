/**=====================================================================
 * Experian
 * Name: ITCANavigationBannerController
 * Description: 
 * Created Date: Aug. 5th 2017
 * Created By: James Wills
 * 
 * Date Modified      Modified By        Description of the update
 * Aug. 5th, 2017     James Wills        ITCA:W-008961 Created.
 * Sept. 1st 2017     James Wills        Tidying up code.
 * Sept. 5th 2017     James Wills        ITCA Issue 1456: Adding functionality to Compare Skill from the Skill Sets list.
 * Sept. 5th 2017     Malcom Russell     ITCA Issue 1460: Added profileExists() method.
=====================================================================*/
global class ITCANavigationBannerController{
     
  public ITCANavigationBannerController() {

  }

  public ITCANavigationBannerController(ITCA_My_Journey_Map_Controller controller) {
       
  }


  public String level1_selectedCareerArea      {get;set;}
  public String level2_selectedSkillSet        {get;set;}
  
  public ITCABannerInfoClass bannerInfoLocal   {get;set;}
    
  

  //ITCA:W-008961
  public class itcaTeamUser{
    public ID userId     {get;set;}
    public ID profileID  {get;set;}
    public String Name   {get;set;}  
  } 
  
  public Map<ID,User> team_Map {get {  return new Map<ID,User>([SELECT id, Name, Title FROM User WHERE ManagerId = :UserInfo.getUserId()]);} set;}  
  public Boolean isManager {get{if(!team_Map.isEmpty()){return true;} else {return false;}}set;}

  //ITCA:W-008961
  public List<itcaTeamUser> teamList {get{
      Map<ID,itcaTeamUser> teamMap = new Map<ID,itcaTeamUser>();    
    
      Map<ID, Career_Architecture_User_Profile__c> currentUserProfile_Map = new Map<ID, Career_Architecture_User_Profile__c>
                                                                           ([SELECT id, CreatedDate, LastModifiedDate, Employee__c, Employee__r.Name, Employee__r.Title, Role__c, State__c, Status__c, (SELECT id,  Skill__c, Skill__r.id, Skill__r.name, Level__c FROM Career_Architecture_Skills_Plans__r)
                                                                           FROM Career_Architecture_User_Profile__c WHERE State__c = 'Current' AND Employee__c IN :team_Map.values()]);            
      for(Career_Architecture_User_Profile__c caup : currentUserProfile_Map.values()){      
        itcaTeamUser newUser = new itcaTeamUser();

        newUser.userID    = caup.Employee__r.id;
        newUser.profileID = caup.id;
        newUser.name      = caup.Employee__r.Name; 
      
        teamMap.put(newUser.userId,newUser);
      }            
      return teamMap.values();    
    }
    set;
  }
   

  //Called for MySkillsSummary
  public void mySkillsSummaryAction(){
    bannerInfoLocal.currentActiveTab='isSummary';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();   
    bannerInfoLocal.isSummaryBuild();
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});
  }
  
  
  public void isSummaryAction(){
    bannerInfoLocal.currentActiveTab='isSummary';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();   
    bannerInfoLocal.isSummaryBuild();
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});
  }
     
  //Called for CompareSkillSetsl
  public void myJourneyMapAction(){
    bannerInfoLocal.currentActiveTab='isMyJourneyMap';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>(); 
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});
  }
  
   
  //Called for CompareSkillSets
  public void compareSkillSetAction(){
    bannerInfoLocal.currentActiveTab='isCompareSKillSet';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();    
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});
  }
     
     
  //Called for CompareSkillSets
  public void compareCareerAreaAction(){
    bannerInfoLocal.currentActiveTab='isCompareCareerArea';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();      
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});
  }
  
  
  //Method to find top 5 skill sets for the skill
  public void matchSkillSetsAction(){
    bannerInfoLocal.currentActiveTab='isMatchSKills';    
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();
    
    //Cookie activeTabCookie = new Cookie('activeTabCookie', bannerInfoLocal.currentActiveTab, null,-1,false);
    //ApexPages.currentPage().setCookies(new Cookie[]{activeTabCookie});                
  }
  
  public void compareCareerAreaFromSkills(){}
  
  //ITCA:I1456
  public void compareSkillSetFromSkills(){
    compareSkillSetAction();
    compareSkillSet();
  }
       
  //Method to compare Skills under selected skill set and current user skills
  public void compareCareerArea(){
    
    String careerAreaName = null;
        
    if(level1_selectedCareerArea != null){
      careerAreaName=level1_selectedCareerArea;
    }
                      
    if (careerAreaName != null){         
      bannerInfoLocal.employeeSkillDisplay_CarComp_SortList = new List<ITCABannerInfoClass.employeeSkillDisplaySort >();
            
      List<AggregateResult> skillforCurrentSkillSetSelection = new list<AggregateResult>(
                                                               [SELECT Skill__r.Id,Skill__r.name, Max(Level_In_Number__c) 
                                                               FROM Skill_Set_to_Skill__c 
                                                               WHERE Experian_Skill_Set__r.Career_Area__c =:careerAreaName 
                                                               GROUP BY Skill__r.Name,Skill__r.Id]);
            
      for(AggregateResult skillforCurrentSkillSetSelRec:skillforCurrentSkillSetSelection){
        //Instantiate employeeSkillDisplay with constructor #2 = (AggregateResult skillforCurrentSkillSelRec, Map<String, Decimal> bannerInfoLocal.currentUserSkillLevelMap)
        bannerInfoLocal.employeeSkillDisplay_CarComp_SortList.add(new ITCABannerInfoClass.employeeSkillDisplaySort(new ITCABannerInfoClass.employeeSkillDisplay(skillforCurrentSkillSetSelRec,bannerInfoLocal.currentUserSkillLevelMap) ));            
      }
      bannerInfoLocal.employeeSkillDisplay_CarComp_SortList.sort();
            
      bannerInfoLocal.employeeSkillDisplay_CarComp_List = new List<ITCABannerInfoClass.employeeSkillDisplay>();
            
      for(ITCABannerInfoClass.employeeSkillDisplaySort employeeSkillDisplaySortRec :bannerInfoLocal.employeeSkillDisplay_CarComp_SortList){
        bannerInfoLocal.employeeSkillDisplay_CarComp_List.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
      }                         
    }               
  }
          
          
  //Method to compare Skills under Selected Career Area and current user skills
  public void compareSkillSet(){
    
    String skillSetId = null;
      
    if(level2_selectedSkillSet != null){
      skillSetId=level2_selectedSkillSet;
    }
                
    if(skillSetId != null){            
      bannerInfoLocal.employeeSkillDisplay_SkillComp_SortList = new List<ITCABannerInfoClass.employeeSkillDisplaySort >();
            
      List<AggregateResult> skillforCurrentSkillSetSelection = new list<AggregateResult>(
                                                                         [SELECT Skill__r.Id,Skill__r.name, Max(Level_In_Number__c) 
                                                                         FROM Skill_Set_to_Skill__c 
                                                                         WHERE Experian_Skill_Set__c =:skillSetId 
                                                                         GROUP BY Skill__r.Name,Skill__r.Id]);
            
      for(AggregateResult skillforCurrentSkillSetSelRec:skillforCurrentSkillSetSelection){
        bannerInfoLocal.employeeSkillDisplay_SkillComp_SortList.add(new ITCABannerInfoClass.employeeSkillDisplaySort(new ITCABannerInfoClass.employeeSkillDisplay(skillforCurrentSkillSetSelRec,bannerInfoLocal.currentUserSkillLevelMap) ));
      }
      bannerInfoLocal.employeeSkillDisplay_SkillComp_SortList.sort();
            
      bannerInfoLocal.employeeSkillDisplay_SkillComp_List = new List<ITCABannerInfoClass.employeeSkillDisplay>();
            
      for(ITCABannerInfoClass.employeeSkillDisplaySort employeeSkillDisplaySortRec :bannerInfoLocal.employeeSkillDisplay_SkillComp_SortList){
        bannerInfoLocal.employeeSkillDisplay_SkillComp_List.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
      }                   
    }               
  }          
  
  //ITCA:I1460
  public boolean profileExists {get{
  
      Career_Architecture_User_Profile__c[] userprofile = [select id from Career_Architecture_User_Profile__c where employee__c=:UserInfo.getUserId() limit 1];
  
      if (!userprofile.isEmpty()){ return true;}
      else {return false;}
    }
  }
  
  
}