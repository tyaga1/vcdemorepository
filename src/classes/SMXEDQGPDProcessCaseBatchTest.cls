/**=====================================================================
 * Experian
 * Name: SMXEDQGPDProcessCaseBatchTest
 * Description: 
 * Created Date: 19/04/2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 =====================================================================*/
@isTest
private class SMXEDQGPDProcessCaseBatchTest {
  
  static testmethod void testBatch(){
    
    system.assertEquals(0,[SELECT COUNT() FROM Feedback__c]);
    
    Test.startTest();

    SMXEDQGPDProcessCaseBatch b = new  SMXEDQGPDProcessCaseBatch();
    Database.executeBatch(b);

    Test.stopTest();
    
    system.assertEquals(1,[SELECT COUNT() FROM Feedback__c WHERE DataCollectionId__c = :SMXEDQGPDProcessCaseBatch.strSurveyId]);

  }
 
  @testSetup
  private static void prepareTestData() {
    Account a = Test_Utils.insertAccount();
    
    Contact c = Test_Utils.insertContact(a.Id);
    
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    testUser.Global_Business_Line__c = 'Corporate';
    testUser.Business_Line__c = 'Corporate';
    testUser.Business_Unit__c = 'APAC:SE';
    testUser.Region__c = 'North America';
    insert testUser;
    system.runAs(testUser){
        Case cs = new Case(
          Subject = 'CCC1', 
          AccountId = a.Id, 
          Status = 'New', 
          ContactId = c.Id,          
          CreatedDate = Datetime.now().addDays(-2).addHours(-1),
          Type = 'TEST',
          Reason = 'TEST',
          Origin = 'Email',
          RecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE , 'EDQ GPD Support')
        );
        insert cs;
        
        List<Case> newCS = [SELECT Id, Status, ClosedDate FROM Case WHERE Id =: cs.Id];
        
        for(Case closeCs : newCS){ 
        closeCs.Status = 'Closed Resolved';        
        }
        update newCS;
    }

  }

    
}