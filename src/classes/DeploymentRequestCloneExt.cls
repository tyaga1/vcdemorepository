/**=====================================================================
 * Experian
 * Name: DeploymentRequestCloneExt
 * Description: 
 * Created Date: 25 Feb 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified       Modified By          Description of the update
 * Apr 13, 2016        Paul Kissick         Added support for Completed field on a clone
 =====================================================================*/
public with sharing class DeploymentRequestCloneExt {
  
  ApexPages.StandardController stdCon;
  
  Deployment_Request__c depReqMaster;
  
  public DeploymentRequestCloneExt(ApexPages.StandardController con) {
    stdCon = con;
    depReqMaster = (Deployment_Request__c)stdCon.getRecord();
  }
  
  public PageReference doClone() {
    
    Id depReqMasterId = depReqMaster.Id;
    Set<String> depReqFields = Deployment_Request__c.sobjectType.getDescribe().fields.getMap().keySet();
    Set<String> depCaseFields = Deployment_Cases__c.sobjectType.getDescribe().fields.getMap().keySet(); // (Deployment_Request_Slot__c)
    Set<String> depCompFields = Deployment_Component__c.sobjectType.getDescribe().fields.getMap().keySet();  // (Slot__c)
    
    List<String> depReqFieldList = new List<String>(depReqFields);
    List<String> depCaseFieldList = new List<String>(depCaseFields);
    List<String> depCompFieldList = new List<String>(depCompFields);
    
    Deployment_Request__c masterReq = (Deployment_Request__c)Database.query('SELECT '+String.join(depReqFieldList,',')+' FROM Deployment_Request__c WHERE Id = :depReqMasterId');
    List<Deployment_Cases__c> masterDepCases = (List<Deployment_Cases__c>)Database.query('SELECT '+String.join(depCaseFieldList,',')+' FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :depReqMasterId');
    List<Deployment_Component__c> masterDepComps = (List<Deployment_Component__c>)Database.query('SELECT '+String.join(depCompFieldList,',')+' FROM Deployment_Component__c WHERE Slot__c = :depReqMasterId');
    
    Savepoint sp = Database.setSavepoint(); 
    try {
      Deployment_Request__c clonedReq = masterReq.clone(false,true,false,false);
      clonedReq.Release__c = (String.isNotBlank(masterReq.Release__c)) ? masterReq.Release__c.mid(0,70) + ' - Copy' : 'Copy';
      clonedReq.Status__c = 'Not Started';
      insert clonedReq;
    
      List<Deployment_Cases__c> clonedDepCases = new List<Deployment_Cases__c>();
      List<Deployment_Component__c> clonedDepComps = new List<Deployment_Component__c>();
    
      for(Deployment_Cases__c dCase : masterDepCases) {
        Deployment_Cases__c cdCase = dCase.clone(false,true,false,false);
        cdCase.Deployment_Request_Slot__c = clonedReq.Id;
        clonedDepCases.add(cdCase);
      }
    
      for(Deployment_Component__c dComp : masterDepComps) {
        Deployment_Component__c cdComp = dComp.clone(false,true,false,false);
        cdComp.Slot__c = clonedReq.Id;
        cdComp.Completed__c = false;
        clonedDepComps.add(cdComp);
      }
    
      insert clonedDepCases;
      insert clonedDepComps;
    
      ApexPages.StandardController newCon = new ApexPages.StandardController(clonedReq);
    
      return newCon.view();
    }
    catch (Exception e) {
      Database.rollback(sp);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
    }
    return null;
  }
  
}