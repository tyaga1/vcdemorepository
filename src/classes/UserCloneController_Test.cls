/*=============================================================================
 * Experian
 * Name: UserCloneController_Test
 * Description: 
 * Created Date: 13 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 13 Jul 2016 (QA)   Paul Kissick          CRM2:W-005416 - New User set up - clone permissions,chatter groups from another user
 * 15 Jul 2016 (QA)   Paul Kissick          CRM2:W-005416 - Adding more to support changes to controller class.
 =============================================================================*/

@isTest
private class UserCloneController_Test {

  static String testUserEmail1 = 'kjbdhfdsf897@experian.com';
  static String testUserEmail2 = 'kajushfdj909@experian.com';
  static String testUserEmail3 = 'hjasfkasj8798@experian.com';
  static String testGroupName = 'my new group ajhbaskjdfhas92928';

  static testMethod void testStandardNonChatter() {
    
    User u1 = [SELECT Id FROM User WHERE Email = :testUserEmail1];
    User u2 = [SELECT Id FROM User WHERE Email = :testUserEmail2];
    
    ApexPages.currentPage().getParameters().put('Id',u2.Id);
    UserCloneController ucc = new UserCloneController();
    
    ucc.cloneFromUser.ManagerId = u1.Id;
    ucc.loadFromUserMembership();
    
    Test.startTest();
    
    ucc.selectAllPerms();
    ucc.selectAllGroups();
    ucc.selectAllQueues();
    ucc.deselectAllChatter();
    
    ucc.applySelections();
    
    system.assert(ucc.backToUser() != null);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM PermissionSetAssignment WHERE AssigneeId = :u2.Id AND PermissionSet.IsOwnedByProfile = false]);
    system.assertEquals(1, [SELECT COUNT() FROM GroupMember WHERE UserOrGroupId = :u2.Id AND Group.Name = :testGroupName]);
    system.assertEquals(0, [SELECT COUNT() FROM CollaborationGroupMember WHERE MemberId = :u2.Id]);
    
  }
  
  static testMethod void testChatterGroup() {
    User u3 = [SELECT Id FROM User WHERE Email = :testUserEmail3];
    User u2 = [SELECT Id FROM User WHERE Email = :testUserEmail2];
    
    ApexPages.currentPage().getParameters().put('Id',u2.Id);
    UserCloneController ucc = new UserCloneController();
    
    ucc.cloneFromUser.ManagerId = u3.Id;
    ucc.loadFromUserMembership();
    
    Test.startTest();
    
    ucc.deselectAllPerms();
    ucc.deselectAllGroups();
    ucc.deselectAllQueues();
    ucc.selectAllChatter();
    
    ucc.applySelections();
    
    Test.stopTest();
    
    system.assertEquals(0, [SELECT COUNT() FROM PermissionSetAssignment WHERE AssigneeId = :u2.Id AND PermissionSet.IsOwnedByProfile = false]);
    system.assertEquals(0, [SELECT COUNT() FROM GroupMember WHERE UserOrGroupId = :u2.Id AND Group.Name = :testGroupName]);
    system.assertEquals(1, [SELECT COUNT() FROM CollaborationGroupMember WHERE MemberId = :u2.Id]);
  }
  
  @testSetup
  private static void setupData() {
    
    User newUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    newUser1.Email = testUserEmail1;
    User newUser2 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    newUser2.Email = testUserEmail2;
    User newUser3 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    newUser3.Email = testUserEmail3;
    insert new List<User>{newUser1, newUser2, newUser3};
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      // Create a permission set, group and chatter group.
      PermissionSet ps1 = new PermissionSet(
        Label = 'Brand New Testing Set',
        Name = 'Brand_New_Testing_Set'
      );
      insert ps1;
      
      Group grp = Test_Utils.createGroup(false);
      grp.Name = testGroupName;
      insert grp;
      
      CollaborationGroup chatGrp1 = new CollaborationGroup(
        Name = 'Test Group 098 '+String.valueOf(Math.random()* 1000),
        CollaborationType = 'Private'
      );
      insert chatGrp1;
      
      // Now assign all of these to 1 of the users
      PermissionSetAssignment psa1 = new PermissionSetAssignment(
        PermissionSetId = ps1.Id,
        AssigneeId = newUser1.Id
      );
      insert psa1;
      
      GroupMember grpMem1 = new GroupMember(
        GroupId = grp.Id,
        UserOrGroupId = newUser1.Id
      );
      insert grpMem1;
      
      // Assigning this to another user to another test.
      CollaborationGroupMember chatMemb = new CollaborationGroupMember(
        CollaborationGroupId = chatGrp1.Id,
        MemberId = newUser3.Id
      );
      insert chatMemb;
      
    }
    
  }
  
}