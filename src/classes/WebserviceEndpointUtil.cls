/**=====================================================================
 * Experian
 * Name: WebserviceEndpointUtil
 * Description: Utility class to return relevant details for the webservice
 * Created Date: Oct 29th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
public class WebserviceEndpointUtil {
  
  public static Webservice_Endpoint__c getWebserviceDetails(String name) {
    List<Webservice_Endpoint__c> wsEnds = [
      SELECT Id, Name, URL__c, Username__c, Password__c, Timeout__c, Key__c, Active__c
      FROM Webservice_Endpoint__c 
      WHERE Name = :name 
      LIMIT 1
    ];
    if (wsEnds.size() == 1) {
      Webservice_Endpoint__c ws = wsEnds[0];
      Blob key = EncodingUtil.base64Decode(ws.Key__c);
      ws.Password__c = Crypto.decryptWithManagedIV('AES256', key, EncodingUtil.base64Decode(ws.Password__c)).toString();
      ws.Key__c = null;
      return ws;
    }
    return new Webservice_Endpoint__c();
  }
  
  public static Webservice_Endpoint__c createWebserviceKey(Webservice_Endpoint__c wsIn) {
    Webservice_Endpoint__c ws = new Webservice_Endpoint__c(Id = wsIn.Id, Key__c = EncodingUtil.base64Encode(Crypto.generateAesKey(256)));
    update ws;
    return wsIn;
  }
  
  public static Webservice_Endpoint__c setWebservicePassword(Webservice_Endpoint__c wsIn, String password) {
    String passKey = [SELECT Key__c FROM Webservice_Endpoint__c WHERE Id = :wsIn.Id AND Key__c != null].Key__c;
    String encPass = EncodingUtil.base64encode(Crypto.encryptWithManagedIV('AES256', EncodingUtil.base64Decode(passKey), Blob.valueOf(password)));
    Webservice_Endpoint__c ws = new Webservice_Endpoint__c(Id = wsIn.Id, Password__c = encPass, Password_Set__c = true);
    update ws;
    return wsIn;
  }
  
  
}