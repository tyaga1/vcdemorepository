/**=====================================================================
 * Appirio, Inc
 * Name: Order_Credit_WS_Test
 * Description: T-417697: To Test funcitonality of Order_Credit_WS webservice
 * Created Date: Jul 08th, 2015
 * Created By: Naresh Kr Ojha (Appirio) for Task 
 * 
 * Date Modified        Modified By              Description of the update
 * Jul 09th, 2015       Naresh Kr Ojha           T-417869: Updated class to cover Asset credited related functionality
 * Aug 12th, 2015       Terri Jiles              renamed Order_Credit_WS_Test and covered s
 * Aug 17th, 2015       Naresh Kr Ojha           Fixed failures on this test class.
 * Dec 2nd, 2015        Paul Kissick             Fixed testCreditTransactionalSaleOrder to run as testuser
 * Dec 16th, 2015       Paul Kissick             Case 01250120: Fix for duplicate error 
 * Apr 7th, 2016        Paul Kissick             Case 01932085: Fixing Test User Email Domain
 * Oct 20th, 2016       James Wills              Case #02088090 - Resolved issue with test following validation rule change
 =====================================================================*/
@isTest
private class Order_Credit_WS_Test {
  
  @isTest
  static void testCreditTransactionalSaleOrder () {
    // create User
    Profile p = [SELECT ID FROM Profile WHERE Name =: Constants.PROFILE_SYS_ADMIN ];
    
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    UserRole copsRole = [SELECT Id FROM UserRole WHERE Name = :Constants.ROLE_NA_COPS];
    testUser1.UserRoleId = copsRole.Id;
    insert testUser1;
  
    system.runAs(testUser1) {
      // Create an account
      Account testAccount = Test_Utils.insertAccount();
      Contact newcontact  = new Contact (FirstName = 'Larry', LastName = 'Ellison',
                                          AccountId = testAccount.Id, Email = 'larrye@email.com');
      Product2 product = Test_Utils.insertProduct();
      product.RevenueScheduleType = Constants.REVENUE_SCHEDULED_TYPE_REPEAT;    
      product.RevenueInstallmentPeriod = Constants.INSTALLMENT_PERIOD_DAILY;    
      product.NumberOfRevenueInstallments = 2;    
      //product.CanUseQuantitySchedule = false;
      product.CanUseRevenueSchedule = true;
      
      update product;
      //Pricebook2 pricebook = Test_Utils.getPriceBook2();
      //Pricebook2 standardPricebook = Test_Utils.getPriceBook2(Constants.STANDARD_PRICE_BOOK);
      Billing_Product__c billingProduct = Test_Utils.insertBillingProduct();
      PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
      


      Test.startTest();
      //insert Locked Transactional Sale Order
      Order__c order = Test_Utils.insertOrder (false, testAccount.Id, newcontact.Id, null);
      order.Locked__c = false;
      order.Transactional_Sale__c = true;
      order.SaaS_Order__c = true;
      insert order;

      //insert order lines
      Order_Line_Item__c oli = Test_Utils.insertOrderLineItems (false, order.Id, billingProduct.Id);
      insert oli;
      Asset newAsset = insertOrderAsset(true, testAccount.ID, order.ID);

      order.Locked__c = true;
      update order;

      Order_Credit_WS.creditOrder(order.ID);
      Test.stopTest();
      Order__c  testOrder = [SELECT ID, Type__c, Credited_Date__c, 
                  (SELECT Id, Credited_Date__c, Credited__c, Status__c FROM Assets__r) 
                   FROM Order__c WHERE Id =: order.Id LIMIT 1];
            
      system.assertEquals(testOrder.Type__c, Constants.ORDER_TYPE_CREDITED);
      system.assertEquals(testOrder.Credited_Date__c, system.Today());
      //system.assert(testOrder.Assets__r.size() > 0);
      system.assert(testOrder.Assets__r.get(0).Status__c == 'Credited');
      system.assert(testOrder.Assets__r.get(0).Credited__c == true);
      system.assert(testOrder.Assets__r.get(0).Credited_Date__c == system.today());

    }

  }

  @isTest
  static void testAttemptToCreditNonTransactionalOrder () {
    // create User
    Profile p = [SELECT ID FROM Profile WHERE Name =: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
  
    system.runAs(testUser1) {
      // Create an account   
      Account testAccount = Test_Utils.insertAccount();
      testAccount.Is_Competitor__c = true;
      update testAccount;

      Address__c address = Test_Utils.insertAddress(true); // Case  01250120 - Moved address insertion to here.
      // insert Account Registered Address as it mandatory
      Account_Address__c accountAddress =  Test_Utils.insertAccountAddress(true, address.ID, testAccount.ID); 
  
      // Create an opportunity
      Opportunity testOpp = Test_Utils.createOpportunity(testAccount.Id);
      testOpp.Has_Senior_Approval__c = true;
      testOpp.StageName = Constants.OPPTY_STAGE_3;
      //testOpp.Below_Review_Thresholds__c = 'Yes';
      testOpp.Starting_Stage__c = Constants.OPPTY_STAGE_7;
      testOpp.Amount = 100;
      testOpp.Channel_Type__c = Constants.OPPTY_CHANNEL_TYPE_INDIRECT;
      testOpp.Type = Constants.OPPTY_TYPE_RENEWAL;
      insert testOpp;
      
      Test_Utils.createOpptyTasks(testOpp.Id, true);
  
      Contact newcontact  = new Contact (FirstName = 'Larry', LastName = 'Ellison',
                                          AccountId = testAccount.Id, Email = 'larrye@email.com');
      insert newcontact;
      
      
      // insert Contact Address as it mandatory
      Contact_Address__c contactAddress = Test_Utils.insertContactAddress(true, address.Id, newcontact.Id);
      
      OpportunityContactRole oppContactRole = new OpportunityContactRole(ContactId = newcontact.Id, OpportunityId = testOpp.Id, IsPrimary = true, Role = Constants.DECIDER);
      insert oppContactRole ;
      
      //////////////////////
      // Create Opportunity Line Item
      Product2 product = Test_Utils.insertProduct();
      product.RevenueScheduleType = Constants.REVENUE_SCHEDULED_TYPE_REPEAT;    
      product.RevenueInstallmentPeriod = Constants.INSTALLMENT_PERIOD_DAILY;    
      product.NumberOfRevenueInstallments = 2;    
      //product.CanUseQuantitySchedule = false;
      product.CanUseRevenueSchedule = true;
      
      update product;
      //Pricebook2 pricebook = Test_Utils.getPriceBook2();
      //Pricebook2 standardPricebook = Test_Utils.getPriceBook2(Constants.STANDARD_PRICE_BOOK);
      Billing_Product__c oppLineItemBillingProduct = Test_Utils.insertBillingProduct();
      PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
      //insert OLI
      //OpportunityLineItem opportunityLineItem2 = Test_Utils.insertOpportunityLineItem(testOpp.Id, stdPricebookEntry.Id);
      OpportunityLineItem opportunityLineItem2 =  new OpportunityLineItem (TotalPrice = 200, UnitPrice = null, Quantity = 1,
                                                     OpportunityId = testOpp.Id, PricebookEntryId = stdPricebookEntry.Id,
                                                     Start_Date__c = Date.today().addDays(5), End_Date__c = Date.today().addDays(10),
                                                     Order_Type__c = Constants.STATUS_NEW);
                                                     
      opportunityLineItem2.Type__c = Constants.OPPTY_LINE_ITEM_TYPE_RENEWAL;//Case #02088090
                                                     
      insert opportunityLineItem2;
  
      Competitor__c comp = Test_Utils.createCompetitor(testOpp.Id);
      insert comp;
      Test.startTest();

      OpportunityTrigger_OrderHelper.isExecuted = false;

      OpportunityTriggerHandler.isAfterUpdateTriggerExecuted = false;
      OpportunityTriggerHandler.isBeforeUpdateTriggerExecuted = false;
      testOpp.StageName = Constants.OPPTY_STAGE_7;
      testOpp.Channel_Type__c = 'Direct';
      //testOpp.Status__c = Constants.OPPTY_CLOSED_WON;
      testOpp.Primary_Reason_W_L__c = Constants.PRIMARY_REASON_WLC_DATA_QUALITY;
      //system.assertEquals(null, testOpp);
      update testOpp;
      
      Order__c orderRecord = [SELECT ID FROM Order__c WHERE Opportunity__c =: testOpp.ID LIMIT 1];
      insertOrderAsset(true, testAccount.Id, orderRecord.Id);
      system.assertNotEquals(orderRecord.ID, null); 
        
      //Calling webservice
      Order_Credit_WS.creditOrder(orderRecord.ID);
      Test.stopTest();
      orderRecord = [SELECT ID, Type__c, Credited_Date__c, 
                     (SELECT Id, Credited_Date__c, Credited__c, Status FROM Assets__r) 
                     FROM Order__c WHERE Opportunity__c =: testOpp.ID LIMIT 1];
            
      system.assertNotEquals(orderRecord.Type__c, Constants.ORDER_TYPE_CREDITED);
      system.assertNotEquals(orderRecord.Credited_Date__c, system.Today());
      system.assert(orderRecord.Assets__r.size() > 0);
      //system.assert(orderRecord.Assets__r.get(0).Status == 'Credited');
      system.assert(orderRecord.Assets__r.get(0).Credited__c != true);
      system.assert(orderRecord.Assets__r.get(0).Credited_Date__c != system.today());
    }
  }

  //================================================================//
  //Experian SaaS- Create Asset for order and Return
  //================================================================//
  private static Asset insertOrderAsset(Boolean isInsert, String accountId, String orderId){
    Asset asset = new Asset (
                              Name = 'Test Order Asset',
                              AccountId = accountId,
                              Order__c = orderId,
                              Release_Version__c = 'Test Version1',
                              Operating_System__c = 'PC+',
                              Renewal_Discount__c = 5,
                              Data_Usage__c = 'N/A',
                              Price = 1.00,
                              Users_To__c = 2);
    if(isInsert){
      insert asset;
    }
    return asset;
  }
  
  @testSetup
  private static void setupTestData() {
    Test_Utils tu = new Test_Utils();
  }
}