/******************************************************************************
 * Name: expcomm_reopenCase.cls
 * Created Date: 2/22/2017
 * Created By: Hay Win, UCInnovation
 * Description : Used by reopen case button lightning component in customer community
 * Change Log- 
 ****************************************************************************/
public with sharing class expcomm_reopenCase{
public static Case origCase {get;set;}
    
    public expcomm_reopenCase() {
        origCase = new Case();
    }
    
    @AuraEnabled
    public static boolean isResolved(String caseID) {
        origCase = [Select Id, Status from Case where Id =: caseID];
        if (origCase.Status == 'Resolved') {
            return true;
        } else {
            return false;
        }
        
    }
    
    @AuraEnabled
    public static void changeStatus(String caseID) {
        origCase = [Select Id, Status from Case where Id =: caseID];
        if (origCase.Status == 'Resolved') {
            origCase.Status = 'Open';
            update origCase;
        }
        
        
    }
}