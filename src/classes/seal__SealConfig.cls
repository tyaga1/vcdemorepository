/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SealConfig {
    @RemoteAction
    global static String validateConnection() {
        return null;
    }
    @RemoteAction
    global static String validateScheduleUser() {
        return null;
    }
}
