/**=====================================================================
 * Experian
 * Name: ITCA_Homepage_Controller_Test
 * Description: Test Class for ITCA_Homepage_Controller
 *
 * Created Date: Aug. 30th 2017
 * Created By: James Wills for ITCA Project
 * 
 * Date Modified        Modified By           Description of the update
 * 1st September 2017   James Wills           ITCA: Minor changes
 * 6th September 2017   James Wills           ITCA: Updated to reflect recent changes in ITCA_Homepage_Controller
=====================================================================*/
@isTest
private class ITCA_Homepage_Controller_Test{

 private static testMethod void test_ITCA_Homepage_Controller_1(){
    
   PageReference pageRef = Page.ITCA_Home_Page;        
   Test.setCurrentPage(pageRef);
   ITCA_Homepage_Controller hpc1 = new ITCA_Homepage_Controller();
      
   //ApexPages.StandardController stdController = new ApexPages.StandardController(hpc1);
   
    createProfile();
        
    Test.startTest();
  
      String ck = hpc1.getitcaCookie();
      
      Boolean profEx = hpc1.profileExists;
      
      //Id itca = hpc1.ITCAChatterGroupId;
      
    Test.stopTest();
  
  }
  

  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
    
    //CollaborationGroup cg = new CollaborationGroup(Name = label.ITCA_Chatter_Group,
    //                                               CollaborationType='Private');
    //insert cg;
   
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=psList[0].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){
   
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= userInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level6',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill LIMIT 1].id  );
    
    insert casp1;
  }


}