/**=====================================================================
 * Experian
 * Name: opportunityAssetsConsoleExtension_Test
 * Description: Test Class for contactAssetsConsoleExtension. 
 * 
 * Created Date: Sep 15th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified        Modified By                  Description of the update
 *
 =====================================================================*/
 @isTest
public class opportunityAssetsConsoleExtension_Test{

  public static testMethod void opportunityAssetsConsoleExtensionTest(){
  
  
    //Create Account and Contact
    Account testAccount = Test_Utils.insertAccount();
    Contact newcontact  = Test_Utils.insertContact(testAccount.id);
    
     //Create Opportunity
    Opportunity testOpp = Test_Utils.createOpportunity(testAccount.Id);
    insert testOpp;

     
    //Create User and AccountTeamMember
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    testUser1.Business_unit__c=Constants.UK_I_MS_DATA_QUALITY;
    testUser1.DefaultCurrencyIsoCode='GBP';
    testUser1.CurrencyIsoCode='GBP';
    insert testUser1;
    Id testuserId = [select id from user where Business_unit__c=:Constants.UK_I_MS_DATA_QUALITY limit 1].id;
   
   
    //Create Asset
    Asset tstAst = Test_utils.insertAsset(false, testAccount.ID);
    tstAst.Order_Owner_BU_Stamp__c=Constants.UK_I_MS_DATA_QUALITY;
    tstAst.Status='Delivered';
    insert tstAst;    
    
    
    Test.startTest();
    System.runAs(testUser1) {
      PageReference pageRef = Page.opportunityassetsConsole;
      Test.setCurrentPageReference(pageRef);
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)testOpp);
    
      ApexPages.currentPage().getParameters().put('Id', testOpp.id);
      opportunityAssetsConsoleExtension controller = new opportunityAssetsConsoleExtension(stdController);
     
      Asset[] accass=controller.accountAssets;

     
    } 
    Test.stopTest();
   
    
    
  }


}