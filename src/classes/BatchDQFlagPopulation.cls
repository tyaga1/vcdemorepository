/**=====================================================================
 * Name: BatchDQFlagPopulation
 * Description: CRM2 W-005615: Updates the Has_Decision_Maker__c checkbox
 * Created Date: Aug 16th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified     Modified By        Description of the update
 * 25 Aug, 2016      Paul Kissick       CRM2:W-005615: Improved to find all opp contact roles, even deleted, and refactored to
                                        look at the opportunity as the primary scope object
 * 31 Aug, 2016      Paul Kissick       CRM2:W-005615: Added Isdeleted = false to main query.
 =====================================================================*/

public class BatchDQFlagPopulation implements Database.Batchable<sObject>, Database.Stateful {

  public Datetime startTime;
  
  public List<String> errorsFound = new List<String>();

  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    startTime = Datetime.now();
    
    Datetime lastRunTime = BatchHelper.getBatchClassTimestamp('BatchDQFlagPopulation');
    if (lastRunTime == null) {
      // Set the last run time to the start of 2010, just to make sure we get all the opps to update, given this has never run before.
      lastRunTime = Datetime.newInstanceGmt(2010,1,1,0,0,0);
    }
    
    // Switching this out to find all changes to the opp contact roles, and pull out those opps.
    return Database.getQueryLocator ([
      SELECT Id, Has_Decision_Maker__c
      FROM Opportunity
      WHERE Id IN (
        SELECT OpportunityId
        FROM OpportunityContactRole
        WHERE LastModifiedDate >= :lastRunTime
      )
      AND IsDeleted = false
      ALL ROWS
    ]);
    // PK-310816- Adding IsDeleted = false, since I added ALL ROWS to get all changes to the OpportunityContactRole object.
  }

  //===========================================================================
  // Execute - 
  // it checks for all Contact Roles and updates the Opportunities that have a new Decision Maker
  //===========================================================================
  public void execute (Database.BatchableContext bc, List<Opportunity> scope) {
    
    List<Opportunity> oppsToUpdate = new List<Opportunity>();
    
    List<OpportunityContactRole> oppContRoles = [
      SELECT Id, Role, OpportunityId
      FROM OpportunityContactRole
      WHERE OpportunityId IN :(new Map<Id, Opportunity>(scope)).keySet()
    ];
    
    Map<Id, Boolean> decMakerFound = new Map<Id, Boolean>();
    
    for (OpportunityContactRole ocr : oppContRoles) {
      if (!decMakerFound.containsKey(ocr.OpportunityId)) {
        decMakerFound.put(ocr.OpportunityId, false);
      }
      if (decMakerFound.get(ocr.OpportunityId) == false && ocr.Role == Constants.DECIDER) {
        // Set this map to true when we find a dec maker for the opp
        decMakerFound.put(ocr.OpportunityId, true);
      }
    }
    
    for (Opportunity opp : scope) {
      oppsToUpdate.add(
        new Opportunity(
          Id = opp.Id, 
          Has_Decision_Maker__c = (decMakerFound.containsKey(opp.Id) && decMakerFound.get(opp.Id)==true)
        )
      );
    }
    
    List<Database.SaveResult> srList = Database.update(oppsToUpdate, false);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        errorsFound.add('Opp Id: ' + oppsToUpdate.get(i).Id + '\n' + BatchHelper.parseErrors(srList.get(i).getErrors()));
      }
    }
  }

  public void finish(Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchDQFlagPopulation', false);
    
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += 'Errors found: \n\n'+String.join(errorsFound, '\n\n');
    }
    
    bh.sendEmail();
    
    // Store the original start time to find any newly changed opp contact roles from the time this batch started
    BatchHelper.setBatchClassTimestamp('BatchDQFlagPopulation', startTime);
    
  }

}