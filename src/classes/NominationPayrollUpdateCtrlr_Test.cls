/*=============================================================================
 * Experian
 * Name: NominationPayrollUpdateCtrlr_Test
 * Description: Test class for NominationPayrollUpdateCtrlr
 * Created Date: 20 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 * 
 =============================================================================*/

@isTest
private class NominationPayrollUpdateCtrlr_Test {
  
  static String holdGuid = BatchHelper.newGuid();

  static testMethod void emptyTest() {
    
    PageReference repPage = Page.NominationPayrollUpdate;
    Test.setCurrentPage(repPage);
    repPage.getParameters().put('guid', '129084721047241');
    
    NominationPayrollUpdateCtrlr c = new NominationPayrollUpdateCtrlr();
    system.assert(c.loadReport() == null);
    system.assert(c.reportFields.size() > 0);
    system.assert(c.nominationsToUpdate.size() == 0);
    system.assert(c.finished() != null);
    
  }
  
  static testMethod void dataTest() {
    
    List<Nomination__c> noms = [SELECT Payroll_Batch_GUID__c FROM Nomination__c WHERE Notify_Payroll__c = true];
    
    for (Nomination__c n : noms) {
      n.Payroll_Batch_GUID__c = holdGuid;
    }
    update noms;
    
    Test.startTest();
    
    PageReference repPage = Page.NominationPayrollUpdate;
    Test.setCurrentPage(repPage);
    repPage.getParameters().put('guid', holdGuid);
    
    NominationPayrollUpdateCtrlr c = new NominationPayrollUpdateCtrlr();
    system.assert(c.loadReport() == null);
    system.assert(c.reportFields.size() > 0);
    system.assert(c.nominationsToUpdate.size() == noms.size());
    system.assert(c.getTheTable() != null);
    system.assert(c.saveChanges() == null);
    
  }
  
  @testSetup
  private static void setupData() {
    
    NominationTestHelper_Test.createTestUsers();
    
    NominationTestHelper_Test.createRecognitionBadges();
    
    NominationTestHelper_Test.createNominations(NominationHelper.NomConstants.get('Approved'), NominationHelper.NomConstants.get('Level2SpotAward'), 2, false);
    
    NominationTestHelper_Test.createNominations(NominationHelper.NomConstants.get('Won'), NominationHelper.NomConstants.get('Level3Individual'), 2, false);
    
  }
  
}