/**=====================================================================
 * Experian
 * Name: SaaSlastCaseComment_Test 
 * Description: Test class for SaaSlastCaseComment
 * Created Date: Sep 7th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified      Modified By                  Description of the update
 *
 * Jan, 5th 2016	  James Wills				   Case #01266714: Updated reference to constant to reflect change in name (see comment below).
 *
 *===================================================================== */
@isTest
private class SaaSlastCaseComment_Test {
  
  static testMethod void lastCaseCommentTest() {
    // Insert case, insert a comment, or two, and check the last one is retrieved...
    Case c1 = new Case(
      Subject = 'Test Case 1', Status = 'Open',RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_REC_TYPE_SFDC_SUPPORT).getRecordTypeId() //Case #01266714 James Wills; Changed from Constants.CASE_REC_TYPE_CRM_REQUEST
    );
    insert c1;
    
    CaseComment cc1 = new CaseComment(ParentId = c1.Id, CommentBody = 'Test Comment 2');
    insert cc1;
    
    Solution s1 = new Solution(SolutionNote = 'Details of the Solution....', SolutionName = 'Subject of the Solution', Status = 'Reviewed');
    insert s1;
    
    CaseSolution cs1 = new CaseSolution(CaseId = c1.Id, SolutionId = s1.Id);
    insert cs1;
    
    SaaSlastCaseComment lastComment = new SaaSlastCaseComment();
    lastComment.caseId = c1.Id;
    system.assertEquals('Test Comment 2', lastComment.lastCaseComment,'Last Comment Incorrect');
    system.assertNotEquals('',lastComment.solutionSubjectAndDescription,'Last Solution Missing');
    
    system.assertNotEquals('',lastComment.NewLine);

  }
  
  @testSetup static void loadSettings() {
    
    Global_Settings__c gl = new Global_Settings__c(Name = Constants.GLOBAL_SETTING);
    insert gl;
    
  }
}