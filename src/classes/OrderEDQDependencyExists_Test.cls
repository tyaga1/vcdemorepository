/**********************************************************************************************
 * Experian 
 * Name         : OrderEDQDependencyExists_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleEDQDependencyValidation04" & OrderEDQDependencyExists
 * Created Date : September 8th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Nov 11th, 2015               Paul Kissick                Case 01266075: Removed bad global setting entry, and testSchedulable
***********************************************************************************************/

@isTest
private class OrderEDQDependencyExists_Test {
    
  private static testMethod void testOrderEDQDependency() {
    Test.startTest();
    OrderEDQDependencyExists batchToProcess = new OrderEDQDependencyExists();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    
    system.assertEquals(1,[SELECT COUNT() FROM Order__C where (Owner_BU_on_Order_Create_Date__c LIKE '%Data Quality%' OR Saas_Order_Line_Count__c > 0)]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(true);
    tstAcc.EDQ_Dependency_Exists__c = false;
                
    update new List<Account>{tstAcc};
    
    Contact tstCon = Test_utils.insertEDQContact(tstAcc.ID,true);
        
    update new List<Contact>{tstCon};
    
    User tstUser = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Business_Unit__c = 'NA MS Data Quality';
        
    insert new List<User>{tstUser};
    IsDataAdmin__c IsdataAdmin = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    
    insert IsdataAdmin;
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.OwnerId = tstUser.Id;
    tstOpp.StageName = Constants.OPPTY_STAGE_7;
        
    insert new List<Opportunity>{tstOpp};
    
    Order__c tstOrd = Test_utils.insertOrder(true,tstAcc.ID,tstCon.ID,tstOpp.ID);
    tstOrd.OwnerId = tstUser.Id;
    tstOrd.Owner_BU_on_Order_Create_Date__c = 'NA MS Data Quality';
        
    update new List<Order__c>{tstOrd};
    
    delete IsdataAdmin;
        
  }
  
}