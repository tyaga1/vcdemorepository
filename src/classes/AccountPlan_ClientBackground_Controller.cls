/**=====================================================================
 * Name: AccountPlan_ClientBackground_Controller
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Mar. 11th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Mar. 11th, 2016       James Wills           Case #01848189: Created
 * June 2nd 2016         James Wills           Case #01949954 AP - List views
 * Oct. 26th, 2016       James Wills           Case #01983126 Added toLabel() translations to SOQL
  =====================================================================*/
public with sharing class AccountPlan_ClientBackground_Controller{

  
  public ID accountPlanId {get;set;}
  Account_Plan__c accountPlan {get;set;}
  
  public Account account {get;set;}
  
  public ID selectedId {get;set;}
  
  public Integer pageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSize = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Case_List__c;
  
  public Integer casesLimit= relatedListSize; 
  public Integer additionalCasesToView {
    get{
      if(endAPCaseNumber+relatedListSize > totalAPCases){
        return totalAPCases-endAPCaseNumber;
      } else {
        return relatedListSize;
      } 
    }
  set;}
  

  public Integer startAPCaseNumber {get;set;}
  public Integer endAPCaseNumber {get;set;}
  public Integer totalAPCases {get;set;}
  

  public List<openCasesWrapper> openCasesForAccountTeamList {
    get{
      return buildAccountTeamOpenCasesList();
    }    
    set;
  }
  
  public class openCasesWrapper{
    public String ID {get;set;}
    public String CaseNumber {get;set;}
    public String AccountID {get;set;}
    public String AccountName {get;set;}
    public String ContactID {get;set;}
    public String ContactName {get;set;}
    public String Subject {get;set;}
    public String Priority {get;set;}
    public String CreatedDate {get;set;}
    public String Status {get;set;}
    public String CaseRecordType {get;set;}
    public String Owner {get;set;}
    public String CaseReason {get;set;}
    public String Type {get;set;}
  }
  

  public AccountPlan_ClientBackground_Controller( ApexPages.Standardcontroller std){
    
    accountPlan = (Account_Plan__c) std.getRecord();
    accountPlanId = accountPlan.id;
    
    openCasesForAccountTeamList= new List<openCasesWrapper>();
    
    buildAccountTeamOpenCasesList();
  }

  public void clientBackgroundTabUpdateCookies(){
  
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, Label.ACCOUNTPLANNING_ClientBackgroundTab);
  
  }


  
  public List<openCasesWrapper> buildAccountTeamOpenCasesList(){
    
    List<openCasesWrapper> openCasesForAccountTeamListLocal = new List<openCasesWrapper>();

    List<Account_Account_Plan_Junction__c> accountPlanHierarchyList = [SELECT Account__c from Account_Account_Plan_Junction__c where Account_Plan__c = :accountPlanId];
  
    List<ID> accountsInHierarchy = new List<ID>();
    for(Account_Account_Plan_Junction__c  acc : accountPlanHierarchyList){
       accountsInHierarchy.add(acc.Account__c);
    }
    
    List<Case> openCases = new List<Case>();
    if(accountsInHierarchy.size()>0){
      openCases = [SELECT id, CaseNumber, AccountID, Account.Name, ContactID, Contact.Name, Subject, toLabel(Priority), CreatedDate, toLabel(Status), toLabel(RecordType.Name), Owner.Name, toLabel(Reason), toLabel(Type)
                  FROM Case 
                  WHERE (AccountId = :accountPlan.Account__c OR AccountId IN :accountsInHierarchy) 
                  AND (RecordType.Name != 'New User' AND 
                       RecordType.Name != 'Salesforce.com Support' AND
                       RecordType.Name != 'New Product' AND
                       RecordType.Name != 'New Competitor' AND
                       RecordType.Name != 'CSDA Contract Request' AND
                       RecordType.Name != 'CSDA CIS BA Request' AND
                       RecordType.Name != 'Account Access Request')                  
                  AND (NOT Status LIKE '%Closed%')
                  LIMIT :casesLimit
                  ];
    } else {
      openCases = [SELECT id, CaseNumber, AccountID, Account.Name, ContactID, Contact.Name, Subject, Priority, CreatedDate, Status, RecordType.Name, Owner.Name, Reason, Type 
                  FROM Case 
                  WHERE AccountId = :accountPlan.Account__c  
                  AND (RecordType.Name != 'New User' AND 
                       RecordType.Name != 'Salesforce.com Support' AND
                       RecordType.Name != 'New Product' AND
                       RecordType.Name != 'New Competitor' AND
                       RecordType.Name != 'CSDA Contract Request' AND
                       RecordType.Name != 'CSDA CIS BA Request' AND
                       RecordType.Name != 'Account Access Request')    
                  AND (NOT Status LIKE '%Closed%')
                  LIMIT :casesLimit
                  ];
    }
    
    if(openCases.size()>0){
      Integer i = 0;
      while(i < casesLimit && i < openCases.size()){      
      //for(Case caseFromHierarchy: openCases){
        openCasesWrapper openCase = new openCasesWrapper();
        openCase.ID               = openCases[i].id;
        openCase.CaseNumber       = openCases[i].CaseNumber;
        openCase.AccountID        = openCases[i].AccountID;
        openCase.AccountName      = openCases[i].Account.Name;
        openCase.ContactID        = openCases[i].ContactID;
        openCase.ContactName      = openCases[i].Contact.Name;
        openCase.Subject          = openCases[i].Subject;
        openCase.Priority         = openCases[i].Priority;
  
        String openDate           = String.valueOf(openCases[i].CreatedDate);
        openCase.CreatedDate      = openDate;     

        openCase.Status           = openCases[i].Status;

        openCase.CaseRecordType   = openCases[i].RecordType.Name;
        openCase.Owner            = openCases[i].Owner.Name;
        openCase.CaseReason       = openCases[i].Reason;
        openCase.Type             = openCases[i].Type;
        
        openCasesForAccountTeamListLocal.add(openCase);
        i++;
      }          
    }
    
    
    //Now set values used for Case list navigation
    if(openCasesForAccountTeamListLocal.size()>0){
      startAPCaseNumber = pageOffSet+1;
      endAPCaseNumber = pageOffset + openCasesForAccountTeamListLocal.size();  
      AggregateResult[] APCases = new AggregateResult[]{};
      
      if(accountsInHierarchy.size()>0){
        APCases = [SELECT COUNT(CaseNumber)TotalAPCases FROM Case 
                  WHERE (AccountId = :accountPlan.Account__c OR AccountId IN :accountsInHierarchy) 
                  AND (RecordType.Name != 'New User' AND 
                       RecordType.Name != 'Salesforce.com Support' AND
                       RecordType.Name != 'New Product' AND
                       RecordType.Name != 'New Competitor' AND
                       RecordType.Name != 'CSDA Contract Request' AND
                       RecordType.Name != 'CSDA CIS BA Request' AND
                       RecordType.Name != 'Account Access Request')                  
                  AND (NOT Status LIKE '%Closed%')];
      } else {
         APCases = [SELECT COUNT(CaseNumber)TotalAPCases FROM Case 
                   WHERE AccountId = :accountPlan.Account__c  
                   AND (RecordType.Name != 'New User' AND 
                       RecordType.Name != 'Salesforce.com Support' AND
                       RecordType.Name != 'New Product' AND
                       RecordType.Name != 'New Competitor' AND
                       RecordType.Name != 'CSDA Contract Request' AND
                       RecordType.Name != 'CSDA CIS BA Request' AND
                       RecordType.Name != 'Account Access Request')    
                   AND (NOT Status LIKE '%Closed%')];
      }
      
      totalAPCases  = (Integer)APCases[0].get('TotalAPCases');    
    } else {
      startAPCaseNumber = 0;
      endAPCaseNumber = 0;  
      totalAPCases = 0;
    }
    
    return openCasesForAccountTeamListLocal;
      
  }
  
  
  public PageReference viewCase(){
    clientBackgroundTabUpdateCookies();
    
    PageReference caseToView = new PageReference('/' + selectedId);

    return caseToView;
  }
  
  
  public PageReference viewAccountPlanCaseListFull(){
  
    clientBackgroundTabUpdateCookies();  

    PageReference newAccountPlanCasePageRef = Page.AccountPlanClientBackgroundCases; 

    return newAccountPlanCasePageRef;
  }


  public void seeMoreCases(){   
    CasesLimit+=additionalCasesToView;
  }


  public PageReference backToAccountPlan(){  
    PageReference accountPlan = new PageReference('/' + accountPlanId ); 
    accountPlan.setRedirect(true); 
    return accountPlan;      
  }



}