/**=====================================================================
 * Experian
 * Name: AccountSegmentTriggerHandler
 * Description: This class was written to populate the Global and Regional Strategic Client fields
 *              of the Account, based upon the values present in Account_Segment__c records, so that that
 *              these could be written to the Account_Staging__c table.
 *              There is a test in the class OpportunityTrigger_AccountSegment_Test for this class.
   Before Insert :
   Before Update :                                 
   Before Delete :  
   
 * Created Date: Dec. 8th 2016
 * Created By: James Wills for DRM Project
 * 
 * Date Modified            Modified By                  Description of the update
 * 4th May, 2017            James Wills                  InsideSales:W-008298 - Added methods to update CIS and BIS fields on parent Accounts
 *
=====================================================================*/


public class AccountSegmentTriggerHandler{

  public static void afterInsert (List<Account_Segment__c> newList) {
    AccountSegmentTriggerHandler.insertAccountStrategicFields(newList);
    AccountSegmentTriggerHandler.afterInsertUpdateCISandBISValues(newList);
  }  

  public static void afterUpdate (List<Account_Segment__c> newList, Map<Id, Account_Segment__c> oldMap) {
      AccountSegmentTriggerHandler.updateAccountStrategicFields(newList, oldMap);
      AccountSegmentTriggerHandler.afterUpdateUpdateCISandBISValues(newList, oldMap);
  }
  
  public static void afterDelete (List<Account_Segment__c> oldList) {
      AccountSegmentTriggerHandler.deleteAccountStrategicFields(oldList);
      AccountSegmentTriggerHandler.afterDeleteUpdateCISandBISValues(oldList);
  }
  
  
  public static void afterInsertIsDataAdmin (List<Account_Segment__c> newList) {
    AccountSegmentTriggerHandler.insertAccountStrategicFields(newList);
    AccountSegmentTriggerHandler.afterInsertUpdateCISandBISValues(newList);
  }
  
  public static void afterUpdateIsDataAdmin (List<Account_Segment__c> newList, Map<Id, Account_Segment__c> oldMap) {
      AccountSegmentTriggerHandler.updateAccountStrategicFields(newList, oldMap);
      AccountSegmentTriggerHandler.afterUpdateUpdateCISandBISValues(newList, oldMap);
  }  

  public static void afterDeleteIsDataAdmin (List<Account_Segment__c> oldList) {
      AccountSegmentTriggerHandler.deleteAccountStrategicFields(oldList);
      AccountSegmentTriggerHandler.afterDeleteUpdateCISandBISValues(oldList);
  }


  public static void insertAccountStrategicFields(List<Account_Segment__c> newList){    
    Map<Id, Account_Segment__c> newRegionAccountSegments_Map = new Map<Id, Account_Segment__c>();
        
    for(Account_Segment__c accSeg : newList){
      if (accSeg.Type__c == 'Region') {
        //system.debug('DEBUG: New Account_Segment__c of Type__c Region: ' + accSeg.Id);
        newRegionAccountSegments_Map.put(accSeg.Id, accSeg);
      }
    }    
    if(newRegionAccountSegments_Map.isEmpty()){
      return;
    } else {
      updateAccountStrategicValues(newRegionAccountSegments_Map);
    }  
  }
  
  public static void updateAccountStrategicFields(List<Account_Segment__c> newList, Map<Id, Account_Segment__c> oldMap){    
    Map<Id, Account_Segment__c> changedAccountSegments_Map = new Map<Id, Account_Segment__c>();
    
    for(Account_Segment__c accSeg : newList){
      Account_Segment__c old = oldMap.get(accSeg.Id);
      if (accSeg.Type__c == 'Region' && (accSeg.Strategic_Client__c != old.Strategic_Client__c)) {
        //system.debug('DEBUG: Regional_Strategic_Account__c for record ' + accSeg.Id + ' changed.');
        changedAccountSegments_Map.put(accSeg.Id, accSeg);
      }
    }    
    if(changedAccountSegments_Map.isEmpty()){
      return;
    } else {
      updateAccountStrategicValues(changedAccountSegments_Map);
    }
  }

  public static void deleteAccountStrategicFields(List<Account_Segment__c> newList){    
    Map<Id, Account_Segment__c> changedAccountSegments_Map = new Map<Id, Account_Segment__c>();
    
    for(Account_Segment__c accSeg : newList){
      if (accSeg.Type__c == 'Region') {
        //system.debug('DEBUG: Regional_Strategic_Account__c for record ' + accSeg.Id + ' changed.');
        changedAccountSegments_Map.put(accSeg.Id, accSeg);
      }
    }    
    if(changedAccountSegments_Map.isEmpty()){
      return;
    } else {
      updateAccountStrategicValues(changedAccountSegments_Map);
    }             
  }
  
  
  public static void updateAccountStrategicValues(Map<Id, Account_Segment__c> changedAccountSegments_Map){
    List<Account_Segment__c> changedAccountSegmentsList = changedAccountSegments_Map.values();
    Map<Id, Account> accountsToUpdate_Map = new Map<Id, Account>();
    
    for(Account_Segment__c accSeg2 : changedAccountSegmentsList){
      Account accToUpdate = new Account();
      
      //Check to see if more than one Account_Segment__c is being updated for the same Account
      if(accountsToUpdate_Map.containsKey(accSeg2.Account__c)){
        accToUpdate = accountsToUpdate_Map.get(accSeg2.Account__c);
      } else {//We have already found a field to be updated on this parent Account
        accToUpdate.id = accSeg2.Account__c;
        accountsToUpdate_Map.put(accToUpdate.id, accToUpdate);
      }
      
      if(accSeg2.Value__c        ==Constants.REGION_GLOBAL){
        accToUpdate.Global_Strategic_Client__c         = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      } else if(accSeg2.Value__c ==Constants.REGION_NORTH_AMERICA){
        accToUpdate.NA_Regional_Strategic_Client__c    = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      } else if(accSeg2.Value__c ==Constants.REGION_UKI ){
        accToUpdate.UK_I_Regional_Strategic_Client__c  = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      } else if(accSeg2.Value__c ==Constants.REGION_EMEA){
        accToUpdate.EMEA_Regional_Strategic_Client__c  = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      } else if(accSeg2.Value__c ==Constants.REGION_APAC){
        accToUpdate.APAC_Regional_Strategic_Client__c  = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      } else if(accSeg2.Value__c ==Constants.REGION_LATIN_AMERICA){
        accToUpdate.LATAM_Regional_Strategic_Client__c = (accSeg2.Strategic_Client__c && !accSeg2.isDeleted) ? true : false;
      }                                      
                              
      /*system.debug('DEBUG1: ' + accToUpdate.id + accSeg2.Account__r.Region__c +  ' Regional_Strategic_Account__c: ' + accSeg2.Strategic_Client__c +' ' +
                   accToUpdate.Global_Strategic_Client__c        + ' ' + accToUpdate.APAC_Regional_Strategic_Client__c + ' ' +
                   accToUpdate.EMEA_Regional_Strategic_Client__c + ' ' + accToUpdate.NA_Regional_Strategic_Client__c + ' ' +
                   accToUpdate.UK_I_Regional_Strategic_Client__c + ' ' + accToUpdate.LATAM_Regional_Strategic_Client__c);*/
    }
    
    List<Account> accountsToUpdate = accountsToUpdate_Map.values();
    
    if(accountsToUpdate_Map.isEmpty()){
      return;
    }
    
    try{
      update accountsToUpdate;
    } catch(Exception e){
      system.debug('\n[AccountSegmentTriggerHandler: updateAccountStrategicValues]: ['+e.getMessage()+']]');
      apexLogHandler.createLogAndSave('AccountSegmentTriggerHandler','uupdateAccountStrategicValues', e.getStackTraceString(), e);
    }  
  }
  
  public static void afterInsertUpdateCISandBISValues(List<Account_Segment__c> newList){
    Map<ID, Account_Segment__c> changedAccountSegments_Map = new Map<ID, Account_Segment__c>();
    
    for(Account_Segment__c accSeg : newList){
      if(accSeg.Name.contains('BIS') || accSeg.Name.contains('CIS')){
        changedAccountSegments_Map.put(accSeg.id, accSeg);
      }    
    }
    
    if(changedAccountSegments_Map.isEmpty()){
      return;
    } else {    
      updateBISandCISSegmentValues(changedAccountSegments_Map);
    }
  }
  
  public static void afterUpdateUpdateCISAndBISValues(List<Account_Segment__c> newList, Map<ID, Account_Segment__c> oldMap){
    Map<ID, Account_Segment__c> changedAccountSegments_Map = new Map<ID, Account_Segment__c>();
  
    for(Account_Segment__c accSeg : newList){
      if(accSeg.Name.contains('BIS') || accSeg.Name.contains('CIS')){
        if(accSeg.Relationship_Type__c != oldMap.get(accSeg.id).Relationship_Type__c){
          changedAccountSegments_Map.put(accSeg.id, accSeg);
        }
      }    
    }
  
    if(changedAccountSegments_Map.isEmpty()){
      return;
    } else {    
      updateBISandCISSegmentValues(changedAccountSegments_Map);
    }
  }
  
  public static void afterDeleteUpdateCISAndBISValues(List<Account_Segment__c> oldList){
    Map<ID, Account_Segment__c> changedAccountSegments_Map = new Map<ID, Account_Segment__c>();
  
   for(Account_Segment__c accSeg : oldList){
      if(accSeg.Name.contains('BIS') || accSeg.Name.contains('CIS')){
        changedAccountSegments_Map.put(accSeg.id, accSeg);
      }    
    }
  
    if(changedAccountSegments_Map.isEmpty()){
      return;
    } else {    
      updateBISandCISSegmentValues(changedAccountSegments_Map);
    }
  }
  
  
  public static void updateBISandCISSegmentValues(Map<ID, Account_Segment__c> newMap){
    List<ID> accListToUpdate = new List<ID>();    
    
    for(Account_Segment__c accSeg : newMap.values()){
      accListToUpdate.add(accSeg.Account__c);    
    }
    
    Map<ID, Account> accMap = new Map<ID, Account>([SELECT id FROM Account WHERE id IN :accListToUpdate]);
    
    for(Account_Segment__c accSeg2 : newMap.values()){
      if(accSeg2.Name.contains('BIS')){
        if(!accSeg2.isDeleted){
          accMap.get(accSeg2.Account__c).BIS_Relationship_Type__c = accSeg2.Relationship_Type__c;
        } else {
          accMap.get(accSeg2.Account__c).BIS_Relationship_Type__c = Label.Account_Segment_Deleted + ' ' + datetime.now();
        }
      } else if(accSeg2.Name.contains('CIS')){
        if(!accSeg2.isDeleted){
          accMap.get(accSeg2.Account__c).CIS_Relationship_Type__c = accSeg2.Relationship_Type__c;
        } else {
          accMap.get(accSeg2.Account__c).CIS_Relationship_Type__c = Label.Account_Segment_Deleted + ' ' + datetime.now();
        }
      }    
    }
    
    try{
      update accMap.values();
    } catch(Exception e){
      system.debug('\n[AccountSegmentTriggerHandler: updateCISandBISSegmentValues]: ['+e.getMessage()+']]');
      apexLogHandler.createLogAndSave('AccountSegmentTriggerHandler','updateCISandBISSegmentValues', e.getStackTraceString(), e);
    }      
    
  }
  

}