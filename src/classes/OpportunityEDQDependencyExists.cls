/**=====================================================================
 * Experian
 * Name: OpportunityEDQDependencyExists
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Opportunities with an EDQ Dependency
 * Created Date: 6/18/2015
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 14th, 2015               Paul Kissick                 Adding support for testing
 * Nov 9th, 2015                Paul Kissick                 Case 01266075: Optimisations for queries taking too long.
 =====================================================================*/
global class OpportunityEDQDependencyExists implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
    return Database.getQueryLocator ([
      SELECT Id, AccountId
      FROM Opportunity
      WHERE Owner_s_Business_Unit__c LIKE '%Data Quality%' 
      AND IsWon = true
      AND Account.EDQ_Dependency_Exists__c = false
    ]);
  }

  global void execute (Database.BatchableContext bc, List<Opportunity> scope) {

    Set<Id> accIdSet = new Set<Id>();
    List<Account> accountsList = new List<Account>();

    for(Opportunity opp : scope) {
      accIdSet.add(opp.AccountId);
    }

    for(Id accId : accIdSet) {
      accountsList.add(
        new Account(
          Id = accId, 
          EDQ_Dependency_Exists__c = true
        )
      );
    }
    
    if (accountsList != null && accountsList.size() > 0) {
      try {             
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('OpportunityEDQDependencyExists','execute', ex.getStackTraceString(), ex);
      }
    }
  }

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'OpportunityEDQDependencyExists', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new AssetEDQDependencyExists(),'Asset - EDQ Dependency Exists '+String.valueOf(Datetime.now().getTime()),2,ScopeSizeUtility.getScopeSizeForClass('AssetEDQDependencyExists'));
    }
    
  }

}