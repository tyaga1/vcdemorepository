/*=====================================================================
 * Experian
 * Name: AccountPartnerController_Test
 * Description: This class is used to Test the functionality of AccountPartnerController.
 *
 * Created Date: Aug 25th, 2016
 * Created By: Manoj (Experian)
 *
 * Date Modified            Modified By         Description of the update
 * Aug 25th, 2016           Manoj Gopu          CRM2:W-005614 - Accounts - Opportunity list where account is the partner
 * Sep 8th, 2016            Paul Kissick        CRM2:W-005614: Fixed test. Had to add (seeAllData=true) due to known issue
                                                (https://success.salesforce.com/issues_view?id=a1p3A000000eaw8QAA)
 * Sep 8th, 2016            Manoj Gopu          CRM2:W-005614: Added Asserts to the Test Method.
 * Sep 14th, 2016           Paul Kissick        CRM2:W-005614: Fixed assertions to look the right way
 ======================================================================*/
@isTest(seeAllData = true)
private class AccountPartnerController_Test {

  static testMethod void myUnitTest() {

    Account acc = Test_Utils.insertAccount();
    Account acc1 = Test_Utils.insertAccount();
    Account acc2 = Test_Utils.insertAccount();

    Opportunity opty = Test_Utils.createOpportunity(acc1.Id);
    opty.Amount = 123;
    insert opty;

    list<Partner> lstPart = new list<Partner>();

    Partner objP = new Partner();
    objP.OpportunityId = opty.Id;
    objP.AccountToId = acc.Id;
    lstPart.add(objP);

    for (integer i = 1; i <= 10; i++) {
      Partner objP1 = new Partner();
      objP1.AccountFromId = acc1.Id;
      objP1.AccountToId = acc.Id;
      lstPart.add(objP1);
    }
    insert lstPart;

    Partner objP6 = new Partner();
    objP6.AccountFromId = acc1.Id;
    objP6.AccountToId = acc.Id;
    insert objP6;

    Test.startTest();

    ApexPages.StandardController sc = new ApexPages.StandardController(acc);

    AccountPartnerController objCont = new AccountPartnerController(sc);

    system.assert(objCont.backToAccount() != null);

    String sortexp = objCont.sortExpression;
    system.assertEquals('DESC', objCont.getSortDirection());

    objCont.sortExpression = 'Stage';
    string str = objCont.getSortDirection();
    objCont.displayPartnerData();
    system.assertEquals('ASC', objCont.getSortDirection());

    objCont.sortExpression = 'CloseDate';
    objCont.displayPartnerData();
    system.assertEquals(5, objCont.totallistsize);
    system.assertEquals(true, objCont.isShowMore);

    Partner objP12 = new Partner();
    objP12.AccountFromId=acc.Id;
    objP12.AccountToId=acc1.Id;

    list<Partner> lstP=new list<Partner>();
    lstP.add(objP12);

    objCont.deletedId = objP6.Id;
    objCont.deletePartner();

    system.assertEquals(0, [SELECT COUNT() FROM Partner WHERE Id = :objP6.Id]);

    objCont.nextBtnClick();
    system.assertEquals(2, objCont.getPageNumber());

    objCont.previousBtnClick();
    system.assertEquals(1, objCont.getPageNumber());

    objCont.getPageSize();
    objCont.getPreviousButtonEnabled();
    objCont.getNextButtonDisabled();
    objCont.getTotalPageNumber();
    Integer newPageIndex = 1;
    objCont.BindData(newPageIndex);
    objCont.pageData(newPageIndex);
    objCont.LastpageData(newPageIndex);
    objCont.LastbtnClick();
    objCont.FirstbtnClick();

    objCont.sortExpression='Opportunity Account';
    objCont.displayPartnerData();

    objCont.sortExpression='Opportunity Account';
    objCont.displayPartnerData();

    objCont.sortExpression='Opportunity';
    objCont.displayPartnerData();

    objCont.sortExpression='Opportunity';
    objCont.displayPartnerData();

    objCont.sortExpression='Stage';
    objCont.displayPartnerData();

    objCont.sortExpression='Stage';
    objCont.displayPartnerData();

    objCont.sortExpression='StartDate';
    objCont.displayPartnerData();

    objCont.sortExpression='StartDate';
    objCont.displayPartnerData();

    objCont.sortExpression='EndDate';
    objCont.displayPartnerData();

    objCont.sortExpression='EndDate';
    objCont.displayPartnerData();

    objCont.sortExpression='Owner';
    objCont.displayPartnerData();

    objCont.sortExpression='Owner';
    objCont.displayPartnerData();

    objCont.sortExpression='Region';
    objCont.displayPartnerData();

    objCont.sortExpression='Region';
    objCont.displayPartnerData();

    objCont.sortExpression='BusinessUnit';
    objCont.displayPartnerData();

    objCont.sortExpression='BusinessUnit';
    objCont.displayPartnerData();

    objCont.sortExpression='Role';
    objCont.displayPartnerData();

    objCont.sortExpression='Role';
    objCont.displayPartnerData();

    objCont.sortExpression='Primary';
    objCont.displayPartnerData();

    objCont.sortExpression='Primary';
    objCont.displayPartnerData();

    objCont.sortExpression='Amount';
    objCont.displayPartnerData();

    objCont.sortExpression='Amount';
    objCont.displayPartnerData();

    objCont.sortExpression='';
    string st1r=objCont.getSortDirection();

    ApexPages.currentPage().getParameters().put('Id',null);
    ApexPages.StandardController sc1 = new ApexPages.StandardController(new Account());

    AccountPartnerController objCont1=new AccountPartnerController(sc1);

    Test.stopTest();

  }

}