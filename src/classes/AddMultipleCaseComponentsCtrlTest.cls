/**=====================================================================
 * Experian
 * Name: AddMultipleCaseComponentsCtrlTest
 * Description: Test class for AddMultipleCaseComponentsCtrl
 *
 * Created Date: Unknown
 * Created By: Nur Azlini Ammary
 *
 * Date Modified      Modified By                Description of the update
 *
 * Jan 5th, 2016      James Wills                Case #01266714: Changed reference to Constant to reflect change in name (see comment below).
 * Mar 8th, 2016      Sadar Yacob                Case #01825428: Added support for Agile Accelerator
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class AddMultipleCaseComponentsCtrlTest {

  //===========================================================================
  // PK Notes: Improve this test by adding assertions
  //===========================================================================
  static testMethod void DeploySuccess() {
    // createBasicRecords();
    Case c1 = [SELECT Id FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];

    PageReference pageRef = Page.AddMultipleCaseComponents;
    Test.setCurrentPage(pageRef);

    ApexPages.currentPage().getParameters().put('Id', c1.Id);

    Test.startTest();

    AddMultipleCaseComponentsCtrl controller = new AddMultipleCaseComponentsCtrl();
    List<AddMultipleCaseComponentsCtrl.ObjectWrapper> wrapper = controller.getMetadataComponentList();

    controller.searchCriteria = new AddMultipleCaseComponentsCtrl.SearchCriteria();
    //controller.searchCriteria.componentName = metaDataType;
    controller.search();

    wrapper = controller.getMetadataComponentList();
    wrapper.get(0).selected = true;
    wrapper.get(1).selected = true;
    controller.setMetadataComponentList(wrapper);

    system.assert(controller.componentTypeList.size() > 0);
    system.assert(controller.deploymentTypeList.size() > 0);
    system.assert(controller.actionList.size() > 0);
    system.assert(controller.configTypeList.size() > 0);
    
    controller.addComponent();
    controller.getPageSize();
    controller.getMetadataComponentListCount();
    controller.previous();
    controller.next();
    controller.first();
    controller.last();

    Test.stopTest();

  }

  static testMethod void SearchWithNoRecord() {

    Case c1 = [SELECT Id FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];

    PageReference pageRef = Page.AddMultipleCaseComponents;
    Test.setCurrentPage(pageRef);

    ApexPages.currentPage().getParameters().put('Id', c1.Id);

    Test.startTest();

    AddMultipleCaseComponentsCtrl controller = new AddMultipleCaseComponentsCtrl();
    controller.searchCriteria = new AddMultipleCaseComponentsCtrl.SearchCriteria();
    controller.searchCriteria.componentName = string.valueOf(DateTime.Now());
    controller.searchCriteria.componentType = string.valueOf(DateTime.Now());
    controller.search();
    controller.getMetadataComponentList();

    Test.stopTest();
    system.assertEquals(0, controller.getMetadataComponentListCount());

  }

  //===========================================================================
  // PK Notes: Improve this test by adding assertions
  //===========================================================================
  static testMethod void BackPage() {

    Case c1 = [SELECT Id FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];

    PageReference pageRef = Page.AddMultipleCaseComponents;
    Test.setCurrentPage(pageRef);

    AddMultipleCaseComponentsCtrl controller = new AddMultipleCaseComponentsCtrl();
    ApexPages.currentPage().getParameters().put('Id', c1.Id);

    Test.startTest();

    controller.back();

    Test.stopTest();

  }

  static testMethod void FailedWithNoItemSelected() {

    Case c1 = [SELECT Id FROM Case WHERE Subject = 'Test Case Subject' LIMIT 1];

    PageReference pageRef = Page.AddMultipleCaseComponents;
    Test.setCurrentPage(pageRef);

    ApexPages.currentPage().getParameters().put('Id', c1.Id);

    Test.startTest();

    AddMultipleCaseComponentsCtrl controller = new AddMultipleCaseComponentsCtrl();
    controller.getMetadataComponentList();
    controller.addComponent();

    Test.stopTest();
    
    Set<String> pageErrorDetails = new Set<String>();

    for(ApexPages.Message pgMsg : ApexPages.getMessages()) {
      pageErrorDetails.add(pgMsg.getDetail());
    }

    system.assert(pageErrorDetails.contains('Please select at least one component to be added.'),'Error not found.');

  }
  
 static testMethod void DeploySuccessForAgileStory() {
 
   agf__ADM_Work__c as1 = [SELECT Id, agf__Status__c FROM agf__ADM_Work__c WHERE agf__Subject__c  = 'Test Agile Story' LIMIT 1];

    PageReference pageRef = Page.AddMultipleCaseComponents;
    Test.setCurrentPage(pageRef);

    ApexPages.currentPage().getParameters().put('Id', as1.Id);

    Test.startTest();

    AddMultipleCaseComponentsCtrl controller = new AddMultipleCaseComponentsCtrl();
    List<AddMultipleCaseComponentsCtrl.ObjectWrapper> wrapper = controller.getMetadataComponentList();

    controller.searchCriteria = new AddMultipleCaseComponentsCtrl.SearchCriteria();
    //controller.searchCriteria.componentName = metaDataType;
    controller.search();

    wrapper = controller.getMetadataComponentList();
    wrapper.get(0).selected = true;
    wrapper.get(1).selected = true;
    controller.setMetadataComponentList(wrapper);

    controller.addComponent();
    controller.getPageSize();
    controller.getMetadataComponentListCount();
    controller.previous();
    controller.next();
    controller.first();
    controller.last();

    Test.stopTest();

  }  
  
  
  @testSetup
  private static void createBasicRecords(){
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN LIMIT 1];
    User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser1;
    system.runAs(testUser1) {
      IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(false);
    
      //create Account
      Account acc = Test_Utils.insertAccount();
      String crmRecordType = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.CASE_REC_TYPE_SFDC_SUPPORT); //Case #01266714 James Wills; Changed from Constants.CASE_REC_TYPE_CRM_REQUEST
    
      //Create Case
      Case RMACase = new Case();
      RMACase.RecordTypeId = crmRecordType;
      RMACase.Status = Constants.CASE_STATUS_IN_PROGRESS;
      RMACase.Implementation_Status__c = Constants.CASE_STATUS_IN_PROGRESS;
      RMACase.Type = Constants.CASE_TYPE_INCIDENT;
      RMACase.Account__c = acc.Id;
      RMACase.Subject = 'Test Case Subject';
      insert RMACase;
      
      //Create Agile Story
      crmRecordType = DescribeUtility.getRecordTypeIdByName('agf__ADM_Work__c', 'User Story'); 
      
     //Create Scrum Team
     agf__ADM_Scrum_Team__c newTeam  = new agf__ADM_Scrum_Team__c(Name = 'Test Team',agf__Active__c=true,agf__Cloud__c ='IT');
     insert newTeam;
    
     //Create Agile Product Tag
     agf__ADM_Product_Tag__c  newProductTag = new agf__ADM_Product_Tag__c(Name = 'GCSS Product Team',agf__Team__c =newTeam.Id,agf__Team_Tag_Key__c ='123', agf__Active__c=true );
    
     insert newProductTag;
    
     agf__ADM_Work__c agileStory = new agf__ADM_Work__c();
     agileStory.agf__Assignee__c =testUser1.Id;
     agileStory.agf__Subject__c = 'Test Agile Story';
     agileStory.agf__Status__c = 'New';
     agileStory.RecordTypeId = crmRecordType;
     agileStory.agf__Description__c = 'Agile Description goes here';
     agileStory.agf__Product_Tag__c =newProductTag.Id;
     insert agileStory;
    
      //create metadata components
      Metadata_Component__c metaDataCom1 = new Metadata_Component__c(
        Component_API_Name__c = 'mc1_is_apexclass',
        Component_Type__c = 'Apex Class'
      );
      
      Metadata_Component__c metaDataCom2 = new Metadata_Component__c(
        Component_API_Name__c = 'mc2_is_apexclass',
        Component_Type__c = 'Apex Class'
      );
      
      insert new Metadata_Component__c[]{metaDataCom1,metaDataCom2};
    }
  }

}