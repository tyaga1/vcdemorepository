/*======================================================================================
 * Experian Plc.
 * Name: BatchChatterGroupArchMaint_Test
 * Description: Test Class for BatchChatterGroupArchiveMaintenance
 * Created Date: May 31st, 2016
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By                Description of the update
 
 =======================================================================================*/

@isTest
private class BatchChatterGroupArchMaint_Test {
  
  static string testEmail = 'testEmailAddress098908@experian.com';
  
  static testMethod void testBatch() {
        
    User testUser = [SELECT Id FROM User WHERE Email = :testEmail];
    
    List<CollaborationGroup> listTestGroups = [SELECT Id, Name, LastFeedModifiedDate, LastModifiedDate, LastViewedDate, OwnerId, Owner.Email, Owner.Name, IsArchived FROM CollaborationGroup WHERE OwnerId = :testUser.Id];
    
    Test.startTest();
        
    BatchChatterGroupArchiveMaintenance.processGroups(listTestGroups);
    
    system.assert(Limits.getEmailInvocations() > 0);
    
    Test.stopTest();
    
  }
  
  
  static testMethod void testScheduler() {
    
    Test.startTest();
    
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleBatchChatterGroupArchMaint '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleBatchChatterGroupArchMaint());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    
    Test.stopTest();

    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  }
  
  @testSetup
  static void setupData() {
    
    Global_Settings__c gblSetting = new Global_Settings__c();
    gblSetting.name = Constants.GLOBAL_SETTING;
    gblSetting.chatterGrpActy_DaysAfter__c = 1;
    gblSetting.chatterGrpActy_DaysBefore__c = 0;
    gblSetting.chatterGrpActy_FromAddress__c = 'testEmailAddress098908@experian.com';
    insert gblSetting;
    
    User tstUser = Test_utils.createUser(Constants.PROFILE_EXP_SALES_ADMIN);
    tstUser.Email = testEmail;
    insert tstUser;
    
    CollaborationGroup testGroup = Test_Utils.createCollaborationGroup(tstUser.Id, false, 'Public');
    insert testGroup;
  }  
  
}