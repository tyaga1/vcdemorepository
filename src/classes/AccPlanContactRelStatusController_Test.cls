/**=====================================================================
 * Appirio, Inc
 * Name        : AccPlanContactRelStatusController_Test
 * Description : Handler class for AccPlanContactRelStatusController (for T-289161)
 * Created Date: Jul 8th, 2014
 * Created By  : Sonal Shrivastava (Appirio JDC)
 
 * Date Modified      Modified By                  Description of the update
 * Sep 8th, 2015      Paul Kissick                 I-179463: Duplicate Management Failures
 =====================================================================*/
@isTest
private class AccPlanContactRelStatusController_Test {

	static testMethod void myUnitTest() {
		Database.DMLOptions dml = new Database.DMLOptions();
    dml.DuplicateRuleHeader.allowSave = true;
    
		Account account = Test_Utils.insertAccount();
		
		Contact con1 = Test_Utils.insertContact(account.Id);
    Contact con2 = Test_Utils.createContact(account.Id);
    con2.ReportsToId = con1.Id;
    Database.insert(con2,dml);
    
    Contact con3 = Test_Utils.createContact(account.Id);
    con3.ReportsToId = con2.Id;
    Database.insert(con3,dml);
    
    Account_Plan__c accPlan = Test_Utils.insertAccountPlan(true, account.Id);
    
    
    Account_Plan_Contact__c apc1 = new Account_Plan_Contact__c(Account_Plan__c = accPlan.id, Contact__c = con1.Id, Experian_Relationship__c = 'Positive');
    Account_Plan_Contact__c apc2 = new Account_Plan_Contact__c(Account_Plan__c = accPlan.id, Contact__c = con2.Id, Experian_Relationship__c = 'Negative');
    Account_Plan_Contact__c apc3 = new Account_Plan_Contact__c(Account_Plan__c = accPlan.id, Contact__c = con3.Id, Experian_Relationship__c = 'Neutral');
    
    List<Account_Plan_Contact__c> lstAccPlanContact = new List<Account_Plan_Contact__c>();
    lstAccPlanContact.add(apc1);
    lstAccPlanContact.add(apc2);
    lstAccPlanContact.add(apc3);
    insert lstAccPlanContact;
    
    
    PageReference pageRef = Page.AccPlanContactRelStatusPage;
    pageRef.getParameters().put('id' , accPlan.id);
    Test.setCurrentPage(pageRef);
     
   // ApexPages.Pagereference pg = Page.AccPlanContactRelStatusPage;    
   // pg.getParameters().put('accPlanId', accPlan.id);
  //  Test.setCurrentPage(pg);
    
    Test.startTest();
    AccPlanContactRelStatusController controller = new AccPlanContactRelStatusController();
    System.assertEquals(3, controller.mapConName_NoOfCol.keySet().size());
    System.assertEquals(3, controller.mapLevel_LstContact.keySet().size());
	Test.stopTest();
	}
}