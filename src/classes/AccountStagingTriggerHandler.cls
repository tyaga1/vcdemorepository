/**=====================================================================
 * Experian
 * Name: AccountStagingTriggerHandler
 * Description: TriggerHandler for Account_Staging__c 
 *
 * The AccountStagingTriggerHandler methods will not be called when the user is an isDataAdmin()
 *
   After Update :                                 
   
 * Created Date: Dec. 15th 2016
 * Created By: James Wills for DRM Project
 * 
 * Date Modified                 Modified By                  Description of the update
 * 05/01/2017                    James Wills                  DRM:W-006453: Added updateAccounts() method.
 * 27/01/2017                    James Wills                  DRM:W-006453: No longer needed as these updates will be done manually by the DQ Team
 * 04/03/2017                    James Wills                  DRM:W-006452: Added checkForClientIDInCrossReferenceTable().
 * 06/03/2017                    James Wills                  DRM:W-007488: Added updateAccountStagingFieldsAfterDQProcessing() to update/clear Account Staging fields for DQ matching process.
 * 11/04/2017                    James Wills                  Case 02367544: Added new event to write to Transaction Queue table (Action Code 6->4).
 * 09/05/2017                    James Wills                  Case 02375814: Automate Tagging of Duplicate DQ Matching Account Staging Records.
=====================================================================*/

public class AccountStagingTriggerHandler{
   
  public static void beforeUpdate(List<Account_Staging__c> newList, Map<ID, Account_Staging__c> oldMap){

    updateAccountStagingFieldsAfterDQProcessing(newList, oldMap);
    checkForClientIDInCrossReferenceTableBeforeUpdate(newList, oldMap);
  }

  public static void afterUpdate(List<Account_Staging__c> newList, Map<ID, Account_Staging__c> oldMap){
    
    updateTransactionQueueOnActionCodeChange(newList, oldMap);   
    
    createNewSICrossReferenceRecord_List(newList, oldMap);
    
  }

  public static void beforeUpdateIsDataAdmin(List<Account_Staging__c> newList, Map<ID, Account_Staging__c> oldMap){

    updateAccountStagingFieldsAfterDQProcessing(newList, oldMap);
    
    checkForClientIDInCrossReferenceTableBeforeUpdate(newList, oldMap);

  }

  public static void afterUpdateIsDataAdmin(List<Account_Staging__c> newList, Map<ID, Account_Staging__c> oldMap){
    
    updateTransactionQueueOnActionCodeChange(newList, oldMap);
    
    createNewSICrossReferenceRecord_List(newList, oldMap);
    
  }


  public static void updateTransactionQueueOnActionCodeChange(List<Account_Staging__c> newList, Map<Id, Account_Staging__c> oldMap){
    
    Map<Account_Staging__c,String> newTransactionQueueRecords_Map = new Map<Account_Staging__c,String>();
    
    for(Account_Staging__c accStage : newList){
      Account_Staging__c accStageFromMap = oldMap.get(accStage.id);
      
      if(accStage.Action_Code__c == 4 && accStageFromMap.Action_Code__c == 1){
        newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_1_to_4);
        
      } else if(accStage.Action_Code__c == 4 && accStageFromMap.Action_Code__c == 5){
        newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_5_to_4);
        
      } else if(accStage.Action_Code__c == 4 && accStageFromMap.Action_Code__c == 3){
         newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_3_to_4);
         
      } else if(accStage.Action_Code__c == 3 && accStageFromMap.Action_Code__c == 2){
        if(accStage.Account_ID__c!=null){
          newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_2_to_3);
        } else {
          accStage.addError('You cannot set this record to DQ Processed because the Account_ID__c field is blank.');
        }        
      } else if(accStage.Action_Code__c == 6 && accStageFromMap.Action_Code__c == 2){
        newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_2_to_6);
        
        //Case 02367544
      } else if(accStage.Action_Code__c == 4 && accStageFromMap.Action_Code__c == 6){
        newTransactionQueueRecords_Map.put(accStage,Label.Action_Code_Changed_From_6_to_4);
        
      }
    }
  
    if(newTransactionQueueRecords_Map.isEmpty()){
      return;
    }
    
    createTransactionQueueEntries(newTransactionQueueRecords_Map);
    
  }



  public static void checkForClientIDInCrossReferenceTableBeforeUpdate(List<Account_Staging__c> newList, Map<Id, Account_Staging__c> oldMap){
    
    Map<ID, List<Account_Staging__c>> accountStageRecsToCheck_Map = new Map<ID, List<Account_Staging__c>>();
    Map<ID, Set<String>> accountStageRecsSIsToCheck_Map = new Map<ID, Set<String>>();//Used to check for duplicate SI Keys for Accounts in Account Stage before SI Cross Ref records are created.
    
    for(Account_Staging__c accStage : newList){
      Account_Staging__c accStageFromMap = oldMap.get(accStage.id);
      
      if(accStage.Action_Code__c == 3 && accStageFromMap.Action_Code__c == 2){
        if(accountStageRecsToCheck_Map.containsKey(accStage.Account_Id__c)==false){
          accountStageRecsToCheck_Map.put(accStage.Account_Id__c, new List<Account_Staging__c>{accStage});
          accountStageRecsSIsToCheck_Map.put(accStage.Account_Id__c, new Set<String>{accStage.SI_Key__c});
        } else {
          if(accountStageRecsSIsToCheck_Map.get(accStage.Account_Id__c).contains(accStage.SI_Key__c)==false){
            accountStageRecsToCheck_Map.get(accStage.Account_Id__c).add(accStage);
            accountStageRecsSIsToCheck_Map.get(accStage.Account_Id__c).add(accStage.SI_Key__c);
          } else if(accountStageRecsSIsToCheck_Map.get(accStage.Account_Id__c).contains(accStage.SI_Key__c)==true){
            accStage.DQ_Status__c = Label.SI_Key_Matching_Duplicate_Key_in_Payload;//Case 02375814
          }
        }
      }
    }
  
    if(accountStageRecsToCheck_Map.isEmpty()){
      return;
    } 
    
    Boolean siKeyPresent = false;
    Set<ID> accIDSet = accountStageRecsToCheck_Map.keySet();
    Map<ID, Account> account_Map = new Map<ID, Account>([SELECT Id, (SELECT Id, CLIENT_ID__c FROM SalesInsight_Cross_References__r) FROM Account WHERE Id IN :accIDSet]);    
    List<Account_Staging__c> accStageForNewSIrecs = new List<Account_Staging__c>();
    
    for(ID accID: accountStageRecsToCheck_Map.keySet()){
      for(Account_Staging__c accSt : accountStageRecsToCheck_Map.get(accID)){      
        if(account_Map.containsKey(accSt.Account_Id__c)){
          String siKeyToCheck = accSt.SI_Key__c;
          //Check to see if the record has already been created
          for(SalesInsight_Cross_Reference__c si : account_Map.get(accSt.Account_Id__c).SalesInsight_Cross_References__r){
            if(si.CLIENT_ID__c == siKeyToCheck){
              siKeyPresent=true;
              accSt.DQ_Status__c = Label.SI_Key_Matching_Duplicate_Key_In_Related_List;//Case 02375814
              break;
            }
          }
          if(siKeyPresent==false){
            accSt.DQ_Status__c = Label.SI_Key_Matching_Generate_Matched_Record;//Case 02375814
          }
          siKeyPresent=false;
        }
      }
    }

  }
  
  
  public static void createNewSICrossReferenceRecord_List(List<Account_Staging__c> newList, Map<ID, Account_Staging__c> oldMap){
    List<SalesInsight_Cross_Reference__c> sicrList = new List<SalesInsight_Cross_Reference__c>();
    List<Account_Staging__c> accStageForNewSIrecs = new List<Account_Staging__c>();
    
    for(Account_Staging__c accStage : newList){    
      Account_Staging__c accStageFromMap = oldMap.get(accStage.id);
      
      if((accStage.Action_Code__c == 3 && accStageFromMap.Action_Code__c == 2) && (accStage.DQ_Status__c == Label.SI_Key_Matching_Generate_Matched_Record)){
        accStageForNewSIRecs.add(accStage);
      }
    }            
    
    if(accStageForNewSIrecs.isEmpty()){
      return;
    } 
    
    
    for(Account_Staging__c accStage : accStageForNewSIrecs){
      SalesInsight_Cross_Reference__c siNew = new SalesInsight_Cross_Reference__c();
    
      siNew.Account__c      = accStage.Account_ID__c;
      siNew.CL_Address__c   = accStage.BillingStreet__c;
      siNew.CL_City__c      = accStage.BillingCity__c;
      siNew.CL_Country__c   = accStage.BillingCountry__c;
      siNew.CL_Name__c      = accStage.SI_Name__c;
      siNew.CL_Post__c      = accStage.BillingPostCode__c;
      siNew.CL_State__c     = accStage.BillingState__c;
      siNew.Client_ID__c    = accStage.SI_Key__c;
      siNew.SFDC_ID__c      = accStage.id;
      Datetime dtNow        = DateTime.Now();

      siNew.TX_Timestamp__c = String.valueOf(Datetime.Now());
      
      sicrList.add(siNew);
      
    }
    
    try{
      insert sicrList;    
     } catch(DMLException e){
      system.debug('AccountStagingTriggerHandler: createNewSICrossReferenceRecord_List Exception: ' + e.getMessage());
     }
    
  }

  public static void updateAccountStagingFieldsAfterDQProcessing(List<Account_Staging__c> newList, Map<Id, Account_Staging__c> oldMap){
    
    Map<ID, Account_Staging__c> accountStageRecsToUpdate_Map = new Map<ID, Account_Staging__c>();
    List<Account_Staging__c> accountStageRecsToReset_List = new List<Account_Staging__c>();
        
    List<ID> accIDList = new List<ID>();
    for(Account_Staging__c accStage : newList){
      Account_Staging__c accStageFromMap = oldMap.get(accStage.id);
      
      if(accStage.Account_ID__c!=null && (accStage.Account_ID__c != accStageFromMap.Account_ID__c)){
        accountStageRecsToUpdate_Map.put(accStage.Account_Id__c, accStage);
        accIDList.add(accStage.Account_ID__c);

      //The user has cleared the value of Account_ID__c, therefore clear the Account Staging values
      } else if(accStage.Account_ID__c==null && (accStageFromMap.Account_ID__c!=null)){
        accountStageRecsToReset_List.add(accStage);
      }
    }
  
    if(accountStageRecsToUpdate_Map.isEmpty()==false){
      updateAccountStagingFieldsWithAccountValues(accountStageRecsToUpdate_Map, accIDList);
    }
  
    if(accountStageRecsToReset_List.isEmpty()==false){
      resetAccountStagingFields(accountStageRecsToReset_List);
    }
  
  }
  
  
  public static void updateAccountStagingFieldsWithAccountValues(Map<ID, Account_Staging__c> accountStageRecsToCheck_Map, List<ID> accIDList){
  
    Map<ID, Account> matchedAccount_Map = new Map<ID, Account>([SELECT id, Name, ParentId, Account_Type__c, Additional_AKA__c, Additional_FKA__c, Phone,
                                        SI_Key__c, SI_Name__c, DQ_Status__c, Experian_Company__c, Partner_Type__c,
                                        Strategic_Parent__c, Ultimate_Parent_Account__c, VBM_Code__c, Global_Strategic_Client__c,
                                        APAC_Regional_Strategic_Client__c, EMEA_Regional_Strategic_Client__c, LATAM_Regional_Strategic_Client__c,
                                        NA_Regional_Strategic_Client__c, UK_I_Regional_Strategic_Client__c, Client_Tier__c,
                                        BillingStreet, BillingCity, BillingCountry, BillingPostalCode, BillingState, CreatedBy.Id, CreatedDate,
                                        LastModifiedBy.Id, LastModifiedDate
                                        FROM Account WHERE id IN :accIDList]);

    for(Account_Staging__c accStage : accountStageRecsToCheck_Map.values()){
      
      Account matchedAccount = matchedAccount_Map.get(accStage.Account_ID__c);
                
        accStage.Parent_Account_Id__c = matchedAccount.ParentId;
        accStage.Account_Type__c      = matchedAccount.Account_Type__c;
        accStage.Additional_AKA__c    = matchedAccount.Additional_AKA__c;
        accStage.Additional_FKA__c    = matchedAccount.Additional_FKA__c;
        accStage.Phone__c             = matchedAccount.Phone;
        accStage.Account_Name__c      = matchedAccount.Name;
        
        //accStage.SI_Key__c            = matchedAccount.SI_Key__c;
        //accStage.SI_Name__c           = matchedAccount.SI_Name__c;

        accStage.DQ_Status__c         = matchedAccount.DQ_Status__c;
        accStage.Experian_Company__c  = matchedAccount.Experian_Company__c == true ? 'true' : 'false';
        accStage.Partner_Type__c      = matchedAccount.Partner_Type__c;
        accStage.Strategic_Parent__c  = matchedAccount.Strategic_Parent__c == true  ? 'true' : 'false';
        accStage.Ultimate_Parent_Account__c          = matchedAccount.Ultimate_Parent_Account__c;
        accStage.VBM_Code__c                         = matchedAccount.VBM_Code__c;
        accStage.Global_Regional_Strategic_Client__c = matchedAccount.Global_Strategic_Client__c == true ? 'true' : 'false';      

        accStage.APAC_Regional_Strategic_Client__c  = matchedAccount.APAC_Regional_Strategic_Client__c  == true ? 'true' : 'false';
        accStage.EMEA_Regional_Strategic_Client__c  = matchedAccount.EMEA_Regional_Strategic_Client__c  == true ? 'true' : 'false';
        accStage.LATAM_Regional_Strategic_Client__c = matchedAccount.LATAM_Regional_Strategic_Client__c == true ? 'true' : 'false';
        accStage.NA_Regional_Strategic_Client__c    = matchedAccount.NA_Regional_Strategic_Client__c       == true ? 'true' : 'false';
        accStage.UK_I_Regional_Strategic_Client__c  = matchedAccount.UK_I_Regional_Strategic_Client__c   == true ? 'true' : 'false';       
      
        //Billing Details          
        accStage.BillingStreet__c   = matchedAccount.BillingStreet;
        accStage.BillingCity__c     = matchedAccount.BillingCity;
        accStage.BillingCountry__c  = matchedAccount.BillingCountry;
        accStage.BillingPostCode__c = matchedAccount.BillingPostalCode;
        accStage.BillingState__c    = matchedAccount.BillingState;
        
        accStage.Account_CreatedBy__c        = matchedAccount.CreatedBy.Id;
        accStage.Account_CreatedDate__c      = matchedAccount.CreatedDate;
        accStage.Account_LastModifiedBy__c   = matchedAccount.LastModifiedBy.Id;
        accStage.Account_LastModifiedDate__c = matchedAccount.LastModifiedDate;
        
        accStage.Client_Tier__c = matchedAccount.Client_Tier__c;
            
    
    }
  
  }

  public static void resetAccountStagingFields(List<Account_Staging__c> accountStageRecsToCheck_List){
    
    for(Account_Staging__c accStage : accountStageRecsToCheck_List){
        accStage.Parent_Account_Id__c = Null;
        accStage.Account_Type__c      = Null;
        accStage.Additional_AKA__c    = Null;
        accStage.Additional_FKA__c    = Null;
        accStage.Phone__c             = Null;
        accStage.Account_Name__c      = Null;
        
        accStage.SI_Key__c            = Null;
        accStage.SI_Name__c           = Null;

        accStage.DQ_Status__c         = Null;
        accStage.Experian_Company__c  = Null;
        accStage.Partner_Type__c      = Null;
        accStage.Strategic_Parent__c  = Null;
        accStage.Ultimate_Parent_Account__c = Null;
        accStage.VBM_Code__c          = Null;
        accStage.Global_Regional_Strategic_Client__c = Null;      

        accStage.APAC_Regional_Strategic_Client__c  = Null;
        accStage.EMEA_Regional_Strategic_Client__c  = Null;
        accStage.LATAM_Regional_Strategic_Client__c = Null;
        accStage.NA_Regional_Strategic_Client__c = Null;
        accStage.UK_I_Regional_Strategic_Client__c = Null;
      
        //Billing Details          
        accStage.BillingStreet__c = Null;
        accStage.BillingCity__c = Null;
        accStage.BillingCountry__c = Null;
        accStage.BillingPostCode__c = Null;
        accStage.BillingState__c = Null;
        
        accStage.Account_CreatedBy__c = Null;
        accStage.Account_CreatedDate__c = Null;
        accStage.Account_LastModifiedBy__c = Null;
        accStage.Account_LastModifiedDate__c = Null;
        accStage.Client_Tier__c = Null;
        
    }
    
  }


  public static void createTransactionQueueEntries(Map<Account_Staging__c,String> newTransactionQueueRecords_Map){
    List<Transaction_Queue__c> newTransactionQueue_List = new List<Transaction_Queue__c>();
    
    for(Account_Staging__c accStage : newTransactionQueueRecords_Map.keySet()){
      Transaction_Queue__c newTQRec = new Transaction_Queue__c();
       
      newTQRec.Action_Type__c             = 'Update'; 
      newTQRec.Completed_Time__c          = Datetime.Now();
      newTQRec.Operation_Type__c          = newTransactionQueueRecords_Map.get(accStage);
      newTQRec.SF_Record_ID__c            = accStage.id;
      newTQRec.Start_Time__c              = Datetime.Now();
      newTQRec.Target_Application__c      = 'Salesforce';
      newTQRec.Transaction_Object_Name__c = accStage.getsObjectType().getDescribe().getName();//The value returned is Account_Staging__c
      newTQRec.Transaction_Status__c      = 'Completed';
      newTQRec.Transaction_Type__c        = 'Inbound';
      
      //newTQRec.Message__c = ;
      //newTQRec.Duration__c = ;
      //newTQRec.External_Source_ID__c = ;
      //newTQRec.Time_Trigger_15__c = Datetime.Now();
       
      newTransactionQueue_List.add(newTQRec);
    }
    
    try{
      insert newTransactionQueue_List;
    } catch(DMLException e){
      system.debug('AccountStagingTriggerHandler: createTransactionQueueEntries Exception: ' + e.getMessage());
    }
    
  }

  
}