/**=====================================================================
 * Name: FeedCommentTriggerTest
 * Description: Trigger for FeedComment object                 
 * Created Date: Aug 25th, 2017
 * Created By: Mauricio Murillo
 * 
 * Date Modified                Modified By                  Description of the update
**=====================================================================*/
@isTest
public class FeedCommentTriggerHandler_Test {

    @isTest
    public static void testDeletePosts(){
    
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        Account acc = Test_Utils.insertAccount();
        User testUser2 = Test_Utils.createUser(Constants.PROFILE_EXP_SALES_MANAGER);
        insert testUser2;
        Id postId;
        Id postCommentId;
        Id postCommentId2;
        Id postCommentId3;
        Id postCommentId4;
        FeedItem post;
        FeedComment postComment;
        FeedComment postComment2;
        FeedComment postComment3;
        FeedComment postComment4;
        Test.startTest();
        
        system.runAs(testUser1) {
            //create and insert post
            post = new FeedItem();
            post.Body = 'HelloThere';
            post.ParentId = acc.Id;
            post.Title = 'FileName';
            insert post;            
            postId = post.Id;
            
            //create and insert comment
            postComment = new FeedComment();
            postComment.CommentBody = 'HelloThere';
            postComment.FeedItemId = postId;
            insert postComment;            
            postCommentId = postComment.Id;
            
            postComment2 = new FeedComment();
            postComment2.CommentBody = 'HelloThere';
            postComment2.FeedItemId = postId;
            insert postComment2;            
            postCommentId2 = postComment2.Id;            
             
            try{        
                delete postComment;
            }catch(Exception e){System.debug('Exception: ' + e);}
        }
        
        system.runAs(testUser2) {
            //create and insert post
            postComment3 = new FeedComment();
            postComment3.CommentBody = 'HelloThere';
            postComment3.FeedItemId = postId;
            insert postComment3;            
            postCommentId3 = postComment3.Id;

            postComment4 = new FeedComment();
            postComment4.CommentBody = 'HelloThere';
            postComment4.FeedItemId = postId;
            insert postComment4;            
            postCommentId4 = postComment4.Id;
            
            try{        
                delete postComment3;
                delete postComment2; //should not be able to
            }catch(Exception e){System.debug('Exception: ' + e);}
                        
        }   
        
        system.runAs(testUser1) {
            try{        
                delete postComment4; //should be able to
            }catch(Exception e){System.debug('Exception: ' + e);}
        }
        
        Test.stopTest();
        
        List<FeedComment> post1List = [Select id from FeedComment where id = :postCommentId];
        List<FeedComment> post2List = [Select id from FeedComment where id = :postCommentId2];
        List<FeedComment> post3List = [Select id from FeedComment where id = :postCommentId3];
        List<FeedComment> post4List = [Select id from FeedComment where id = :postCommentId4];
                                
        System.assertEquals(post1List.size(), 0);                   
        System.assertEquals(post2List.size(), 1);
        System.assertEquals(post3List.size(), 0);
        System.assertEquals(post4List.size(), 0);
        
    }
    
    
}