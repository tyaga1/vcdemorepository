/**=====================================================================
 * Experian
 * Name: BatchOpportunityLIScheduleRevenue (Was SumScheduleRevenueNextFY)
 * Description: The following batch class is designed to be scheduled to run once every night.
                This class will get all updated Opportunity line items schedules and fill up 
                the FY revenue for the current and Next FY
 * Created Date: 19 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 4, 2016                  Paul Kissick                 Case 01914500: Adding new field First_12_Months_Revenue_Impact__c
 * Apr 4, 2016                  Paul Kissick                 Case 01928992: Adding support to run on all opps
 =====================================================================*/
public class BatchOpportunityLIScheduleRevenue implements Database.Batchable<sObject>, Database.Stateful  {
  
  public Boolean runAllOpenOpps = false;  // runtime variable to run for all open opps
  
  public Boolean runAllOpps = false;  // runtime variable to run for all opps
  
  private Boolean newFYFound = false; // Holds whether we are running over an FY change
  
  private List<String> updateErrors; // Holds all update errors
  
  public Date closeDateStart; // runtime variable to find all opps between close dates
  public Date closeDateEnd;
  
  private Boolean customRun = false; // identifies this as a custom run batch (not timed)
  
  private Datetime startTime;  // Holds when the batch started to find all updated schedules after this time
  
  @testVisible private Date runDate; // Use to set a specific test date
  
  @testVisible private static Map<String,Decimal> revTotalsMapClean = new Map<String,Decimal>{
    'CFY_SR_April__c' => 0.0, 
    'CFY_SR_May__c' => 0.0, 
    'CFY_SR_June__c' => 0.0, 
    'CFY_SR_July__c' => 0.0, 
    'CFY_SR_August__c' => 0.0, 
    'CFY_SR_September__c' => 0.0, 
    'CFY_SR_October__c' => 0.0, 
    'CFY_SR_November__c' => 0.0, 
    'CFY_SR_December__c' => 0.0, 
    'CFY_SR_January__c' => 0.0, 
    'CFY_SR_February__c' => 0.0, 
    'CFY_SR_March__c' => 0.0,
    'NFY_SR_April__c' => 0.0, 
    'NFY_SR_May__c' => 0.0, 
    'NFY_SR_June__c' => 0.0, 
    'NFY_SR_July__c' => 0.0, 
    'NFY_SR_August__c' => 0.0, 
    'NFY_SR_September__c' => 0.0, 
    'NFY_SR_October__c' => 0.0, 
    'NFY_SR_November__c' => 0.0, 
    'NFY_SR_December__c' => 0.0, 
    'NFY_SR_January__c' => 0.0, 
    'NFY_SR_February__c' => 0.0, 
    'NFY_SR_March__c' => 0.0
  };
  
  
  public Database.QueryLocator start (Database.BatchableContext bc) {
    updateErrors = new List<String>();
    startTime = system.now();
    
    if (runDate == null) {
      runDate = Date.today();
    }
    
    Datetime lastRunTime = BatchHelper.getBatchClassTimestamp('BatchOpportunityLIScheduleRevenue');
    if (lastRunTime == null) {
      lastRunTime = Datetime.now().addDays(-7);
    }
    
    if (runAllOpps) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM OpportunityLineItem
      ]);
    }
    
    if (runAllOpenOpps) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM OpportunityLineItem
        WHERE Opportunity.IsClosed = false
      ]);
    }
    if (closeDateStart != null && closeDateEnd != null) {
      customRun = true; // Used to identify this as a custom run (not timed)
      return Database.getQueryLocator([
        SELECT Id
        FROM OpportunityLineItem
        WHERE Opportunity.CloseDate >= :closeDateStart 
        AND Opportunity.CloseDate <= :closeDateEnd
      ]);
    }
    return Database.getQueryLocator([
      SELECT Id
      FROM OpportunityLineItem 
      WHERE Id IN (
        SELECT OpportunityLineItemId
        FROM OpportunityLineItemSchedule WHERE SystemModStamp >= :lastRunTime
      )
    ]);
  }
  
  // Breaking this into a separate method to call from other places...
  public static OpportunityLineItem processLineItem(OpportunityLineItem oli, 
                                                    List<OpportunityLineItemSchedule> oliSched,
                                                    Date cfyStart, Date cfyEnd, Date nfyStart, Date nfyEnd) {
    Decimal totalRevCFY = 0.0;
    Decimal totalRevNFY = 0.0;
    Decimal first12Months = 0.0;
    
    Date oliStart;
    Date oli1Year;
    
    Map<String,Decimal> revTotalsMap = new Map<String,Decimal>(revTotalsMapClean);
      
    if (oliSched != null && oliSched.size() > 0) {
	    for (OpportunityLineItemSchedule olis : oliSched) {
	      if (oliStart == null) {
	        oliStart = olis.ScheduleDate;
	        oli1Year = oliStart.addYears(1);
	      }
	      Date sDate = olis.ScheduleDate;
	      
	      Decimal revVal = (olis.Revenue != null) ? olis.Revenue : 0.0;
	      
	      //Case 01914500 : Adding support for first 12 months 
	      if (sDate >= oliStart && sDate < oli1Year) {
	        first12Months += revVal;
	      }
	      
	      String fieldName = '';
	      if (sDate >= cfyStart && sDate <= cfyEnd) {
	        // current fy
	        totalRevCFY += revVal;
	        fieldName = monthToFieldNameCurr.get(sDate.month());
	      }
	      if (sDate >= nfyStart && sDate <= nfyEnd) {
	        // next fy
	        totalRevNFY += revVal;
	        fieldName = monthToFieldNameNext.get(sDate.month());
	      }
	      if (String.isNotBlank(fieldName)) {
	        revTotalsMap.put(fieldName,revTotalsMap.get(fieldName)+revVal);
	      }
	    }
	  }
	  // Case 01806431: This must run, even if there are no records!
	  oli.Current_FY_revenue_impact__c = totalRevCFY;
	  oli.Next_FY_revenue_impact__c = totalRevNFY;
	  oli.First_12_Months_Revenue_Impact__c = first12Months;
	  
	  for(String fieldName : revTotalsMap.keySet()) {
	    oli.put(fieldName, revTotalsMap.get(fieldName));
	  }
    return oli;
  }
  
  static Map<Integer,String> monthToFieldNameCurr = new Map<Integer,String>{
    1 => 'CFY_SR_January__c',
    2 => 'CFY_SR_February__c',
    3 => 'CFY_SR_March__c',
    4 => 'CFY_SR_April__c',
    5 => 'CFY_SR_May__c',
    6 => 'CFY_SR_June__c',
    7 => 'CFY_SR_July__c',
    8 => 'CFY_SR_August__c',
    9 => 'CFY_SR_September__c',
    10 => 'CFY_SR_October__c',
    11 => 'CFY_SR_November__c',
    12 => 'CFY_SR_December__c'
  };

  static Map<Integer,String> monthToFieldNameNext = new Map<Integer,String>{
    1 => 'NFY_SR_January__c',
    2 => 'NFY_SR_February__c',
    3 => 'NFY_SR_March__c',
    4 => 'NFY_SR_April__c',
    5 => 'NFY_SR_May__c',
    6 => 'NFY_SR_June__c',
    7 => 'NFY_SR_July__c',
    8 => 'NFY_SR_August__c',
    9 => 'NFY_SR_September__c',
    10 => 'NFY_SR_October__c',
    11 => 'NFY_SR_November__c',
    12 => 'NFY_SR_December__c'
  };
  
  public void execute (Database.BatchableContext BC, List<OpportunityLineItem> scope) {
    Integer fiscalYearStartMonth = [
      SELECT FiscalYearStartMonth 
      FROM Organization 
      WHERE Id = :Userinfo.getOrganizationId()
    ].FiscalYearStartMonth;
    
    Date cfyStart;    
    if (runDate.month() < fiscalYearStartMonth) {
      cfyStart = Date.newInstance(runDate.year()-1, fiscalYearStartMonth, 1);
    }
    else {
      cfyStart = Date.newInstance(runDate.year(), fiscalYearStartMonth, 1);
    }
    
    if (runDate == cfyStart && !runAllOpps) {
      newFYFound = true;
    }
    
    Date cfyEnd = cfyStart.addYears(1).addDays(-1);
    Date nfyStart = cfyEnd.addDays(1);
    Date nfyEnd = nfyStart.addYears(1).addDays(-1);
    
    system.debug('CFY Start : '+cfyStart+', CFY End : '+cfyEnd+', NFY Start : '+nfyStart+', NFY End : '+nfyEnd);
    
    List<OpportunityLineItem> oppLineItemsList = [
      SELECT Id, SystemModStamp, 
             Current_FY_revenue_impact__c, 
             CFY_SR_April__c, CFY_SR_May__c, CFY_SR_June__c,
             CFY_SR_July__c, CFY_SR_August__c, CFY_SR_September__c,
             CFY_SR_October__c, CFY_SR_November__c, CFY_SR_December__c,
             CFY_SR_January__c, CFY_SR_February__c, CFY_SR_March__c,
             Next_FY_revenue_impact__c,
             NFY_SR_April__c, NFY_SR_May__c, NFY_SR_June__c,
             NFY_SR_July__c, NFY_SR_August__c, NFY_SR_September__c,
             NFY_SR_October__c, NFY_SR_November__c, NFY_SR_December__c,
             NFY_SR_January__c, NFY_SR_February__c, NFY_SR_March__c,
             First_12_Months_Revenue_Impact__c,
        (
         SELECT Id, Revenue, ScheduleDate, OpportunityLineItemId
         FROM OpportunityLineItemSchedules
         ORDER BY ScheduleDate ASC
//         WHERE ScheduleDate = NEXT_FISCAL_YEAR OR ScheduleDate = THIS_FISCAL_YEAR
         ) 
      FROM OpportunityLineItem 
      WHERE Id IN :scope
    ];

    for(OpportunityLineItem oli : oppLineItemsList) {
      oli = processLineItem(oli, oli.OpportunityLineItemSchedules, cfyStart, cfyEnd, nfyStart, nfyEnd);
    }
    
    List<Database.SaveResult> updateResList = Database.update(oppLineItemsList,false);
    for(Database.SaveResult sr : updateResList) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
  }
  
  public void finish (Database.BatchableContext BC) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchOpportunityLIScheduleRevenue', false);
    
    if (!customRun) {
      // Only set this if not in a custom run
      BatchHelper.setBatchClassTimestamp('BatchOpportunityLIScheduleRevenue', startTime);
    }
    
    String emailBody = '';
    if (updateErrors != null && updateErrors.size() > 0) {
      bh.batchHasErrors = true;
      emailBody += String.join(updateErrors,'\n');
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
    
    
    // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF NEXT FY
    BatchOpportunityRevenue b = new BatchOpportunityRevenue();
    if (runAllOpenOpps) {
      b.runAllOpenOpps = runAllOpenOpps;
    }
    if (runAllOpps) {
      b.runAllOpps = runAllOpps;
    }
    if (closeDateStart != null && closeDateEnd != null) {
      b.closeDateStart = closeDateStart;
      b.closeDateEnd = closeDateEnd;
    }
    if (newFYFound) {
      // prepare to schedule and run for all opps
      BatchOpportunityLIScheduleRevenue b2 = new BatchOpportunityLIScheduleRevenue();
      b2.runAllOpps = true;
      if (!Test.isRunningTest()) {
        system.scheduleBatch(b2, 'BatchOpportunityLIScheduleRevenue'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchOpportunityLIScheduleRevenue'));
      }
    }
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      system.scheduleBatch(b, 'BatchOpportunityRevenue'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchOpportunityRevenue'));
    }
  }
  
  
}