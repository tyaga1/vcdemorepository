/**=====================================================================
 * Appirio, Inc
 * Name: AccountAssignmentTeamTriggerHandler
 * Description: Handler class for the Account_Assignment_Team__c object
 * Created Date: Jan 21st, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 21st, 2015               Arpita Bose                  Created. T-354803: Logic for after insert to create the AccountTeamMembers
 * Jan 21st, 2015               Noopur                       Added logic for After Delete to delete the AccountTeamMembers
 * Feb 3rd, 2015                Nathalie Le Guay             T-358842: Added Assignment_Team__c.Account_Executive__c to the list of ATM to remove (on delete)
 * Apr 3rd, 2015                Nathalie Le Guay             I-154256: Fix deleteRelatedAccountTeamMembers() so that Account Executive 
 *                                                           will be removed from the Account Team if they were part of an Assignment Team
 *                                                           with no Team member
 * Jul 29th, 2015               Paul Kissick                 - Removing with sharing.
 * Aug 17th, 2015               Paul Kissick                 Case #01098427 - Fix to find missed assignments
 * Jan 11th, 2016               Paul Kissick                 Case #01084405 - Swap order of share/team insertion to Share->Team
 * Mar 03rd, 2016               Sadar Yacob                  Case #:01843114 - Set Opty Access to Read instead of Edit
 * Jun 15th, 2016               Paul Kissick                 Case #02024883: Fixing AccountShare problems (v37.0)
 * May 10th, 2017               James Wills                  InsideSales:W-007994 - Created method updateAssingmentTeamMemberRoleFieldsOnAccount()
 * May 25th, 2017               James Wills                  Case: 02426211: Updated so that changes to Role would be refected on Account fields for BIS & CIS AEs.
 =====================================================================*/
public class AccountAssignmentTeamTriggerHandler {
  
  public static void beforeInsert(List<Account_Assignment_Team__c> newList) {
    setAssignmentTriggered(newList);
  }
  
  //=================================================
  // After Insert Call
  //=================================================
  public static void afterInsert (List<Account_Assignment_Team__c> newList, Map<ID, Account_Assignment_Team__c> newMap) {
    addAccAssigTeamMemberToAccTeam(newList);
    
    updateAssignmentTeamMemberRoleFieldsOnAccount(newMap, 'insert');
  }

  //=================================================
  // After Update Call
  //=================================================
  public static void afterUpdate (Map<ID, Account_Assignment_Team__c> newMap) {
    
    updateAssignmentTeamMemberRoleFieldsOnAccount(newMap, 'update');
  }


  //=================================================
  // After Delete Call
  //=================================================
  public static void afterDelete (Map<Id,Account_Assignment_Team__c> oldMap) {
    deleteRelatedAccountTeamMembers(oldMap);
    
    updateAssignmentTeamMemberRoleFieldsOnAccount(oldMap, 'delete');
  }
  

  public static void setAssignmentTriggered(List<Account_Assignment_Team__c> newList) {
    for (Account_Assignment_Team__c aat : newList) {
      aat.Assignment_Triggered__c = true;
    }
  }

  //===============================================================================================
  // Create the AccountTeamMembers when a junction record (Account_Assignment_Team__c) is created
  //===============================================================================================
  public static void addAccAssigTeamMemberToAccTeam(List<Account_Assignment_Team__c> newAccAssigTeam) {
    Set<Id> assignmentTeamIds = new Set<Id>(); // To help with query
    Map<Id, Assignment_Team__c> assignTeamMap = new Map<Id, Assignment_Team__c>(); // to store the result of the query
    List<AccountTeamMember> lstATMsToInsert = new List<AccountTeamMember>(); // New records to insert
    
    // List<AccountShare> lstAccShare = new List<AccountShare>(); // New records to insert

    // ----------
    // Gather Ids
    //-----------
    for (Account_Assignment_Team__c accAssgnmentTeam : newAccAssigTeam) {
      assignmentTeamIds.add(accAssgnmentTeam.Assignment_Team__c);
    }

    // ------------------------------
    // DB Queries and records storing
    // Ordering Members by
    //-------------------------------
    assignTeamMap = new Map<Id, Assignment_Team__c>([
      SELECT Id, Account_Executive__c,
       (SELECT Id, User__c, Assignment_Team__c, Assignment_Team_Role__c, Assignment_Team__r.Id, User__r.IsActive
        FROM Assignment_Team_Members__r
        WHERE IsActive__c = true)
      FROM Assignment_Team__c 
      WHERE Id IN :assignmentTeamIds
    ]);

    // ---------------------------
    // Creating the access records
    //----------------------------
    Assignment_Team__c team;
    for (Account_Assignment_Team__c aat: newAccAssigTeam) {
      if (!assignTeamMap.containsKey(aat.Assignment_Team__c)) {
        continue;
      }

      // Retrieve the Team header record 
      team = assignTeamMap.get(aat.Assignment_Team__c);

      if (team.Account_Executive__c != null) {
        // Create access (AccountTeamMember and AccountShare) for the Account Executive, specified on the Assignment Team record
        lstATMsToInsert.add(createAccountTeam(aat.Account__c, team.Account_Executive__c, Constants.TEAM_ROLE_ACCOUNT_MANAGER));
        // lstAccShare.add(createAccountShare(aat.Account__c, team.Account_Executive__c)); // 02024883 - No longer required
      }

      // Create access (AccountTeamMember and AccountShare) for each of the members of the Assignment_Team__c
      for (Assignment_Team_Member__c member : team.Assignment_Team_Members__r) {
        // Adding check for an active user account
        if (member.User__r.IsActive) {
          lstATMsToInsert.add(createAccountTeam(aat.Account__c, member.User__c, member.Assignment_Team_Role__c));
          // lstAccShare.add(createAccountShare(aat.Account__c, member.User__c)); // 02024883 - No longer required
        }
      }
    }

    // ---------------------------
    // Inserting records in the DB
    //----------------------------
    if (!lstATMsToInsert.isEmpty()) {
      try {
        // Case 01084405: Switched these to Share then Team
        // insert lstAccShare;
        insert lstATMsToInsert;
      } 
      catch (DMLException ex) {
        ApexLogHandler.createLogAndSave('AccountAssignmentTeamTriggerHandler','addAccAssigTeamMemberToAccTeam', ex.getStackTraceString(), ex);
        for (Integer i = 0; i < ex.getNumDml(); i++) {
          newAccAssigTeam.get(0).addError(ex.getDmlMessage(i)); 
        }
      }
    }
  }

  //=================================================
  // Instantiates AccountTeamMember given details
  //=================================================
  private static AccountTeamMember createAccountTeam(String accId, String userId, String memRole) {
    return new AccountTeamMember(
      AccountId = accId,
      UserId = userId,
      TeamMemberRole = memRole,
      AccountAccessLevel = Constants.ACCESS_LEVEL_EDIT, // Case 02024883 - Moving from accountshare (v37.0)
      OpportunityAccessLevel = Constants.ACCESS_LEVEL_READ, 
      CaseAccessLevel = Constants.ACCESS_LEVEL_NONE
    );
  }

  //=================================================
  // Instantiates AccountShare given details
  //=================================================
  /*
  // 02024883 No longer required
  private static AccountShare createAccountShare(String accId, String userId) {
  
    system.Debug('Creating AccountShare for Accnt ID: '+ accId + ' ; Accnt Access:Edit; Opty Access:is Read ;' +' UserID:' +userId ); 
  
    return new AccountShare(
      AccountAccessLevel = Constants.ACCESS_LEVEL_EDIT,
      AccountId = accId,
      OpportunityAccessLevel = Constants.ACCESS_LEVEL_READ, //03/03/16 :Case #:01843114  Constants.ACCESS_LEVEL_EDIT,
      CaseAccessLevel = Constants.ACCESS_LEVEL_NONE,
      UserOrGroupId = userId
    );
  }
  */

  //===============================================================================================
  // Delete the AccountTeamMembers when a junction record (Account_Assignment_Team__c) is deleted
  // Will not delete if the User is getting access through another Account_Assignment_Team__c record
  //===============================================================================================
  private static void deleteRelatedAccountTeamMembers(Map<Id, Account_Assignment_Team__c> oldMap) {
    List<AccountTeamMember> atmToDelete = new List<AccountTeamMember>();
    Map<Id, Set<Id>> existingAATMap = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> existingAssignmentTeamIds = new Map<Id, Set<Id>>();
    
    Set<Id> accountIds = new Set<Id>();
    Set<Id> assignmentTeamIds = new Set<Id>();
    set<Id> userIds = new Set<Id>();
    
    //fetch the account Ids and the Assignment Teams
    for (Account_Assignment_Team__c AAT : oldMap.values()) {
      accountIds.add(AAT.Account__c);
      assignmentTeamIds.add(AAT.Assignment_Team__c); 
    }
    
    for (Account_Assignment_Team__c AAT : [SELECT Id,Assignment_Team__c,Account__c ,Assignment_Team__r.Account_Executive__c
                                           FROM Account_Assignment_Team__c 
                                           WHERE Id NOT IN :oldMap.values() 
                                           AND Account__c IN :accountIds]) {
      if (!existingAssignmentTeamIds.containsKey(AAT.Assignment_Team__c)) {
        existingAssignmentTeamIds.put(AAT.Assignment_Team__c,new set<Id>{AAT.Account__c});
      }
      else {
        existingAssignmentTeamIds.get(AAT.Assignment_Team__c).add(AAT.Account__c);
      }
    }

    // fetch the assignment team members 
    for (Assignment_Team__c assgnTeam: [SELECT Id, Account_Executive__c, 
                                               (SELECT Id, User__c, Assignment_Team__c, Assignment_Team_Role__c,
                                                       Assignment_Team__r.Account_Executive__c, Assignment_Team__r.Id 
                                                  FROM Assignment_Team_Members__r)
                                        FROM Assignment_Team__c
                                        WHERE (Id IN :assignmentTeamIds 
                                           OR Id IN :existingAssignmentTeamIds.keySet())]) {
      if (assgnTeam.Account_Executive__c != null) {
        userIds.add(assgnTeam.Account_Executive__c);
      }
      for (Assignment_Team_Member__c assgnTeamMembr : assgnTeam.Assignment_Team_Members__r) {
        // gather the users for the assignment team in the deleted records
        if (assignmentTeamIds.contains(assgnTeamMembr.Assignment_Team__c)) {
          userIds.add(assgnTeamMembr.User__c);
        }
        else {
          // fetch the existing assignment team members related to the account gathered above
          // this is to check if there are any other junction record for the account and the user
          for (Id accId : existingAssignmentTeamIds.get(assgnTeamMembr.Assignment_Team__c)) {
            if (!existingAATMap.containsKey(accId)) {
              existingAATMap.put(accId, new set<Id>());
            }
            existingAATMap.get(accId).add(assgnTeamMembr.User__c);
            if (assgnTeamMembr.Assignment_Team__r.Account_Executive__c != null) {
              existingAATMap.get(accId).add(assgnTeamMembr.Assignment_Team__r.Account_Executive__c);
            }
          }
        }
      }
    }
    // fetch the account team members for the gathered userIds and accountIds
    for (AccountTeamMember atm : [SELECT Id, AccountId, UserId, User.FirstName
                                  FROM AccountTeamMember 
                                  WHERE UserId IN :userIds 
                                  AND AccountId IN :accountIds]) {
      // Check if there is no other junction record for the user and account
      // If not then add to delete list
      if (!existingAATMap.containsKey(atm.AccountId)) {
        atmToDelete.add(atm);
      }
      else if (existingAATMap.containsKey(atm.AccountId) && !existingAATMap.get(atm.AccountId).contains(atm.UserId)) {
        atmToDelete.add(atm);
      }
      system.debug('\nUser is: '+ atm.UserId + ' '+ atm.User.FirstName + ' \nand map has: ' + existingAATMap.get(atm.AccountId));
    }
    
    // NLG - Mar 14th, 2015
    // fetch the Confidential_Information__Share records to be deleted.
    List<Confidential_Information__Share> confidentialInfoShares = new List<Confidential_Information__Share>();
    for (Confidential_Information__Share CI_Share : [SELECT c.UserOrGroupId, c.RowCause, c.Parent.Account__c, c.ParentId 
                                                     FROM Confidential_Information__Share c
                                                     WHERE Parent.Account__c IN :accountIds 
                                                     AND UserOrGroupId IN :userIds
                                                     AND RowCause = :Constants.ROWCAUSE_ACCOUNT_TEAM]) {
      confidentialInfoShares.add(CI_Share);
    }
        
    if (!confidentialInfoShares.isEmpty() ) {
      delete confidentialInfoShares;
    }
    // END of NLG
    
    //delete the account team member records.
    if (!atmToDelete.isEmpty()) {
      try {
        delete atmToDelete;
      }
      catch (DMLException ex) {
        ApexLogHandler.createLogAndSave('AccountAssignmentTeamTriggerHandler','deleteRelatedAccountTeamMembers', ex.getStackTraceString(), ex);
        for (Integer i = 0; i < ex.getNumDml(); i++) {
          atmToDelete.get(0).addError(ex.getDmlMessage(i)); 
        }
      }
    }
  }
  
  //W-007994 
  public static void updateAssignmentTeamMemberRoleFieldsOnAccount(Map<ID, Account_Assignment_Team__c> atmMap, String mode){
  
    Map<ID,ID> assignmentTeamMap_BIS = new Map<ID,ID>();
    Map<ID,ID> assignmentTeamMap_CIS = new Map<ID,ID>();
    Map<ID,String> accountMap_BIS = new Map<ID,String>();
    Map<ID,String> accountMap_CIS = new Map<ID,String>();
    Map<ID,String> accountMapForBISAccountExecutives = new Map<ID,String>();
    Map<ID,String> accountMapForCISAccountExecutives = new Map<ID,String>();
    
    List<ID> aatIDList = new List<ID>();
    for(Account_Assignment_Team__c aat : atmMap.values()){
      aatIDList.add(aat.Assignment_Team__c);      
    }
        
    //Only run this method for changes to the Assignment_Team_Member__c if they are the Account Executive
    List<Assignment_Team__c> atList = [SELECT id, Name, Account_Executive__c
                                      FROM Assignment_Team__c 
                                      WHERE id IN :aatIDList
                                      AND ((Name LIKE 'BIS - Growth%' OR Name LIKE 'BIS - Inside%') OR
                                           (Name LIKE 'CIS - Growth%' OR Name LIKE 'CIS - Inside%'))];        
    if(atList.isEmpty()){
      return;
    }

    for(Assignment_Team__c at : atList){
      if(at.Name.LEFT(3)=='BIS'){
        assignmentTeamMap_BIS.put(at.id, at.Account_Executive__c);
      } else if(at.Name.LEFT(3)=='CIS'){
        assignmentTeamMap_CIS.put(at.id, at.Account_Executive__c);
      }
    }

    Map<ID, User> accountExecutiveList = new Map<ID,User>([SELECT id, UserRole.Name FROM User WHERE id IN :assignmentTeamMap_BIS.values() OR id IN :assignmentTeamMap_CIS.values()]);

    for(Account_Assignment_Team__c aat : atmMap.values()){
      for(User u : accountExecutiveList.values()){
        if(u.id==assignmentTeamMap_BIS.get(aat.Assignment_Team__c)){
           accountMap_BIS.put(aat.Account__c, u.UserRole.Name);
           accountMapForBISAccountExecutives.put(aat.Account__c, u.id);
           break;                 
        } else if(u.id==assignmentTeamMap_CIS.get(aat.Assignment_Team__c)){
           accountMap_CIS.put(aat.Account__c, u.UserRole.Name); 
           accountMapForCISAccountExecutives.put(aat.Account__c, u.id);
           break;
        }
      }
    }


    if(accountMap_BIS.isEmpty()==false){
      List<Account> accList1 = [SELECT id, AE_Assignment_Team_Role_BIS__c FROM Account WHERE id IN :accountMap_BIS.keySet()];
      for(Account acc : accList1){
        if(mode!='delete'){
          acc.AE_Assignment_Team_Role_BIS__c = accountMap_BIS.get(acc.id);
          acc.BIS_Account_Executive__c = accountMapForBISAccountExecutives.get(acc.id);
        } else {
          acc.AE_Assignment_Team_Role_BIS__c = '##Account Assignment Team Record Deleted## ' + datetime.now();
          acc.BIS_Account_Executive__c =  null;
        }
      }
        
      try{
        update accList1;
      } catch(Exception e){
        system.debug('\n[AssignmentTeamMemberTriggerHandler: updateAssignmentTeamMemberRoleFieldsOnAccount]: ['+e.getMessage()+']]');
        apexLogHandler.createLogAndSave('AssignmentTeamMemberTriggerHandler','updateAssignmentTeamMemberRoleFieldsOnAccount', e.getStackTraceString(), e);
      } 
    }

    if(accountMap_CIS.isEmpty()==false){
      List<Account> accList2 = [SELECT id, AE_Assignment_Team_Role_CIS__c FROM Account WHERE id IN :accountMap_CIS.keySet()];
      for(Account acc : accList2){
        if(mode!='delete'){
          acc.AE_Assignment_Team_Role_CIS__c = accountMap_CIS.get(acc.id);
          acc.CIS_Account_Executive__c = accountMapForCISAccountExecutives.get(acc.id);
        } else {
          acc.AE_Assignment_Team_Role_CIS__c = '##Account Assignment Team Record Deleted## ' + datetime.now();
          acc.CIS_Account_Executive__c = null;
        }  
      }
      
      try{
        update accList2;
      } catch(Exception e){
        system.debug('\n[AssignmentTeamMemberTriggerHandler: updateAssignmentTeamMemberRoleFieldsOnAccount]: ['+e.getMessage()+']]');
        apexLogHandler.createLogAndSave('AssignmentTeamMemberTriggerHandler','updateAssignmentTeamMemberRoleFieldsOnAccount', e.getStackTraceString(), e);
      } 
    }
    


  }
  
  
}