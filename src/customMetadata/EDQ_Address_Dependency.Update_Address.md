<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update Address</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Address_Field__c</field>
        <value xsi:type="xsd:string">Address__c</value>
    </values>
    <values>
        <field>SObject__c</field>
        <value xsi:type="xsd:string">Update__c</value>
    </values>
    <values>
        <field>Where_AND_1__c</field>
        <value xsi:type="xsd:string">Address__c != null AND Address__r.EDQ_Dependency__c = false</value>
    </values>
</CustomMetadata>
