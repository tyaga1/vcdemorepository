<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Order Billing Address</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Address_Field__c</field>
        <value xsi:type="xsd:string">Billing_Address__c</value>
    </values>
    <values>
        <field>SObject__c</field>
        <value xsi:type="xsd:string">Order__c</value>
    </values>
    <values>
        <field>Where_AND_1__c</field>
        <value xsi:type="xsd:string">(SaaS_Order__c = true OR Has_EDQ_On_Demand_Products__c = true OR Count_of_EDQ_Products__c &gt; 0) AND Billing_Address__c != null AND Billing_Address__r.EDQ_Dependency__c = false</value>
    </values>
</CustomMetadata>
