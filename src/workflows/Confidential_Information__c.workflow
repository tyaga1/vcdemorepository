<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_has_been_Attached</fullName>
        <description>Contract has been Attached</description>
        <protected>false</protected>
        <recipients>
            <field>Contract_Requestor__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Assigned_For_Stage_3_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Business_Unit_from_the_User</fullName>
        <field>Business_Unit__c</field>
        <formula>Text(Owner:User.Business_Unit__c)</formula>
        <name>Populate Business Unit  from the User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Requested_By</fullName>
        <field>Contract_Requestor__c</field>
        <formula>Contract__r.Agreement_Request__r.Requestor_Email__c</formula>
        <name>Update Contract Requested By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contract Attached Confirmation</fullName>
        <actions>
            <name>Contract_has_been_Attached</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( Contract__r.Agreement_Request__r.RecordType.Name = &apos;CSDA CIS Support&apos;,   Attachment_Count__c &gt;0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Business Unit  from the User</fullName>
        <actions>
            <name>Populate_Business_Unit_from_the_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   NOT(     ISBLANK(       Text(Owner:User.Business_Unit__c)     )   ),   NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Contract Requested By</fullName>
        <actions>
            <name>Update_Contract_Requested_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Confidential_Information__c.Contract_Requestor__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
