<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Country_Code_Update_01</fullName>
        <field>ISO_Country_code__c</field>
        <formula>CASE(Country__c,
&quot;Aruba&quot;,&quot;ABW&quot;,
&quot;Afghanistan&quot;,&quot;AFG&quot;,
&quot;Angola&quot;,&quot;AGO&quot;,
&quot;Anguilla&quot;,&quot;AIA&quot;,
&quot;Åland Islands&quot;,&quot;ALA&quot;,
&quot;Albania&quot;,&quot;ALB&quot;,
&quot;Andorra&quot;,&quot;AND&quot;,
&quot;United Arab Emirates&quot;,&quot;ARE&quot;,
&quot;Argentina&quot;,&quot;ARG&quot;,
&quot;Armenia&quot;,&quot;ARM&quot;,
&quot;American Samoa&quot;,&quot;ASM&quot;,
&quot;Antarctica&quot;,&quot;ATA&quot;,
&quot;French Southern Territories&quot;,&quot;ATF&quot;,
&quot;Antigua and Barbuda&quot;,&quot;ATG&quot;,
&quot;Australia&quot;,&quot;AUS&quot;,
&quot;Austria&quot;,&quot;AUT&quot;,
&quot;Azerbaijan&quot;,&quot;AZE&quot;,
&quot;Burundi&quot;,&quot;BDI&quot;,
&quot;Belgium&quot;,&quot;BEL&quot;,
&quot;Benin&quot;,&quot;BEN&quot;,
&quot;Bonaire, Sint Eustatius and Saba&quot;,&quot;BES&quot;,
&quot;Burkina Faso&quot;,&quot;BFA&quot;,
&quot;Bangladesh&quot;,&quot;BGD&quot;,
&quot;Bulgaria&quot;,&quot;BGR&quot;,
&quot;Bahrain&quot;,&quot;BHR&quot;,
&quot;Bahamas&quot;,&quot;BHS&quot;,
&quot;Bosnia and Herzegovina&quot;,&quot;BIH&quot;,
&quot;Saint Barthélemy&quot;,&quot;BLM&quot;,
&quot;Belarus&quot;,&quot;BLR&quot;,
&quot;Belize&quot;,&quot;BLZ&quot;,
&quot;Bermuda&quot;,&quot;BMU&quot;,
&quot;Bolivia, Plurinational State of&quot;,&quot;BOL&quot;,
&quot;Brazil&quot;,&quot;BRA&quot;,
&quot;Barbados&quot;,&quot;BRB&quot;,
&quot;Brunei Darussalam&quot;,&quot;BRN&quot;,
&quot;Bhutan&quot;,&quot;BTN&quot;,
&quot;Bouvet Island&quot;,&quot;BVT&quot;,
&quot;Botswana&quot;,&quot;BWA&quot;,
&quot;Central African Republic&quot;,&quot;CAF&quot;,
&quot;Canada&quot;,&quot;CAN&quot;,
&quot;Cocos (Keeling) Islands&quot;,&quot;CCK&quot;,
&quot;Switzerland&quot;,&quot;CHE&quot;,
&quot;Chile&quot;,&quot;CHL&quot;,
&quot;China&quot;,&quot;CHN&quot;,
&quot;Côte d&apos;Ivoire&quot;,&quot;CIV&quot;,
&quot;Cameroon&quot;,&quot;CMR&quot;,
&quot;Congo, the Democratic Republic of the&quot;,&quot;COD&quot;,
&quot;Congo&quot;,&quot;COG&quot;,
&quot;Cook Islands&quot;,&quot;COK&quot;,
&quot;Colombia&quot;,&quot;COL&quot;,
&quot;Comoros&quot;,&quot;COM&quot;,
&quot;Cape Verde&quot;,&quot;CPV&quot;,
&quot;Costa Rica&quot;,&quot;CRI&quot;,
&quot;Cuba&quot;,&quot;CUB&quot;,
&quot;Curaçao&quot;,&quot;CUW&quot;,
&quot;Christmas Island&quot;,&quot;CXR&quot;,
&quot;Cayman Islands&quot;,&quot;CYM&quot;,
&quot;Cyprus&quot;,&quot;CYP&quot;,
&quot;Czech Republic&quot;,&quot;CZE&quot;,
&quot;Germany&quot;,&quot;DEU&quot;,
&quot;Djibouti&quot;,&quot;DJI&quot;,
&quot;Dominica&quot;,&quot;DMA&quot;,
&quot;Denmark&quot;,&quot;DNK&quot;,
&quot;Dominican Republic&quot;,&quot;DOM&quot;,
&quot;Algeria&quot;,&quot;DZA&quot;,
&quot;Ecuador&quot;,&quot;ECU&quot;,
&quot;Egypt&quot;,&quot;EGY&quot;,
&quot;Eritrea&quot;,&quot;ERI&quot;,
&quot;Western Sahara&quot;,&quot;ESH&quot;,
&quot;Spain&quot;,&quot;ESP&quot;,
&quot;Estonia&quot;,&quot;EST&quot;,
&quot;Ethiopia&quot;,&quot;ETH&quot;,
&quot;Finland&quot;,&quot;FIN&quot;,
&quot;Fiji&quot;,&quot;FJI&quot;,
&quot;Falkland Islands (Malvinas)&quot;,&quot;FLK&quot;,
&quot;France&quot;,&quot;FRA&quot;,
&quot;Faroe Islands&quot;,&quot;FRO&quot;,
&quot;Micronesia, Federated States of&quot;,&quot;FSM&quot;,
&quot;Gabon&quot;,&quot;GAB&quot;,
&quot;United Kingdom&quot;,&quot;GBR&quot;,
&quot;Georgia&quot;,&quot;GEO&quot;,
&quot;Guernsey&quot;,&quot;GGY&quot;,
&quot;Ghana&quot;,&quot;GHA&quot;,
&quot;Gibraltar&quot;,&quot;GIB&quot;,
&quot;Guinea&quot;,&quot;GIN&quot;,
&quot;Guadeloupe&quot;,&quot;GLP&quot;,
&quot;Gambia&quot;,&quot;GMB&quot;,
&quot;Guinea-Bissau&quot;,&quot;GNB&quot;,
&quot;Equatorial Guinea&quot;,&quot;GNQ&quot;,
&quot;Greece&quot;,&quot;GRC&quot;,
&quot;Grenada&quot;,&quot;GRD&quot;,
&quot;Greenland&quot;,&quot;GRL&quot;,
&quot;Guatemala&quot;,&quot;GTM&quot;,
&quot;French Guiana&quot;,&quot;GUF&quot;,
&quot;Guam&quot;,&quot;GUM&quot;,
&quot;Guyana&quot;,&quot;GUY&quot;,
&quot;Hong Kong&quot;,&quot;HKG&quot;,
&quot;Heard Island and McDonald Islands&quot;,&quot;HMD&quot;,
&quot;Honduras&quot;,&quot;HND&quot;,
&quot;Croatia&quot;,&quot;HRV&quot;,
&quot;Haiti&quot;,&quot;HTI&quot;,
&quot;Hungary&quot;,&quot;HUN&quot;,
&quot;Indonesia&quot;,&quot;IDN&quot;,
&quot;Isle of Man&quot;,&quot;IMN&quot;,
&quot;India&quot;,&quot;IND&quot;,
&quot;British Indian Ocean Territory&quot;,&quot;IOT&quot;,
&quot;Ireland&quot;,&quot;IRL&quot;,
&quot;Iran, Islamic Republic of&quot;,&quot;IRN&quot;,
&quot;Iraq&quot;,&quot;IRQ&quot;,
&quot;Iceland&quot;,&quot;ISL&quot;,
&quot;Israel&quot;,&quot;ISR&quot;,
&quot;Italy&quot;,&quot;ITA&quot;,
&quot;Jamaica&quot;,&quot;JAM&quot;,
&quot;Jersey&quot;,&quot;JEY&quot;,
&quot;Jordan&quot;,&quot;JOR&quot;,
&quot;Japan&quot;,&quot;JPN&quot;,
&quot;Kazakhstan&quot;,&quot;KAZ&quot;,
&quot;Kenya&quot;,&quot;KEN&quot;,
&quot;Kyrgyzstan&quot;,&quot;KGZ&quot;,
&quot;Cambodia&quot;,&quot;KHM&quot;,
&quot;Kiribati&quot;,&quot;KIR&quot;,
&quot;Saint Kitts and Nevis&quot;,&quot;KNA&quot;,
&quot;Korea, Republic of&quot;,&quot;KOR&quot;,
&quot;Kuwait&quot;,&quot;KWT&quot;,
&quot;Lao People&apos;s Democratic Republic&quot;,&quot;LAO&quot;,
&quot;Lebanon&quot;,&quot;LBN&quot;,
&quot;Liberia&quot;,&quot;LBR&quot;,
&quot;Libya&quot;,&quot;LBY&quot;,
&quot;Saint Lucia&quot;,&quot;LCA&quot;,
&quot;Liechtenstein&quot;,&quot;LIE&quot;,
&quot;Sri Lanka&quot;,&quot;LKA&quot;,
&quot;Lesotho&quot;,&quot;LSO&quot;,
&quot;Lithuania&quot;,&quot;LTU&quot;,
&quot;Luxembourg&quot;,&quot;LUX&quot;,
&quot;Latvia&quot;,&quot;LVA&quot;,
&quot;&quot;)</formula>
        <name>ISO Country Code Update-01</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ISO_Country_Code_Update_02</fullName>
        <field>ISO_Country_code__c</field>
        <formula>CASE(Country__c,
&quot;Macao&quot;,&quot;MAC&quot;,
&quot;Saint Martin (French part)&quot;,&quot;MAF&quot;,
&quot;Morocco&quot;,&quot;MAR&quot;,
&quot;Monaco&quot;,&quot;MCO&quot;,
&quot;Moldova, Republic of&quot;,&quot;MDA&quot;,
&quot;Madagascar&quot;,&quot;MDG&quot;,
&quot;Maldives&quot;,&quot;MDV&quot;,
&quot;Mexico&quot;,&quot;MEX&quot;,
&quot;Marshall Islands&quot;,&quot;MHL&quot;,
&quot;Macedonia, the former Yugoslav Republic of&quot;,&quot;MKD&quot;,
&quot;Mali&quot;,&quot;MLI&quot;,
&quot;Malta&quot;,&quot;MLT&quot;,
&quot;Myanmar&quot;,&quot;MMR&quot;,
&quot;Montenegro&quot;,&quot;MNE&quot;,
&quot;Mongolia&quot;,&quot;MNG&quot;,
&quot;Northern Mariana Islands&quot;,&quot;MNP&quot;,
&quot;Mozambique&quot;,&quot;MOZ&quot;,
&quot;Mauritania&quot;,&quot;MRT&quot;,
&quot;Montserrat&quot;,&quot;MSR&quot;,
&quot;Martinique&quot;,&quot;MTQ&quot;,
&quot;Mauritius&quot;,&quot;MUS&quot;,
&quot;Malawi&quot;,&quot;MWI&quot;,
&quot;Malaysia&quot;,&quot;MYS&quot;,
&quot;Mayotte&quot;,&quot;MYT&quot;,
&quot;Namibia&quot;,&quot;NAM&quot;,
&quot;New Caledonia&quot;,&quot;NCL&quot;,
&quot;Niger&quot;,&quot;NER&quot;,
&quot;Norfolk Island&quot;,&quot;NFK&quot;,
&quot;Nigeria&quot;,&quot;NGA&quot;,
&quot;Nicaragua&quot;,&quot;NIC&quot;,
&quot;Niue&quot;,&quot;NIU&quot;,
&quot;Netherlands&quot;,&quot;NLD&quot;,
&quot;Norway&quot;,&quot;NOR&quot;,
&quot;Nepal&quot;,&quot;NPL&quot;,
&quot;Nauru&quot;,&quot;NRU&quot;,
&quot;New Zealand&quot;,&quot;NZL&quot;,
&quot;Oman&quot;,&quot;OMN&quot;,
&quot;Pakistan&quot;,&quot;PAK&quot;,
&quot;Panama&quot;,&quot;PAN&quot;,
&quot;Pitcairn&quot;,&quot;PCN&quot;,
&quot;Peru&quot;,&quot;PER&quot;,
&quot;Philippines&quot;,&quot;PHL&quot;,
&quot;Palau&quot;,&quot;PLW&quot;,
&quot;Papua New Guinea&quot;,&quot;PNG&quot;,
&quot;Poland&quot;,&quot;POL&quot;,
&quot;Puerto Rico&quot;,&quot;PRI&quot;,
&quot;Korea, Democratic People&apos;s Republic of&quot;,&quot;PRK&quot;,
&quot;Portugal&quot;,&quot;PRT&quot;,
&quot;Paraguay&quot;,&quot;PRY&quot;,
&quot;Palestinian Territory, Occupied&quot;,&quot;PSE&quot;,
&quot;French Polynesia&quot;,&quot;PYF&quot;,
&quot;Qatar&quot;,&quot;QAT&quot;,
&quot;Réunion&quot;,&quot;REU&quot;,
&quot;Romania&quot;,&quot;ROU&quot;,
&quot;Russian Federation&quot;,&quot;RUS&quot;,
&quot;Rwanda&quot;,&quot;RWA&quot;,
&quot;Saudi Arabia&quot;,&quot;SAU&quot;,
&quot;Sudan&quot;,&quot;SDN&quot;,
&quot;Senegal&quot;,&quot;SEN&quot;,
&quot;Singapore&quot;,&quot;SGP&quot;,
&quot;South Georgia and the South Sandwich Islands&quot;,&quot;SGS&quot;,
&quot;Saint Helena, Ascension and Tristan da Cunha&quot;,&quot;SHN&quot;,
&quot;Svalbard and Jan Mayen&quot;,&quot;SJM&quot;,
&quot;Solomon Islands&quot;,&quot;SLB&quot;,
&quot;Sierra Leone&quot;,&quot;SLE&quot;,
&quot;El Salvador&quot;,&quot;SLV&quot;,
&quot;San Marino&quot;,&quot;SMR&quot;,
&quot;Somalia&quot;,&quot;SOM&quot;,
&quot;Saint Pierre and Miquelon&quot;,&quot;SPM&quot;,
&quot;Serbia&quot;,&quot;SRB&quot;,
&quot;South Sudan&quot;,&quot;SSD&quot;,
&quot;Sao Tome and Principe&quot;,&quot;STP&quot;,
&quot;Suriname&quot;,&quot;SUR&quot;,
&quot;Slovakia&quot;,&quot;SVK&quot;,
&quot;Slovenia&quot;,&quot;SVN&quot;,
&quot;Sweden&quot;,&quot;SWE&quot;,
&quot;Swaziland&quot;,&quot;SWZ&quot;,
&quot;Sint Maarten (Dutch part)&quot;,&quot;SXM&quot;,
&quot;Seychelles&quot;,&quot;SYC&quot;,
&quot;Syrian Arab Republic&quot;,&quot;SYR&quot;,
&quot;Turks and Caicos Islands&quot;,&quot;TCA&quot;,
&quot;Chad&quot;,&quot;TCD&quot;,
&quot;Togo&quot;,&quot;TGO&quot;,
&quot;Thailand&quot;,&quot;THA&quot;,
&quot;Tajikistan&quot;,&quot;TJK&quot;,
&quot;Tokelau&quot;,&quot;TKL&quot;,
&quot;Turkmenistan&quot;,&quot;TKM&quot;,
&quot;Timor-Leste&quot;,&quot;TLS&quot;,
&quot;Tonga&quot;,&quot;TON&quot;,
&quot;Trinidad and Tobago&quot;,&quot;TTO&quot;,
&quot;Tunisia&quot;,&quot;TUN&quot;,
&quot;Turkey&quot;,&quot;TUR&quot;,
&quot;Tuvalu&quot;,&quot;TUV&quot;,
&quot;Taiwan, Province of China&quot;,&quot;TWN&quot;,
&quot;Tanzania, United Republic of&quot;,&quot;TZA&quot;,
&quot;Uganda&quot;,&quot;UGA&quot;,
&quot;Ukraine&quot;,&quot;UKR&quot;,
&quot;United States Minor Outlying Islands&quot;,&quot;UMI&quot;,
&quot;Uruguay&quot;,&quot;URY&quot;,
&quot;United States&quot;,&quot;USA&quot;,
&quot;Uzbekistan&quot;,&quot;UZB&quot;,
&quot;Holy See (Vatican City State)&quot;,&quot;VAT&quot;,
&quot;Saint Vincent and the Grenadines&quot;,&quot;VCT&quot;,
&quot;Venezuela, Bolivarian Republic of&quot;,&quot;VEN&quot;,
&quot;Virgin Islands, British&quot;,&quot;VGB&quot;,
&quot;Virgin Islands, U.S.&quot;,&quot;VIR&quot;,
&quot;Viet Nam&quot;,&quot;VNM&quot;,
&quot;Vanuatu&quot;,&quot;VUT&quot;,
&quot;Wallis and Futuna&quot;,&quot;WLF&quot;,
&quot;Samoa&quot;,&quot;WSM&quot;,
&quot;Yemen&quot;,&quot;YEM&quot;,
&quot;South Africa&quot;,&quot;ZAF&quot;,
&quot;Zambia&quot;,&quot;ZMB&quot;,
&quot;Zimbabwe&quot;,&quot;ZWE&quot;,
&quot;United States of America&quot;,&quot;USA&quot;,
&quot;USA&quot;,&quot;USA&quot;,
&quot;US&quot;,&quot;USA&quot;,
&quot;Channel Islands&quot;,&quot;GBR&quot;,
&quot;Italia&quot;,&quot;ITA&quot;,
&quot;Brasil&quot;,&quot;BRA&quot;,
&quot;Vietnam&quot;,&quot;VNM&quot;,
&quot;Antigua&quot;,&quot;ATG&quot;,
&quot;Brasil&quot;,&quot;BRA&quot;,
&quot;British Virgin Islands&quot;,&quot;VGB&quot;,
&quot;Columbia&quot;,&quot;COL&quot;,
&quot;Curacao&quot;,&quot;CUW&quot;,
&quot;Faroes&quot;,&quot;FRO&quot;,
&quot;Federated States Of Micronesia&quot;,&quot;FSM&quot;,
&quot;Iran&quot;,&quot;IRN&quot;,
&quot;Korea Democratic People&apos;s Rep&quot;,&quot;PRK&quot;,
&quot;The Netherlands&quot;,&quot;NLD&quot;,
&quot;Republic Of Moldova&quot;,&quot;MDA&quot;,
&quot;St. Kitts and Nevis&quot;,&quot;KNA&quot;,
&quot;Sultannate of Oman&quot;,&quot;OMN&quot;,
&quot;UK&quot;,&quot;GBR&quot;,
&quot;&quot;)</formula>
        <name>ISO Country Code Update-02</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CRMOD_ID_on_Address_Object</fullName>
        <description>Set CRMOD ID on Address Object</description>
        <field>CRMOD_ID__c</field>
        <formula>casesafeid(Id)</formula>
        <name>Set CRMOD ID on Address Object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Used_on_Aria_Billing_Accnt_Flg</fullName>
        <description>Used_on_Aria_Billing_Account__c</description>
        <field>Used_on_Aria_Billing_Account__c</field>
        <literalValue>1</literalValue>
        <name>Set the Used on Aria Billing Accnt Flg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_Address_ID_Field</fullName>
        <description>Created for case incident #01250120.  This updates the unique Address_id__c field every time the address is changed using the same format as when the field value is assigned in the Apex Class AddressUtilities.</description>
        <field>Address_id__c</field>
        <formula>Address_1__c +
Address_2__c +
Address_3__c + 
Address_4__c + 
City__c +
TEXT(Country__c) +
CEDEX__c + 
Codiga_Postal__c +
TEXT(County__c) +
District__c +
TEXT(Emirate__c) +
Floor__c + 
Partofterritory__c +
POBox__c +
Postcode__c +
Prefecture__c +
Province__c +
SortingCode__c +
TEXT(State__c) +
Suite__c +
Zip__c</formula>
        <name>Update Unique Address ID Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ISO Country Code Update-01</fullName>
        <actions>
            <name>ISO_Country_Code_Update_01</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates ISO Country Code field based on a fragment of countries to avoid reaching formula characters limits</description>
        <formula>CASE( Country__c, &quot;Aruba&quot;,1, &quot;Afghanistan&quot;,1, &quot;Angola&quot;,1, &quot;Anguilla&quot;,1, &quot;Åland Islands&quot;,1, &quot;Albania&quot;,1, &quot;Andorra&quot;,1, &quot;United Arab Emirates&quot;,1, &quot;Argentina&quot;,1, &quot;Armenia&quot;,1, &quot;American Samoa&quot;,1, &quot;Antarctica&quot;,1, &quot;French Southern Territories&quot;,1, &quot;Antigua and Barbuda&quot;,1, &quot;Australia&quot;,1, &quot;Austria&quot;,1, &quot;Azerbaijan&quot;,1, &quot;Burundi&quot;,1, &quot;Belgium&quot;,1, &quot;Benin&quot;,1, &quot;Bonaire, Sint Eustatius and Saba&quot;,1, &quot;Burkina Faso&quot;,1, &quot;Bangladesh&quot;,1, &quot;Bulgaria&quot;,1, &quot;Bahrain&quot;,1, &quot;Bahamas&quot;,1, &quot;Bosnia and Herzegovina&quot;,1, &quot;Saint Barthélemy&quot;,1, &quot;Belarus&quot;,1, &quot;Belize&quot;,1, &quot;Bermuda&quot;,1, &quot;Bolivia, Plurinational State of&quot;,1, &quot;Brazil&quot;,1, &quot;Barbados&quot;,1, &quot;Brunei Darussalam&quot;,1, &quot;Bhutan&quot;,1, &quot;Bouvet Island&quot;,1, &quot;Botswana&quot;,1, &quot;Central African Republic&quot;,1, &quot;Canada&quot;,1, &quot;Cocos (Keeling) Islands&quot;,1, &quot;Switzerland&quot;,1, &quot;Chile&quot;,1, &quot;China&quot;,1, &quot;Côte d&apos;Ivoire&quot;,1, &quot;Cameroon&quot;,1, &quot;Congo, the Democratic Republic of the&quot;,1, &quot;Congo&quot;,1, &quot;Cook Islands&quot;,1, &quot;Colombia&quot;,1, &quot;Comoros&quot;,1, &quot;Cape Verde&quot;,1, &quot;Costa Rica&quot;,1, &quot;Cuba&quot;,1, &quot;Curaçao&quot;,1, &quot;Christmas Island&quot;,1, &quot;Cayman Islands&quot;,1, &quot;Cyprus&quot;,1, &quot;Czech Republic&quot;,1, &quot;Germany&quot;,1, &quot;Djibouti&quot;,1, &quot;Dominica&quot;,1, &quot;Denmark&quot;,1, &quot;Dominican Republic&quot;,1, &quot;Algeria&quot;,1, &quot;Ecuador&quot;,1, &quot;Egypt&quot;,1, &quot;Eritrea&quot;,1, &quot;Western Sahara&quot;,1, &quot;Spain&quot;,1, &quot;Estonia&quot;,1, &quot;Ethiopia&quot;,1, &quot;Finland&quot;,1, &quot;Fiji&quot;,1, &quot;Falkland Islands (Malvinas)&quot;,1, &quot;France&quot;,1, &quot;Faroe Islands&quot;,1, &quot;Micronesia, Federated States of&quot;,1, &quot;Gabon&quot;,1, &quot;United Kingdom&quot;,1, &quot;Georgia&quot;,1, &quot;Guernsey&quot;,1, &quot;Ghana&quot;,1, &quot;Gibraltar&quot;,1, &quot;Guinea&quot;,1, &quot;Guadeloupe&quot;,1, &quot;Gambia&quot;,1, &quot;Guinea-Bissau&quot;,1, &quot;Equatorial Guinea&quot;,1, &quot;Greece&quot;,1, &quot;Grenada&quot;,1, &quot;Greenland&quot;,1, &quot;Guatemala&quot;,1, &quot;French Guiana&quot;,1, &quot;Guam&quot;,1, &quot;Guyana&quot;,1, &quot;Hong Kong&quot;,1, &quot;Heard Island and McDonald Islands&quot;,1, &quot;Honduras&quot;,1, &quot;Croatia&quot;,1, &quot;Haiti&quot;,1, &quot;Hungary&quot;,1, &quot;Indonesia&quot;,1, &quot;Isle of Man&quot;,1, &quot;India&quot;,1, &quot;British Indian Ocean Territory&quot;,1, &quot;Ireland&quot;,1, &quot;Iran, Islamic Republic of&quot;,1, &quot;Iraq&quot;,1, &quot;Iceland&quot;,1, &quot;Israel&quot;,1, &quot;Italy&quot;,1, &quot;Jamaica&quot;,1, &quot;Jersey&quot;,1, &quot;Jordan&quot;,1, &quot;Japan&quot;,1, &quot;Kazakhstan&quot;,1, &quot;Kenya&quot;,1, &quot;Kyrgyzstan&quot;,1, &quot;Cambodia&quot;,1, &quot;Kiribati&quot;,1, &quot;Saint Kitts and Nevis&quot;,1, &quot;Korea, Republic of&quot;,1, &quot;Kuwait&quot;,1, &quot;Lao People&apos;s Democratic Republic&quot;,1, &quot;Lebanon&quot;,1, &quot;Liberia&quot;,1, &quot;Libya&quot;,1, &quot;Saint Lucia&quot;,1, &quot;Liechtenstein&quot;,1, &quot;Sri Lanka&quot;,1, &quot;Lesotho&quot;,1, &quot;Lithuania&quot;,1, &quot;Luxembourg&quot;,1, &quot;Latvia&quot;,1, 0) == 1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ISO Country Code Update-02</fullName>
        <actions>
            <name>ISO_Country_Code_Update_02</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates ISO Country Code field based on a fragment of countries to avoid reaching formula characters limits</description>
        <formula>CASE(Country__c,  &quot;Macao&quot;,1, &quot;Saint Martin (French part)&quot;,1, &quot;Morocco&quot;,1, &quot;Monaco&quot;,1, &quot;Moldova, Republic of&quot;,1, &quot;Madagascar&quot;,1, &quot;Maldives&quot;,1, &quot;Mexico&quot;,1, &quot;Marshall Islands&quot;,1, &quot;Macedonia, the former Yugoslav Republic of&quot;,1, &quot;Mali&quot;,1, &quot;Malta&quot;,1, &quot;Myanmar&quot;,1, &quot;Montenegro&quot;,1, &quot;Mongolia&quot;,1, &quot;Northern Mariana Islands&quot;,1, &quot;Mozambique&quot;,1, &quot;Mauritania&quot;,1, &quot;Montserrat&quot;,1, &quot;Martinique&quot;,1, &quot;Mauritius&quot;,1, &quot;Malawi&quot;,1, &quot;Malaysia&quot;,1, &quot;Mayotte&quot;,1, &quot;Namibia&quot;,1, &quot;New Caledonia&quot;,1, &quot;Niger&quot;,1, &quot;Norfolk Island&quot;,1, &quot;Nigeria&quot;,1, &quot;Nicaragua&quot;,1, &quot;Niue&quot;,1, &quot;Netherlands&quot;,1, &quot;Norway&quot;,1, &quot;Nepal&quot;,1, &quot;Nauru&quot;,1, &quot;New Zealand&quot;,1, &quot;Oman&quot;,1, &quot;Pakistan&quot;,1, &quot;Panama&quot;,1, &quot;Pitcairn&quot;,1, &quot;Peru&quot;,1, &quot;Philippines&quot;,1, &quot;Palau&quot;,1, &quot;Papua New Guinea&quot;,1, &quot;Poland&quot;,1, &quot;Puerto Rico&quot;,1, &quot;Korea, Democratic People&apos;s Republic of&quot;,1, &quot;Portugal&quot;,1, &quot;Paraguay&quot;,1, &quot;Palestinian Territory, Occupied&quot;,1, &quot;French Polynesia&quot;,1, &quot;Qatar&quot;,1, &quot;Réunion&quot;,1, &quot;Romania&quot;,1, &quot;Russian Federation&quot;,1, &quot;Rwanda&quot;,1, &quot;Saudi Arabia&quot;,1, &quot;Sudan&quot;,1, &quot;Senegal&quot;,1, &quot;Singapore&quot;,1, &quot;South Georgia and the South Sandwich Islands&quot;,1, &quot;Saint Helena, Ascension and Tristan da Cunha&quot;,1, &quot;Svalbard and Jan Mayen&quot;,1, &quot;Solomon Islands&quot;,1, &quot;Sierra Leone&quot;,1, &quot;El Salvador&quot;,1, &quot;San Marino&quot;,1, &quot;Somalia&quot;,1, &quot;Saint Pierre and Miquelon&quot;,1, &quot;Serbia&quot;,1, &quot;South Sudan&quot;,1, &quot;Sao Tome and Principe&quot;,1, &quot;Suriname&quot;,1, &quot;Slovakia&quot;,1, &quot;Slovenia&quot;,1, &quot;Sweden&quot;,1, &quot;Swaziland&quot;,1, &quot;Sint Maarten (Dutch part)&quot;,1, &quot;Seychelles&quot;,1, &quot;Syrian Arab Republic&quot;,1, &quot;Turks and Caicos Islands&quot;,1, &quot;Chad&quot;,1, &quot;Togo&quot;,1, &quot;Thailand&quot;,1, &quot;Tajikistan&quot;,1, &quot;Tokelau&quot;,1, &quot;Turkmenistan&quot;,1, &quot;Timor-Leste&quot;,1, &quot;Tonga&quot;,1, &quot;Trinidad and Tobago&quot;,1, &quot;Tunisia&quot;,1, &quot;Turkey&quot;,1, &quot;Tuvalu&quot;,1, &quot;Taiwan, Province of China&quot;,1, &quot;Tanzania, United Republic of&quot;,1, &quot;Uganda&quot;,1, &quot;Ukraine&quot;,1, &quot;United States Minor Outlying Islands&quot;,1, &quot;Uruguay&quot;,1, &quot;United States&quot;,1, &quot;Uzbekistan&quot;,1, &quot;Holy See (Vatican City State)&quot;,1, &quot;Saint Vincent and the Grenadines&quot;,1, &quot;Venezuela, Bolivarian Republic of&quot;,1, &quot;Virgin Islands, British&quot;,1, &quot;Virgin Islands, U.S.&quot;,1, &quot;Viet Nam&quot;,1, &quot;Vanuatu&quot;,1, &quot;Wallis and Futuna&quot;,1, &quot;Samoa&quot;,1, &quot;Yemen&quot;,1, &quot;South Africa&quot;,1, &quot;Zambia&quot;,1, &quot;Zimbabwe&quot;,1, &quot;United States of America&quot;,1, &quot;USA&quot;,1, &quot;US&quot;,1, &quot;Channel Islands&quot;,1, &quot;Italia&quot;,1, &quot;Brasil&quot;,1, &quot;Vietnam&quot;,1, &quot;Antigua&quot;,1, &quot;Brasil&quot;,1, &quot;British Virgin Islands&quot;,1, &quot;Columbia&quot;,1, &quot;Curacao&quot;,1, &quot;Faroes&quot;,1, &quot;Federated States Of Micronesia&quot;,1, &quot;Iran&quot;,1, &quot;Korea Democratic People&apos;s Rep&quot;,1, &quot;The Netherlands&quot;,1, &quot;Republic Of Moldova&quot;,1, &quot;St. Kitts and Nevis&quot;,1, &quot;Sultannate of Oman&quot;,1, &quot;UK&quot;,1, 0) == 1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Address created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Address__c.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique Address ID Field</fullName>
        <actions>
            <name>Update_Unique_Address_ID_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for case incident #01250120.  This rule is to ensure that the unique Address_id__c field is updated every time the address is changed using the same format as when the value is assigned in the Apex Class AddressUtilities.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the CRMOD ID  on the Address Object</fullName>
        <actions>
            <name>Set_CRMOD_ID_on_Address_Object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Address__c.CRMOD_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update the CRMOD ID  on the Address Object</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
