<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Asset_Update_to_Original_Purchase_Date</fullName>
        <description>Updates the Original Purchase Date field in assets with the creation date</description>
        <field>Original_Purchase_Date__c</field>
        <formula>DATEVALUE( CreatedDate )</formula>
        <name>Asset Update to Original Purchase Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SaaS_Asset_Timestamp</fullName>
        <description>Updated when SaaS asset is created or renewed from direct sales or a SaaS asset has been credited</description>
        <field>SaaS_Timestamp__c</field>
        <formula>Now()</formula>
        <name>SaaS Asset Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Asset_Status_to_Cancelled</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Set Asset Status to Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Install_Date</fullName>
        <description>Sets or Clears Install Date</description>
        <field>InstallDate</field>
        <formula>If( Product_Installed__c  = TRUE,  TODAY(),NULL)</formula>
        <name>Set Install Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Company_Reg_from_account_to_assets</fullName>
        <field>Company_Registration__c</field>
        <formula>Account.Company_Registration__c</formula>
        <name>Stamp Company Reg from account to assets</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Asset Update to Original Purchase Date</fullName>
        <actions>
            <name>Asset_Update_to_Original_Purchase_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Original_Purchase_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Fills up the Original_Purchase_Date__c for reporting as old values contain Sibel Data and new should be related to Salesforce</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SaaS Asset Integration</fullName>
        <actions>
            <name>SaaS_Asset_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Timestamp SaaS asset when it is newly created or renewed from a direct sale.  Or, it has been cancelled.  This is so the SaaS integration adjust the corresponding click balances on the SaaS Platform</description>
        <formula>AND(NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ),  NOT($Profile.Name = $Label.PROFILE_API_ONLY_SAAS_EDQ),   SaaS__c,  OR(   ISNEW(),   ISCHANGED( Order_Line__c ),  ISCHANGED( Status ),  ISCHANGED( UsageEndDate ),  AND(ISCHANGED(  Cancellation_Date__c ), NOT(ISBLANK(Cancellation_Date__c))),  AND( Credited__c , ISCHANGED(Credited__c)),  AND(ISCHANGED(  Credited_Date__c ), NOT(ISBLANK(Credited_Date__c)))      ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Asset Status to Cancelled</fullName>
        <actions>
            <name>Set_Asset_Status_to_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Cancellation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Install Date on Asset</fullName>
        <actions>
            <name>Set_Install_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Automatically sets the Product Install Date on the Asset when a User checks Product Installed? on Asset</description>
        <formula>ISNEW()  || ISCHANGED(Product_Installed__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Company Reg from account to assets</fullName>
        <actions>
            <name>Stamp_Company_Reg_from_account_to_assets</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamp Company Reg from account to opportunity, order and assets on close won</description>
        <formula>AND(     Opportunity__r.IsClosed = true,     LEN(Company_Registration__c )=0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Assets created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
