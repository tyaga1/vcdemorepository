<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_EDQ_Proposition_Idea_is_edited</fullName>
        <description>Idea - EDQ Proposition Idea is edited</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EDQ_Templates/Idea_EDQ_Proposition_is_edited_notification</template>
    </alerts>
    <alerts>
        <fullName>Idea_EDQ_product_Idea_is_edited</fullName>
        <description>Idea - EDQ product Idea is edited</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Idea_EDQ_Product_is_edited_notification</template>
    </alerts>
    <alerts>
        <fullName>New_Comment_Added_to_Idea</fullName>
        <description>New Comment Added to Idea</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Comment_on_Idea</template>
    </alerts>
    <alerts>
        <fullName>Status_Updated_on_an_Idea</fullName>
        <description>Status Updated on an Idea</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Status_Updated_on_Idea</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Last_Status_Change</fullName>
        <field>Last_Status_Change__c</field>
        <formula>Today()</formula>
        <name>Update Last Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Creator on New Comment</fullName>
        <actions>
            <name>New_Comment_Added_to_Idea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(LastCommentDate)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Alert Creator on Status Update</fullName>
        <actions>
            <name>Status_Updated_on_an_Idea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ideas EDQ Product notification on any edit</fullName>
        <actions>
            <name>Idea_EDQ_product_Idea_is_edited</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ideas from EDQ Product Idea zone send email alert upon any edit to Record creator and Related User:Product manager</description>
        <formula>Community.Name==&quot;EDQ Product&quot; &amp;&amp;  OR(  ISCHANGED(Title),  ISCHANGED(Description__c),  ISCHANGED(Categories),  ISCHANGED(Product_Line__c),  ISCHANGED(Product_Element__c),  ISCHANGED(Product_Manager__c),  ISCHANGED(As_a__c),  ISCHANGED(I_want__c),  ISCHANGED(So_that__c),  ISCHANGED(User_Business_Value__c),  ISCHANGED(Time_Criticality__c),  ISCHANGED(Risk_Reduction_Opportunity_Enablement__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ideas EDQ Proposition notification on any edit</fullName>
        <actions>
            <name>Idea_EDQ_Proposition_Idea_is_edited</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Ideas from EDQ Proposition Idea zone send email alert upon any edit to Record creator and Related User:Product manager</description>
        <formula>Community.Name==&quot;EDQ Propositions&quot; &amp;&amp;  OR( ISCHANGED(Title), ISCHANGED(Categories),  ISCHANGED(Reason__c), ISCHANGED(Cross_Experian__c), ISCHANGED(Product_Manager__c),  ISCHANGED(Proposition_Form__c), ISCHANGED(As_a__c),  ISCHANGED(I_want__c),  ISCHANGED(So_that__c), ISCHANGED(Strategy__c), ISCHANGED(Financial_Year__c), ISCHANGED(User_Business_Value__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Status Changed</fullName>
        <actions>
            <name>Update_Last_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
