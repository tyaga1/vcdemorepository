<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ARIA_Order_Approved_Email_Notification</fullName>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <description>ARIA Order Approved Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aria_Order_Details_Approved</template>
    </alerts>
    <alerts>
        <fullName>ARIA_Order_Rejected_Email_Notification</fullName>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <description>ARIA Order Rejected Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aria_Order_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>ARIA_Order_Stuck_Email_Notification</fullName>
        <ccEmails>gcssalesforcesupport@experian.com</ccEmails>
        <ccEmails>sadar.yacob@experian.com</ccEmails>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <ccEmails>GCSAriaSupport@experian.com</ccEmails>
        <description>ARIA Order Stuck Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aria_Order_stuck_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Order_is_not_processed_by_boomi_for_24_hours</fullName>
        <description>Order is not processed by boomi for 24 hours</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARIA_PushtoAriaFlagSetYes</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_When_Approval_for_Aria_Order_is_recalled</fullName>
        <description>Send Email When Approval for Aria Order is recalled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aria_Order_Recall_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Select_Send_to_Aria_Check_box</fullName>
        <description>Selecting the Send to Aria check box to checked.</description>
        <field>SendOrdertoAria__c</field>
        <literalValue>1</literalValue>
        <name>Select Send to Aria Check box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AriaOrder_Record_Type_to_Read_Only</fullName>
        <description>Set AriaOrder Record Type to Read Only</description>
        <field>RecordTypeId</field>
        <lookupValue>Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set AriaOrder Record Type to Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Estimated_Tax_Amt</fullName>
        <field>Estimated_Tax_Amount__c</field>
        <formula>IF ( Billing_System_Country__c =&apos;Netherlands&apos;, Total_Taxable_Items_Amount__c * 0.21,
IF ( Billing_System_Country__c =&apos;Australia&apos;, Total_Taxable_Items_Amount__c * 0.10,0)
)</formula>
        <name>Set Estimated Tax Amt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Experian_ID_on_Arorder_to_SFDC_ID</fullName>
        <description>Set Experian ID on Aria Order to SFDC ID so it can be used to update back from ARIA to SFDC</description>
        <field>Experian_ID__c</field>
        <formula>casesafeid(Id)</formula>
        <name>Set Experian ID on Arorder to SFDC ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Country_based_on_BillingAccnt</fullName>
        <description>Set Order Country based on BillingAccnt</description>
        <field>Order_Country__c</field>
        <formula>Billing_Account__r.Billing_Account_Country__c</formula>
        <name>Set Order Country based on BillingAccnt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Currency_based_on_BillingAccnt</fullName>
        <description>Set Order Currency based on BillingAccnt Currency</description>
        <field>Order_Currency__c</field>
        <formula>Billing_Account__r.Billing_Account_Currency__c</formula>
        <name>Set Order Currency based on BillingAccnt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Status_to_Approved</fullName>
        <description>Set Order Status to Approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Order Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Status_to_Draft</fullName>
        <description>Set the Order Status back to Draft after an Approval is rejected</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set Order Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Status_to_Pending_Approval</fullName>
        <description>Set Order Status to Pending Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Set Order Status to Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Received_In_Aria_Flag_when_Order_Ref</fullName>
        <description>Set Received In Aria Flag when Order Ref # is available in SFDC</description>
        <field>Order_Received_in_ARIA__c</field>
        <literalValue>1</literalValue>
        <name>Set Received In Aria Flag when Order Ref</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_time_stamp_for_Datetime</fullName>
        <description>Setting the time stamp for the push to aria date time</description>
        <field>Push_To_Aria_Datetime__c</field>
        <formula>now()</formula>
        <name>Set the time stamp for Datetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Status_to_Recvd_In_Aria</fullName>
        <description>Update Order Status to Received In Aria</description>
        <field>Status__c</field>
        <literalValue>Received in Aria</literalValue>
        <name>Update Order Status to Recvd In Aria</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_order_detail_to_Boomi</fullName>
        <apiVersion>29.0</apiVersion>
        <description>Send the order details to Aria via Boomi</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getAriaOrder</endpointUrl>
        <fields>Billing_Account_Number__c</fields>
        <fields>Billing_System_Reference__c</fields>
        <fields>Boomi_Update__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>SendOrdertoAria__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Send order detail to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Send Email Alerts when an Aria Order is stuck with out an Order Ref Number</fullName>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.ARIA_Order_Ref_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Order__c.SendOrdertoAria__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Push to Aria</value>
        </criteriaItems>
        <description>Send Email Alerts when an Aria Order is stuck with out an Order Ref Number and Push to Aria Flag is set to Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ARIA_Order_Stuck_Email_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ARIA_Order__c.LastModifiedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <offsetFromField>ARIA_Order__c.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>ARIA_Order_Stuck_Email_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>ARIA_Order__c.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Order to Aria</fullName>
        <actions>
            <name>Select_Send_to_Aria_Check_box</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_the_time_stamp_for_Datetime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_order_detail_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Push to Aria</value>
        </criteriaItems>
        <description>Send the order details to Aria via boomi in an OBM.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Aria Order to Read Only when Received in Aria</fullName>
        <actions>
            <name>Set_AriaOrder_Record_Type_to_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.ARIA_Order_Ref_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Received in Aria</value>
        </criteriaItems>
        <description>Set Aria Order to Read Only when Order Status = Received in Aria</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Estimated Tax Amt on Aria Order</fullName>
        <actions>
            <name>Set_Estimated_Tax_Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.Total_Taxable_Items_Amount__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Set Estimated Tax Amt on ARIA  Order</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Order Country and Currency on Order</fullName>
        <actions>
            <name>Set_Order_Country_based_on_BillingAccnt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Order_Currency_based_on_BillingAccnt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_Account_Country__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Order Country &amp; Currency on Order based on the Billing Account Country and Currency</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set the Experian ID on Aria Order</fullName>
        <actions>
            <name>Set_Experian_ID_on_Arorder_to_SFDC_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>Set the Experian ID on Aria Order</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Status to Received in ARIA when Order Ref Number is populated</fullName>
        <actions>
            <name>Set_Received_In_Aria_Flag_when_Order_Ref</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Order_Status_to_Recvd_In_Aria</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.ARIA_Order_Ref_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Order Status to Received in ARIA when Order Ref # is populated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Send to Aria On Status selected to Push to Aria on Aria Order</fullName>
        <actions>
            <name>Select_Send_to_Aria_Check_box</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Push to Aria</value>
        </criteriaItems>
        <description>Update the send to Aria flag to be checked (TRUE) when the Status is selected &quot;Push To Aria&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
