<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>A_c_Record_Type_to_Serasa_LATAM_Read_Onl</fullName>
        <description>Based on Legacy CRM Name, change the Record Type from Serasa LATAM to Serasa LATAM Read Only</description>
        <field>RecordTypeId</field>
        <lookupValue>LATAM_Serasa_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>A/c Record Type to Serasa LATAM Read Onl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_has_an_EDQ_Dependency</fullName>
        <field>EDQ_Dependency_Exists__c</field>
        <literalValue>1</literalValue>
        <name>Account has an EDQ Dependency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_Global</fullName>
        <description>Change Owner to Global</description>
        <field>OwnerId</field>
        <lookupValue>experianglobal@experian.global</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change Owner to Global</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Account_License_Key_Reference</fullName>
        <field>EDQ_Account_License_Key_Reference__c</field>
        <formula>Global_Unique_ID__c</formula>
        <name>EDQ Account License Key Reference</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SaaS_Account_Timestamp</fullName>
        <description>Timestamps SaaS timestamp on account when SaaS? changes to support edq.com integration</description>
        <field>SaaS_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>SaaS Account Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CSDA_Integration_Id_on_Account</fullName>
        <description>Set CSDA Integration Id on the Account Object</description>
        <field>CSDA_Integration_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set CSDA Integration Id on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Experian_ID_on_Account_Object</fullName>
        <description>Set Experian ID on Account Object</description>
        <field>Experian_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set Experian ID on Account Object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_By</fullName>
        <description>case 02209649</description>
        <field>Last_Modified_By_Not_System__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Set Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_Date</fullName>
        <description>case 02209649</description>
        <field>Last_Modified_Date_Not_System__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Undelivered_Address_Detail_clear_conten</fullName>
        <description>Clear contents of 	Undelivered_Address_Detail__c</description>
        <field>Undelivered_Address_Detail__c</field>
        <name>Undelivered Address Detail clear content</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Undelivered_Address_reason_clear_content</fullName>
        <description>Clear content of 	Undelivered_Address_Reason__c</description>
        <field>Undelivered_Address_Reason__c</field>
        <name>Undelivered Address reason clear content</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acc_Type_to_No_Longer_In_Business</fullName>
        <description>Field update for Account Type.</description>
        <field>Account_Type__c</field>
        <literalValue>No Longer in Business</literalValue>
        <name>Update Acc Type to No Longer In Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CIS_Growth_Text</fullName>
        <field>Has_CIS_Growth_Text__c</field>
        <formula>&quot;share&quot;</formula>
        <name>Update CIS Growth Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CNPJ_Field_for_Consumer_Account</fullName>
        <field>CNPJ_Number__c</field>
        <formula>&quot;c&quot;&amp; MID( Id , 3, 13)</formula>
        <name>Update CNPJ Field for Consumer Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Country_of_Registration</fullName>
        <description>Updating Country of Registration to null when no available registered address.</description>
        <field>Country_of_Registration__c</field>
        <name>Update Country of Registration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ultimate_Last_Call_Time</fullName>
        <field>Ultimate_Last_Call_Time__c</field>
        <formula>qbdialer__LastCallTime__c</formula>
        <name>Update Ultimate Last Call Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Type Change to No Longer in Business</fullName>
        <actions>
            <name>Update_Acc_Type_to_No_Longer_In_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.No_Longer_In_Business__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>No Longer in Business</value>
        </criteriaItems>
        <description>Changing the Account Type to No Longer in Business on checkbox - No Longer In Business</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Ultimate last Call Update</fullName>
        <actions>
            <name>Update_Ultimate_Last_Call_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This checks if the Last Call time is greater than the Ultimate last Call time and Updates the ultimate Last Call time On Account</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), 
NOT(ISNULL( qbdialer__LastCallTime__c)), 
OR(ISNULL( Ultimate_Last_Call_Time__c ), 
AND(NOT(ISNULL( Ultimate_Last_Call_Time__c )), (qbdialer__LastCallTime__c &gt; Ultimate_Last_Call_Time__c) 
) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account has an EDQ Dependency</fullName>
        <actions>
            <name>Account_has_an_EDQ_Dependency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SaaS__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Selects the EDQ Dependency box if this is a Saas Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Serasa LATAM Read Only</fullName>
        <actions>
            <name>A_c_Record_Type_to_Serasa_LATAM_Read_Onl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legacy_CRM_Name__c</field>
            <operation>equals</operation>
            <value>Gestão de Clientes,Serasa Siebel on Premise,Serasa CRMOD</value>
        </criteriaItems>
        <description>Change record type for Serasa when Legacy CRM Name != SFDC</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>If Undelivered_Address%5F%5Fc is unchecked%2C clear Undelivered Address details</fullName>
        <actions>
            <name>Undelivered_Address_Detail_clear_conten</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Undelivered_Address_reason_clear_content</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>LATAM Serasa,LATAM Serasa Read Only</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Undelivered_Address__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Undelivered_Address_Detail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Undelivered_Address_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If Undelivered_Address__c is unchecked, clear the values of  Undelivered_Address_Detail__c and Undelivered_Address_Reason__c , LATAM Serasa, LATAM Serasa Read Only Record types only</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Owner change Global Owner</fullName>
        <actions>
            <name>Change_Owner_to_Global</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>where the owner is not Experian Global user change the owner to Experian Global user</description>
        <formula>OwnerId&lt;&gt;&apos;005i0000001w7K9&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SaaS Account Integration</fullName>
        <actions>
            <name>SaaS_Account_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Timestamps the SaaS Timestamp on the Account when SaaS? changes</description>
        <formula>OR(  (AND (NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; NOT($Profile.Name =  $Label.PROFILE_API_ONLY_SAAS_EDQ), IsChanged(SaaS__c))),  (AND(($Setup.IsDataAdmin__c.IsDataAdmin__c),SaaS__c,IsChanged(Name)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Consumer Account CNPJ Generation</fullName>
        <actions>
            <name>Update_CNPJ_Field_for_Consumer_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumer Account</value>
        </criteriaItems>
        <description>Generate CNPJ number for Consumer Account after created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CIS Growth Sharing</fullName>
        <actions>
            <name>Update_CIS_Growth_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Has_CIS_Growth__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update CSDA Integration Id on new Accounts created in SFDC</fullName>
        <actions>
            <name>Set_CSDA_Integration_Id_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CSDA_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update CSDA Integration Id on new Accounts created in SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Country Registered to Null</fullName>
        <actions>
            <name>Update_Country_of_Registration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Number_of_Registered_Address__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country_of_Registration__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>When the registered address is removed then change the Country of Registration - case 05230</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Account License Key Reference on new Account created in SFDC</fullName>
        <actions>
            <name>EDQ_Account_License_Key_Reference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.EDQ_Account_License_Key_Reference__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Accounts created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Experian ID on new Accounts created in SFDC</fullName>
        <actions>
            <name>Set_Experian_ID_on_Account_Object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Experian_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Experian ID on new Accounts created in SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified Custom Functionality</fullName>
        <actions>
            <name>Set_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>case 02209649</description>
        <formula>NOT($Permission.Bypass_Last_Modified_By)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
