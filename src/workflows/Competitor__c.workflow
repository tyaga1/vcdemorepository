<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Fill_up_competitor_name</fullName>
        <field>Competitor_name_s__c</field>
        <formula>IF( Opportunity__r.Competitor_Count__c &gt; 0,

 Opportunity__r.Competitor_name_s__c &amp; &quot;, &quot; &amp;   Account__r.Name  ,
  Account__r.Name)</formula>
        <name>Fill up competitor name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Report on competitors</fullName>
        <actions>
            <name>Fill_up_competitor_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Competitor__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
