<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Preamble_from_Primary_Preamble</fullName>
        <description>Set Preamble on Subcode from Primary Preamble</description>
        <field>Preamble__c</field>
        <formula>Preamble__c</formula>
        <name>Set Preamble from Primary Preamble</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Sub_Code__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sub_Status_from_Primary_Preamble</fullName>
        <description>Set Sub Status from Primary Preamble</description>
        <field>Sub_Status__c</field>
        <formula>Sub_Status__c</formula>
        <name>Set Sub Status from Primary Preamble</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Sub_Code__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Max_Preamble_on_a_SubCode</fullName>
        <field>Primary_Preamble__c</field>
        <formula>VALUE(Preamble_ID__c )</formula>
        <name>set Max Preamble on a SubCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Sub_Code__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Service_Area_from_Primary_Preamble</fullName>
        <description>Set Service Area from Primary Preamble</description>
        <field>Service_Area__c</field>
        <formula>Service_Area__c

/* If ( Primary__c , Service_Area__c ,&apos;&apos;) */</formula>
        <name>Set Service Area from Primary Preamble</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Sub_Code__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set Max Preamble ID for a SubCode</fullName>
        <actions>
            <name>Set_Preamble_from_Primary_Preamble</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Sub_Status_from_Primary_Preamble</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Max_Preamble_on_a_SubCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_Service_Area_from_Primary_Preamble</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), Sub_Code__r.Primary_Preamble__c &lt;= value(Preamble_ID__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
