<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_SOW_Approved_Email</fullName>
        <description>Auto SOW Approved Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_SOW_Approved</template>
    </alerts>
    <alerts>
        <fullName>Auto_SOW_Rejected_Email</fullName>
        <description>Auto SOW Rejected Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_SOW_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Auto_SOW_Reviewed_Requested_Email</fullName>
        <description>Auto SOW Reviewed Requested Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>karen.power@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maggie.bauer@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_SOW_Reviewed_Requested</template>
    </alerts>
    <alerts>
        <fullName>Auto_Sales_Support_Request_Completed</fullName>
        <description>Auto Sales Support Request Completed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_Sales_Support_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Auto_Sales_Support_Request_Notification</fullName>
        <description>Auto Sales Support Request Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_Sales_Support_Request_Notification</template>
    </alerts>
    <alerts>
        <fullName>Auto_Sales_Support_Request_Rejected</fullName>
        <description>Auto Sales Support Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_Sales_Support_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Auto_Sales_Support_Request_Submission_Email_Alert</fullName>
        <description>Auto Sales Support Request Submission Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Sales_Support_Request_Notification</template>
    </alerts>
    <alerts>
        <fullName>Bid_Support_Request_new_assignment_notification</fullName>
        <description>Bid Support Request - new assignment notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>nicola.bennett@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Bid_Support_Request_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>Bid_Support_Request_new_assignment_notification_NA</fullName>
        <description>Bid Support Request - new assignment notification (NA)</description>
        <protected>false</protected>
        <recipients>
            <recipient>anthony.milani@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Bid_Support_Request_New_assignment_notification</template>
    </alerts>
    <alerts>
        <fullName>Completed_Order_Form_Review_Notification</fullName>
        <description>Completed Order Form Review Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Order_Form_Review_Completed_notification</template>
    </alerts>
    <alerts>
        <fullName>EDQ_NA_Sales_Operations_Team_Sales_Support_Request</fullName>
        <ccEmails>qas-dl-salescoordinators-usa@experian.com.supportuat</ccEmails>
        <description>EDQ NA Sales Operations Team - Sales Support Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>phylicia.bischof@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Support_Request_submitted</template>
    </alerts>
    <alerts>
        <fullName>NA_EDQ_Pre_Sales_Support_Approved</fullName>
        <description>NA EDQ Pre Sales Support Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NA_EDQ_Pre_Sales_Support_Approved</template>
    </alerts>
    <alerts>
        <fullName>NA_EDQ_Pre_Sales_Support_Rejected</fullName>
        <description>NA EDQ Pre Sales Support Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NA_EDQ_Pre_Sales_Support_Rejected</template>
    </alerts>
    <alerts>
        <fullName>New_IRB_Support_Request</fullName>
        <ccEmails>sarah.barber@experian.com</ccEmails>
        <description>New IRB Support Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>calvin.castor@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>greg.carmean@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.hargreaves@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/IRB_Support_Request</template>
    </alerts>
    <alerts>
        <fullName>Order_Form_Review_On_Hold_notification</fullName>
        <description>Order Form Review On Hold notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>paul.blomerth@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Order_Form_Review_On_Hold_notification</template>
    </alerts>
    <alerts>
        <fullName>PreSales_Support_Request_created_for_NA_EDQ_Sales_Engineering</fullName>
        <description>PreSales Support Request created for NA EDQ Sales Engineering</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Support_Request_created_for_NA_EDQ_Sales_Engineering_queue</template>
    </alerts>
    <alerts>
        <fullName>Presales_Request_UK_EDQ_is_submitted</fullName>
        <ccEmails>QAS.Presales@experian.com</ccEmails>
        <description>Presales Request UK EDQ is submitted</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Presales_Request_UK_EDQ_is_submitted</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Live_Contract_Email_Alert</fullName>
        <ccEmails>commercialservices@uk.experian.com</ccEmails>
        <description>Sales Support Live Contract Email Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Task_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Request_Rejection</fullName>
        <description>Sales Support Request Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Owner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Pre_Sales_Support_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Request_submitted</fullName>
        <ccEmails>melissa.kendall@au.experian.com</ccEmails>
        <ccEmails>andrew.rudd@au.experian.com</ccEmails>
        <ccEmails>Malcolm.Evans@experian.com</ccEmails>
        <description>Sales Support Request submitted</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Support_Request_submitted</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Trail_High_Med_Risk_contract</fullName>
        <ccEmails>commercialmanagers@experian.com</ccEmails>
        <description>Sales Support Trail High/Med Risk contract</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Task_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Trail_NDA_DPA_Email_Alert</fullName>
        <ccEmails>experian_cssos@mailgb.custhelp.com</ccEmails>
        <description>Sales Support Trail NDA DPA Email Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Task_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>UK_GTM_CI_Pre_Sales_Consultancy</fullName>
        <description>UK GTM CI Pre Sales Consultancy</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Consultancy_Support_Request_Notification</template>
    </alerts>
    <alerts>
        <fullName>UK_GTM_Consultancy_Owner_assignment</fullName>
        <description>UK GTM Consultancy Owner assignment</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>Opportunity Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GTM_Consultancy_Support_Request_assigned</template>
    </alerts>
    <alerts>
        <fullName>UK_GTM_Id_and_Fraud_Pre_Sales_Email_Alert</fullName>
        <description>UK GTM Id and Fraud Pre Sales Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>carl.dickens@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Consultancy_Support_Request_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_NA_CSDA_Consultancy</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_CSDA_Consultancy</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign NA CSDA Consultancy</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_CCM_A_NZ</fullName>
        <description>Assign to APAC MS CCM A/NZ Queue and email queue email</description>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_CCM_A_NZ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS CCM  A/NZ</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_Targeting_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_Targeting</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS Targeting Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_EDQ_NA_Finance</fullName>
        <field>OwnerId</field>
        <lookupValue>EDQ_NA_Sales_Operations_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to EDQ NA Sales Operations Team</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_EMEA_MS_Spain_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_MS_Spain</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to EMEA MS Spain Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_International_Review_Board</fullName>
        <field>OwnerId</field>
        <lookupValue>International_Review_Board_IRB_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to International Review Board</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_EDQ_Sales_Engineering_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_EDQ_Sales_Engineering</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA EDQ Sales Engineering Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Serasa_CS_Project_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Serasa_CS_Project_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Serasa CS Project Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Serasa_MS_Project_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Serasa_MS_Delivery_BP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Serasa MS Project Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_GTM_CI_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_GTM_CI_Pre_Sales_Consultancy</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK GTM CI Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_GTM_Identity_Fraud_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_GTM_Identity_Fraud_Pre_Sales_Co</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK GTM Identity &amp; Fraud Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_I_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_I_MS_EDQ_SS_Request_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_EDQ_Sales_Support_Request</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I MS EDQ SS Request Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Clear_Re_Submit_Flag</fullName>
        <description>Reg to Auto</description>
        <field>Re_Submit__c</field>
        <literalValue>0</literalValue>
        <name>Auto Clear Re-Submit Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Approved_Date_SOW_Approved</fullName>
        <description>Approval Date</description>
        <field>Date_SOW_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Auto SOW Approved - Date SOW Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Approved_SOW_Rejected</fullName>
        <description>Clear  Rejected</description>
        <field>SOW_Rejected__c</field>
        <literalValue>0</literalValue>
        <name>Auto SOW Approved - SOW Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Approved_SOW_Status</fullName>
        <description>Approved</description>
        <field>SOW_Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Auto SOW Approved - SOW Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Rejected_Removed_Review_Reque</fullName>
        <description>Auto SOW Rejected - Removed Review Request</description>
        <field>SOW_Review_Requested__c</field>
        <literalValue>0</literalValue>
        <name>Auto SOW Rejected - Removed Review Reque</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Rejected_SOW_Status</fullName>
        <description>Auto SOW Rejected - SOW Status</description>
        <field>SOW_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Auto SOW Rejected - SOW Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_SOW_Reviewed_Requested_SOW_Status</fullName>
        <description>Auto SOW Reviewed Requested - SOW Status</description>
        <field>SOW_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Auto SOW Reviewed Requested - SOW Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BidSupport_Request_Assign_NA_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_Bid_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Bid Support Request - Assign NA Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bid_Support_Req_Assign_UK_I_APAC_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>nicola.bennett@experian.global</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Bid Support Req - Assign UK&amp;I/APAC Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_When_Created</fullName>
        <field>Opportunity_Stage_When_Request_Made__c</field>
        <formula>TEXT(Opportunity__r.StageName)</formula>
        <name>Opportunity Stage When Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_assign_owner</fullName>
        <field>OwnerId</field>
        <lookupValue>ANZ_DQT_Presales_Request</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Re assign owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Support_Live_Contract_Owner_Update</fullName>
        <field>OwnerId</field>
        <lookupValue>Identity_Fraud_Commercial_Services</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Sales Support Live Contract Owner Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Support_Trail_DPA_NDA_Owner_Update</fullName>
        <field>OwnerId</field>
        <lookupValue>Identity_Fraud_Sales_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Sales Support Trail/DPA/NDA Owner Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Assigned_Date</fullName>
        <field>Date_Assigned__c</field>
        <formula>NOW()</formula>
        <name>Set Assigned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_BU_NA_CS_EDQ</fullName>
        <field>Business_Unit__c</field>
        <literalValue>NA CS EDQ</literalValue>
        <name>Set BU= NA CS EDQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_for_UK_I_GTM_Sales_Support</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status for UK I GTM Sales Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign NA CSDA Consultancy</fullName>
        <actions>
            <name>Assign_NA_CSDA_Consultancy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>NA CSDA Consultancy Support</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS CCM  A%2FNZ</fullName>
        <actions>
            <name>Assign_to_APAC_MS_CCM_A_NZ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS CCM A/NZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Digital Pre-Sales Request</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;APAC MS EDQ&apos;, Assign to &quot;APAC MS EDQ&quot; Queue for Digital Presales Reuquest</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS EDQ Queue</fullName>
        <actions>
            <name>Assign_to_APAC_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS EDQ</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;APAC MS EDQ&apos;, Assign to &quot;APAC MS EDQ&quot; Queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS Targeting Queue</fullName>
        <actions>
            <name>Assign_to_APAC_MS_Targeting_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>APAC MS Targeting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Submit_to_Delivery_Manager__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;APAC MS Targeting&apos;, Assign to &quot;APAC MS Targeting&quot; Queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to EMEA MS Spain Queue</fullName>
        <actions>
            <name>Assign_to_EMEA_MS_Spain_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>EMEA MS Spain</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;EMEA MS Spain&apos;, Assign to &quot;EMEA MS Spain&quot; Queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to International Review Board</fullName>
        <actions>
            <name>New_IRB_Support_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_International_Review_Board</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>International Review Board (IRB) Support</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA EDQ Sales Engineering Queue</fullName>
        <actions>
            <name>PreSales_Support_Request_created_for_NA_EDQ_Sales_Engineering</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_NA_EDQ_Sales_Engineering_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_BU_NA_CS_EDQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Record_Type__c</field>
            <operation>equals</operation>
            <value>Presales Request NA EDQ</value>
        </criteriaItems>
        <description>When Record Type = &apos;Presales Request NA EDQ&apos;, Assign to &quot;NA EDQ Sales Engineering&quot; Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA MS EDQ Queue</fullName>
        <actions>
            <name>Assign_to_NA_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA MS EDQ</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;NA MS EDQ&apos;, Assign to &quot;NA MS EDQ&quot; Queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Serasa CS Project Queue</fullName>
        <actions>
            <name>Assign_to_Serasa_CS_Project_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Serasa CS</value>
        </criteriaItems>
        <description>When Business_Unit__c = &quot;Serasa CS&quot; Assign it to queue &quot;Serasa CS Project Team&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Serasa MS Delivery Queue</fullName>
        <actions>
            <name>Assign_to_Serasa_MS_Project_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Serasa MS</value>
        </criteriaItems>
        <description>When Business_Unit__c = &quot;Serasa MS&quot; Assign it to queue &quot;&apos;Serasa MS Delivery BP&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to Serasa MS Project Queue</fullName>
        <actions>
            <name>Assign_to_Serasa_MS_Project_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Serasa MS</value>
        </criteriaItems>
        <description>When Business_Unit__c = &quot;Serasa MS&quot; Assign it to queue &quot;Serasa MS Project Team&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to UK%26I MS EDQ Queue</fullName>
        <actions>
            <name>Assign_to_UK_I_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>UK&amp;I MS EDQ</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;UK&amp;I MS EDQ&apos;, Assign to &quot;UK&amp;I MS EDQ&quot; Queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Assign to UK%26I MS EDQ Sales Support Request Queue</fullName>
        <actions>
            <name>Assign_to_UK_I_MS_EDQ_SS_Request_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Business_Unit__c</field>
            <operation>equals</operation>
            <value>UK&amp;I MS EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>Pre-Sales Support</value>
        </criteriaItems>
        <description>When Business_Unit__c = &apos;UK&amp;I MS EDQ&apos;, Assign to &quot;UK&amp;I MS EDQ&quot; Queue, Account not equal to null</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Auto SOW Reviewed Approved</fullName>
        <actions>
            <name>Auto_SOW_Approved_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Auto_SOW_Approved_Date_SOW_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_SOW_Approved_SOW_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_SOW_Approved_SOW_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_SOW_Request_Approved</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Automotive Support Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.SOW_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto SOW Reviewed Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto SOW Reviewed Rejected</fullName>
        <actions>
            <name>Auto_SOW_Rejected_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Auto_SOW_Rejected_Removed_Review_Reque</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_SOW_Rejected_SOW_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Auto_SOW_Rejected</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Automotive Support Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.SOW_Rejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto SOW Reviewed Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto SOW Reviewed Requested</fullName>
        <actions>
            <name>Auto_SOW_Reviewed_Requested_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Auto_SOW_Reviewed_Requested_SOW_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Automotive Support Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>SOW</value>
        </criteriaItems>
        <description>Auto SOW Reviewed Requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Bid Support Assigned Date</fullName>
        <actions>
            <name>Set_Assigned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(Date_Assigned__c ), 

ISCHANGED( OwnerId ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bid Support Request - Assign NA owner</fullName>
        <actions>
            <name>BidSupport_Request_Assign_NA_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bid Support – Major Bid coordination,Bid Support – Limited</value>
        </criteriaItems>
        <description>Assigns an owner when a new Bid Request record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Bid Support Request - Assign UK%26I%2FAPAC owner</fullName>
        <actions>
            <name>Bid_Support_Req_Assign_UK_I_APAC_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>APAC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bid Support Request</value>
        </criteriaItems>
        <description>Assigns an owner when a new Bid Request record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Bid Support Request%3A  New assignment notification</fullName>
        <actions>
            <name>Bid_Support_Request_new_assignment_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify the new owner a bid request has been assigned to them.</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  (RecordType.DeveloperName==&apos;Bid Support Request&apos;) &amp;&amp; ( ISNEW()|| ISCHANGED(  OwnerId  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bid Support Request%3A New assignment notification %28NA%29</fullName>
        <actions>
            <name>Bid_Support_Request_new_assignment_notification_NA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; (RecordType.DeveloperName==&apos;Bid Support Request&apos;) &amp;&amp;   (Owner:User.Username == &apos;anthony.milani@experian.global&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA Order Form Review</fullName>
        <actions>
            <name>EDQ_NA_Sales_Operations_Team_Sales_Support_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_EDQ_NA_Finance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>Order Form Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA MS Data Quality,NA CS Data Quality</value>
        </criteriaItems>
        <description>Assign to the EDQ NA Sales Operations Team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA Order Form Review_On Hold</fullName>
        <actions>
            <name>Order_Form_Review_On_Hold_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Form Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality,NA MS Data Quality</value>
        </criteriaItems>
        <description>When Order Form Review record has its status changed to On Hold by the EDQ NA Finance Team - review needed by the Sales support request creator</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Requester when Order Form Review Approved</fullName>
        <actions>
            <name>Completed_Order_Form_Review_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>Order Form Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NA EDQ PreSales Support Request Approved</fullName>
        <actions>
            <name>NA_EDQ_Pre_Sales_Support_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Presales Request NA EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>NA EDQ PreSales Support Request Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NA EDQ PreSales Support Request Rejected</fullName>
        <actions>
            <name>NA_EDQ_Pre_Sales_Support_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Presales Request NA EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sales_Support_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>NA EDQ PreSales Support Request Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp Sales Support High Med Risk contract</fullName>
        <actions>
            <name>Sales_Support_Trail_High_Med_Risk_contract</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Opportunity Sales Support process for High/Med Risk contract workflow.</description>
        <formula>AND( RecordType.Name == &apos;Sales Task Request&apos;, ISPICKVAL(Type__c, &apos;High/Med Risk contract&apos;) , ! $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp Sales Support Live Contract</fullName>
        <actions>
            <name>Sales_Support_Live_Contract_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sales_Support_Live_Contract_Owner_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Opportunity Sales Support process for Live Contract workflow.</description>
        <formula>AND( RecordType.Name == &apos;Sales Task Request&apos;,    ISPICKVAL(Type__c, &apos;Low Risk Contract&apos;) , !$Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp Sales Support Trail%2FDPA%2FNDA</fullName>
        <actions>
            <name>Sales_Support_Trail_NDA_DPA_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sales_Support_Trail_DPA_NDA_Owner_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Opportunity Sales Support process for Trail/DPA/NDA workflow.</description>
        <formula>AND( RecordType.Name == &apos;Sales Task Request&apos;, OR(ISPICKVAL(Type__c, &apos;Trial/DPA/NDA&apos;),ISPICKVAL(Type__c, &apos;ACV Claim&apos;)),   ! $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Stage When Created</fullName>
        <actions>
            <name>Opportunity_Stage_When_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>12/31/2015 9:00 PM</value>
        </criteriaItems>
        <description>Captures the opportunity stage when the request is setup.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Owner from Queue to Individual</fullName>
        <actions>
            <name>Update_Status_for_UK_I_GTM_Sales_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the owner of the request is amended from the queue to be an individual the status should update to ‘In progress’ from ‘New’ to automate this status change.</description>
        <formula>AND(ISCHANGED(OwnerId ), Owner:User.LastName  &lt;&gt; Null, ( Owner:Queue.QueueName )= Null )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Owner from Queue to Individual for UK GTM Consultancy</fullName>
        <actions>
            <name>UK_GTM_Consultancy_Owner_assignment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email alert when SSR owner changed from queue to individual for UK&amp;I GTM Consultancy record types.</description>
        <formula>AND( RecordType.DeveloperName =&apos;UK_I_GTM_Consultancy_Support&apos;, ISCHANGED(OwnerId ),  Owner:User.LastName &lt;&gt; Null,  ( Owner:Queue.QueueName )= Null  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presales Request UK EDQ is submitted</fullName>
        <actions>
            <name>Presales_Request_UK_EDQ_is_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Presales Request UK EDQ</value>
        </criteriaItems>
        <description>When SSR RT is &quot;Presales Request UK EDQ&quot; is created, send an email alert</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Re assign owner</fullName>
        <actions>
            <name>Re_assign_owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Presales Request ANZ DQT</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Support Request submitted</fullName>
        <actions>
            <name>Sales_Support_Request_submitted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>PMM Analyst Request Detail</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK GTM CI-Pre Sales Consultancy Assignment</fullName>
        <actions>
            <name>UK_GTM_CI_Pre_Sales_Consultancy</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_UK_GTM_CI_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>CI Consultancy</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UK GTM ID%26F Pre Sales Consultancy Assignment</fullName>
        <actions>
            <name>UK_GTM_Id_and_Fraud_Pre_Sales_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_UK_GTM_Identity_Fraud_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sales_Support_Request__c.Type__c</field>
            <operation>equals</operation>
            <value>ID&amp;F Consultancy</value>
        </criteriaItems>
        <description>This rule will assign the request to the UK ID&amp;Fraud Pre-Sales Consultancy queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Auto_SOW_Rejected</fullName>
        <assignedToType>owner</assignedToType>
        <description>The SOW request has been rejected.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Auto SOW Rejected</subject>
    </tasks>
    <tasks>
        <fullName>Auto_SOW_Request_Approved</fullName>
        <assignedToType>owner</assignedToType>
        <description>The SOW request has been approved.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Auto SOW Request Approved</subject>
    </tasks>
</Workflow>
