<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Experian_ID_on_Plan_Relationship</fullName>
        <description>Set Experian ID on Plan Relationship to SFDC ID</description>
        <field>Experian_ID__c</field>
        <formula>Id</formula>
        <name>Set Experian ID on Plan Relationship</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Experian ID on Plan Rel object on create</fullName>
        <actions>
            <name>Set_Experian_ID_on_Plan_Relationship</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>ARIA_Plan_Relationship__c.Parent_Plan_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Experian ID on Plan Rel object on creation of a new record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
