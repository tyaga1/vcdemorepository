<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendEmailWhenSurveyAnsweredAsNoEmailAlert</fullName>
        <description>SendEmailWhenSurveyAnsweredAsNoEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>CaseOwnerEmail__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>CaseOwnerManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/SendEmailWhenSurveyAnsewredAsNoEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Survey_error_notification</fullName>
        <ccEmails>client.loyalty@experian.com</ccEmails>
        <description>Survey error notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Satmetrix_Support_Queue</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Satmetrix_Error_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Most_Recent_Survey_Primary_Score</fullName>
        <description>CRM2:W-005365</description>
        <field>Most_Recent_Survey_Primary_Score__c</field>
        <formula>PrimaryScore__c</formula>
        <name>Update Most Recent Primary Survey Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>SendEmailWhenSurveyAnsewredAsNo</fullName>
        <actions>
            <name>SendEmailWhenSurveyAnsweredAsNoEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Worfklow if the response is &apos;No&apos;, then send an email notification to the case owner and the  manager of case owner</description>
        <formula>TEXT( Case__r.Status ) = &apos;Closed Resolved&apos; &amp;&amp;  Case__r.RecordType.Name = &apos;EMS&apos;  &amp;&amp; ( IF( (PrimaryScore__c = 0 || PrimaryScore__c = 1 || PrimaryScore__c = 2), true, false) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Survey error notification</fullName>
        <actions>
            <name>Survey_error_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Feedback__c.Status__c</field>
            <operation>equals</operation>
            <value>Failure</value>
        </criteriaItems>
        <criteriaItems>
            <field>Feedback__c.StatusDescription__c</field>
            <operation>notEqual</operation>
            <value>The contact has opted out of emails,There is no email address for the contact</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Most Recent Survey Primary Score</fullName>
        <actions>
            <name>Update_Most_Recent_Survey_Primary_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Feedback__c.PrimaryScore__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Feedback__c.DataCollectionName__c</field>
            <operation>equals</operation>
            <value>EMEA MS Customer Service</value>
        </criteriaItems>
        <description>CRM2:W-005365: Client Services - View Contact Survey Score on Cases</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
