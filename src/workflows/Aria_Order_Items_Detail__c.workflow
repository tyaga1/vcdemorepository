<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Taxable_Amt_on_Aria_Order_Lines</fullName>
        <description>If the Billing account is Tax Exempt, then the Product&apos;s Taxable indicator doesnt matter. When Billing account is Non Tax Exempt and the Product is Non Taxable , then also it is Non Taxable.</description>
        <field>Taxable_Amount__c</field>
        <formula>IF (((ARIA_Order__r.Billing_Account__r.Tax_Exempt__c) ||(TEXT(ARIA_Product__r.Taxable_Ind__c)=&apos;Non-Taxable&apos; )), 0,Price_Per_Unit__c * Quantity__c)</formula>
        <name>Set Taxable Amt on Aria Order Lines</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Taxable Amt on Aria Order Lines</fullName>
        <actions>
            <name>Set_Taxable_Amt_on_Aria_Order_Lines</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Aria_Order_Items_Detail__c.Total__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Set Taxable Amount on Aria Order Lines</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
