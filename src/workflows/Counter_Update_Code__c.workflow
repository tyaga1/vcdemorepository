<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Request_Counter_Id_from_Boomi</fullName>
        <apiVersion>32.0</apiVersion>
        <description>Send an outbound message to Boomi to request a new Counter Id</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getCounterUpdateCode</endpointUrl>
        <fields>Counter_Id__c</fields>
        <fields>Data_Usage__c</fields>
        <fields>End_Date__c</fields>
        <fields>Id</fields>
        <fields>Number_of_Clicks__c</fields>
        <fields>Outbound_Message_Toggle__c</fields>
        <fields>Price_Category__c</fields>
        <fields>Product_Data__c</fields>
        <fields>Quantity__c</fields>
        <fields>Registration__c</fields>
        <fields>Start_Date__c</fields>
        <fields>Update_Code__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Request Counter Id from Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Request Counter Code</fullName>
        <actions>
            <name>Request_Counter_Id_from_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>WF Rule to send a request for a new Counter Id on the Counter_Update_Code__c. Will be triggered anytime the Outbound_Message_Toggle__c is updated</description>
        <formula>ISCHANGED( Outbound_Message_Toggle__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
